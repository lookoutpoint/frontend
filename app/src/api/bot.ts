// SPDX-License-Identifier: MIT
export const isBot = /bot|crawler|spider|crawling/i.test(navigator.userAgent);
