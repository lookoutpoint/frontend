// SPDX-License-Identifier: MIT
import LRUCache from "lru-cache";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const caches: LRUCache<any, any>[] = [];

export function newCache<K, V>(opts?: {
  max?: number;
  maxAge?: number;
}): LRUCache<K, V> {
  const cache = new LRUCache<K, V>({
    max: 100,
    maxAge: /*10 mins*/ 10 * 60 * 1000,
    ...opts,
  });
  caches.push(cache);
  return cache;
}

export function resetCaches() {
  for (const cache of caches) {
    cache.reset();
  }
}

// Prune caches every 5 minutes.
function pruneCaches() {
  window.setTimeout(() => {
    for (const cache of caches) {
      cache.prune();
    }

    pruneCaches();
  }, 5 * 60 * 1000);
}

pruneCaches();
