// SPDX-License-Identifier: MIT

export const mapTilerApiKey = import.meta.env.VITE_MAPTILER_API_KEY ?? "";
