// SPDX-License-Identifier: MIT
export enum ListLogicalOp {
  AND = "and",
  OR = "or",
}

export enum ListComparisonOp {
  EQ = "eq",
  NE = "ne",
  GT = "gt",
  GTE = "gte",
  LT = "lt",
  LTE = "lte",
}

export enum ListOperator {
  GT = "gt",
  GTE = "gte",
  LT = "lt",
  LTE = "lte",
}
