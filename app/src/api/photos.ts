// SPDX-License-Identifier: MIT
import { QueryClient, queryOptions } from "@tanstack/react-query";
import { axiosPureGet, axiosPurePost } from "../helpers/http";
import { axios } from "../util";
import { ListComparisonOp, ListLogicalOp, ListOperator } from "./list";

export interface PhotoListFilterExpr {
  op?: ListLogicalOp;
  exprs?: PhotoListFilterExpr[];
  folders?: PhotoListFilterFolder[];
  ratings?: PhotoListFilterRating[];
  keywords?: string[];
  locations?: string[];
  categories?: string[];
  dates?: PhotoListFilterDate[];
  aspectRatios?: PhotoListFilterAspectRatio[];
}

export interface PhotoListFilterFolder {
  path: string;
  fullTree: boolean;
}

export interface PhotoListFilterRating {
  value: number;
  cmpOp: ListComparisonOp;
}

export interface PhotoListFilterDate {
  value: string; // yyyy[mm[dd]]
  cmpOp: ListComparisonOp;
}

export interface PhotoListFilterAspectRatio {
  value: number;
  cmpOp: ListComparisonOp;
}

export enum PhotoListKey {
  SORT_KEY = "sortKey",
  TIMESTAMP = "timestamp",
}

export interface PhotoListParams {
  limit?: number;
  refKey?: PhotoListKey;
  refKeyValue?: string;
  refOp?: ListOperator;
  filter: PhotoListFilterExpr;
}

export function listPhotosQueryOptions(params: PhotoListParams) {
  return queryOptions({
    queryKey: ["photos", "list", params],
    queryFn: async () => {
      return await axiosPurePost<PhotoListPhoto[]>(
        "/photos/photo/list",
        params
      );
    },
  });
}

export async function listPhotos(
  queryClient: QueryClient,
  params: PhotoListParams
): Promise<PhotoListPhoto[] | null> {
  try {
    return await queryClient.fetchQuery(listPhotosQueryOptions(params));
  } catch (error) {
    console.error(`listPhotos: ${error}`);
    return null;
  }
}

export interface PhotoMapListParams extends PhotoListParams {
  mapping: true;
}

export function listMappedPhotosQueryOptions(params: PhotoMapListParams) {
  return queryOptions({
    queryKey: ["photos", "list", params],
    queryFn: async () => {
      return await axiosPurePost<PhotoMapListPhoto[]>(
        "/photos/photo/list",
        params
      );
    },
  });
}

export async function listMappedPhotos(
  queryClient: QueryClient,
  params: PhotoMapListParams
): Promise<PhotoMapListPhoto[] | null> {
  try {
    return await queryClient.fetchQuery(listMappedPhotosQueryOptions(params));
  } catch (error) {
    console.error(`listMappedPhotos: ${error}`);
    return null;
  }
}

export interface PhotoRandomListParams {
  count: number;
  filter: PhotoListFilterExpr;
}

export function randomPhotosQueryOptions(params: PhotoRandomListParams) {
  return queryOptions({
    queryKey: ["photos", "random", params],
    queryFn: async () => {
      return await axiosPurePost<PhotoListPhoto[]>(
        "/photos/photo/random",
        params
      );
    },
    // Do not cache as we want random photos.
    staleTime: 0,
  });
}

export async function randomPhotos(
  queryClient: QueryClient,
  params: PhotoRandomListParams
): Promise<PhotoListPhoto[] | null> {
  try {
    return await queryClient.fetchQuery(randomPhotosQueryOptions(params));
  } catch (error) {
    console.error(`randomPhotos: ${error}`);
    return null;
  }
}

export type PhotoGroup = [string, string]; // [id, display]
export const PHOTO_GROUP_ID = 0;
export const PHOTO_GROUP_DISPLAY = 1;

export interface PhotoGroupParts {
  slug: string[];
  display: string[];
}

export function splitPhotoGroup(
  group: PhotoGroup,
  displaySep: string
): PhotoGroupParts {
  const id = group[PHOTO_GROUP_ID];
  if (id === "/") {
    return { slug: [], display: [] };
  }

  return {
    // Trim the leading and trailing slash from the slug to match the display string
    slug: id.substring(1, id.length - 1).split("/"),
    display: group[PHOTO_GROUP_DISPLAY].split(displaySep),
  };
}

// Default photo structure for photo listings (non-mapping mode).
export interface PhotoListPhoto {
  id: string;

  // Sort keys: there are times (e.g. photo listing) where only one of the
  // sort keys will be defined
  sortKey: string;
  tsSortKey: string;

  storeGen: string;
  width: number;
  height: number;
  title?: string;
  locations?: PhotoGroup[];
  public: boolean;
}

// Photo structure for mapping photo listings.
export interface PhotoMapListPhoto {
  id: string;
  sortKey: string;
  storeGen: string;
  width: number;
  height: number;
  title?: string;
  gpsLocation: { coordinates: [number, number] };
}

export interface FullPhoto extends PhotoListPhoto {
  // Additional metadata
  date?: string;
  description?: string;
  folder: PhotoGroup; // all photos have a folder
  keywords?: PhotoGroup[];
  categories?: PhotoGroup[];
  dateTime?: string;
  rating?: number;
  copyright?: string;
  usageTerms?: string;
  usageWebUrl?: string;
  gpsLocation?: { coordinates: [number, number] };
  gpsAltitude?: number;
  make?: string;
  model?: string;
  lens?: string;
  fNumber?: number;
  focalLength?: number;
  iso?: number;
  exposureTime?: number;
  exposureBias?: number;
}

// Type that represents a mininum of PhotoListPhoto and an optional FullPhoto.
export type PartialFullPhoto = PhotoListPhoto & Partial<FullPhoto>;

// A photo is uniquely identified by its id, but the storeGen value fully identify the version of the photo
// and can be used by the server to ensure that the latest version is being queried.
export interface PhotoIdGen {
  id: string;
  storeGen?: string;
}

/**
 * Query options to /photos/photo/md/{id}?{storeGen} to get metadata for a photo.
 */
export function getPhotoMetadataQueryOptions({ id, storeGen }: PhotoIdGen) {
  return queryOptions({
    // Query key does not contain storeGen. For the duration of the user's browser session,
    // we assume that the metadata will not change (need a reload to check). The presence of storeGen
    // allows the browser to cache the metadata result across sessions, but does not affect
    // the intra-session behaviour.
    queryKey: ["photos", "metadata", id],
    queryFn: async () => {
      return await axiosPureGet<FullPhoto>(
        `/photos/photo/md/${id}${storeGen ? `?${storeGen}` : ""}`
      );
    },
  });
}

export async function getPhotoMetadata(
  queryClient: QueryClient,
  id: PhotoIdGen
): Promise<FullPhoto> {
  return await queryClient.fetchQuery(getPhotoMetadataQueryOptions(id));
}

export type PhotoWithLocation = PhotoListPhoto &
  Required<Pick<FullPhoto, "gpsLocation">>;

export async function listRandomMappedPhotos(
  filter: PhotoListFilterExpr,
  count: number
): Promise<PhotoWithLocation[]> {
  return axios
    .post<PhotoWithLocation[]>("photos/photo/random-map", { count, filter })
    .then(({ data, status }) => {
      if (status === 200) return data;
      throw new Error(status + "");
    });
}
