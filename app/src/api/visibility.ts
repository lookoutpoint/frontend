// SPDX-License-Identifier: MIT
import { axios } from "../util";

// Keep in sync with backend.
export const VIS_TOKEN_PUBLIC = "#P";
export const VIS_TOKEN_ALL = "*";

export interface VisibilityToken {
  id: string;
  display: string;
  links?: VisibilityTokenLink[];
}

export interface VisibilityTokenLink {
  activationValue: string;
  activationCount?: number;
  redirect?: string;
  comment?: string;
  disabled?: boolean;
}

export async function listVisibilityTokens(): Promise<
  VisibilityToken[] | null
> {
  const { data, status } = await axios.post<VisibilityToken[]>(
    "/photos/visibility/list",
    {}
  );
  return status === 200 ? data : null;
}

export async function createVisToken(
  name: string
): Promise<VisibilityToken | null> {
  try {
    const { data, status } = await axios.post<VisibilityToken>(
      "/photos/visibility/create",
      { name }
    );
    return status === 200 ? data : null;
  } catch (e) {
    console.log(`createVisToken error: ${e}`);
  }
  return null;
}

export async function createVisTokenLink(
  id: string,
  opts: { redirect?: string; comment?: string }
): Promise<VisibilityTokenLink | null> {
  try {
    const { data, status } = await axios.post<VisibilityTokenLink>(
      "/photos/visibility/link/create",
      {
        id: id,
        ...opts,
      }
    );
    return status === 200 ? data : null;
  } catch (e) {
    console.log(`createVisTokenLink error: ${e}`);
  }
  return null;
}

export async function updateVisTokenLink(
  actValue: string,
  opts: { redirect?: string; comment?: string; disabled?: boolean }
): Promise<boolean> {
  try {
    const { status } = await axios.post("/photos/visibility/link/update", {
      actValue: actValue,
      ...opts,
    });
    return status === 204;
  } catch (e) {
    console.log(`updateVisTokenLink error: ${e}`);
  }
  return false;
}

export async function activateVisTokenLink(
  actValue: string
): Promise<string | null> {
  try {
    const { data, status } = await axios.post<{ redirect: string }>(
      "/photos/visibility/link/activate",
      { actValue }
    );
    return status === 200 ? data.redirect : null;
  } catch (e) {
    console.log(`activateVisTokenLink error: ${e}`);
  }
  return null;
}
