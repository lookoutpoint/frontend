// SPDX-License-Identifier: MIT
import { useQuery } from "@tanstack/react-query";
import { axiosPost, axiosPurePost } from "../helpers/http";
import { UTCDate } from "./date";
import { GroupType } from "./photoGroups";

// The enums in this file must be in sync with the backend.
export enum EventButton {
  GROUP_CHILDREN = "group/children",
  GROUP_MAP = "group/map",
  GROUP_DESCRIPTION = "group/description",
  GROUP_HIGHLIGHTS = "group/highlights",
  GROUP_INFO = "group/info",
  GROUP_MORE = "group/more",
  GROUP_ORDER = "group/order",
  GROUP_ORDER_BY_CAMERA = "group/orderByCamera",
  GROUP_ORDER_BY_UPLOAD = "group/orderByUpload",
  GROUP_PHOTOS = "group/photos",
  GROUP_TIMELINE = "group/timeline",
  GROUP_RESET = "group/reset",
  LIGHTBOX_CLOSE = "lightbox/close",
  LIGHTBOX_INFO = "lightbox/info",
  LIGHTBOX_NAV_NEXT = "lightbox/nav/next",
  LIGHTBOX_NAV_PREV = "lightbox/nav/prev",
  LIGHTBOX_SLIDESHOW = "lightbox/slideshow",
  LOCATION_HIKES = "location/hikes",
  LOCATION_PARKS = "location/parks",
  MENU = "menu",
  PARK_HIKES = "park/hikes",
  SCROLL_TO_TOP = "scrollToTop",
  SITE_THEME = "site/theme",
  TIMELINE_SIZE = "timeline/size",
}

export enum EventLink {
  HEADER_LOGO = "header/logo",
  HOME = "/",
  BLOGS = "/blogs/",
  RECENT = "/recent/",
}

export enum EventContext {
  BLOG = "blog",
  HOME = "home",
  HOME_EXPLORE_BLOGS = "home/explore/blogs",
  HOME_EXPLORE_CATEGORIES = "home/explore/categories",
  HOME_EXPLORE_FOLDERS = "home/explore/folders", // backend refers to folder instead of collection
  HOME_EXPLORE_KEYWORDS = "home/explore/keywords",
  HOME_EXPLORE_LOCATIONS = "home/explore/locations",
  HOME_EXPLORE_RECENT = "home/explore/recent",
  HOME_HLPHOTO_INLINE_LIGHTBOX = "home/hlphoto/inline-lightbox",
  HOME_HLPHOTO_LIGHTBOX = "home/hlphoto/lightbox",
  HOME_TOP_BAR = "home/top-bar",
  MENU = "menu",
  RECENT = "recent",
  // Special marker for default context
  DEFAULT = "$default",
}

export enum AppEvent {
  BUTTON_CLICK = "button/click",
  GROUP_CLICK = "group/click",
  LIGHTBOX_SHOW = "lightbox/show",
  LIGHTBOX_SLIDESHOW_PHOTO = "lightbox/slideshow/photo",
  LIGHTBOX_SWIPE_NEXT = "lightbox/swipe/next",
  LIGHTBOX_SWIPE_PREV = "lightbox/swipe/prev",
  LINK_CLICK = "link/click",
  PAGE_VIEW = "page/view",
  PAGE_VIEW_TYPE = "page/view-type",
  PHOTO_CLICK = "photo/click",
  PHOTO_VIEW_COVER = "photo/view/cover",
  PHOTO_VIEW_DETAIL = "photo/view/detail",
  PHOTO_VIEW_HOME = "photo/view/home",
  PHOTO_VIEW_LIGHTBOX = "photo/view/lightbox",
  PHOTO_VIEW_THUMB = "photo/view/thumb",
  PHOTO_VIEW_ZOOM = "photo/view/zoom",
  THEME_DARK = "theme/dark",
  THEME_LIGHT = "theme/light",
  SITE_VISIT = "site/visit",
}

export interface EventInfo {
  event: string;
  object?: string;
  context?: string;
  extra?: string;
  count?: number; // defaults to 1
}

export async function addEvents(events: EventInfo[], beacon?: boolean) {
  const url = `/events/add`;
  const data = JSON.stringify(events);

  if (beacon) {
    // Use sendBeacon so that we can send at end of page.
    navigator.sendBeacon(url, data);
  } else {
    axiosPost(url, data);
  }
}

export function makeEventPhotoID(photo: string | { id: string }): string {
  const id = typeof photo === "string" ? photo : photo.id;
  return `/photo/${id}`;
}

export function makeEventGroupID(
  groupType: GroupType,
  group: string | { id: string },
  groupTypeSuffix?: string
): string {
  let prefix: string = groupType;
  if (prefix === GroupType.TIMELINE_GROUP) {
    prefix = "timeline";
  } else if (prefix === GroupType.COLLECTION) {
    prefix = "folder";
  }
  if (groupTypeSuffix) {
    prefix = `${prefix}-${groupTypeSuffix}`;
  }

  const id = typeof group === "string" ? group : group.id;
  return `/${prefix}${id}`;
}

// Admin only APIs

export enum EventGranularity {
  DAY = 0,
  MONTH = 1,
  YEAR = 2,
  ALL_TIME = 3,
}

export interface EventCountQuery {
  startDate?: UTCDate;
  endDate?: UTCDate;
  granularity: EventGranularity;

  event?: string;
  eventRegex?: boolean;
  object?: string;
  objectRegex?: boolean;
  context?: string;
  contextRegex?: boolean;
  extra?: string;
  extraRegex?: boolean;

  groupByEvent?: boolean;
  groupByObject?: boolean;
  groupByContext?: boolean;
  groupByExtra?: boolean;

  limit?: number;
}

export type EventCountQueryFilter = Pick<
  EventCountQuery,
  | "event"
  | "eventRegex"
  | "object"
  | "objectRegex"
  | "context"
  | "contextRegex"
  | "extra"
  | "extraRegex"
>;

export interface EventCountResult {
  id?: {
    date?: UTCDate;
    event?: string;
    object?: string;
    context?: string;
    extra?: string;
  };
  count: number;
}

interface RawEventCountResult {
  id?: {
    // date is a string rather than a Date
    date?: string;
    event?: string;
    object?: string;
    context?: string;
    extra?: string;
  };
  count: number;
}

// Query hook to count events for the given parameters.
export function useCountEvents(
  rawEventQueryParams: EventCountQuery
): EventCountResult[] {
  // Convert dates to ISO date strings (with UTC timezone).
  const eventQueryParams: Omit<EventCountQuery, "startDate" | "endDate"> & {
    startDate?: string;
    endDate?: string;
  } = {
    ...rawEventQueryParams,
    startDate: rawEventQueryParams.startDate
      ? rawEventQueryParams.startDate.getLocal().toISOString()
      : undefined,
    endDate: rawEventQueryParams.endDate
      ? rawEventQueryParams.endDate.getLocal().toISOString()
      : undefined,
  };

  const query = useQuery({
    queryKey: ["events", "count", eventQueryParams],
    queryFn: async () => {
      return await axiosPurePost<RawEventCountResult[]>(
        "/events/count",
        eventQueryParams
      );
    },
    staleTime: 5 * 60 * 1000, // 5 mins
  });

  if (!query.data) {
    return [];
  }

  return query.data.map((o) => {
    const n: EventCountResult = { count: o.count };
    if (o.id) {
      n.id = {
        event: o.id.event,
        object: o.id.object,
        context: o.id.context,
        extra: o.id.extra,
      };

      if (o.id.date) {
        n.id.date = UTCDate.fromLocal(new Date(o.id.date));
      }
    }
    return n;
  });
}
