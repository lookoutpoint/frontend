// SPDX-License-Identifier: MIT
import { ListOperator } from "./list";
import { axiosCachedPost, axiosPost, axiosPurePost } from "../helpers/http";
import { PhotoListFilterExpr } from "./photos";
import { UserSession } from "../helpers/Contexts";
import { newCache } from "./cache";
import {
  useQuery,
  UseQueryOptions,
  UseQueryResult,
} from "@tanstack/react-query";
import { useCallback } from "react";

export type ErrorOr<T> = T | { error: string };

// General groups
export interface CoverPhoto {
  id: string;
  storeGen: string;
  width: number;
  height: number;
}

export interface Group {
  id: string;
  display: string;
  coverPhoto?: CoverPhoto;
  public: boolean;
  metaDescription?: string;
  richText?: string;
  sortOrder?: number | string; // admin only
  visTokens?: string[]; // admin only
}

export interface GroupListParams {
  id: string;
  limit?: number;
  refId?: string;
  refOp?: ListOperator;
  coverPhoto?: boolean;
  maxRelDepth?: number; // -1 for infinite, 0 for default depth of 1
  sortIgnoreDepth?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  extension?: { [key: string]: any };
}

export enum GroupType {
  CATEGORY = "category",
  DATE = "date",
  COLLECTION = "collection",
  KEYWORD = "keyword",
  LOCATION = "location",
  PAGE = "page",
  TIMELINE_GROUP = "timeline-group",
}

export const GROUP_SEP = {
  [GroupType.CATEGORY]: ":",
  [GroupType.DATE]: "/",
  [GroupType.COLLECTION]: "/",
  [GroupType.KEYWORD]: undefined,
  [GroupType.LOCATION]: ",",
  [GroupType.PAGE]: "/",
  [GroupType.TIMELINE_GROUP]: "|",
};

export const GROUP_PLURAL = {
  [GroupType.CATEGORY]: "categories",
  [GroupType.DATE]: "dates",
  [GroupType.COLLECTION]: "collections",
  [GroupType.KEYWORD]: "keywords",
  [GroupType.LOCATION]: "locations",
  [GroupType.PAGE]: "pages",
  [GroupType.TIMELINE_GROUP]: "timeline groups",
};

export function getGroupName(type: GroupType, group: Group): string {
  const sep = GROUP_SEP[type];
  if (sep) {
    const sepIdx = group.display.lastIndexOf(sep);
    return group.display.substring(sepIdx + 1);
  } else {
    return group.display;
  }
}

function mapGroupTypeToAPI(type: GroupType): string {
  if (type === GroupType.COLLECTION) {
    // Backend API equivalent is "folder"
    return "folder";
  }
  // 1:1 mapping
  return type;
}

export enum GroupPhotoProperties {
  HAS_PHOTOS = 1 << 0,
  HAS_HIGHLIGHT_PHOTOS = 1 << 1,
  HAS_MAPPABLE_PHOTOS = 1 << 2,
}

export interface GroupMetaProperties {
  hasChildren: boolean;
  hasPhotos: GroupPhotoProperties;
}

export type GroupInfo<G extends Group> = G & GroupMetaProperties;

export function getGroupInfoQueryKey(
  type: GroupType,
  id: string,
  coverPhoto = false
) {
  return ["group", type, "info", id, coverPhoto];
}

type GroupInfoResult<G extends Group> = Omit<G, "id"> & GroupMetaProperties;
export function useGroupInfoQuery<G extends Group>(
  type: GroupType,
  id: string,
  coverPhoto = false,
  options?: Partial<UseQueryOptions<GroupInfoResult<G>>>
): UseQueryResult<G & GroupMetaProperties, Error> {
  return useQuery({
    ...options,
    queryKey: getGroupInfoQueryKey(type, id, coverPhoto),
    queryFn: async () => {
      return await axiosPurePost<GroupInfoResult<G>>(
        `/photos/${mapGroupTypeToAPI(type)}/info`,
        { id, coverPhoto }
      );
    },
    select: useCallback(
      (group: GroupInfoResult<G>) => {
        return { id, ...group } as GroupInfo<G>;
      },
      [id]
    ),
  });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const groupChildrenCache = newCache<string, any>();
async function getGroupChildren<G extends Group>(
  session: UserSession,
  type: GroupType,
  params: GroupListParams
): Promise<G[] | null> {
  const cacheKey = `${type},${params.id},${params.limit},${params.refId},${
    params.refOp
  },${params.coverPhoto},${params.maxRelDepth},${JSON.stringify(
    params.extension
  )}`;

  return (
    await axiosCachedPost<G[]>(
      session,
      `/photos/${mapGroupTypeToAPI(type)}/children`,
      params,
      cacheKey,
      groupChildrenCache
    )
  ).data;
}

export interface GroupUpdateParams {
  display?: string;
  sortOrder?: string | number;
  metaDescription?: string;
  richText?: string;
}

export async function updateGroup(
  type: GroupType,
  id: string,
  opts: GroupUpdateParams
): Promise<boolean> {
  return (
    await axiosPost(`/photos/${type}/update`, {
      id,
      ...opts,
    })
  ).success;
}

// Folders (frontend calls these Collections)

export interface Folder extends Group {
  selfVisTokens?: string[]; // admin only
}

export async function getFolderChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<Folder>(session, GroupType.COLLECTION, params);
}

export async function folderVisibilityToken(
  mode: "add" | "delete",
  folder: string,
  target: string,
  token: string
): Promise<boolean> {
  return (
    await axiosPost("/photos/folder/visibility-token", {
      mode,
      folder,
      target,
      token,
    })
  ).success;
}

// Keywords

export type Keyword = Group;

export async function getKeywordChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<Keyword>(session, GroupType.KEYWORD, params);
}

// Categories

export interface Category extends Group {
  related?: { [key: string]: string[] };
}

export async function getCategoryChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<Category>(session, GroupType.CATEGORY, params);
}

// Locations

export type Location = Group;

export async function getLocationChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<Location>(session, GroupType.LOCATION, params);
}

// Dates

// Dont'a alias with standard Date
export type DateGroup = Group;

export async function getDateChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<DateGroup>(session, GroupType.DATE, params);
}

// Timeline groups

export interface TimelineGroup extends Group {
  startDate?: string;
  endDate?: string;
  filter?: PhotoListFilterExpr;
  selfVisTokens?: string[]; // admin only
  unwrappedFilter?: PhotoListFilterExpr; // admin only
}

export function getTimelineGroupId(group: TimelineGroup): string {
  return group.id.substring(group.id.indexOf("/", 1));
}

export function folderIdToTimelineGroupPrefix(folderId: string): string {
  return folderId.replaceAll("/", ":");
}

export function makeTimelineGroupIdFromFolder(
  folderId: string,
  otherId?: string
) {
  if (otherId && otherId.length > 0 && otherId[0] === "/") {
    // Strip leading slash
    otherId = otherId.slice(1);
  }

  return `/${folderIdToTimelineGroupPrefix(folderId)}/${otherId ?? ""}`;
}

export function useTimelineGroupInfoQuery(
  folderId: string,
  otherId?: string,
  coverPhoto = false,
  options?: Partial<UseQueryOptions<GroupInfoResult<TimelineGroup>>>
) {
  return useGroupInfoQuery<TimelineGroup>(
    GroupType.TIMELINE_GROUP,
    makeTimelineGroupIdFromFolder(folderId, otherId),
    coverPhoto,
    options
  );
}

export async function getTimelineGroupChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<TimelineGroup>(
    session,
    GroupType.TIMELINE_GROUP,
    params
  );
}

// Returns id of created timeline group or null on error
export async function createTimelineGroup(
  folderId: string,
  name?: string,
  filter?: unknown
): Promise<[boolean, string | undefined]> {
  const { data, success } = await axiosPost<ErrorOr<string>>(
    "/photos/timeline-group/create",
    {
      folder: folderId,
      name,
      filter,
    }
  );

  if (success && data) {
    if (typeof data === "string") {
      return [true, data as string];
    } else if (data.error) {
      return [false, data.error];
    }
  }
  return [false, undefined];
}

export async function syncTimelineGroup(id: string) {
  return (await axiosPost("/photos/timeline-group/sync", { id })).success;
}

export async function deleteTimelineGroup(id: string) {
  return (await axiosPost("/photos/timeline-group/delete", { id })).success;
}

export async function timelineGroupVisibilityToken(
  mode: "add" | "delete",
  id: string,
  token: string,
  recurse?: boolean
): Promise<boolean> {
  return (
    await axiosPost("/photos/timeline-group/visibility-token", {
      mode,
      id,
      token,
      recurse,
    })
  ).success;
}

// Returns true on success, otherwise error string or false.
export async function updateTimelineGroupFilter(
  id: string,
  filter: unknown
): Promise<boolean | string> {
  const { data, success } = await axiosPost<{ error?: string }>(
    "/photos/timeline-group/update-filter",
    { id, filter }
  );
  if (success) {
    if (data && data.error) {
      return `${data.error}`;
    }
    return true;
  } else {
    return false;
  }
}

// Pages

export interface Page extends Group {
  selfVisTokens?: string[]; // admin only
}

export async function getPageChildren(
  session: UserSession,
  params: GroupListParams
) {
  return getGroupChildren<Page>(session, GroupType.PAGE, params);
}

// Returns id of created timeline group or null on error
export async function createPage(
  name: string
): Promise<[boolean, string | undefined]> {
  const { data, success } = await axiosPost<ErrorOr<string>>(
    "/photos/page/create",
    {
      name,
    }
  );

  if (success && data) {
    if (typeof data === "string") {
      return [true, data as string];
    } else if (data.error) {
      return [false, data.error];
    }
  }
  return [false, undefined];
}

export async function deletePage(id: string) {
  return (await axiosPost("/photos/page/delete", { id })).success;
}

export async function pageVisibilityToken(
  mode: "add" | "delete",
  id: string,
  token: string,
  recurse?: boolean
): Promise<boolean> {
  return (
    await axiosPost("/photos/page/visibility-token", {
      mode,
      id,
      token,
      recurse,
    })
  ).success;
}

// Home group selection

export interface GroupSelection {
  recent: Group[];
  other: Group[];
}

export interface HomeGroupSelection {
  folders: GroupSelection;
  locations: GroupSelection;
  categories: GroupSelection;
  keywords: GroupSelection;
}

const homeGroupsCache = newCache<string, HomeGroupSelection>();
export async function getHomeGroups(
  session: UserSession,
  recent: number,
  total: number
) {
  const cacheKey = "";

  return (
    await axiosCachedPost<HomeGroupSelection>(
      session,
      "/photos/group/home",
      { recent, total },
      cacheKey,
      homeGroupsCache
    )
  ).data;
}
