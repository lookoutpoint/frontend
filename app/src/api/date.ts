// SPDX-License-Identifier: MIT
export class UTCDate {
  public date: Date;

  public constructor(date: Date) {
    this.date = date;
  }

  public getLocal(): Date {
    return new Date(
      Date.UTC(
        this.date.getFullYear(),
        this.date.getMonth(),
        this.date.getDate()
      )
    );
  }

  public static fromLocal(date: Date): UTCDate {
    return new UTCDate(
      new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate())
    );
  }
}
