// SPDX-License-Identifier: MIT
import React from "react";
import { createRoot } from "react-dom/client";
import { HelmetProvider } from "react-helmet-async";
import { App } from "./App";
import { configure } from "axios-hooks";
import { axios } from "./util";

// CSS
import "./index.css";

// Configure Axios hooks
configure({ axios });

const container = document.getElementById("root")!;
const root = createRoot(container);
root.render(
  <React.StrictMode>
    <HelmetProvider>
      <App />
    </HelmetProvider>
  </React.StrictMode>
);
