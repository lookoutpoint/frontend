// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeEventGroupID } from "../api/events";
import {
  getGroupName,
  getKeywordChildren,
  Group,
  GroupType,
  GROUP_PLURAL,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { useEventTracker } from "../hooks/useEventTracker";
import { IconKeywords } from "../components/Icons";
import { regexRoute } from "../helpers/route";
import { titleCase } from "../helpers/titleCase";
import { PhotoGroupList } from "./components/PhotoGroupList";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";

export const PhotoKeyword: React.FC<{ root?: boolean }> = ({ root }) => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  if (root) {
    // Root folder.
    return regexRoute(url, [
      {
        // URLs: <empty>, @children/
        regex: /^(?:|@children\/)$/,
        render: () => <PhotoKeywordImpl keywordSlug="/" />,
      },
    ]);
  } else {
    // Child keyword.
    return regexRoute(url, [
      {
        // URL: :keywordSlug/<more>
        // <more>:
        // <empty>
        // @info\/
        // @map\/
        // @photos/:photoTitleSlug?/:photoId
        regex: /^([^/@]+\/)(?:@info\/|@map\/|@photos\/(?:(?:[^/]+\/)?(.+))?)?$/,
        render: (m) => {
          const keywordSlug = `/${m[1]}`;
          const photoId = m[2];

          return (
            <PhotoKeywordImpl keywordSlug={keywordSlug} photoId={photoId} />
          );
        },
      },
    ]);
  }
};

interface ImplProps {
  keywordSlug: string;
  photoId?: string;
}

const PhotoKeywordImpl: React.FC<ImplProps> = ({ keywordSlug, photoId }) => {
  useEventTracker(makeEventGroupID(GroupType.KEYWORD, keywordSlug));

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      groupUrl: `/${GROUP_PLURAL[GroupType.KEYWORD]}/`,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.KEYWORD]),
      navUrlPrefix: `/${GroupType.KEYWORD}/`,
    },
    keywordSlug
  );

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(
    (group: Group) => {
      setNavState(keywordSlug, group.display);
    },
    [keywordSlug, setNavState]
  );

  // When the keyword changes, rebuild approximation.
  useEffect(() => {
    setNavState(keywordSlug);
  }, [keywordSlug, setNavState]);

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        groupId={keywordSlug}
        groupType={GroupType.KEYWORD}
        getGroupName={useCallback(
          (g: Group) => getGroupName(GroupType.KEYWORD, g),
          []
        )}
        childrenIcon={<IconKeywords />}
        buildPhotoListFilter={useCallback(
          (groupId) => ({ keywords: [groupId] }),
          []
        )}
        getChildren={getKeywordChildren}
        lightboxPhotoId={photoId}
        onGroupInfo={onGroupInfo}
      />
    </div>
  );
};
