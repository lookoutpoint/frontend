// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import { Helmet } from "react-helmet-async";
import { Navigate } from "react-router-dom";
import styled from "styled-components";
import { useUserSession } from "../hooks/useUserSession";
import { makeTitleString } from "../util";

const Form = styled.form`
  max-width: 400px;
  margin: 0 auto;
`;
const FormRow = styled.div`
  margin: 0.25em 0;
  & label {
    display: inline-block;
    width: 49%;
  }
  & input {
    display: inline-block;
    width: 49%;
  }
`;
const SubmitRow = styled.div`
  margin: 0.25em 0;
  text-align: right;
`;
const ErrorMessage = styled.div`
  margin: 0.5em 0;
  padding: 0.5em;
  border: 1px solid var(--error-border-color);
  background-color: var(--error-bg-color);
  color: var(--error-text-color);
`;

export const UserLogin: React.FC = () => {
  const userSession = useUserSession();

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState<string | null>(null);
  const [inProgress, setInProgress] = useState(false);

  const submit = useCallback(
    (event: React.FormEvent) => {
      // Prevent page from refreshing.
      event.preventDefault();

      if (name.length === 0) {
        setMessage("Must specify a name.");
        return;
      } else if (password.length === 0) {
        setMessage("Must specify a password.");
        return;
      }

      // Validation complete.
      setMessage(null);
      setInProgress(true);

      // Send request to login
      userSession.login(name, password).then(
        () => {
          setInProgress(false);
        },
        () => {
          setInProgress(false);
          setMessage("Failed to login.");
        }
      );
    },
    [name, password, userSession]
  );

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Login")}</title>
      </Helmet>

      {userSession.isLoggedIn && <Navigate to="/" />}

      <Form onSubmit={submit}>
        {message && <ErrorMessage>{message}</ErrorMessage>}
        <FormRow>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormRow>
        <FormRow>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormRow>
        <SubmitRow>
          <input type="submit" value="Login" disabled={inProgress} />
        </SubmitRow>
      </Form>
    </div>
  );
};
