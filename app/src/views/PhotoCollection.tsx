// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useMemo } from "react";
import { useParams } from "react-router-dom";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { PhotoGroupList } from "./components/PhotoGroupList";
import {
  folderIdToTimelineGroupPrefix,
  getFolderChildren,
  getGroupName,
  Group,
  GroupType,
  GROUP_PLURAL,
  GROUP_SEP,
  TimelineGroup,
} from "../api/photoGroups";
import {
  Addition,
  usePhotoGroupNavState,
} from "../hooks/usePhotoGroupNavState";
import { makeEventGroupID } from "../api/events";
import { useEventTracker } from "../hooks/useEventTracker";
import { regexRoute } from "../helpers/route";
import { titleCase } from "../helpers/titleCase";
import { IconCollections } from "../components/Icons";
import { lazily } from "react-lazily";

const { PhotoFolderSettingsExtension } = lazily(
  () => import("./components/PhotoFolderSettingsExtension")
);

function getTimelineGroupEventObjectFromURLPath(path: string): string {
  // Path is of the form /collection/<folder-id>/@timeline/<timeline-id>/
  // Timeline group id is of the form /<modified-folder-id>/<timeline-id>/
  const sep1 = path.indexOf("/@timeline");
  const folderId = path.slice("/collection".length, sep1 + 1);
  const modifiedFolderId = folderIdToTimelineGroupPrefix(folderId);
  const timelineId = path.slice(sep1 + 10);
  return makeEventGroupID(
    GroupType.TIMELINE_GROUP,
    `/${modifiedFolderId}${timelineId}`
  );
}

export const PhotoCollection: React.FC<{ root?: boolean }> = ({ root }) => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  if (root) {
    // Root folder.
    return regexRoute(url, [
      {
        // URLs:
        // <empty>
        // @children/
        // @map/
        // @photos/
        // @photos/:photoTitleSlug?/:photoId
        regex: /^(?:|@children\/|@map\/|@photos\/(?:(?:[^/]+\/)?(.+))?)$/,
        render: (m) => (
          <PhotoCollectionImpl collectionSlug="/" photoId={m[1]} />
        ),
      },
    ]);
  } else {
    // Child or timeline folder.
    return regexRoute(url, [
      {
        // URL: :folderSlug+/:moreUrl
        regex: /^((?:[^/@]+\/)+)(.*)$/,
        render: (m) => {
          const folderSlug = `/${m[1]}`;
          const moreUrl = m[2];

          return regexRoute(moreUrl, [
            {
              // URLs:
              // <empty>
              // @children/
              // @info/
              // @map/
              // @photos/
              // @photos/:photoTitleSlug?/:photoId
              regex:
                /^(?:|@children\/|@info\/|@map\/|@photos\/(?:(?:[^/]+\/)?(.+))?)$/,
              render: (m) => (
                <PhotoCollectionImpl
                  collectionSlug={folderSlug}
                  photoId={m[1]}
                />
              ),
            },
            {
              // URLs:
              // @timeline/:timelineSlug*/@/:photoTitleSlug?/:photoId
              // @timeline/:timelineSlug*/
              regex: /^@timeline\/((?:[^/]+\/)*)(?:@\/(?:[^/]+\/)?(.+))?$/,
              render: (m) => (
                <PhotoCollectionImpl
                  collectionSlug={folderSlug}
                  timelineSlug={`/${m[1] ?? ""}`}
                  photoId={m[2]}
                />
              ),
            },
          ]);
        },
      },
    ]);
  }
};

interface ImplProps {
  collectionSlug: string;
  photoId?: string;
  timelineSlug?: string;
}

const PhotoCollectionImpl: React.FC<ImplProps> = ({
  collectionSlug,
  photoId,
  timelineSlug,
}) => {
  // Set context, which has to handle timeline view and folder view.
  const context = timelineSlug
    ? getTimelineGroupEventObjectFromURLPath(
        `/collection${collectionSlug}@timeline${timelineSlug}`
      )
    : makeEventGroupID(GroupType.COLLECTION, collectionSlug);
  useEventTracker(context);

  const navAdditions = useMemo<Addition[]>(() => {
    if (!timelineSlug) return [];
    return [
      {
        urlPrefix: "@timeline/",
        slug: timelineSlug,
        getEventObject: getTimelineGroupEventObjectFromURLPath,
      },
    ];
  }, [timelineSlug]);

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      groupUrl: `/${GROUP_PLURAL[GroupType.COLLECTION]}/`,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.COLLECTION]),
      navUrlPrefix: `/${GroupType.COLLECTION}/`,
      displaySep: GROUP_SEP[GroupType.COLLECTION],
    },
    collectionSlug,
    /*display*/ undefined,
    /*view*/ undefined,
    ...navAdditions
  );

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(
    (group: Group, timelineGroup?: TimelineGroup) => {
      if (timelineGroup) {
        setNavState(collectionSlug, group.display, {
          urlPrefix: "@timeline/",
          slug: timelineGroup.id.slice(timelineGroup.id.indexOf("/", 1)),
          display: timelineGroup.display.slice(
            timelineGroup.display.indexOf("|") + 1
          ),
          displaySep: GROUP_SEP[GroupType.TIMELINE_GROUP],
          getEventObject: getTimelineGroupEventObjectFromURLPath,
        });
      } else {
        setNavState(collectionSlug, group.display);
      }
    },
    [collectionSlug, setNavState]
  );

  // When the folder path changes, rebuild approximation.
  useEffect(() => {
    setNavState(collectionSlug, undefined, ...navAdditions);
  }, [collectionSlug, navAdditions, setNavState]);

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        groupId={collectionSlug}
        groupType={GroupType.COLLECTION}
        childrenIcon={<IconCollections />}
        lightboxPhotoId={photoId}
        timelineGroupPath={timelineSlug}
        getGroupName={useCallback(
          (g: Group) => getGroupName(GroupType.COLLECTION, g),
          []
        )}
        buildPhotoListFilter={useCallback(
          (groupId) => ({ folders: [{ path: groupId, fullTree: true }] }),
          []
        )}
        getChildren={getFolderChildren}
        onGroupInfo={onGroupInfo}
        SettingsExtension={PhotoFolderSettingsExtension}
      />
    </div>
  );
};
