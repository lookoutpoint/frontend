// SPDX-License-Identifier: MIT

import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { NotFound } from "./NotFound";
import { useEventTracker } from "../hooks/useEventTracker";
import {
  getGroupName,
  GroupType,
  Page,
  useGroupInfoQuery,
} from "../api/photoGroups";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { useErrorBoundary } from "react-error-boundary";
import { NotFoundError } from "./ErrorView";
import { RichText } from "../components/RichText";
import { Helmet } from "react-helmet-async";
import { titleCase } from "../helpers/titleCase";
import { makeTitleString } from "../util";

export const PageView: React.FC = () => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  // Page url must be a valid slug id
  if (url.length === 0 || url[url.length - 1] !== "/") {
    return <NotFound what="" />;
  }

  return <PageImpl pageSlug={`/${url}`} />;
};

interface ImplProps {
  pageSlug: string;
}

const PageImpl: React.FC<ImplProps> = ({ pageSlug }) => {
  useEventTracker(pageSlug);
  const { showBoundary } = useErrorBoundary();

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      navUrlPrefix: "/",
    },
    pageSlug
  );

  // When the slug changes, rebuild approximation.
  useEffect(() => {
    setNavState(pageSlug);
  }, [pageSlug, setNavState]);

  const pageQuery = useGroupInfoQuery<Page>(GroupType.PAGE, pageSlug);
  const page = pageQuery.data;

  const title = page ? getGroupName(GroupType.PAGE, page) : undefined;

  useEffect(() => {
    if (pageQuery.isFetching) return;

    if (!page) {
      showBoundary(new NotFoundError("page"));
    }
  }, [page, pageQuery.isFetching, showBoundary]);

  return (
    <div>
      {/* Metadata */}
      {title && (
        <Helmet>
          <title>{makeTitleString(titleCase(title))}</title>
          {page && page.metaDescription && (
            <meta name="description" content={page.metaDescription} />
          )}
        </Helmet>
      )}
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      {page && <RichText text={page.richText ?? ""} currentUrl={pageSlug} />}
    </div>
  );
};
