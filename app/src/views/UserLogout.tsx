// SPDX-License-Identifier: MIT
import React, { useEffect } from "react";
import { Helmet } from "react-helmet-async";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { resetCaches } from "../api/cache";
import { useUserSession } from "../hooks/useUserSession";
import { makeTitleString } from "../util";

const Message = styled.div`
  text-align: center;
`;

export const UserLogout: React.FC = () => {
  const userSession = useUserSession();
  const navigate = useNavigate();

  useEffect(() => {
    userSession.logout();

    // Reset all caches because they could have non-public content.
    resetCaches();
    navigate("/");
  }, [navigate, userSession]);

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Logout")}</title>
      </Helmet>

      <Message>Logging out...</Message>
    </div>
  );
};
