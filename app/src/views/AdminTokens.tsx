// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useState } from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { Dialog, RenderProps } from "../components/Dialog";
import {
  FormGrid,
  FormButton,
  FlexRow,
  IndentedFlexRow,
  DividedRows,
  FlexRowNoMargin,
} from "../components/Form";
import { EditableField } from "../components/EditableField";
import {
  createVisToken,
  createVisTokenLink,
  listVisibilityTokens,
  updateVisTokenLink,
  VisibilityToken,
  VisibilityTokenLink,
} from "../api/visibility";
import { CircleIconButton } from "../components/CircleIconButton";
import styled from "styled-components";
import {
  IconAdd,
  IconAngleDown,
  IconAngleRight,
  IconCheck,
  IconCopy,
} from "../components/Icons";

export const AdminTokens: React.FC = () => {
  // List of tokens.
  const [tokens, setTokens] = useState<VisibilityToken[]>([]);

  const refreshTokens = useCallback(async () => {
    const tokens = await listVisibilityTokens();
    setTokens(tokens ?? []);
  }, []);

  const onChange = useCallback(() => refreshTokens(), [refreshTokens]);

  useEffect(() => {
    refreshTokens();
  }, [refreshTokens]);

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Tokens")}</title>
      </Helmet>
      <BreadcrumbNav
        nav={[{ label: "Admin", link: "/admin" }]}
        activeLabel={"Tokens"}
      />
      <Dialog
        render={(rprops) => {
          return (
            <DividedRows>
              {/* Per-token settings */}
              {tokens.map((t) => (
                <Token
                  key={t.id}
                  rprops={rprops}
                  token={t}
                  onChange={onChange}
                />
              ))}
              {/* Add new token */}
              <AddToken rprops={rprops} onChange={onChange} />
            </DividedRows>
          );
        }}
      />
    </div>
  );
};

const Input = styled.input`
  min-width: 250px;
`;

interface TokenProps {
  rprops: RenderProps;
  token: VisibilityToken;
  onChange: () => void;
}

const Token: React.FC<TokenProps> = ({ rprops, token, onChange }) => {
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <FlexRow>
        <CircleIconButton
          toggle
          toggleActive={toggle}
          title={`${toggle ? "Collapse" : "Expand"} token options`}
          onClick={() => setToggle(!toggle)}
        >
          {toggle ? <IconAngleDown /> : <IconAngleRight />}
        </CircleIconButton>
        <span>{token.display}</span>
      </FlexRow>
      {toggle && (
        <DividedRows>
          <IndentedFlexRow>
            <b>Links</b>
          </IndentedFlexRow>
          {token.links?.map((l) => (
            <TokenLink
              key={l.activationValue}
              rprops={rprops}
              link={l}
              onChange={onChange}
            />
          ))}
          <AddLink rprops={rprops} token={token} onChange={onChange} />
        </DividedRows>
      )}
    </>
  );
};

interface TokenLinkProps {
  rprops: RenderProps;
  link: VisibilityTokenLink;
  onChange: () => void;
}

const TokenLink: React.FC<TokenLinkProps> = ({ rprops, link, onChange }) => {
  const { processing, setProcessing, setError } = rprops;

  const doUpdate = useCallback(
    async ({
      redirect,
      comment,
      disabled,
    }: {
      redirect?: string;
      comment?: string;
      disabled?: boolean;
    }) => {
      // If there are no changes, don't update.
      if (
        (redirect === undefined || redirect === link.redirect) &&
        (comment === undefined || comment === link.comment) &&
        (disabled === undefined || disabled === link.disabled)
      ) {
        return;
      }

      setProcessing(true);

      const success = await updateVisTokenLink(link.activationValue, {
        redirect,
        comment,
        disabled,
      });

      if (success) {
        // Success; refresh
        onChange();
      } else {
        setError("Error: Could not update visibility link");
      }

      setProcessing(false);
    },
    [
      link.activationValue,
      link.comment,
      link.disabled,
      link.redirect,
      onChange,
      setError,
      setProcessing,
    ]
  );

  // Activation link.
  const activationLink = `${window.location.protocol}//${window.location.host}/vtactivate/${link.activationValue}`;
  const copyLinkToClipboard = useCallback(() => {
    navigator.clipboard.writeText(activationLink).then(
      () => {},
      () => setError("Error: Could not copy to clipboard.")
    );
  }, [activationLink, setError]);

  return (
    <IndentedFlexRow>
      <CircleIconButton
        disabled={processing}
        toggle
        toggleActive={!link.disabled}
        title={`${link.disabled ? "Enable" : "Disable"} token link`}
        onClick={() => doUpdate({ disabled: !(link.disabled ?? false) })}
      >
        <IconCheck />
      </CircleIconButton>
      <FormGrid>
        <span>Link:</span>
        <FlexRowNoMargin>
          <span>{activationLink}</span>
          <CircleIconButton
            title="Copy link to clipboard"
            onClick={() => copyLinkToClipboard()}
          >
            <IconCopy />
          </CircleIconButton>
        </FlexRowNoMargin>
        <label>Redirect:</label>
        <EditableField
          placeholder="redirect"
          disabled={processing}
          curValue={link.redirect ?? ""}
          onChange={(v) => doUpdate({ redirect: v })}
        />
        <label>Comment:</label>
        <EditableField
          placeholder="comment"
          disabled={processing}
          curValue={link.comment ?? ""}
          onChange={(v) => doUpdate({ comment: v })}
        />
        <label>Activation count:</label>
        <div>{link.activationCount ?? 0}</div>
      </FormGrid>
    </IndentedFlexRow>
  );
};

interface AddLinkProps {
  rprops: RenderProps;
  token: VisibilityToken;
  onChange: () => void;
}

const AddLink: React.FC<AddLinkProps> = ({ rprops, token, onChange }) => {
  const { processing, setProcessing, setError } = rprops;

  const [toggle, setToggle] = useState(false);

  const [redirect, setRedirect] = useState("");
  const [comment, setComment] = useState("");

  const doAdd = useCallback(async () => {
    setProcessing(true);

    const link = await createVisTokenLink(token.id, { redirect, comment });

    if (link) {
      // Success; reset everything
      setToggle(false);
      setRedirect("");
      setComment("");

      onChange();
    } else {
      setError("Error: Could not create visibility link.");
    }

    setProcessing(false);
  }, [comment, onChange, redirect, setError, setProcessing, token.id]);

  return (
    <IndentedFlexRow>
      <CircleIconButton
        disabled={processing}
        toggle
        toggleActive={toggle}
        title="Add token link"
        onClick={() => setToggle(!toggle)}
      >
        <IconAdd />
      </CircleIconButton>
      {toggle && (
        <FormGrid>
          <label htmlFor="add-link-redirect">Redirect:</label>
          <Input
            id="add-link-redirect"
            placeholder="redirect"
            disabled={processing}
            value={redirect}
            onChange={(e) => setRedirect(e.target.value)}
          />
          <label htmlFor="add-link-comment">Comment:</label>
          <Input
            id="add-link-comment"
            placeholder="comment"
            disabled={processing}
            value={comment}
            onChange={(e) => setComment(e.target.value)}
          />
          <FormButton disabled={processing} onClick={() => doAdd()}>
            Add
          </FormButton>
        </FormGrid>
      )}
    </IndentedFlexRow>
  );
};

interface AddTokenProps {
  rprops: RenderProps;
  onChange: () => void;
}

const AddToken: React.FC<AddTokenProps> = ({ rprops, onChange }) => {
  const { processing, setProcessing, setError } = rprops;

  const [toggle, setToggle] = useState(false);
  const [name, setName] = useState("");

  const doAdd = useCallback(async () => {
    if (name.trim().length === 0) return;

    setProcessing(true);

    const visToken = await createVisToken(name);

    if (visToken !== null) {
      // Success.
      setName("");
      onChange();
    } else {
      setError("Error: Could not create new visibility token.");
    }
    setProcessing(false);
  }, [name, onChange, setError, setProcessing]);

  return (
    <FlexRow>
      <CircleIconButton
        disabled={processing}
        toggle
        toggleActive={toggle}
        title="Add token"
        onClick={() => setToggle(!toggle)}
      >
        <IconAdd />
      </CircleIconButton>
      {toggle && (
        <FormGrid>
          <label htmlFor="add-token-name">Name:</label>
          <Input
            id="add-token-name"
            placeholder="name"
            disabled={processing}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <FormButton disabled={processing} onClick={() => doAdd()}>
            Add
          </FormButton>
        </FormGrid>
      )}
    </FlexRow>
  );
};
