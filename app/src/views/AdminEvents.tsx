// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { EventGranularity } from "../api/events";
import { subDays } from "date-fns";
import { DateSelectionPanel } from "../components/events/DateSelectionPanel";
import { SummaryPanel } from "../components/events/SummaryPanel";
import { UTCDate } from "../api/date";
import {
  ControlBar,
  ControlBarButton,
  ControlBarItem,
} from "../components/ControlBar";
import { Explore } from "../components/events/Explore";
import { Groups } from "../components/events/Groups";
import { ToggleDiv } from "../components/ToggleDiv";
import { useTheme } from "../hooks/useTheme";
import { ThemeType } from "../helpers/Contexts";

export interface DateSelection {
  granularity: EventGranularity;
  startDate: UTCDate;
  endDate: UTCDate;
}

enum View {
  SUMMARY,
  GROUPS,
  EXPLORE,
}

export const AdminEvents: React.FC = () => {
  // Force light theme for events UX as I haven't bothered to style everything for both.
  const theme = useTheme();
  useEffect(() => {
    const prevTheme = theme.type;
    theme.setType(ThemeType.LIGHT);
    return () => theme.setType(prevTheme);
    // We only run this on mount and dismount.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Compute today's date (in UTC) once to avoid case where date changes during the lifetime of this component.
  // Due to how dates work in Javascript (always local time), we use dates in "local time" even though the components actually represent UTC time.
  const today = useMemo(() => {
    return UTCDate.fromLocal(new Date(Date.now()));
  }, []);

  const [dateSel, setDateSel] = useState<DateSelection>(() => {
    // Start off with 90-day view
    return {
      granularity: EventGranularity.DAY,
      startDate: new UTCDate(subDays(today.date, 89)),
      endDate: today,
    };
  });

  const [view, setView] = useState<View>(View.SUMMARY);

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Events")}</title>
      </Helmet>
      <BreadcrumbNav
        nav={[{ label: "Admin", link: "/admin" }]}
        activeLabel={"Events"}
      />
      <ControlBar>
        <ControlBarItem>
          <ControlBarButton
            title="Summary"
            toggle
            toggleActive={view === View.SUMMARY}
            onClick={useCallback(() => setView(View.SUMMARY), [])}
          >
            S
          </ControlBarButton>
        </ControlBarItem>
        <ControlBarItem>
          <ControlBarButton
            title="Groups"
            toggle
            toggleActive={view === View.GROUPS}
            onClick={useCallback(() => setView(View.GROUPS), [])}
          >
            G
          </ControlBarButton>
        </ControlBarItem>
        <ControlBarItem>
          <ControlBarButton
            title="Explore"
            toggle
            toggleActive={view === View.EXPLORE}
            onClick={useCallback(() => setView(View.EXPLORE), [])}
          >
            E
          </ControlBarButton>
        </ControlBarItem>
      </ControlBar>
      <DateSelectionPanel
        today={today}
        dateSel={dateSel}
        setDateSelection={setDateSel}
      />
      <ToggleDiv show={view === View.SUMMARY}>
        <SummaryPanel dateSel={dateSel} />
      </ToggleDiv>
      <ToggleDiv show={view === View.GROUPS}>
        <Groups dateSel={dateSel} />
      </ToggleDiv>
      <ToggleDiv show={view === View.EXPLORE}>
        <Explore dateSel={dateSel} />
      </ToggleDiv>
    </div>
  );
};
