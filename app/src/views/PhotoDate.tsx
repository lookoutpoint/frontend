// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect } from "react";
import { useParams } from "react-router-dom";
import { makeEventGroupID } from "../api/events";
import { ListComparisonOp } from "../api/list";
import {
  Group,
  getDateChildren,
  GroupType,
  GROUP_PLURAL,
  GROUP_SEP,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { useEventTracker } from "../hooks/useEventTracker";
import { IconDates } from "../components/Icons";
import { regexRoute } from "../helpers/route";
import { titleCase } from "../helpers/titleCase";
import { PhotoGroupList } from "./components/PhotoGroupList";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";

export const PhotoDate: React.FC<{ root?: boolean }> = ({ root }) => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  if (root) {
    // Root folder.
    return regexRoute(url, [
      {
        // URLs: <empty>, @children/
        regex: /^(?:|@children\/)$/,
        render: () => <PhotoDateImpl />,
      },
    ]);
  } else {
    // Child date.
    return regexRoute(url, [
      {
        // URL: :year/:month?/:day?<more>
        // <more>:
        // <empty>
        // @children/
        // @photos/:photoTitleSlug?/:photoId
        regex:
          /^(\d{4})\/(?:(\d{2})\/)?(?:(\d{2})\/)?(?:@children\/|@photos\/(?:(?:[^/]+\/)?(.+))?)?$/,
        render: (m) => {
          const [, year, month, day, photoId] = m;

          return (
            <PhotoDateImpl
              year={year}
              month={month}
              day={day}
              photoId={photoId}
            />
          );
        },
      },
    ]);
  }
};

interface ImplProps {
  year?: string;
  month?: string;
  day?: string;
  photoId?: string;
}

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function buildDisplaySlug(year?: number, month?: number, day?: number): string {
  let slug = "";
  if (year) {
    slug = `${year}`;
    if (month) {
      slug += "/" + monthNames[month - 1];

      if (day) {
        slug += "/" + day;
      }
    }
  }

  return slug;
}

// For dates, we generate our own display name in the date format we want to use for display.
function getGroupName(date: Pick<Group, "id">): string {
  // eslint-disable-next-line prefer-const
  let [, year, month, day] = date.id.split("/");
  day = day ? `${+day}` : "";
  month = month ? monthNames[+month - 1] : "";

  if (day) {
    return `${month} ${day}, ${year}`;
  }
  if (month) {
    return `${month} ${year}`;
  }
  if (year) {
    return year;
  }
  return "";
}

const PhotoDateImpl: React.FC<ImplProps> = ({ year, month, day, photoId }) => {
  const yearVal = year ? parseInt(year) : undefined;
  const monthVal = month ? parseInt(month) : undefined;
  const dayVal = day ? parseInt(day) : undefined;

  const slug =
    "/" +
    (year ? `${year}/` : "") +
    (month ? `${month}/` : "") +
    (day ? `${day}/` : "");
  const displaySlug = buildDisplaySlug(yearVal, monthVal, dayVal);

  useEventTracker(makeEventGroupID(GroupType.DATE, slug));

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      groupUrl: `/${GROUP_PLURAL[GroupType.DATE]}/`,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.DATE]),
      navUrlPrefix: `/${GroupType.DATE}/`,
      displaySep: GROUP_SEP[GroupType.DATE],
    },
    slug,
    displaySlug
  );

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(() => {
    setNavState(slug, displaySlug);
  }, [setNavState, slug, displaySlug]);

  // When the date changes, rebuild approximation.
  useEffect(() => {
    setNavState(slug);
  }, [slug, setNavState]);

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        groupId={slug}
        groupType={GroupType.DATE}
        childrenIcon={<IconDates />}
        getGroupName={getGroupName}
        buildPhotoListFilter={useCallback((groupId) => {
          return {
            dates: [
              {
                value: groupId.replaceAll("/", ""),
                cmpOp: ListComparisonOp.EQ,
              },
            ],
          };
        }, [])}
        getChildren={getDateChildren}
        lightboxPhotoId={photoId}
        onGroupInfo={onGroupInfo}
      />
    </div>
  );
};
