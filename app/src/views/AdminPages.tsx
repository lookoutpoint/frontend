// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useReducer, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  getGroupName,
  Group,
  GroupType,
  GROUP_PLURAL,
  getPageChildren,
  createPage,
  GROUP_SEP,
  deletePage,
  Page,
  pageVisibilityToken,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { IconAdd, IconAdmin, IconKeywords } from "../components/Icons";
import { regexRoute } from "../helpers/route";
import { titleCase } from "../helpers/titleCase";
import { PhotoGroupList } from "./components/PhotoGroupList";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";
import { CenteredText, Error } from "../components/Utility";
import { ListComparisonOp } from "../api/list";
import { Dialog, RenderProps } from "../components/Dialog";
import { FlexDiv, FlexRow } from "../components/Form";
import { styled } from "styled-components";
import { ControlBarButton, ControlBarItem } from "../components/ControlBar";
import { SettingsExtensionProps } from "./components/PhotoGroupSettings";
import { VIS_TOKEN_ALL } from "../api/visibility";
import {
  AdminSelfVisTokens,
  RecurseTokenParam,
} from "./components/AdminSelfVisTokens";

export const AdminPages: React.FC = () => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  return regexRoute(url, [
    {
      // URLs: <empty>, @children/, @info/
      regex: /^([^@]*)(?:|@children\/|@info\/)$/,
      render: (m) => {
        return <AdminPagesImpl pageSlug={`/${m[1]}`} />;
      },
    },
  ]);
};

interface ImplProps {
  pageSlug: string;
}

const AdminPagesImpl: React.FC<ImplProps> = ({ pageSlug }) => {
  const navigate = useNavigate();

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      groupUrl: `/admin/pages/`,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.PAGE]),
      navUrlPrefix: `/admin/pages/`,
      displaySep: GROUP_SEP[GroupType.PAGE],
    },
    pageSlug
  );

  const [display, setDisplay] = useState("");

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(
    (group: Group) => {
      setNavState(pageSlug, group.display);
      setDisplay(group.display);
    },
    [pageSlug, setNavState]
  );

  // When the page changes, rebuild approximation.
  useEffect(() => {
    setNavState(pageSlug);
  }, [pageSlug, setNavState]);

  // Refresh key is used to force a render of the PhotoGroupList.
  // Used when creating the root page for the first time to refresh the PhotoGroupList once the
  // root page exists.
  const [refreshKey, incRefreshKey] = useReducer((s) => s + 1, 0);

  const [showCreatePage, setShowCreatePage] = useState(false);
  const [showDeletePage, setShowDeletePage] = useState(false);

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        key={refreshKey}
        groupId={pageSlug}
        groupType={GroupType.PAGE}
        getGroupName={useCallback(
          (g: Group) => getGroupName(GroupType.PAGE, g),
          []
        )}
        childrenIcon={<IconKeywords />}
        buildPhotoListFilter={useCallback(
          () => ({
            // Impossible to satisfy, there are no photos.
            ratings: [
              { value: 1, cmpOp: ListComparisonOp.LT },
              { value: 1, cmpOp: ListComparisonOp.GT },
            ],
          }),
          []
        )}
        getChildren={getPageChildren}
        onGroupInfo={onGroupInfo}
        makeGroupUrl={(g) => `/admin/pages${g.id}`}
        cbAdminCustom={
          <React.Fragment>
            <ControlBarItem>
              <ControlBarButton
                title="Create page"
                fontSizeEm={1.15}
                onClick={() => setShowCreatePage(true)}
              >
                <IconAdd />
              </ControlBarButton>
            </ControlBarItem>
            <ControlBarItem>
              <ControlBarButton
                title="Delete page"
                fontSizeEm={1.15}
                onClick={() => setShowDeletePage(true)}
              >
                <IconAdd />
              </ControlBarButton>
            </ControlBarItem>
          </React.Fragment>
        }
        notFound={
          pageSlug === "/" && <CreateRootPage refresh={incRefreshKey} />
        }
        SettingsExtension={SettingsExtension}
      />
      {showCreatePage && (
        <CreatePageDialog
          parentId={display}
          onClose={() => setShowCreatePage(false)}
          onCreate={(id) => {
            setShowCreatePage(false);
            navigate(`/admin/pages${id}`);
          }}
        />
      )}
      {showDeletePage && (
        <DeletePageDialog
          id={pageSlug}
          onClose={() => setShowDeletePage(false)}
          onDelete={() => {
            setShowDeletePage(false);
            if (pageSlug === "/") {
              // Root. Refresh.
              incRefreshKey();
            } else {
              // Drop last component of path. Keep trailing '/'.
              const last = pageSlug.lastIndexOf("/", pageSlug.length - 2);
              navigate(`/admin/pages${pageSlug.substr(0, last + 1)}`);
            }
          }}
        />
      )}
    </div>
  );
};

// Button to create root page when it does not exist.
const CreateRootPage: React.FC<{ refresh: () => void }> = ({ refresh }) => {
  const [createError, setCreateError] = useState<string | null>(null);
  const [processing, setProcessing] = useState(false);

  const create = useCallback(async () => {
    setProcessing(true);

    try {
      const [success, id] = await createPage("");
      if (!success || id === undefined) {
        setCreateError("Error: Could not create root page.");
        return;
      }

      // Refresh.
      refresh();
    } finally {
      setProcessing(false);
    }
  }, [refresh]);

  return (
    <CenteredText>
      <p>No root page.</p>
      <button disabled={processing} onClick={create}>
        Create root page
      </button>
      {createError && <Error>{createError}</Error>}
    </CenteredText>
  );
};

// Create new child page dialog

interface CreatePageDialogProps {
  parentId: string;
  onClose: () => void;
  onCreate: (pageId: string) => void;
}

const CreatePageDialog: React.FC<CreatePageDialogProps> = ({
  parentId,
  onClose,
  onCreate,
}) => {
  return (
    <Dialog
      modalLevel={0}
      title={
        <span>
          <IconAdmin /> Create page
        </span>
      }
      onClose={onClose}
      render={(rprops) => (
        <CreatePageDialogImpl
          parentDisplay={parentId}
          rprops={rprops}
          onCreate={onCreate}
        />
      )}
    />
  );
};

const FullWidthFlexDiv = styled(FlexDiv)`
  width: 100%;
`;
const StyledInput = styled.input`
  width: 100%;
`;

interface CreatePageDialogImplProps {
  parentDisplay: string;
  rprops: RenderProps;
  onCreate: (pageId: string) => void;
}

const CreatePageDialogImpl: React.FC<CreatePageDialogImplProps> = ({
  parentDisplay,
  rprops,
  onCreate: onCreateCb,
}) => {
  const [name, setName] = useState("");

  const onCreate = useCallback(() => {
    rprops.setError(null);
    rprops.setProcessing(true);

    (async () => {
      const [success, str] = await createPage(`${parentDisplay}/${name}`);
      rprops.setProcessing(false);
      if (success && str) {
        // str is the page id
        onCreateCb(str);
      } else {
        rprops.setError(str ?? "Could not create page");
      }
    })();
  }, [name, onCreateCb, parentDisplay, rprops]);

  const onRef = useCallback((ref: HTMLElement | null) => {
    if (!ref) return;

    // Focus on mount.
    ref.focus();
  }, []);

  return (
    <div>
      <FlexRow>
        <label htmlFor="name">
          <b>Name:</b>
        </label>
        <FullWidthFlexDiv>
          {parentDisplay}/
          <StyledInput
            ref={onRef}
            value={name}
            onChange={(e) => setName(e.target.value)}
            disabled={rprops.processing}
          />
        </FullWidthFlexDiv>
      </FlexRow>
      <FlexRow>
        <button disabled={rprops.processing} onClick={onCreate}>
          Create
        </button>
      </FlexRow>
    </div>
  );
};

// Delete page dialog
interface DeletePageDialogProps {
  id: string;
  onClose: () => void;
  onDelete: () => void;
}

const DeletePageDialog: React.FC<DeletePageDialogProps> = ({
  id,
  onClose,
  onDelete: onDeleteCb,
}) => {
  return (
    <Dialog
      modalLevel={0}
      title={
        <span>
          <IconAdmin /> Delete page
        </span>
      }
      onClose={onClose}
      render={(rprops) => {
        const onDelete = async () => {
          rprops.setError(null);
          rprops.setProcessing(true);

          const success = await deletePage(id);
          rprops.setProcessing(false);
          if (success) {
            onDeleteCb();
          } else {
            rprops.setError("Could not delete page.");
          }
        };

        return (
          <div>
            <CenteredText>
              <p>
                Are you sure you want to delete this page? It must not have any
                child pages.
              </p>
              <button disabled={rprops.processing} onClick={onDelete}>
                Delete page
              </button>
            </CenteredText>
          </div>
        );
      }}
    />
  );
};

const SettingsExtension: React.FC<SettingsExtensionProps<Page>> = ({
  rprops,
  groupInfo: page,
  tokenMap,
  refresh,
}) => {
  const selfVisTokens = (page.selfVisTokens ?? [])
    .filter((t) => t !== VIS_TOKEN_ALL)
    .sort();

  return (
    <>
      <AdminSelfVisTokens
        selfVisTokens={selfVisTokens}
        rprops={rprops}
        tokenMap={tokenMap}
        refresh={refresh}
        initTokenParams={/*recurse*/ false}
        renderTokenParams={(idPrefix, rprops, params, onParamsChange) => (
          <RecurseTokenParam
            idPrefix={idPrefix}
            rprops={rprops}
            params={params}
            onParamsChange={onParamsChange}
          />
        )}
        deleteToken={(token, recurse) => {
          return pageVisibilityToken("delete", page.id, token, recurse);
        }}
        addToken={(token, recurse) => {
          return pageVisibilityToken("add", page.id, token, recurse);
        }}
      />
    </>
  );
};
