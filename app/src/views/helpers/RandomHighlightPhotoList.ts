// SPDX-License-Identifier: MIT
import { QueryClient } from "@tanstack/react-query";
import { ListComparisonOp } from "../../api/list";
import {
  getPhotoMetadata,
  PhotoListPhoto,
  randomPhotos,
} from "../../api/photos";
import { PhotoListQuery } from "../../helpers/PhotoListQuery";

export class RandomHighlightPhotoList extends PhotoListQuery<"id"> {
  constructor(private queryClient: QueryClient) {
    super("id", async (count) => this.fetchNew(count), 3);
  }

  private fetchedIds = new Set<PhotoListPhoto["id"]>();
  private insertIds: string[] = [];

  insertId(id: string): void {
    if (this.fetchedIds.has(id)) return;
    this.insertIds.push(id);
  }

  // This assumes we are always fetching forward and won't be called in the middle
  // of an existing photo sequence (in the ListQuery).
  private async fetchNew(count: number): Promise<PhotoListPhoto[]> {
    let attempt = 0;
    const photos: PhotoListPhoto[] = [];

    while (photos.length < count && this.insertIds.length > 0) {
      const id = this.insertIds.shift()!;
      const photo = await getPhotoMetadata(this.queryClient, { id });
      photos.push(photo);
      this.fetchedIds.add(photo.id);
    }

    // Stop if we can't find new photos after three photos.
    // We only want to return new photos otherwise the photo list will have duplicate
    // ids, which is bad.
    while (photos.length < count && attempt < 3) {
      // Try rawFetch first.
      let newPhotos = await this.rawFetch(count);

      // Filter out photos that we were already fetched before.
      newPhotos = newPhotos.filter((p) => !this.fetchedIds.has(p.id));

      // Add new photos.
      photos.push(...newPhotos);
      newPhotos.forEach((p) => this.fetchedIds.add(p.id));

      attempt++;
    }

    return photos;
  }

  private async rawFetch(count: number): Promise<PhotoListPhoto[]> {
    const photos = await randomPhotos(this.queryClient, {
      count,
      filter: {
        ratings: [{ value: 4, cmpOp: ListComparisonOp.GTE }],
        aspectRatios: [
          { value: 9 / 16, cmpOp: ListComparisonOp.GTE },
          { value: 16 / 9, cmpOp: ListComparisonOp.LTE },
        ],
      },
    });
    return photos ?? [];
  }
}
