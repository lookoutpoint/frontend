// SPDX-License-Identifier: MIT
import pluralize from "pluralize";
import React, { useCallback, useEffect, useState } from "react";
import { Helmet } from "react-helmet-async";
import { useParams } from "react-router-dom";
import {
  getCategoryChildren,
  getGroupName,
  Group,
  GroupType,
  GROUP_PLURAL,
  GROUP_SEP,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { titleCase } from "../helpers/titleCase";
import { makeTitleString } from "../util";
import { PhotoGroupList } from "./components/PhotoGroupList";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";
import { useEventTracker } from "../hooks/useEventTracker";
import { makeEventGroupID } from "../api/events";
import { regexRoute } from "../helpers/route";
import { IconCategories } from "../components/Icons";
import { getCategoryIcon } from "../helpers/Icons";

export const PhotoCategory: React.FC<{ root?: boolean }> = ({ root }) => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  if (root) {
    // Root folder.
    return regexRoute(url, [
      {
        // URLs: <empty>, @children/
        regex: /^(?:|@children\/)$/,
        render: () => <PhotoCategoryImpl categorySlug="/" />,
      },
    ]);
  } else {
    // Child category and/or value.
    return regexRoute(url, [
      {
        // URL: :categoryNameSlug/:moreUrl
        regex: /^([^/@]+\/)(.*)$/,
        render: (m) => {
          const categoryNameSlug = m[1].slice(0, m[1].length - 1); // exclude trailing '/'
          const categorySlug = `/${categoryNameSlug}/`;
          const moreUrl = m[2];

          return regexRoute(moreUrl, [
            {
              // URLs:
              // <empty>
              // @children/
              // @info/
              // @map/
              // @photos/
              // @photos/:photoTitleSlug?/:photoId
              regex:
                /^(?:@children\/|@info\/|@map\/|@photos\/(?:(?:[^/]+\/)?(.+))?)?$/,
              render: (m) => (
                <PhotoCategoryImpl categorySlug={categorySlug} photoId={m[1]} />
              ),
            },
            {
              // URLs: :categoryValueSlug/<more>
              // <more>:
              // <empty>
              // @children/
              // @map/
              // @photos/
              // @photos/:photoTitleSlug?/:photoId
              regex:
                /^([^/@]+\/)(?:@children\/|@map\/|@photos\/(?:(?:[^/]+\/)?(.+))?)?$/,
              render: (m) => {
                const categoryValueSlug = m[1].slice(0, m[1].length - 1); // exclude trailing '/'
                const categorySlug = `/${categoryNameSlug}/${categoryValueSlug}/`;
                const photoId = m[2];

                return (
                  <PhotoCategoryImpl
                    categorySlug={categorySlug}
                    photoId={photoId}
                  />
                );
              },
            },
          ]);
        },
      },
    ]);
  }
};

interface ImplProps {
  categorySlug: string; // full id
  photoId?: string;
}

const PhotoCategoryImpl: React.FC<ImplProps> = ({ categorySlug, photoId }) => {
  useEventTracker(makeEventGroupID(GroupType.CATEGORY, categorySlug));

  const [, navState, setNavState] = usePhotoGroupNavState(
    {
      groupUrl: `/${GROUP_PLURAL[GroupType.CATEGORY]}/`,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.CATEGORY]),
      navUrlPrefix: `/${GroupType.CATEGORY}/`,
      displaySep: GROUP_SEP[GroupType.CATEGORY],
    },
    categorySlug
  );

  const [icon, setIcon] = useState<React.ReactNode>(<IconCategories />);

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(
    (group: Group) => {
      // Set nav state
      setNavState(categorySlug, group.display);

      // Set icon
      let catId = categorySlug.substring(
        categorySlug.lastIndexOf("/", categorySlug.length - 2) + 1
      );
      catId = catId.substring(0, catId.length - 1);
      setIcon(getCategoryIcon(catId));
    },
    [categorySlug, setNavState]
  );

  // When the category changes, rebuild approximation.
  useEffect(() => {
    setNavState(categorySlug);
  }, [categorySlug, setNavState]);

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Categories")}</title>
      </Helmet>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        groupId={categorySlug}
        groupType={GroupType.CATEGORY}
        childrenIcon={icon}
        getGroupName={useCallback(
          (g: Group) => getGroupName(GroupType.CATEGORY, g),
          []
        )}
        buildPhotoListFilter={useCallback(
          (groupId) => ({ categories: [groupId] }),
          []
        )}
        getChildren={getCategoryChildren}
        getChildrenLabel={useCallback((g: Group) => {
          if (g.id === "/") {
            return titleCase(GROUP_PLURAL[GroupType.CATEGORY]);
          }
          return titleCase(pluralize(getGroupName(GroupType.CATEGORY, g)));
        }, [])}
        lightboxPhotoId={photoId}
        onGroupInfo={onGroupInfo}
      />
    </div>
  );
};
