// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo } from "react";
import { EventContext } from "../api/events";
import {
  getGroupName,
  getTimelineGroupChildren,
  Group,
  GroupType,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { useEventTracker } from "../hooks/useEventTracker";
import {
  GroupInfiniteScroll,
  GroupInfiniteScrollEvents,
} from "../components/GroupInfiniteScroll";
import { PHOTO_PADDING } from "../components/PhotoConstants";
import { useUserSession } from "../hooks/useUserSession";
import { useWindowSize } from "../hooks/useWindowSize";
import { GroupListQuery } from "../helpers/GroupListQuery";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";

// Expected path:
// - /blogs/
export const Blogs: React.FC = () => {
  const session = useUserSession();
  const windowSize = useWindowSize();
  useEventTracker(EventContext.BLOG);

  const [, navState] = usePhotoGroupNavState(
    {
      groupUrl: "/blogs/",
      groupLabel: "Blogs",
      navUrlPrefix: `/${GroupType.COLLECTION}/`,
    },
    "/"
  );

  const glq = useMemo(() => {
    return new GroupListQuery((count, refId, refOp) =>
      getTimelineGroupChildren(session, {
        id: "/",
        limit: count,
        refId,
        refOp,
        coverPhoto: true,
        maxRelDepth: 0,
        extension: { sortBy: "timestamp" },
      })
    );
  }, [session]);

  // Min width: 100 px
  // Max width: 400 px
  // In between, try to size to show at least two items
  const itemWidth = Math.max(
    100,
    Math.min(400, windowSize.width / 2 - PHOTO_PADDING * 2)
  );
  const galleryEvents = useMemo<GroupInfiniteScrollEvents>(
    () => ({
      context: EventContext.BLOG,
      groupType: GroupType.TIMELINE_GROUP,
    }),
    []
  );

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <GroupInfiniteScroll
        groupListQuery={glq}
        queryForward={false} // show most recent first
        getGroupName={(g) => getGroupName(GroupType.TIMELINE_GROUP, g)}
        makeGroupUrl={useCallback((g: Group) => {
          // Get folder id
          const sep = g.id.indexOf("/", 1);
          const collId = g.id.substring(1, sep).replaceAll(":", "/");
          return `/${GroupType.COLLECTION}${collId}@timeline/`;
        }, [])}
        desiredPhotoWidth={itemWidth}
        photoRatio={3 / 2}
        showOverlay
        events={galleryEvents}
      />
    </div>
  );
};
