// SPDX-License-Identifier: MIT
import { PHOTO_PADDING } from "../../components/PhotoConstants";
import { useWindowSize } from "../../hooks/useWindowSize";

export function useHomeGroupRowLimit(): number {
  const windowSize = useWindowSize();
  return windowSize.width <= 600 ? 2 : 1;
}

export function useHomeGroupPhotoWidth(): number {
  const windowSize = useWindowSize();
  // Min width: 100 px
  // Max width: 300 px
  // In between, try to size to show at least two items
  return Math.max(150, Math.min(300, windowSize.width / 2 - PHOTO_PADDING * 2));
}
