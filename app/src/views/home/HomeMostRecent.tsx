// SPDX-License-Identifier: MIT
import React, { useMemo, useState } from "react";
import { AppEvent, EventContext, EventLink } from "../../api/events";
import { ListComparisonOp } from "../../api/list";
import { listPhotos, PhotoListKey } from "../../api/photos";
import { IconRecent } from "../../components/Icons";
import { EventDesc } from "../../helpers/EventTracker";
import { PhotoListQuery } from "../../helpers/PhotoListQuery";
import { usePhotoListSizes } from "../../hooks/usePhotoListSizes";
import { GroupDiv, GroupTitleLink } from "../home/HomeGroups";
import { useHomeGroupRowLimit } from "./hooks";
import { useQueryClient } from "@tanstack/react-query";
import { PhotoGalleryViewWrapper } from "../components/PhotoGalleryViewWrapper";

interface HomeMostRecentProps {
  lightboxPhotoId?: string;
}

export const HomeMostRecent: React.FC<HomeMostRecentProps> = ({
  lightboxPhotoId,
}) => {
  const queryClient = useQueryClient();

  const [photoListQuery] = useState(() => {
    return new PhotoListQuery("tsSortKey", (count, refId, refOp) =>
      listPhotos(queryClient, {
        limit: count,
        refKey: PhotoListKey.TIMESTAMP,
        refKeyValue: refId,
        refOp: refOp,
        filter: {
          ratings: [{ value: 4, cmpOp: ListComparisonOp.GTE }],
        },
      })
    );
  });

  const titleLinkEvent = useMemo<EventDesc>(
    () => ({
      event: AppEvent.LINK_CLICK,
      object: EventLink.RECENT,
      context: EventContext.HOME_EXPLORE_RECENT,
    }),
    []
  );

  const photoListSizes = usePhotoListSizes();

  return (
    <GroupDiv $index={0}>
      <GroupTitleLink to="/recent/" event={titleLinkEvent}>
        <IconRecent /> <span>Recent</span>
      </GroupTitleLink>
      <PhotoGalleryViewWrapper
        photoListQuery={photoListQuery}
        photoListQueryForward={false}
        showGalleryOverlay={true}
        rootUrl="/@recent"
        closeUrl="/"
        stdHeight={(photoListSizes.photoStdHeight * 2) / 3}
        rowLimit={useHomeGroupRowLimit()}
        lightboxPhotoId={lightboxPhotoId}
        eventContext={EventContext.HOME_EXPLORE_RECENT}
      />
    </GroupDiv>
  );
};
