// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useMemo, useState } from "react";
import styled from "styled-components";
import { AppEvent, EventContext } from "../../api/events";
import {
  getGroupName,
  getHomeGroups,
  Group,
  GroupSelection,
  GroupType,
  GROUP_PLURAL,
  GROUP_SEP,
  HomeGroupSelection,
} from "../../api/photoGroups";
import {
  GroupInfiniteScroll,
  GroupInfiniteScrollEvents,
} from "../../components/GroupInfiniteScroll";
import {
  IconCategories,
  IconCollections,
  IconKeywords,
  IconLocations,
} from "../../components/Icons";
import { LinkWithEvent } from "../../components/LinkWithEvent";
import { OVERLAY_PADDING } from "../../components/PhotoConstants";
import { useProcessingIndicatorActions } from "../../hooks/useProcessingIndicator";
import { useUserSession } from "../../hooks/useUserSession";
import { EventDesc } from "../../helpers/EventTracker";
import {
  GroupListQuery,
  makeFetchGroupsFromList,
} from "../../helpers/GroupListQuery";
import { titleCase } from "../../helpers/titleCase";
import { getLocFancyGroupName } from "../../helpers/locations";
import { useHomeGroupRowLimit, useHomeGroupPhotoWidth } from "./hooks";

const GroupsDiv = styled.div``;

// index is used to alternate background color
export const GroupDiv = styled.div<{ $index: number }>`
  display: flex;
  flex-direction: column;
  padding-bottom: 1.5em;
  background-color: ${(p) => `var(--homegroup-bg-color${1 + (p.$index % 2)})`};
`;

export const GroupTitleLink = styled(LinkWithEvent)`
  color: var(--homegroup-color);
  text-decoration: none;
  padding: ${OVERLAY_PADDING * 2}em;
  text-align: center;
  font-size: 110%;

  &:hover {
    color: var(--caption-hover-color);
    background-color: var(--caption-hover-bg-color);
  }

  span {
    text-decoration: underline;
    text-decoration-color: var(--homegroup-color);
  }

  &:hover span {
    text-decoration-color: var(--homegroup-hover-color);
  }
`;

export const HomeGroups: React.FC = () => {
  const session = useUserSession();
  const procIndicatorActions = useProcessingIndicatorActions();

  const [groups, setGroups] = useState<HomeGroupSelection | null>(null);

  // Fetch home groups.
  useEffect(() => {
    let unmounted = false;

    procIndicatorActions.start();
    getHomeGroups(session, 3, 8).then((groups) => {
      procIndicatorActions.stop();
      if (unmounted) return;
      if (groups) {
        setGroups(groups);
      }
    });

    return () => {
      unmounted = true;
    };
  }, [procIndicatorActions, session]);

  return groups ? (
    <GroupsDiv>
      <HomeGroup
        groupType={GroupType.COLLECTION}
        icon={<IconCollections />}
        groupSel={groups.folders}
        getGroupName={(group) => getGroupName(GroupType.COLLECTION, group)}
        eventContext={EventContext.HOME_EXPLORE_FOLDERS}
        index={0}
      />
      <HomeGroup
        groupType={GroupType.LOCATION}
        icon={<IconLocations />}
        groupSel={groups.locations}
        getGroupName={(group) => {
          // Return two most-specific locations.
          const sep = GROUP_SEP[GroupType.LOCATION];
          const locs = group.display.split(sep);
          return locs
            .slice(Math.max(0, locs.length - 2))
            .reverse()
            .join(`${sep} `);
        }}
        getFancyGroupName={(group) => {
          // Return two most-specific locations.
          const sep = GROUP_SEP[GroupType.LOCATION];
          let locs = group.display.split(sep);
          locs = locs.slice(Math.max(0, locs.length - 2)).reverse();
          const elements: JSX.Element[] = [];
          for (const loc of locs) {
            if (elements.length > 0) {
              elements.push(<React.Fragment key="sep">{sep} </React.Fragment>);
            }
            elements.push(
              <React.Fragment key={`loc-${elements.length}`}>
                {getLocFancyGroupName(loc)}
              </React.Fragment>
            );
          }
          return <>{elements}</>;
        }}
        eventContext={EventContext.HOME_EXPLORE_LOCATIONS}
        index={1}
      />
      <HomeGroup
        groupType={GroupType.CATEGORY}
        icon={<IconCategories />}
        groupSel={groups.categories}
        getGroupName={(group) => {
          // Return entire display string (i.e. category and value).
          const sep = GROUP_SEP[GroupType.CATEGORY];
          return group.display.replaceAll(sep, `${sep} `);
        }}
        eventContext={EventContext.HOME_EXPLORE_CATEGORIES}
        index={2}
      />
      <HomeGroup
        groupType={GroupType.KEYWORD}
        icon={<IconKeywords />}
        groupSel={groups.keywords}
        getGroupName={(group) => group.display}
        eventContext={EventContext.HOME_EXPLORE_KEYWORDS}
        index={3}
      />
    </GroupsDiv>
  ) : null;
};

interface HomeGroupProps {
  groupType: GroupType;
  icon: React.ReactNode;
  groupSel: GroupSelection;
  getGroupName: (group: Group) => string;
  getFancyGroupName?: (group: Group) => JSX.Element;
  eventContext: EventContext;
  index: number;
}

const HomeGroup: React.FC<HomeGroupProps> = ({
  groupType,
  icon,
  groupSel,
  getGroupName,
  getFancyGroupName,
  eventContext,
  index,
}) => {
  const groupUrl = `/${GROUP_PLURAL[groupType]}/`;
  const groupUrlPrefix = `/${groupType}`;
  const title = titleCase(GROUP_PLURAL[groupType]);

  const glq = useMemo(() => {
    const items = [...groupSel.recent, ...groupSel.other];
    return new GroupListQuery(makeFetchGroupsFromList("id", items));
  }, [groupSel.other, groupSel.recent]);

  const titleLinkEvent = useMemo<EventDesc>(() => {
    return {
      event: AppEvent.GROUP_CLICK,
      // Id of root group is "/"
      object: `${groupUrlPrefix}/`,
      context: eventContext,
    };
  }, [eventContext, groupUrlPrefix]);
  const galleryEvents = useMemo<GroupInfiniteScrollEvents>(
    () => ({
      context: eventContext,
      groupType: groupType,
    }),
    [eventContext, groupType]
  );

  return (
    <GroupDiv $index={index + 2 /* offset by 2 for recent and blogs groups */}>
      <GroupTitleLink to={groupUrl} event={titleLinkEvent}>
        {icon} <span>{title}</span>
      </GroupTitleLink>
      <GroupInfiniteScroll
        groupListQuery={glq}
        getGroupName={getGroupName}
        getFancyGroupName={getFancyGroupName}
        makeGroupUrl={useCallback(
          (g: Group) => {
            return `${groupUrlPrefix}${g.id}`;
          },
          [groupUrlPrefix]
        )}
        rowLimit={useHomeGroupRowLimit()}
        desiredPhotoWidth={useHomeGroupPhotoWidth()}
        photoRatio={3 / 2}
        showOverlay
        events={galleryEvents}
      />
    </GroupDiv>
  );
};
