// SPDX-License-Identifier: MIT
import React, { useMemo } from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../../util";
import { HomeRandomHighlightPhoto } from "./HomeRandomHighlightPhoto";
import { HomeGroups } from "./HomeGroups";
import { HomeMostRecent } from "./HomeMostRecent";
import { HomeTimelines } from "./HomeTimelines";
import styled from "styled-components";
import {
  ControlBar,
  ControlBarButton,
  ControlBarGroup,
  ControlBarItem,
} from "../../components/ControlBar";
import { useEventTracker } from "../../hooks/useEventTracker";
import { AppEvent, EventContext } from "../../api/events";
import { EventDesc } from "../../helpers/EventTracker";
import { useParams } from "react-router-dom";
import { GROUP_PLURAL, GroupType } from "../../api/photoGroups";
import {
  IconCategoryAnimal,
  IconCategoryFlower,
  IconCategoryHike,
  IconCategoryPark,
  IconLocal,
  IconTravel,
} from "../../components/Icons";

const HomeDiv = styled.div`
  margin-top: 0em;
`;

const ExploreSection = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
  padding: 0.5em 0;
`;

const ExploreTitle = styled.div`
  text-align: center;
  font-size: calc(var(--fancy-title-size) * 1.2);
  font-style: italic;
  font-family: var(--fancy-font), cursive;
  background-color: var(--control-bar-bg-color);
  border-top: 1px solid var(--control-bar-border-color);
  border-bottom: 1px solid var(--control-bar-border-color);
  padding: 0.5em 0;
`;

interface RouteParams {
  photoId?: string;
  recentPhotoId?: string;
  [key: string]: string | undefined;
}

export const Home: React.FC = () => {
  const { photoId: lightboxPhotoId, recentPhotoId: recentLightboxPhotoId } =
    useParams<RouteParams>();

  useEventTracker(EventContext.HOME);

  return (
    <HomeDiv>
      <Helmet>
        <title>{makeTitleString()}</title>
      </Helmet>
      <ControlBar sticky>
        <ControlBarGroup>
          <HomeControlBarButton
            title="Local"
            link={`/${GroupType.COLLECTION}/Local/`}
            icon={<IconLocal />}
            event={AppEvent.GROUP_CLICK}
          />
          <HomeControlBarButton
            title="Travel"
            link={`/${GroupType.COLLECTION}/Travel/`}
            icon={<IconTravel />}
            event={AppEvent.GROUP_CLICK}
          />
          <HomeControlBarButton
            title="Hikes"
            link={`/${GROUP_PLURAL[GroupType.LOCATION]}/@hikes/`}
            icon={<IconCategoryHike />}
            event={AppEvent.LINK_CLICK}
          />
          <HomeControlBarButton
            title="Parks"
            link={`/${GROUP_PLURAL[GroupType.LOCATION]}/@parks/`}
            icon={<IconCategoryPark />}
            event={AppEvent.LINK_CLICK}
          />
          <HomeControlBarButton
            title="Animals"
            link={`/${GroupType.CATEGORY}/animal/`}
            icon={<IconCategoryAnimal />}
            event={AppEvent.GROUP_CLICK}
          />
          <HomeControlBarButton
            title="Flowers"
            link={`/${GroupType.CATEGORY}/flower/`}
            icon={<IconCategoryFlower />}
            event={AppEvent.GROUP_CLICK}
          />
        </ControlBarGroup>
      </ControlBar>
      <HomeRandomHighlightPhoto lightboxPhotoId={lightboxPhotoId} />
      <ExploreSection>
        <ExploreTitle>Explore</ExploreTitle>
        <HomeMostRecent lightboxPhotoId={recentLightboxPhotoId} />
        <HomeTimelines />
        <HomeGroups />
      </ExploreSection>
    </HomeDiv>
  );
};

interface HomeControlBarButtonProps {
  title: string;
  link: string;
  icon: React.ReactNode;
  event: AppEvent;
}

const HomeControlBarButton: React.FC<HomeControlBarButtonProps> = ({
  title,
  link,
  icon,
  event,
}) => {
  const eventDesc = useMemo<EventDesc>(() => {
    return {
      event,
      object: link,
      context: EventContext.HOME_TOP_BAR,
    };
  }, [event, link]);

  return (
    <ControlBarItem>
      <ControlBarButton title={title} link={link} eventDesc={eventDesc}>
        {icon}
      </ControlBarButton>
    </ControlBarItem>
  );
};
