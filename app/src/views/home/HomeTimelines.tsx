// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo } from "react";
import {
  getGroupName,
  getTimelineGroupChildren,
  Group,
  GroupType,
} from "../../api/photoGroups";
import { useUserSession } from "../../hooks/useUserSession";
import { GroupListQuery } from "../../helpers/GroupListQuery";
import { GroupDiv, GroupTitleLink } from "../home/HomeGroups";
import { useHomeGroupPhotoWidth, useHomeGroupRowLimit } from "./hooks";
import {
  GroupInfiniteScroll,
  GroupInfiniteScrollEvents,
} from "../../components/GroupInfiniteScroll";
import { AppEvent, EventLink, EventContext } from "../../api/events";
import { EventDesc } from "../../helpers/EventTracker";
import { IconBlog } from "../../components/Icons";

export const HomeTimelines: React.FC = () => {
  const session = useUserSession();

  const glq = useMemo(() => {
    return new GroupListQuery((count, refId, refOp) =>
      getTimelineGroupChildren(session, {
        id: "/",
        limit: count,
        refId,
        refOp,
        coverPhoto: true,
        maxRelDepth: 0,
        extension: { sortBy: "timestamp" },
      })
    );
  }, [session]);

  const titleLinkEvent = useMemo<EventDesc>(() => {
    return {
      event: AppEvent.LINK_CLICK,
      object: EventLink.BLOGS,
      context: EventContext.HOME_EXPLORE_BLOGS,
    };
  }, []);
  const galleryEvents = useMemo<GroupInfiniteScrollEvents>(
    () => ({
      context: EventContext.HOME_EXPLORE_BLOGS,
      groupType: GroupType.TIMELINE_GROUP,
    }),
    []
  );

  return (
    <GroupDiv $index={1}>
      <GroupTitleLink to={"/blogs/"} event={titleLinkEvent}>
        <IconBlog /> <span>Adventure Blogs</span>
      </GroupTitleLink>
      <GroupInfiniteScroll
        groupListQuery={glq}
        queryForward={false} // show most recent first
        getGroupName={(g) => getGroupName(GroupType.TIMELINE_GROUP, g)}
        makeGroupUrl={useCallback((g: Group) => {
          // Get folder id
          const sep = g.id.indexOf("/", 1);
          const collId = g.id.substring(1, sep).replaceAll(":", "/");
          return `/${GroupType.COLLECTION}${collId}@timeline/`;
        }, [])}
        rowLimit={useHomeGroupRowLimit()}
        desiredPhotoWidth={useHomeGroupPhotoWidth()}
        photoRatio={3 / 2}
        showOverlay
        events={galleryEvents}
      />
    </GroupDiv>
  );
};
