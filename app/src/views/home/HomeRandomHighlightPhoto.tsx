// SPDX-License-Identifier: MIT
import React, {
  CSSProperties,
  useCallback,
  useContext,
  useEffect,
  useLayoutEffect,
  useMemo,
  useReducer,
  useRef,
  useState,
} from "react";
import { useWindowSize } from "../../hooks/useWindowSize";
import {
  PhotoGroup,
  PhotoListPhoto,
  PHOTO_GROUP_DISPLAY,
  PHOTO_GROUP_ID,
  splitPhotoGroup,
  PartialFullPhoto,
  getPhotoMetadataQueryOptions,
} from "../../api/photos";
import styled from "styled-components";
import { ConstrainedPhoto } from "../../components/ConstrainedPhoto";
import { PhotoLink, PhotoLinkDiv } from "../../components/PhotoLink";
import { PhotoLightbox } from "../../components/PhotoLightbox";
import { CircleIconButton } from "../../components/CircleIconButton";
import {
  OVERLAY_PADDING,
  PHOTO_PADDING,
} from "../../components/PhotoConstants";
import { RandomHighlightPhotoList } from "../helpers/RandomHighlightPhotoList";
import { useEventListener } from "../../hooks/useEventListener";
import { useNavigate } from "react-router-dom";
import {
  makeLightboxPhotoUrl,
  swipeAnimationDuration,
} from "../../helpers/photos";
import { useProcessingIndicatorActions } from "../../hooks/useProcessingIndicator";
import { GroupType, GROUP_SEP } from "../../api/photoGroups";
import { titleCase } from "../../helpers/titleCase";
import { useEventTracker } from "../../hooks/useEventTracker";
import { LinkWithEvent } from "../../components/LinkWithEvent";
import {
  AppEvent,
  EventButton,
  EventContext,
  makeEventPhotoID,
} from "../../api/events";
import { EventDesc } from "../../helpers/EventTracker";
import {
  IconArrowLeft,
  IconArrowRight,
  IconCollection,
  IconHike,
  IconLocation,
  IconPark,
} from "../../components/Icons";
import { LocationType, stripAndGetLocationType } from "../../helpers/locations";
import { CanonicalPhotoUrl } from "../../components/CanonicalPhotoUrl";
import { useErrorBoundary } from "react-error-boundary";
import { useQueryClient } from "@tanstack/react-query";

const PhotoContainer = styled.div`
  position: relative;
  overflow: hidden;
`;

const PhotoInfoFlex = styled.div`
  display: flex;
  flex-flow: row wrap;
  column-gap: 0.5em;
  min-width: 0; /* set to 0 to allow photo info to be forced to shrink */
`;

const PhotoNavFlex = styled.div`
  display: flex;
  flex-flow: row wrap;
  min-width: 2em; /* one button width */

  & > *:first-child {
    margin-right: 0.5em;
  }
`;

const PhotoInfoDiv = styled.div`
  padding: ${OVERLAY_PADDING * 2}em;
  background-color: var(--caption-bg-color);
  color: var(--caption-color);

  // Set cursor to default because the cursor will be pointer
  // because of the PhotoLinkDiv wrapper. However, clicking on a non-child
  // area of the PhotoInfo does not go anywhere.
  cursor: default;

  display: grid;
  grid-template-columns: 1fr minmax(2em, auto);
  grid-template-rows: auto;
  justify-items: stretch;
  column-gap: 0.5em;

  @media (hover: hover) {
    *:hover > & {
      background-color: var(--caption-hover-bg-color);
      color: var(--caption-hover-color);
    }
    *:hover > & ${PhotoInfoFlex} * {
      color: var(--caption-hover-color);
    }
    *:hover > & ${PhotoNavFlex} * {
      color: var(--caption-hover-color);
    }
  }
`;

const PhotoTitle = styled(LinkWithEvent)`
  padding: 0.25em;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  color: var(--text-color);
`;

const PhotoInfoLink = styled(LinkWithEvent)`
  color: var(--caption-color);
  text-decoration: underline;
  padding: 0.25em 0em;
  text-align: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  flex-shrink: 1;

  &:hover {
    color: var(--caption-hover-color);
  }

  /* icon spacing to not get underline on space in between */
  & > svg,
  & > :first-child {
    margin-right: 0.5ex;
  }
`;

const PhotoWrapperBase = styled(PhotoLinkDiv)`
  display: flex;
  flex-direction: column;
`;

const PhotoWrapper = styled(PhotoWrapperBase)`
  position: absolute;
  left: 50%;
  top: 0;
  transform: translateX(-50%);

  border-bottom-left-radius: 1em;
  border-radius: 1em;
  overflow: hidden;

  @keyframes hrhp-toLeft {
    100% {
      left: calc(-1 * var(--gallery-swipe-gap));
      transform: translateX(-100%);
    }
  }

  @keyframes hrhp-toCenter {
    100% {
      left: 50%;
      transform: translateX(-50%);
    }
  }

  @keyframes hrhp-toRight {
    100% {
      left: calc(100% + var(--gallery-swipe-gap));
      transform: translateX(0);
    }
  }
`;

const HiddenPhotoWrapper = styled(PhotoWrapper)`
  position: absolute;
  display: none;
`;

interface PhotoState {
  photo: PartialFullPhoto | null;
  prevPhoto: PartialFullPhoto | null;
  nextPhoto: PartialFullPhoto | null;
}

enum TouchMode {
  START,
  MOVE,
  END,
}

interface TouchState {
  mode: TouchMode;
  startX: number;
  curX: number;
  mainAnimation?: string;
  prevAnimation?: string;
  nextAnimation?: string;
  heightAnimation?: CSSProperties;
}

const TOUCH_THRESHOLD = 10;

const EVENT_CONTEXT = EventContext.HOME_HLPHOTO_INLINE_LIGHTBOX;

interface Props {
  lightboxPhotoId?: string;
}

// The photo list and photo state is intended to persist across component instantiation.
// This enables reuse of the HomeRandomHighlightPhoto state for the entire session. So navigation via back/forward
// back to this component will show the same state.
interface SessionState {
  photoList: RandomHighlightPhotoList;
  photoState: PhotoState;
  setPhotoState: React.Dispatch<React.SetStateAction<PhotoState>>;
}

const SessionStateContext = React.createContext<SessionState | null>(null);

export const HomeRandomHighlightPhotoSessionStateProvider: React.FC<{
  children?: React.ReactNode;
}> = ({ children }) => {
  const queryClient = useQueryClient();

  const [photoState, setPhotoState] = useState<PhotoState>(() => {
    return {
      photo: null,
      prevPhoto: null,
      nextPhoto: null,
    };
  });

  // Only create the list once.
  const [photoList] = useState(() => {
    return new RandomHighlightPhotoList(queryClient);
  });

  return (
    <SessionStateContext.Provider
      value={{ photoList, photoState, setPhotoState }}
    >
      {children}
    </SessionStateContext.Provider>
  );
};

function useSessionState(): SessionState {
  const state = useContext(SessionStateContext);
  if (!state) {
    throw new Error("Need a HomeRandomHighlightPhotoSessionStateProvider");
  }
  return state;
}

export const HomeRandomHighlightPhoto: React.FC<Props> = ({
  lightboxPhotoId,
}) => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const windowSize = useWindowSize();
  const procIndicatorActions = useProcessingIndicatorActions();
  const eventTracker = useEventTracker();
  const { photoList, photoState, setPhotoState } = useSessionState();
  const { photo, prevPhoto, nextPhoto } = photoState;

  const showLightbox = lightboxPhotoId !== undefined;

  // Updates the session's photo state. This means updating the primary photo and also its
  // adjacent photos.
  const setPhoto = useCallback(
    (photo: PhotoListPhoto) => {
      // Retrieves the full photo for the given photo.
      // First will try synchronously. If that fails, will start async fetch for full photo
      // and update photo state when it is ready.
      const getFullPhoto = (
        key: keyof PhotoState,
        photo: PhotoListPhoto | null | undefined
      ): PartialFullPhoto | null => {
        if (!photo) return null;

        const queryOptions = getPhotoMetadataQueryOptions(photo);

        // Try sync first
        const fullPhoto = queryClient.getQueryData(queryOptions.queryKey);
        if (fullPhoto) {
          return fullPhoto;
        }

        // Fall back to async.
        queryClient
          .fetchQuery(queryOptions)
          .then((fullPhoto) => {
            setPhotoState((s) => {
              const cur = s[key];
              // Race condition check: only update full photo if the photo id is still the same.
              if (cur === null || cur.id !== photo.id) return s;
              return { ...s, [key]: fullPhoto };
            });
          })
          .catch((error) => {
            console.error(`photo metadata query failed: ${error}`);
          });

        // Use whatever photo info is available for now.
        return photo;
      };

      // Try to get adjacent photos synchronously.
      const adj = photoList.adjacentSync(photo.id, true);
      const x = {
        photo: getFullPhoto("photo", photo),
        prevPhoto: getFullPhoto("prevPhoto", adj?.prev),
        nextPhoto: getFullPhoto("nextPhoto", adj?.next),
      };
      setPhotoState(x);

      // Fetch adjacent.
      photoList.adjacent(photo.id, true).then((adj) => {
        const prevPhoto = getFullPhoto("prevPhoto", adj?.prev);
        const nextPhoto = getFullPhoto("nextPhoto", adj?.next);

        // Begin pre-fetching the next-next photo so that we can prevent any flicker
        // as we come up to the end of the current batch.
        if (adj?.next) {
          photoList.adjacent(adj!.next.id, true);
        }

        setPhotoState((s) => {
          // Don't change if the main photo has changed.
          if (s.photo?.id !== photo.id) return s;

          return {
            photo: s.photo,
            prevPhoto,
            nextPhoto,
          };
        });
      });
    },
    [photoList, queryClient, setPhotoState]
  );

  // Fetch of initial photo if photo state is not initialized.
  const initializing = useRef(photo === null);

  const { showBoundary } = useErrorBoundary();

  useEffect(() => {
    // Only initialize once.
    if (!initializing.current) return;
    initializing.current = false;

    if (lightboxPhotoId !== undefined) {
      // This is the case if user is navigating directly to a lightbox url,
      // so we want the first "random" photo fetched to be that photo.
      photoList.insertId(lightboxPhotoId);
    }

    procIndicatorActions.start();
    photoList
      .batchAfter(undefined, 1)
      .then(
        (photos) => {
          if (photos.length > 0) {
            setPhoto(photos[0]);
          }
        },
        (error) => {
          showBoundary(error);
        }
      )
      .finally(() => {
        procIndicatorActions.stop();
      });
  }, [
    initializing,
    lightboxPhotoId,
    photoList,
    procIndicatorActions,
    setPhoto,
    showBoundary,
  ]);

  // Synchronize lightbox photo id and show lightbox state.
  // const shouldShowLightbox = !!(lightboxPhotoId && photo);
  // if (shouldShowLightbox !== showLightbox) {
  //   setShowLightbox(shouldShowLightbox);
  // }

  // Sizing of photo
  const photoRef = useRef<HTMLDivElement | null>(null);
  const [maxPhoto, setMaxPhoto] = useState({
    width: 0,
    height: 0,
  });

  // Compute photo max dimensions
  useLayoutEffect(() => {
    const maxWidth = Math.max(
      300,
      windowSize.width - windowSize.scrollBarWidth - 4 * PHOTO_PADDING
    );

    // The min height is 300px, the max height is the viewport height -
    // top of the wrapper - offset for some of the explore section.
    //
    // Also account for scrollY, which may be non-zero as we're navigating from
    // another page with a non-zero scrollY.
    const maxHeight = Math.max(
      300,
      windowSize.height -
        photoRef.current!.getBoundingClientRect().top -
        window.scrollY -
        175
    );
    setMaxPhoto({ width: maxWidth, height: maxHeight });
  }, [windowSize.height, windowSize.scrollBarWidth, windowSize.width]);

  // Photo view event.
  useEffect(() => {
    if (!photo || showLightbox) return;
    eventTracker.addEvent({
      event: AppEvent.PHOTO_VIEW_HOME,
      object: makeEventPhotoID(photo),
      context: EVENT_CONTEXT,
    });
  }, [eventTracker, photo, showLightbox]);

  // For lightbox
  const onLightboxClose = useCallback(() => {
    navigate("/");
  }, [navigate]);

  const onPhotoNav = useCallback(
    (photo: PhotoListPhoto) => {
      setPhoto(photo);
      navigate(makeLightboxPhotoUrl("/@/", photo), { replace: true });
    },
    [navigate, setPhoto]
  );

  // In-page mini-lightbox
  const onNext = useCallback(() => {
    setPhoto(nextPhoto!);
    eventTracker.addUXEvent({
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.LIGHTBOX_NAV_NEXT,
      context: EVENT_CONTEXT,
    });
  }, [eventTracker, nextPhoto, setPhoto]);

  const onPrev = useCallback(() => {
    setPhoto(prevPhoto!);
    eventTracker.addUXEvent({
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.LIGHTBOX_NAV_PREV,
      context: EVENT_CONTEXT,
    });
  }, [eventTracker, prevPhoto, setPhoto]);

  // Used to get height of next/prev photo containers
  const prevPhotoContainer = useRef<HTMLDivElement | null>(null);
  const nextPhotoContainer = useRef<HTMLDivElement | null>(null);

  // Touch events
  const [touchState, setTouchState] = useState<TouchState | undefined>(
    undefined
  );

  const onTouchStart = useCallback(
    (e: React.TouchEvent) => {
      // If already in touch state, don't interrupt (e.g. in END).
      if (touchState) return;

      const { clientX: x } = e.touches[0];
      setTouchState({ mode: TouchMode.START, startX: x, curX: x });
    },
    [touchState]
  );
  const onTouchMove = useCallback(
    (e: React.TouchEvent) => {
      // Don't interrupt END.
      if (!touchState || touchState.mode === TouchMode.END) return;

      const { clientX: x } = e.touches[0];
      setTouchState((s) => {
        return s
          ? { ...s, mode: TouchMode.MOVE, curX: x }
          : { mode: TouchMode.MOVE, startX: x, curX: x }; // this shouldn't happen, but fallback
      });
    },
    [touchState]
  );
  const onTouchEnd = useCallback(() => {
    if (!touchState) return;

    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;
    if (!dragToNext && !dragToPrev) {
      // Nothing to do.
      setTouchState(undefined);
      return;
    }

    // Something is being dragged. Set animation for appropriate photos.
    const duration = swipeAnimationDuration(delta, windowSize.width);

    if (dragToNext) {
      setTouchState({
        ...touchState,
        mode: TouchMode.END,
        mainAnimation: `hrhp-toLeft ${duration}s forwards`,
        nextAnimation: `hrhp-toCenter ${duration}s forwards`,
        heightAnimation: {
          height: `${nextPhotoContainer.current!.clientHeight}px`,
          transition: `height ${duration}s`,
        },
      });
    } else if (dragToPrev) {
      setTouchState({
        ...touchState,
        mode: TouchMode.END,
        mainAnimation: `hrhp-toRight ${duration}s forwards`,
        prevAnimation: `hrhp-toCenter ${duration}s forwards`,
        heightAnimation: {
          height: `${prevPhotoContainer.current!.clientHeight}px`,
          transition: `height ${duration}s`,
        },
      });
    }
  }, [nextPhoto, prevPhoto, touchState, windowSize.width]);
  const onAnimationEnd = useCallback(() => {
    if (!touchState) return;

    // Animation ended. Trigger actual navigation to prev/next photo.
    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;

    if (dragToNext) {
      onNext();
      eventTracker.addUXEvent({
        event: AppEvent.LIGHTBOX_SWIPE_NEXT,
        context: EVENT_CONTEXT,
      });
    } else if (dragToPrev) {
      onPrev();
      eventTracker.addUXEvent({
        event: AppEvent.LIGHTBOX_SWIPE_PREV,
        context: EVENT_CONTEXT,
      });
    }

    setTouchState(undefined);
  }, [eventTracker, nextPhoto, onNext, onPrev, prevPhoto, touchState]);
  const onTouchCancel = useCallback(() => setTouchState(undefined), []);

  let mainPhotoCss: CSSProperties | undefined;
  let prevPhotoCss: CSSProperties | undefined;
  let nextPhotoCss: CSSProperties | undefined;

  if (touchState) {
    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;
    if (dragToNext || dragToPrev) {
      mainPhotoCss = {
        left: `calc(50% + ${delta}px)`,
        animation: touchState.mainAnimation,
      };
    }

    if (dragToNext) {
      nextPhotoCss = {
        display: "flex",
        left: `calc(${windowSize.width + delta}px + var(--gallery-swipe-gap))`,
        transform: "translateX(0)",
        animation: touchState.nextAnimation,
      };
    }

    if (dragToPrev) {
      prevPhotoCss = {
        display: "flex",
        left: `calc(${delta}px - var(--gallery-swipe-gap))`,
        transform: "translateX(-100%)",
        animation: touchState.prevAnimation,
      };
    }
  }

  // Navigation via keyboard.
  const onKeyDown = useCallback(
    (e: KeyboardEvent) => {
      // If lightbox is showing, it also handles arrow navigation, so don't do it again.
      if (showLightbox) return;

      if (e.key === "ArrowLeft" && prevPhoto) {
        onPrev();
      } else if (e.key === "ArrowRight" && nextPhoto) {
        onNext();
      }
    },
    [nextPhoto, onNext, onPrev, prevPhoto, showLightbox]
  );

  useEventListener(window, "keydown", onKeyDown);

  const mainPhotoContainerRef = useRef<HTMLDivElement>(null);

  // Container height. This is sized based on the height of the main photo.
  const [containerHeight, setContainerHeight] = useState(0);

  // Callbacks for photo resize events.
  // The container height is based on the main photo, so whenever the main photo is resized
  // (which also includes when a new photo is first loaded), trigger a render to get the
  // new container height.
  const [photoWidth, setPhotoWidth] = useState(0);
  const [checkContainerHeight, triggerCheckContainerHeight] = useReducer(
    (i) => i + 1,
    0
  );
  const onPhotoResize = useCallback((width: number) => {
    setPhotoWidth(width);
    triggerCheckContainerHeight();
  }, []);

  // Update container height. Do it when the checkContainerHeight state is updated or
  // whenever the photo changes since that means there may be new metadata and so the
  // info section of the photo may change size.
  useLayoutEffect(() => {
    if (mainPhotoContainerRef.current) {
      setContainerHeight(mainPhotoContainerRef.current.clientHeight);
    }
  }, [checkContainerHeight, photo]);

  const [nextPhotoWidth, setNextPhotoWidth] = useState(0);
  const onNextPhotoResize = useCallback(
    (width: number) => setNextPhotoWidth(width),
    []
  );

  const [prevPhotoWidth, setPrevPhotoWidth] = useState(0);
  const onPrevPhotoResize = useCallback(
    (width: number) => setPrevPhotoWidth(width),
    []
  );

  return (
    <>
      <PhotoContainer
        ref={photoRef}
        style={{
          height: `${containerHeight}px`,
          ...touchState?.heightAnimation,
        }}
      >
        {photo && (
          // Each ConstrainedPhoto below is keyed to the photo id so that ConstrainedPhoto
          // internal state is not reused across different photos (i.e. special logic to prevent
          // downsizing).
          <PhotoWrapper
            onTouchStart={onTouchStart}
            onTouchMove={onTouchMove}
            onTouchEnd={onTouchEnd}
            onTouchCancel={onTouchCancel}
            onAnimationEnd={onAnimationEnd}
            style={mainPhotoCss}
            key={photo.id}
            borderRadius={1}
          >
            <div ref={mainPhotoContainerRef}>
              {/* Use a link so it appears semantically as a link. */}
              <PhotoLink
                to={makeLightboxPhotoUrl("/@/", photo)}
                // The hover border is around the enclosing PhotoWrapper, so no border
                // around just the photo.
                hoverBorderWidth="0"
                event={{
                  event: AppEvent.PHOTO_CLICK,
                  object: makeEventPhotoID(photo!),
                  context: EVENT_CONTEXT,
                }}
              >
                <ConstrainedPhoto
                  photo={photo}
                  constrainWidth
                  constrainHeight
                  maxWidth={maxPhoto.width}
                  maxHeight={maxPhoto.height}
                  resizedCallback={onPhotoResize}
                  // This image is typically the Largest Content Paint, so fetch with highest priority.
                  // Unless the lightbox photo id is set, in which case that is shown on top of this.
                  fetchPriority={lightboxPhotoId ? undefined : "high"}
                />
              </PhotoLink>
              <PhotoInfo
                width={photoWidth}
                photo={photo}
                hasPrev={prevPhoto !== null}
                hasNext={nextPhoto !== null}
                onPrev={onPrev}
                onNext={onNext}
              />
            </div>
          </PhotoWrapper>
        )}
        {/* Next photo */}
        {nextPhoto && (
          <HiddenPhotoWrapper style={nextPhotoCss} key={nextPhoto.id}>
            <div ref={nextPhotoContainer}>
              <ConstrainedPhoto
                photo={nextPhoto}
                constrainWidth
                constrainHeight
                maxWidth={maxPhoto.width}
                maxHeight={maxPhoto.height}
                resizedCallback={onNextPhotoResize}
              />
              <PhotoInfo width={nextPhotoWidth} photo={nextPhoto} fakeNav />
            </div>
          </HiddenPhotoWrapper>
        )}
        {/* Prev photo */}
        {prevPhoto && (
          <HiddenPhotoWrapper style={prevPhotoCss} key={prevPhoto.id}>
            <div ref={prevPhotoContainer}>
              <ConstrainedPhoto
                photo={prevPhoto}
                constrainWidth
                constrainHeight
                maxWidth={maxPhoto.width}
                maxHeight={maxPhoto.height}
                resizedCallback={onPrevPhotoResize}
              />
              <PhotoInfo width={prevPhotoWidth} photo={prevPhoto} fakeNav />
            </div>
          </HiddenPhotoWrapper>
        )}
      </PhotoContainer>
      {showLightbox && (
        <>
          <CanonicalPhotoUrl photoId={photo!} />
          <PhotoLightbox
            photoId={photo!}
            photoListQuery={photoList}
            queryForward
            closeUrl="/"
            onClose={onLightboxClose}
            onPhotoNav={onPhotoNav}
            makePhotoLink={(photo) => makeLightboxPhotoUrl("/@/", photo)}
            eventContext={EventContext.HOME_HLPHOTO_LIGHTBOX}
          />
        </>
      )}
    </>
  );
};

interface PhotoInfoProps {
  width: number;
  photo: PartialFullPhoto;
  hasPrev?: boolean;
  hasNext?: boolean;
  fakeNav?: boolean;
  onPrev?: () => void;
  onNext?: () => void;
}

const PhotoInfo: React.FC<PhotoInfoProps> = ({
  width,
  photo,
  hasPrev,
  hasNext,
  fakeNav,
  onPrev,
  onNext,
}) => {
  // Folder
  let folder: PhotoGroup | undefined;
  if (photo.folder) {
    const split = splitPhotoGroup(
      photo.folder,
      GROUP_SEP[GroupType.COLLECTION]
    );
    folder = [
      `/${GroupType.COLLECTION}${photo.folder[PHOTO_GROUP_ID]}`,
      split.display[split.display.length - 1],
    ];
  }

  // Park & Hike: get location if present, otherwise check categories (legacy behaviour)
  let park: PhotoGroup | undefined;
  let hike: PhotoGroup | undefined;

  // Location
  let location: PhotoGroup | undefined;
  if (photo.locations && photo.locations.length > 0) {
    // Use first (primary) location.
    const split = splitPhotoGroup(
      photo.locations[0],
      GROUP_SEP[GroupType.LOCATION]
    );

    // Look for the deepest park and hike. Remove that from the location so that it doesn't get duplicated.
    for (let i = split.display.length - 1; i >= 0; --i) {
      const { display, type } = stripAndGetLocationType(split.display[i]);
      if (type === LocationType.OTHER) continue;
      if (!park && type === LocationType.PARK) {
        park = [
          "/location/" + split.slug.slice(0, i + 1).join("/") + "/",
          titleCase(display),
        ];
        split.display.splice(i, 1); // don't need to update slug as nothing will reference past this point
      }
      if (!hike && type === LocationType.HIKE) {
        hike = [
          "/location/" + split.slug.slice(0, i + 1).join("/") + "/",
          titleCase(display),
        ];
        split.display.splice(i, 1); // don't need to update slug as nothing will reference past this point
      }
    }

    if (split.display.length > 0) {
      // Use the deepest location.
      location = [
        `/location${photo.locations[0][PHOTO_GROUP_ID]}`,
        split.display[split.display.length - 1],
      ];
    }
  }

  if (photo.categories) {
    for (const cat of photo.categories) {
      const [id] = cat;
      if (park === undefined && id.startsWith("/park/")) {
        const split = splitPhotoGroup(cat, GROUP_SEP[GroupType.CATEGORY]);
        park = [
          `/category${id}`,
          titleCase(split.display[split.display.length - 1]),
        ];
      } else if (hike === undefined && id.startsWith("/hike/")) {
        const split = splitPhotoGroup(cat, GROUP_SEP[GroupType.CATEGORY]);
        hike = [
          `/category${id}`,
          titleCase(split.display[split.display.length - 1]),
        ];
      }
    }
  }

  const useLinkClickEvent = (
    group: PhotoGroup | undefined
  ): EventDesc | undefined => {
    return useMemo(() => {
      if (!group) return undefined;
      return {
        event: AppEvent.GROUP_CLICK,
        object: group[PHOTO_GROUP_ID],
        context: EVENT_CONTEXT,
      };
    }, [group]);
  };

  const folderLinkEvent = useLinkClickEvent(folder);
  const hikeLinkEvent = useLinkClickEvent(hike);
  const parkLinkEvent = useLinkClickEvent(park);
  const locationLinkEvent = useLinkClickEvent(location);

  return (
    <PhotoInfoDiv
      style={{ width: `calc(${width}px - ${OVERLAY_PADDING * 4}em)` }}
    >
      <PhotoInfoFlex>
        {photo.title && (
          <PhotoTitle
            to={makeLightboxPhotoUrl("/@/", photo)}
            event={{
              event: AppEvent.PHOTO_CLICK,
              object: makeEventPhotoID(photo!),
              context: EVENT_CONTEXT,
            }}
          >
            {photo.title}
          </PhotoTitle>
        )}
        {folder && (
          <PhotoInfoLink to={folder[PHOTO_GROUP_ID]} event={folderLinkEvent!}>
            <IconCollection />
            {folder[PHOTO_GROUP_DISPLAY]}
          </PhotoInfoLink>
        )}
        {hike && (
          <PhotoInfoLink to={hike[PHOTO_GROUP_ID]} event={hikeLinkEvent!}>
            <IconHike />
            {hike[PHOTO_GROUP_DISPLAY]}
          </PhotoInfoLink>
        )}
        {park && (
          <PhotoInfoLink to={park[PHOTO_GROUP_ID]} event={parkLinkEvent!}>
            <IconPark />
            {park[PHOTO_GROUP_DISPLAY]}
          </PhotoInfoLink>
        )}
        {!park && location && (
          <PhotoInfoLink
            to={location[PHOTO_GROUP_ID]}
            event={locationLinkEvent!}
          >
            <IconLocation />
            {location[PHOTO_GROUP_DISPLAY]}
          </PhotoInfoLink>
        )}
      </PhotoInfoFlex>
      <PhotoNavFlex>
        {(hasPrev || fakeNav) && (
          <CircleIconButton
            title="Wait... go back"
            onClick={onPrev}
            sizeEm={1.5}
            fontSizeEm={1}
          >
            <IconArrowLeft />
          </CircleIconButton>
        )}
        {(hasNext || fakeNav) && (
          <CircleIconButton
            title="I feel lucky!"
            onClick={onNext}
            sizeEm={1.5}
            fontSizeEm={1}
          >
            <IconArrowRight />
          </CircleIconButton>
        )}
      </PhotoNavFlex>
    </PhotoInfoDiv>
  );
};
