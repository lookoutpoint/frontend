// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import { Helmet } from "react-helmet-async";
import { Navigate } from "react-router-dom";
import styled from "styled-components";
import { makeTitleString } from "../util";
import { useUserSession } from "../hooks/useUserSession";

const Form = styled.form`
  max-width: 400px;
  margin: 0 auto;
`;
const FormRow = styled.div`
  margin: 0.25em 0;
  & label {
    display: inline-block;
    width: 49%;
  }
  & input {
    display: inline-block;
    width: 49%;
  }
`;
const SubmitRow = styled.div`
  margin: 0.25em 0;
  text-align: right;
`;
const ErrorMessage = styled.div`
  margin: 0.25 0;
  padding: 0.25em;
  border: 1px solid #ff0000;
  background-color: #ffe0e0;
  color: #ff0000;
`;

export const UserAdd: React.FC = () => {
  const userSession = useUserSession();

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [message, setMessage] = useState<string | null>(null);
  const [inProgress, setInProgress] = useState(false);

  const submit = useCallback(
    (event: React.FormEvent) => {
      // Prevent page from refreshing.
      event.preventDefault();

      if (name.length === 0) {
        setMessage("Must specify a name.");
        return;
      } else if (password.length === 0) {
        setMessage("Must specify a password.");
        return;
      } else if (password2 !== password) {
        setMessage("Password fields do not match.");
        return;
      }

      // Validation complete.
      setMessage(null);
      setInProgress(true);

      // Send request to add user.
      userSession.addUser(name, password).then(
        () => {
          setInProgress(false);
        },
        () => {
          setInProgress(false);
          setMessage("Failed to add user.");
        }
      );
    },
    [name, password, password2, userSession]
  );

  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Add User")}</title>
      </Helmet>

      {userSession.isLoggedIn && <Navigate to="/" />}

      <Form onSubmit={submit}>
        {message && <ErrorMessage>{message}</ErrorMessage>}
        <FormRow>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormRow>
        <FormRow>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormRow>
        <FormRow>
          <label htmlFor="password2">Repeat password:</label>
          <input
            type="password"
            id="password2"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
          ></input>
        </FormRow>
        <SubmitRow>
          <input type="submit" value="Add" disabled={inProgress} />
        </SubmitRow>
      </Form>
    </div>
  );
};
