// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import { Helmet } from "react-helmet-async";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { Dialog, RenderProps } from "../components/Dialog";
import { makeTitleString } from "../util";
import { DividedRows, Row } from "../components/Form";
import styled from "styled-components";
import { axiosGet, axiosPost } from "../helpers/http";

export const AdminGroups: React.FC = () => {
  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Groups")}</title>
      </Helmet>
      <BreadcrumbNav
        nav={[{ label: "Admin", link: "/admin" }]}
        activeLabel={"Groups"}
      />
      <Dialog
        render={(rprops) => {
          return (
            <DividedRows>
              <Row>
                <GroupAdmin
                  rprops={rprops}
                  label="Categories"
                  urlPrefix="/photos/category"
                />
              </Row>
              <Row>
                <GroupAdmin
                  rprops={rprops}
                  label="Collections"
                  urlPrefix="/photos/folder"
                />
              </Row>
              <Row>
                <GroupAdmin
                  rprops={rprops}
                  label="Dates"
                  urlPrefix="/photos/date"
                />
              </Row>
              <Row>
                <GroupAdmin
                  rprops={rprops}
                  label="Keywords"
                  urlPrefix="/photos/keyword"
                />
              </Row>
              <Row>
                <GroupAdmin
                  rprops={rprops}
                  label="Locations"
                  urlPrefix="/photos/location"
                />
              </Row>
            </DividedRows>
          );
        }}
      />
    </div>
  );
};

interface GroupProps {
  rprops: RenderProps;
  label: string;
  urlPrefix: string;
}

const Label = styled.div`
  font-weight: bold;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr auto;
  row-gap: 1em;
  column-gap: 1em;
`;

const EmptyIdsTextArea = styled.textarea`
  grid-row: 2;
  grid-column: 1;
  min-height: 10em;
`;

const DeleteRes = styled.pre`
  grid-row: 2;
  grid-column: 2;
`;

const GroupAdmin: React.FC<GroupProps> = ({ rprops, label, urlPrefix }) => {
  const { processing, setProcessing, setError } = rprops;

  const [emptyIdsStr, setEmptyIdsStr] = useState<string>("");
  const [deleteResStr, setDeleteResStr] = useState<string>("");

  const listEmpty = useCallback(() => {
    setProcessing(true);
    setDeleteResStr(""); // reset
    axiosGet<string[]>(`${urlPrefix}/empty/list`).then(({ success, data }) => {
      setProcessing(false);
      if (!success) {
        setError("Error: Could not list empty groups.");
        setEmptyIdsStr("");
      } else {
        setError(null);
        setEmptyIdsStr(JSON.stringify(data, undefined, 2));
      }
    });
  }, [setEmptyIdsStr, setError, setProcessing, urlPrefix]);

  const deleteEmpty = useCallback(() => {
    setProcessing(true);
    axiosPost<unknown>(`${urlPrefix}/empty/delete`, emptyIdsStr).then(
      ({ success, data }) => {
        setProcessing(false);
        if (!success || data === null) {
          setError("Error: Could not delete empty groups.");
          setDeleteResStr("");
        } else {
          setError(null);
          setDeleteResStr(JSON.stringify(data, undefined, 2));
        }
      }
    );
  }, [emptyIdsStr, setError, setProcessing, urlPrefix]);

  const onEmptyIdsChange = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      setEmptyIdsStr(e.target.value);
    },
    []
  );

  return (
    <>
      <Label>{label}</Label>
      <Grid>
        <button disabled={processing} onClick={listEmpty}>
          List empty
        </button>
        {emptyIdsStr && !deleteResStr && (
          <button disabled={processing} onClick={deleteEmpty}>
            Delete empty
          </button>
        )}
        {emptyIdsStr && (
          <EmptyIdsTextArea value={emptyIdsStr} onChange={onEmptyIdsChange} />
        )}
        {deleteResStr && <DeleteRes>{deleteResStr}</DeleteRes>}
      </Grid>
    </>
  );
};
