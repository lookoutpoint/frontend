// SPDX-License-Identifier: MIT
import React, { useState } from "react";
import { Helmet } from "react-helmet-async";
import { useParams } from "react-router-dom";
import { AppEvent, EventButton, EventContext } from "../api/events";
import { ListComparisonOp } from "../api/list";
import { listPhotos, PhotoListKey } from "../api/photos";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import {
  ControlBar,
  ControlBarButton,
  ControlBarGroup,
  ControlBarItem,
} from "../components/ControlBar";
import { useEventTracker } from "../hooks/useEventTracker";
import { IconCaption } from "../components/Icons";
import { LocalStorageKey, useLocalStorageBool } from "../helpers/localStorage";
import { PhotoListQuery } from "../helpers/PhotoListQuery";
import { makeTitleString } from "../util";
import { PhotoGalleryViewWrapper } from "./components/PhotoGalleryViewWrapper";
import { useQueryClient } from "@tanstack/react-query";

interface RouteParams {
  photoId?: string;
  [key: string]: string | undefined;
}

export const Recent: React.FC = () => {
  const { photoId: lightboxPhotoId } = useParams<RouteParams>();

  const queryClient = useQueryClient();
  useEventTracker(EventContext.RECENT);

  // Photo list query. Fixed query.
  const [photoListQuery] = useState(() => {
    return new PhotoListQuery("tsSortKey", (count, refId, refOp) =>
      listPhotos(queryClient, {
        limit: count,
        refKey: PhotoListKey.TIMESTAMP,
        refKeyValue: refId,
        refOp: refOp,
        filter: {
          ratings: [{ value: 4, cmpOp: ListComparisonOp.GTE }],
        },
      })
    );
  });

  // Gallery show info state.
  const [galleryShowCaption, setGalleryShowCaption] = useLocalStorageBool(
    LocalStorageKey.VIEW_RECENT_GALLERY_SHOW_CAPTION
  );

  return (
    <>
      <Helmet>
        <title>{makeTitleString("Recent Photos")}</title>
      </Helmet>
      <BreadcrumbNav nav={[]} activeLabel="Recent Photos" />
      <ControlBar sticky>
        <ControlBarGroup>
          <ControlBarItem>
            <ControlBarButton
              toggle
              toggleActive={galleryShowCaption}
              title="Caption"
              fontSizeEm={1.15}
              onClick={() => setGalleryShowCaption(!galleryShowCaption)}
              keyShortcut="c"
              eventDesc={{
                event: AppEvent.BUTTON_CLICK,
                object: EventButton.GROUP_INFO,
                context: EventContext.RECENT,
              }}
            >
              <IconCaption />
            </ControlBarButton>
          </ControlBarItem>
        </ControlBarGroup>
      </ControlBar>
      <PhotoGalleryViewWrapper
        photoListQuery={photoListQuery}
        photoListQueryForward={false}
        showGalleryOverlay={galleryShowCaption}
        rootUrl="/recent/@"
        closeUrl="/recent/"
        lightboxPhotoId={lightboxPhotoId}
        eventContext={EventContext.RECENT}
      />
    </>
  );
};
