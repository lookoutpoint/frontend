// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useState } from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { Dialog, RenderProps } from "../components/Dialog";
import { DividedRows, Row } from "../components/Form";
import { axiosPost } from "../helpers/http";

export const AdminDatabase: React.FC = () => {
  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Database")}</title>
      </Helmet>
      <BreadcrumbNav
        nav={[{ label: "Admin", link: "/admin" }]}
        activeLabel={"Database"}
      />
      <Dialog
        render={(rprops) => {
          return <Actions rprops={rprops} />;
        }}
      />
    </div>
  );
};

interface SchemaVerData {
  current: number;
  latest: number;
}

const Actions: React.FC<{ rprops: RenderProps }> = ({ rprops }) => {
  const { processing, setProcessing, setError } = rprops;

  const [versions, setVersions] = useState<SchemaVerData | null>(null);

  const refreshVersions = useCallback(() => {
    axiosPost<SchemaVerData>("/photos/db/schema/version", null).then(
      ({ data }) => {
        if (data) {
          setVersions(data);
          setError(null);
        } else {
          setError("Error: Could not get schema versions.");
        }
      }
    );
  }, [setError]);

  useEffect(refreshVersions, [refreshVersions]);

  const updateVersions = useCallback(() => {
    setProcessing(true);
    axiosPost<void>("/photos/db/schema/update", null).then(({ success }) => {
      setProcessing(false);
      if (!success) {
        setError("Error: Could not update schema.");
      } else {
        setError(null);
      }
      refreshVersions();
    });
  }, [refreshVersions, setError, setProcessing]);

  const updateRelatedCategories = useCallback(() => {
    setProcessing(true);
    axiosPost<void>("/photos/category/update-related", {}).then(
      ({ success }) => {
        setProcessing(false);
        if (!success) {
          setError("Error: Could not update related categories.");
        } else {
          setError(null);
        }
      }
    );
  }, [setError, setProcessing]);

  const generateSitemap = useCallback(() => {
    setProcessing(true);
    axiosPost<void>("/photos/sitemap/generate", undefined).then(
      ({ success }) => {
        setProcessing(false);
        if (!success) {
          setError("Error: Could not generate sitemap.");
        } else {
          setError(null);
        }
      }
    );
  }, [setError, setProcessing]);

  const [visTokenCountData, setVisTokenCountData] =
    useState<unknown>(undefined);
  const checkAndFixVisTokenCounts = useCallback(
    (checkOnly: boolean) => {
      setProcessing(true);
      axiosPost<unknown>("/photos/db/check-fix-vis-token-counts", {
        checkOnly,
      }).then(({ success, data }) => {
        setProcessing(false);
        if (!success) {
          setError("Error: Could not check/fix vis token counts.");
        } else {
          setError(null);
        }
        setVisTokenCountData(data);
      });
    },
    [setError, setProcessing]
  );

  return (
    <DividedRows>
      <div>
        <p>
          Schema version:{" "}
          {versions && versions.current < versions.latest && (
            <button disabled={processing} onClick={updateVersions}>
              Update
            </button>
          )}
        </p>
        {versions && (
          <ul>
            <li>Current: {versions.current}</li>
            <li>Latest: {versions.latest}</li>
          </ul>
        )}
      </div>
      <Row>
        Update related categories:{" "}
        <button disabled={processing} onClick={updateRelatedCategories}>
          Update
        </button>
      </Row>
      <Row>
        Generate sitemap:{" "}
        <button disabled={processing} onClick={generateSitemap}>
          Generate
        </button>
      </Row>
      <Row>
        Check vis token counts:{" "}
        <button
          disabled={processing}
          onClick={() => checkAndFixVisTokenCounts(true)}
        >
          Check
        </button>{" "}
        <button
          disabled={processing || visTokenCountData === undefined}
          onClick={() => checkAndFixVisTokenCounts(false)}
        >
          Fix
        </button>
        {visTokenCountData !== undefined && (
          <pre>{JSON.stringify(visTokenCountData, undefined, 2)}</pre>
        )}
      </Row>
    </DividedRows>
  );
};
