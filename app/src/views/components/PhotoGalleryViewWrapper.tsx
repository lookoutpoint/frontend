// SPDX-License-Identifier: MIT
import { useCallback, useMemo } from "react";
import { FullPhoto, PhotoListPhoto } from "../../api/photos";
import { useNavigate } from "react-router-dom";
import { PhotoListQuery } from "../../helpers/PhotoListQuery";
import { PhotoGallery } from "../../components/PhotoGallery";
import { makeLightboxPhotoUrl } from "../../helpers/photos";
import { CanonicalPhotoUrl } from "../../components/CanonicalPhotoUrl";
import { usePhotoStoreGenCache } from "../../hooks/usePhotoStoreGenCache";

interface Props<K extends keyof PhotoListPhoto> {
  photoListQuery: PhotoListQuery<K>;
  photoListQueryForward: boolean;
  showGalleryOverlay?: boolean;
  hidden?: boolean;
  stdHeight?: number;
  rowLimit?: number;
  rootUrl: string;
  closeUrl?: string; // defaults to rootUrl
  lightboxPhotoId?: string;
  eventContext?: string;
}

export function PhotoGalleryViewWrapper<K extends keyof PhotoListPhoto>(
  props: Props<K>
) {
  const {
    photoListQuery,
    photoListQueryForward,
    showGalleryOverlay,
    hidden,
    stdHeight,
    rowLimit,
    rootUrl,
    closeUrl: rawCloseUrl,
    lightboxPhotoId,
    eventContext,
  } = props;

  const navigate = useNavigate();
  const closeUrl = rawCloseUrl ?? rootUrl;

  const makePhotoUrl = useCallback(
    (photo: Pick<FullPhoto, "id" | "title">): string => {
      return makeLightboxPhotoUrl(rootUrl, photo);
    },
    [rootUrl]
  );

  const { setCachedPhoto, makePhotoIdGen } = usePhotoStoreGenCache();

  const onPhotoClick = useCallback(
    (photo: PhotoListPhoto) => {
      // Manually push URL to history.
      navigate(makePhotoUrl(photo));

      // Set cached photo so that store gen can be used to pass to lightbox.
      setCachedPhoto(photo);

      // Don't do default behaviour (particularly, don't navigate to photo link via typical browser means).
      return false;
    },
    [navigate, makePhotoUrl, setCachedPhoto]
  );

  const onPhotoNav = useCallback(
    (photo: PhotoListPhoto) => {
      // Replace the current URL. Don't want to pollute history with each navigation.
      // Assumes that that the URL has already been updated to the lightbox URL (i.e. via onPhotoClick).
      navigate(makePhotoUrl(photo), { replace: true });

      // Set cached photo so that store gen can be used to pass to lightbox.
      setCachedPhoto(photo);
    },
    [navigate, makePhotoUrl, setCachedPhoto]
  );

  const onLightboxClose = useCallback(() => {
    navigate(closeUrl);
  }, [navigate, closeUrl]);

  return (
    <>
      <CanonicalPhotoUrl photoId={makePhotoIdGen(lightboxPhotoId)} />
      <PhotoGallery
        photoListQuery={photoListQuery}
        photoListQueryForward={photoListQueryForward}
        showGalleryOverlay={showGalleryOverlay}
        hidden={hidden}
        stdHeight={stdHeight}
        rowLimit={rowLimit}
        makePhotoUrl={makePhotoUrl}
        onGalleryClick={onPhotoClick}
        lightbox={useMemo(
          () => ({
            photoId: makePhotoIdGen(lightboxPhotoId),
            closeUrl: closeUrl,
            onNav: onPhotoNav,
            onClose: onLightboxClose,
            modifyTitle: true,
          }),
          [
            closeUrl,
            lightboxPhotoId,
            makePhotoIdGen,
            onLightboxClose,
            onPhotoNav,
          ]
        )}
        eventContext={eventContext}
      />
    </>
  );
}
