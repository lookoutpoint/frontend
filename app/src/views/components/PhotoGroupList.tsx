// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useState,
} from "react";
import {
  getPhotoMetadataQueryOptions,
  listPhotos,
  PhotoListFilterExpr,
  PhotoListKey,
} from "../../api/photos";
import styled from "styled-components";
import { PhotoListQuery } from "../../helpers/PhotoListQuery";
import {
  ControlBar,
  ControlBarButton,
  ControlBarGroup,
  ControlBarItem,
  ControlBarLabel,
} from "../../components/ControlBar";
import {
  Group,
  GroupListParams,
  GroupType,
  GROUP_PLURAL,
  TimelineGroup,
  getGroupName as getGenericGroupName,
  getTimelineGroupId,
  GroupPhotoProperties,
  useGroupInfoQuery,
  getGroupInfoQueryKey,
  makeTimelineGroupIdFromFolder,
} from "../../api/photoGroups";
import { PhotoGalleryViewWrapper } from "./PhotoGalleryViewWrapper";
import { GroupTimeline } from "./GroupTimeline";
import { Helmet } from "react-helmet-async";
import {
  localStorageGetBool,
  LocalStorageKey,
  useLocalStorageBool,
} from "../../helpers/localStorage";
import { GroupListQuery } from "../../helpers/GroupListQuery";
import { NotFound } from "../NotFound";
import { useNavigate, useLocation } from "react-router-dom";
import { useUserSession } from "../../hooks/useUserSession";
import { UserSession } from "../../helpers/Contexts";
import {
  GroupInfiniteScroll,
  GroupInfiniteScrollEvents,
} from "../../components/GroupInfiniteScroll";
import { titleCase } from "../../helpers/titleCase";
import { makeTitleString } from "../../util";
import { usePhotoListSizes } from "../../hooks/usePhotoListSizes";
import { ListComparisonOp } from "../../api/list";
import { SettingsExtensionProps } from "./PhotoGroupSettings";
import { GroupTextEdit } from "./GroupTextEdit";
import { RichText } from "../../components/RichText";
import { AppEvent, EventButton, makeEventGroupID } from "../../api/events";
import { LinkWithEvent } from "../../components/LinkWithEvent";
import {
  IconAdmin,
  IconAngleLeft,
  IconAngleRight,
  IconCamera,
  IconCaption,
  IconClock,
  IconEdit,
  IconEllipsisH,
  IconEllipsisV,
  IconHighlight,
  IconInfo,
  IconMap,
  IconPhoto,
  IconRestart,
  IconTimeline,
  IconTimeOrderAsc,
  IconTimeOrderDesc,
  IconUpload,
} from "../../components/Icons";
import {
  stripViewFromUrl,
  VIEW_NOT_FOUND,
  VIEW_TIMELINE,
  parseViewFromUrl,
  VIEW_CHILDREN,
  VIEW_PHOTOS,
  ViewSelection,
  VIEW_EMPTY,
  VIEW_INFO,
  VIEW_MAP,
} from "../../helpers/views";
import { lazily } from "react-lazily";
import { ToggleDiv } from "../../components/ToggleDiv";
import { QueryClient, useQuery, useQueryClient } from "@tanstack/react-query";

const { MapView } = lazily(() => import("./MapView"));

const { PhotoGroupSettings } = lazily(() => import("./PhotoGroupSettings"));

export interface CustomView {
  label: string;
  urlId: string;
  btnContent: JSX.Element;
  render: (hidden: boolean) => JSX.Element;
  eventObject?: EventButton;
}

interface Props<G extends Group> {
  groupId: string;
  groupType: GroupType;
  childrenIcon: React.ReactNode;

  lightboxPhotoId?: string;
  timelineGroupPath?: string;

  getGroupName: (group: G) => string;
  getFancyGroupName?: (Group: G) => JSX.Element;
  buildPhotoListFilter: (groupId: string) => PhotoListFilterExpr;
  getChildren: (
    session: UserSession,
    params: GroupListParams
  ) => Promise<G[] | null>;

  getChildRowGroup?: (group: G) => string | undefined;
  getChildId?: (group: G) => string;
  renderChildRowGroupLabel?: (rowGroup: string) => JSX.Element | undefined;
  getChildrenLabel?: (group: G) => string;
  onGroupInfo?: (group: G, timelineGroup?: TimelineGroup) => void;
  makeGroupUrl?: (group: G) => string;
  onView?: (view?: string) => void;

  cbChildrenCustom?: JSX.Element;
  cbAdminCustom?: JSX.Element;
  SettingsExtension?: React.FC<SettingsExtensionProps<G>>;

  customViews?: CustomView[];

  // What to render if group is not found. If not specified, will render <NotFound />.
  notFound?: React.ReactNode;
}

function groupAllowPhotos(type: GroupType, id: string) {
  // If this is a root group, only show photos for collections.
  // It is technically possible for other types, but it doesn't make much sense
  // and some types (e.g. keywords and dates) don't support this filter.
  return id !== "/" || type === GroupType.COLLECTION;
}

class ListQueries<G extends Group> {
  readonly photosByCamera?: PhotoListQuery<"sortKey" | "tsSortKey">;
  readonly highlightPhotosByCamera?: PhotoListQuery<"sortKey" | "tsSortKey">;
  readonly photosByUpload?: PhotoListQuery<"tsSortKey" | "sortKey">;
  readonly highlightPhotosByUpload?: PhotoListQuery<"tsSortKey" | "sortKey">;
  readonly children: GroupListQuery<G>;

  constructor(
    queryClient: QueryClient,
    session: UserSession,
    groupType: GroupType,
    groupId: string,
    photoFilter: PhotoListFilterExpr,
    getChildren: Props<G>["getChildren"]
  ) {
    const noPhotos = !groupAllowPhotos(groupType, groupId);

    if (!noPhotos) {
      const highlightPhotoFilter = { ...photoFilter };
      highlightPhotoFilter.ratings = highlightPhotoFilter.ratings
        ? [...highlightPhotoFilter.ratings]
        : [];
      highlightPhotoFilter.ratings.push({
        value: 4,
        cmpOp: ListComparisonOp.GTE,
      });

      this.photosByUpload = new PhotoListQuery<"sortKey" | "tsSortKey">(
        "tsSortKey",
        async (limit, refKeyValue, refOp) => {
          return await listPhotos(queryClient, {
            limit,
            refKey: PhotoListKey.TIMESTAMP,
            refKeyValue,
            refOp,
            filter: photoFilter,
          });
        }
      );

      this.highlightPhotosByUpload = new PhotoListQuery<
        "sortKey" | "tsSortKey"
      >("tsSortKey", async (limit, refKeyValue, refOp) => {
        return await listPhotos(queryClient, {
          limit,
          refKey: PhotoListKey.TIMESTAMP,
          refKeyValue,
          refOp,
          filter: highlightPhotoFilter,
        });
      });

      this.photosByCamera = new PhotoListQuery<"sortKey" | "tsSortKey">(
        "sortKey",
        async (limit, refKeyValue, refOp) => {
          return await listPhotos(queryClient, {
            limit,
            refKey: PhotoListKey.SORT_KEY,
            refKeyValue,
            refOp,
            filter: photoFilter,
          });
        }
      );

      this.highlightPhotosByCamera = new PhotoListQuery<
        "sortKey" | "tsSortKey"
      >("sortKey", async (limit, refKeyValue, refOp) => {
        return await listPhotos(queryClient, {
          limit,
          refKey: PhotoListKey.SORT_KEY,
          refKeyValue,
          refOp,
          filter: highlightPhotoFilter,
        });
      });
    }

    this.children = new GroupListQuery<G>(async (limit, refId, refOp) => {
      return await getChildren(session, {
        id: groupId,
        limit,
        refId,
        refOp,
        coverPhoto: true,
        // For groups, list the full tree. Except for dates, where it wouldn't make sense.
        maxRelDepth: groupType === GroupType.DATE ? 0 : -1,
      });
    });
  }
}

const ControlBarTimelineNav = styled(LinkWithEvent)<{ $align: string }>`
  display: flex;
  align-items: center;
  color: var(--link-color);
  text-decoration: none;
  font-size: 0.9em;
  text-align: ${(p) => p.$align};

  &:hover {
    color: var(--link-color);
  }
  &:hover span {
    text-decoration: underline;
  }

  > * + * {
    margin-left: 0.25em;
  }
`;

export function PhotoGroupList<G extends Group>(
  props: React.PropsWithChildren<Props<G>>
): JSX.Element {
  const {
    groupId,
    groupType,
    childrenIcon,
    lightboxPhotoId,
    timelineGroupPath,
    getGroupName,
    getFancyGroupName,
    buildPhotoListFilter,
    getChildren,
    getChildRowGroup,
    getChildId,
    renderChildRowGroupLabel,
    getChildrenLabel,
    onGroupInfo,
    makeGroupUrl,
    onView,
    cbChildrenCustom,
    cbAdminCustom,
    SettingsExtension,
    customViews,
    notFound,
  } = props;

  const navigate = useNavigate();
  const location = useLocation();
  const queryClient = useQueryClient();
  const session = useUserSession();

  const url = stripViewFromUrl(location.pathname);

  const supportsTimeline = groupType === GroupType.COLLECTION;
  const supportsInfo = groupType !== GroupType.DATE;

  // Current view state
  const [currentView, setRawCurrentView] = useState<ViewSelection | null>(null);

  const setCurrentView = useCallback(
    (view: ViewSelection, urlAction?: "replace" | "push") => {
      setRawCurrentView(view);
      if (!urlAction || view === VIEW_NOT_FOUND || view === VIEW_EMPTY) return;

      // Update url if necessary.
      const newUrl = `${url}@${view}/`;
      navigate(newUrl, { replace: urlAction === "replace" });

      if (onView) {
        onView(view);
      }
    },
    [navigate, onView, url]
  );

  // Invoke onView callback if view changes or if callback changes.
  useEffect(() => {
    if (onView) {
      onView(currentView || undefined);
    }
  }, [currentView, onView]);

  // Group info state.
  const allowPhotos = groupAllowPhotos(groupType, groupId);
  const groupQuery = useGroupInfoQuery<G>(groupType, groupId);
  const group = groupQuery.data;

  // Default to has photos if a lightbox photo id is specified
  const hasPhotos =
    !!lightboxPhotoId ||
    (group &&
      allowPhotos &&
      (group.hasPhotos & GroupPhotoProperties.HAS_PHOTOS) !== 0);
  const hasHighlightPhotos =
    group &&
    allowPhotos &&
    (group.hasPhotos & GroupPhotoProperties.HAS_HIGHLIGHT_PHOTOS) !== 0;
  const hasMappablePhotos =
    group &&
    allowPhotos &&
    (group.hasPhotos & GroupPhotoProperties.HAS_MAPPABLE_PHOTOS) !== 0;
  const hasChildren = group && group.hasChildren;

  // Root timeline group state.
  const rootTimelineGroupQuery = useGroupInfoQuery<TimelineGroup>(
    GroupType.TIMELINE_GROUP,
    makeTimelineGroupIdFromFolder(groupId),
    false,
    {
      enabled: supportsTimeline,
    }
  );
  const rootTimelineGroup = rootTimelineGroupQuery.data;

  // Timeline group state.
  const timelineGroupQuery = useGroupInfoQuery<TimelineGroup>(
    GroupType.TIMELINE_GROUP,
    makeTimelineGroupIdFromFolder(groupId, timelineGroupPath),
    false,
    {
      enabled: timelineGroupPath !== undefined,
    }
  );
  const timelineGroup = timelineGroupQuery.data;

  // If no timeline group is found for the given path, this is a "not found" error
  // unless we are in admin mode. In admin mode, we allow a new timeline group to be created
  // if there isn't one.
  if (
    timelineGroupPath !== undefined &&
    !timelineGroupQuery.isFetching &&
    !timelineGroup &&
    !session.isAdmin &&
    currentView !== VIEW_NOT_FOUND
  ) {
    setRawCurrentView(VIEW_NOT_FOUND);
  }

  // Determine current event context. This is primarily based on the group type and id unless the current view is a timeline.
  const eventContext = useMemo(() => {
    if (currentView === VIEW_TIMELINE) {
      // Timeline context.
      const group = timelineGroup ?? rootTimelineGroup;
      if (group) {
        return makeEventGroupID(GroupType.TIMELINE_GROUP, group);
      }
    }
    return makeEventGroupID(groupType, groupId);
  }, [currentView, groupId, groupType, rootTimelineGroup, timelineGroup]);

  // Collection of list queries.
  const listQueries = useMemo(() => {
    const photoFilter = buildPhotoListFilter(groupId);
    return new ListQueries(
      queryClient,
      session,
      groupType,
      groupId,
      photoFilter,
      getChildren
    );
  }, [
    buildPhotoListFilter,
    getChildren,
    groupId,
    groupType,
    queryClient,
    session,
  ]);

  // Effect to set the current view.
  // Triggered by two things:
  // 1. When group is set (by group info initialization)
  // 2. When the view in the url changes.
  const viewFromUrl = parseViewFromUrl(location.pathname);
  useLayoutEffect(() => {
    // If group info is fetching, don't do anything.
    if (
      groupQuery.isPending ||
      (supportsTimeline && rootTimelineGroupQuery.isPending)
    )
      return;

    if (!group) {
      setCurrentView(VIEW_NOT_FOUND);
      return;
    }

    if (viewFromUrl) {
      // Don't update url.
      setCurrentView(viewFromUrl);
    } else {
      // Set default view based on what is defined on this group.
      let view: ViewSelection = VIEW_EMPTY;
      if (rootTimelineGroup) {
        view = VIEW_TIMELINE;
      } else if (hasChildren) {
        view = VIEW_CHILDREN;
      } else if (hasPhotos) {
        view = VIEW_PHOTOS;
      } else if (group.richText) {
        view = VIEW_INFO;
      }

      // Replace the current url as there is no view currently defined.
      setCurrentView(view, "replace");
    }
  }, [
    group,
    setCurrentView,
    hasChildren,
    rootTimelineGroup,
    viewFromUrl,
    hasPhotos,
    groupQuery.isPending,
    rootTimelineGroupQuery.isPending,
    supportsTimeline,
  ]);

  // Gallery control bar complex state.
  const [
    showComplexControls,
    setShowComplexControls,
    resetShowComplexControls,
  ] = useLocalStorageBool(LocalStorageKey.VIEW_PHOTO_GALLERY_COMPLEX_CONTROLS);

  // Gallery show caption state.
  const [galleryShowCaption, setGalleryShowCaption, resetGalleryShowCaption] =
    useLocalStorageBool(LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_CAPTION);

  // Gallery show highlights state.
  // If the group has no highlight photos, of course don't ever show highlights
  // (but keep the local storage state unchanged).
  const [
    rawGalleryShowHighlights,
    setGalleryShowHighlights,
    resetGalleryShowHighlights,
  ] = useLocalStorageBool(
    LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
      groupType.toLowerCase()
  );

  // Corner case: if the photo is not a highlight, make sure we are not just showing highlights.
  // This is needed to handle the case where a non-highlight photo is directly linked. In order
  // to get navigation to work for this case, the query needs to use the non-highlight list.
  const lightboxPhotoQuery = useQuery({
    ...getPhotoMetadataQueryOptions({ id: lightboxPhotoId ?? "" }),
    enabled: lightboxPhotoId !== undefined,
  });

  useEffect(() => {
    const photo = lightboxPhotoQuery.data;
    if (!photo) return;
    if (photo.rating === undefined || photo.rating < 4) {
      setGalleryShowHighlights(false);
    }
  }, [lightboxPhotoQuery.data, setGalleryShowHighlights]);

  // Combined flag to show only highlight photos in gallery.
  const galleryShowHighlights = rawGalleryShowHighlights && hasHighlightPhotos;

  // Gallery sorts in forward order.
  const [
    galleryQueryForward,
    setGalleryQueryForward,
    resetGalleryQueryForward,
  ] = useLocalStorageBool(
    LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX +
      groupType.toLowerCase()
  );

  // Gallery sort time selector.
  const [
    galleryOrderByCamera,
    setGalleryOrderByCamera,
    resetGalleryOrderByCamera,
  ] = useLocalStorageBool(
    LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX +
      groupType.toLowerCase()
  );

  const resetControlBar = useCallback(() => {
    resetShowComplexControls();
    resetGalleryShowCaption();
    resetGalleryShowHighlights();
    resetGalleryQueryForward();
    resetGalleryOrderByCamera();
  }, [
    resetGalleryOrderByCamera,
    resetGalleryQueryForward,
    resetGalleryShowCaption,
    resetGalleryShowHighlights,
    resetShowComplexControls,
  ]);

  // Refresh group info. Primarily to update visibility tokens (and possibly cover photo).
  const refreshGroupInfo = useCallback(() => {
    queryClient.invalidateQueries({
      queryKey: getGroupInfoQueryKey(groupType, groupId),
    });
  }, [groupId, groupType, queryClient]);

  // Refresh timeline group info.
  const refreshTimelineGroupInfo = useCallback(() => {
    queryClient.invalidateQueries({
      queryKey: getGroupInfoQueryKey(
        GroupType.TIMELINE_GROUP,
        makeTimelineGroupIdFromFolder(groupId)
      ),
    });
    if (timelineGroupPath !== undefined) {
      queryClient.invalidateQueries({
        queryKey: getGroupInfoQueryKey(
          GroupType.TIMELINE_GROUP,
          makeTimelineGroupIdFromFolder(groupId, timelineGroupPath)
        ),
      });
    }
  }, [groupId, queryClient, timelineGroupPath]);

  useEffect(() => {
    // If there is group info and a callback is requested, invoke the callback.
    if (group && onGroupInfo) {
      onGroupInfo(group, timelineGroup ?? undefined);
    }
  }, [group, onGroupInfo, timelineGroup]);

  // Timeline group nav.
  const [timelineGroupNav, setTimelineGroupNav] = useState({
    prev: null as TimelineGroup | null,
    next: null as TimelineGroup | null,
  });

  const onTimelineGroupNavInfo = useCallback(
    (prev: TimelineGroup | null, next: TimelineGroup | null) => {
      // Don't update group nav if the timeline group is still being fetched.
      // This is needed to prevent unnecessary flashing of the nav urls when switching between
      // timeline groups, as the timeline group is nullified (to prevent other flashing) while
      // the new timeline group info is being fetched. During that time, without this, we will
      // show the nav for the root timeline group, which isn't the right thing to show.
      if (!timelineGroupQuery.isFetching) {
        setTimelineGroupNav({ prev, next });
      }
    },
    [timelineGroupQuery.isFetching]
  );

  // Photo list sizes
  const photoListSizes = usePhotoListSizes();

  // Control bar items.
  const cbViewGroup: JSX.Element[] = [];
  const cbPhotoGroup: JSX.Element[][] = [];
  const cbAdminGroup: JSX.Element[] = [];
  let cbLeft: JSX.Element | undefined;
  let cbRight: JSX.Element | undefined;

  // Rich text flags.
  const [editInfo, setEditInfo] = useState(false);

  // Info. Root groups do not have info view.
  let infoElem = null;
  if (
    supportsInfo &&
    group &&
    group.id !== "/" &&
    (group.richText || session.isAdmin)
  ) {
    cbViewGroup.push(
      <ControlBarItem key="info">
        <ControlBarButton
          toggle
          toggleActive={currentView === VIEW_INFO}
          title="Info"
          fontSizeEm={1.15}
          link={`${url}@${VIEW_INFO}/`}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_INFO,
            context: eventContext,
          }}
        >
          <IconInfo />
        </ControlBarButton>
      </ControlBarItem>
    );

    if (currentView === VIEW_INFO && session.isAdmin) {
      cbAdminGroup.push(
        <ControlBarItem key="edit-info">
          <ControlBarButton
            title="Edit"
            fontSizeEm={1.15}
            onClick={() => setEditInfo(!editInfo)}
            toggle
            toggleActive={editInfo}
          >
            <IconEdit />
          </ControlBarButton>
        </ControlBarItem>
      );
    }

    infoElem = (
      <ToggleDiv show={currentView === VIEW_INFO}>
        {editInfo && (
          <GroupTextEdit
            groupType={groupType}
            group={group}
            refresh={refreshGroupInfo}
            close={() => setEditInfo(false)}
          />
        )}
        <RichText text={group.richText ?? ""} currentUrl={url} />
      </ToggleDiv>
    );
  }

  // Timeline
  let timelineElem = null;
  if (group && supportsTimeline && (rootTimelineGroup || session.isAdmin)) {
    // Element to show timeline. May be hidden depending on current view.
    const rootTimelineUrlPrefix = `/${groupType}${groupId}@timeline`;

    // If showing root timeline group, always show it.
    // If showing a child (timelineGroupPath exists), then only show it once it has been loaded.
    timelineElem = (
      <ToggleDiv show={currentView === VIEW_TIMELINE}>
        <GroupTimeline
          groupType={groupType}
          groupId={groupId}
          rootTimelineGroup={rootTimelineGroup}
          displayTimelineGroup={timelineGroup}
          rootTimelineUrlPrefix={rootTimelineUrlPrefix}
          timelineGroupPath={timelineGroupPath}
          refresh={refreshTimelineGroupInfo}
          onGroupNavInfo={onTimelineGroupNavInfo}
          hidden={currentView !== VIEW_TIMELINE}
          lightboxPhotoId={
            currentView === VIEW_TIMELINE ? lightboxPhotoId : undefined
          }
        />
      </ToggleDiv>
    );

    // Root group does not support timeline.
    if (group.id !== "/") {
      cbViewGroup.push(
        <ControlBarItem key="timeline">
          <ControlBarButton
            toggle
            toggleActive={currentView === VIEW_TIMELINE}
            title="Timeline"
            fontSizeEm={1.15}
            link={`/${groupType}${group.id}@${VIEW_TIMELINE}/`}
            eventDesc={{
              event: AppEvent.BUTTON_CLICK,
              object: EventButton.GROUP_TIMELINE,
              context: eventContext,
            }}
          >
            <IconTimeline />
          </ControlBarButton>
        </ControlBarItem>
      );
    }

    // Timeline nav
    if (currentView === VIEW_TIMELINE) {
      if (timelineGroupNav.prev) {
        cbLeft = (
          <ControlBarTimelineNav
            key="prev"
            to={
              rootTimelineUrlPrefix + getTimelineGroupId(timelineGroupNav.prev)
            }
            $align="left"
            event={{
              event: AppEvent.GROUP_CLICK,
              object: makeEventGroupID(
                GroupType.TIMELINE_GROUP,
                timelineGroupNav.prev
              ),
              context: eventContext,
            }}
          >
            <IconAngleLeft />
            <span>
              {getGenericGroupName(
                GroupType.TIMELINE_GROUP,
                timelineGroupNav.prev
              )}
            </span>
          </ControlBarTimelineNav>
        );
      }

      if (timelineGroupNav.next) {
        cbRight = (
          <ControlBarTimelineNav
            key="next"
            to={
              rootTimelineUrlPrefix + getTimelineGroupId(timelineGroupNav.next)
            }
            $align="right"
            event={{
              event: AppEvent.GROUP_CLICK,
              object: makeEventGroupID(
                GroupType.TIMELINE_GROUP,
                timelineGroupNav.next
              ),
              context: eventContext,
            }}
          >
            <span>
              {getGenericGroupName(
                GroupType.TIMELINE_GROUP,
                timelineGroupNav.next
              )}
            </span>
            <IconAngleRight />
          </ControlBarTimelineNav>
        );
      }
    }
  }

  // Children
  let childrenElem = null;

  const groupListEvents = useMemo<GroupInfiniteScrollEvents>(
    () => ({
      context: eventContext,
      groupType,
    }),
    [eventContext, groupType]
  );

  if (group && hasChildren) {
    // Element to show children list. May be hidden depending on current view.
    childrenElem = (
      <ToggleDiv show={currentView === VIEW_CHILDREN}>
        {listQueries && (
          <GroupInfiniteScroll
            groupListQuery={listQueries.children}
            getGroupName={getGroupName}
            getFancyGroupName={getFancyGroupName}
            getRowGroup={getChildRowGroup}
            renderStartOfRowGroup={renderChildRowGroupLabel}
            makeGroupUrl={
              makeGroupUrl ??
              ((g) => {
                return `/${groupType}${getChildId ? getChildId(g) : g.id}`;
              })
            }
            photoRatio={photoListSizes.groupPhotoRatio}
            desiredPhotoWidth={photoListSizes.groupDesiredPhotoWidth}
            hidden={currentView !== VIEW_CHILDREN}
            events={groupListEvents}
          />
        )}
      </ToggleDiv>
    );

    // Control bar element to toggle children view.
    const childrenLabel =
      getChildrenLabel && group
        ? getChildrenLabel(group)
        : titleCase(GROUP_PLURAL[groupType]);

    cbViewGroup.push(
      <ControlBarItem key="children">
        <ControlBarButton
          toggle
          toggleActive={currentView === VIEW_CHILDREN}
          title={childrenLabel}
          fontSizeEm={1.15}
          link={`${url}@${VIEW_CHILDREN}/`}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_CHILDREN,
            context: eventContext,
          }}
        >
          {childrenIcon}
        </ControlBarButton>
      </ControlBarItem>
    );
  }

  // Map view.
  let mapElem = null;

  const allowMapView = localStorageGetBool(LocalStorageKey.FEATURE_MAP_VIEW);

  if (group && hasMappablePhotos && allowMapView) {
    mapElem = (
      <ToggleDiv show={currentView === VIEW_MAP}>
        <MapView
          group={group}
          hidden={currentView !== VIEW_MAP}
          buildPhotoListFilter={buildPhotoListFilter}
        />
      </ToggleDiv>
    );

    cbViewGroup.push(
      <ControlBarItem key="map">
        <ControlBarButton
          toggle
          toggleActive={currentView === VIEW_MAP}
          title="Map"
          fontSizeEm={1.15}
          link={`${url}@${VIEW_MAP}/`}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_MAP,
            context: eventContext,
          }}
        >
          <IconMap />
        </ControlBarButton>
      </ControlBarItem>
    );
  }

  // Custom views.
  const customViewElems: JSX.Element[] = [];

  if (group && customViews) {
    for (const customView of customViews) {
      customViewElems.push(
        <ToggleDiv
          key={`view-${customView.urlId}`}
          show={currentView === customView.urlId}
        >
          {customView.render(currentView !== customView.urlId)}
        </ToggleDiv>
      );

      cbViewGroup.push(
        <ControlBarItem key={customView.urlId}>
          <ControlBarButton
            toggle
            toggleActive={currentView === customView.urlId}
            title={customView.label}
            fontSizeEm={1.15}
            link={`${url}@${customView.urlId}/`}
            eventDesc={
              customView.eventObject
                ? {
                    event: AppEvent.BUTTON_CLICK,
                    object: customView.eventObject,
                    context: eventContext,
                  }
                : undefined
            }
          >
            {customView.btnContent}
          </ControlBarButton>
        </ControlBarItem>
      );
    }
  }

  // Photo gallery.
  let photoGalleryElem = null;
  if (group && hasPhotos) {
    const listQuery = galleryOrderByCamera
      ? galleryShowHighlights
        ? listQueries?.highlightPhotosByCamera
        : listQueries?.photosByCamera
      : galleryShowHighlights
      ? listQueries?.highlightPhotosByUpload
      : listQueries?.photosByUpload;

    // Element to show photo gallery. May be hidden depending on current view.
    photoGalleryElem = (
      <ToggleDiv show={currentView === VIEW_PHOTOS}>
        {listQuery && (
          <PhotoGalleryViewWrapper
            photoListQuery={listQuery}
            photoListQueryForward={galleryQueryForward}
            showGalleryOverlay={galleryShowCaption}
            hidden={currentView !== VIEW_PHOTOS}
            stdHeight={photoListSizes.photoStdHeight}
            rootUrl={`${url}@${VIEW_PHOTOS}/`}
            closeUrl={`${url}@${VIEW_PHOTOS}/`}
            lightboxPhotoId={
              currentView === VIEW_PHOTOS ? lightboxPhotoId : undefined
            }
            eventContext={eventContext}
          />
        )}
      </ToggleDiv>
    );

    // Control bar element to toggle photo view.
    cbViewGroup.push(
      <ControlBarItem key="photos">
        <ControlBarButton
          toggle
          toggleActive={currentView === VIEW_PHOTOS}
          title="Photos"
          fontSizeEm={1.15}
          link={`${url}@${VIEW_PHOTOS}/`}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_PHOTOS,
            context: eventContext,
          }}
        >
          <IconPhoto />
        </ControlBarButton>
      </ControlBarItem>
    );

    // Control bar element to control if gallery should show only highlights.
    cbPhotoGroup.push([]);
    if (hasHighlightPhotos) {
      cbPhotoGroup[0].push(
        <ControlBarItem key="photo-highlights">
          <ControlBarButton
            toggle
            toggleActive={galleryShowHighlights}
            title="Highlights"
            fontSizeEm={1.15}
            onClick={() => setGalleryShowHighlights(!galleryShowHighlights)}
            keyShortcut="h"
            eventDesc={{
              event: AppEvent.BUTTON_CLICK,
              object: EventButton.GROUP_HIGHLIGHTS,
              context: eventContext,
            }}
          >
            <IconHighlight />
          </ControlBarButton>
        </ControlBarItem>
      );
    }

    // Control bar element to control if gallery should show photo caption.
    cbPhotoGroup[0].push(
      <ControlBarItem key="photo-caption">
        <ControlBarButton
          toggle
          toggleActive={galleryShowCaption}
          title="Caption"
          fontSizeEm={1.15}
          onClick={() => setGalleryShowCaption(!galleryShowCaption)}
          keyShortcut="c"
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_INFO,
            context: eventContext,
          }}
        >
          <IconCaption />
        </ControlBarButton>
      </ControlBarItem>
    );

    // Photo order
    cbPhotoGroup.push([
      <ControlBarItem key="photo-order">
        <ControlBarButton
          toggle /* "fake" toggle to get better hover effect */
          title="Order"
          onClick={() => setGalleryQueryForward(!galleryQueryForward)}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_ORDER,
            context: eventContext,
          }}
        >
          {galleryQueryForward ? <IconTimeOrderAsc /> : <IconTimeOrderDesc />}
        </ControlBarButton>
      </ControlBarItem>,
      <ControlBarItem key="photo-order-label" style={{ paddingRight: 0 }}>
        <ControlBarLabel title="Time">
          <IconClock />
        </ControlBarLabel>
      </ControlBarItem>,
      <div key="photo-order-equal">
        <ControlBarLabel>=</ControlBarLabel>
      </div>,

      <ControlBarItem key="photo-order-camera">
        <ControlBarButton
          toggle
          toggleActive={galleryOrderByCamera}
          title="Camera"
          fontSizeEm={1.15}
          onClick={() => setGalleryOrderByCamera(true)}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_ORDER_BY_CAMERA,
            context: eventContext,
          }}
        >
          <IconCamera />
        </ControlBarButton>
      </ControlBarItem>,
      <ControlBarItem key="photo-order-upload">
        <ControlBarButton
          toggle
          toggleActive={!galleryOrderByCamera}
          title="Upload"
          fontSizeEm={1.15}
          onClick={() => setGalleryOrderByCamera(false)}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_ORDER_BY_UPLOAD,
            context: eventContext,
          }}
        >
          <IconUpload />
        </ControlBarButton>
      </ControlBarItem>,
      <ControlBarItem key="photo-reset">
        <ControlBarButton
          toggle
          title="Reset"
          fontSizeEm={1.15}
          onClick={resetControlBar}
          eventDesc={{
            event: AppEvent.BUTTON_CLICK,
            object: EventButton.GROUP_RESET,
            context: eventContext,
          }}
        >
          <IconRestart />
        </ControlBarButton>
      </ControlBarItem>,
    ]);
  }

  // Admin state and actions
  const [showSettings, setShowSettings] = useState(false);

  if (session.isAdmin && group) {
    cbAdminGroup.push(
      <ControlBarItem key="admin-settings">
        <ControlBarButton
          title="Settings"
          fontSizeEm={1.15}
          onClick={() => setShowSettings(true)}
        >
          <IconAdmin />
        </ControlBarButton>
      </ControlBarItem>
    );
    if (cbAdminCustom) {
      cbAdminGroup.push(
        <React.Fragment key="admin-custom">{cbAdminCustom}</React.Fragment>
      );
    }
  }

  const btnComplexControls = (
    <ControlBarItem>
      <ControlBarButton
        toggle
        toggleActive={showComplexControls}
        title={showComplexControls ? "Less" : "More"}
        fontSizeEm={1.15}
        onClick={() => setShowComplexControls(!showComplexControls)}
        eventDesc={{
          event: AppEvent.BUTTON_CLICK,
          object: EventButton.GROUP_MORE,
          context: eventContext,
        }}
      >
        {showComplexControls ? <IconEllipsisH /> : <IconEllipsisV />}
      </ControlBarButton>
    </ControlBarItem>
  );

  if (currentView === VIEW_NOT_FOUND) {
    if (notFound) {
      return <>{notFound}</>;
    }
    return (
      <NotFound
        what={timelineGroupPath ? GroupType.TIMELINE_GROUP : groupType}
      />
    );
  }

  // Page title
  let title: string | undefined;

  if (group) {
    if (timelineGroup) {
      title = getGenericGroupName(GroupType.TIMELINE_GROUP, timelineGroup);
    } else if (currentView === VIEW_TIMELINE && rootTimelineGroup) {
      title = getGenericGroupName(GroupType.TIMELINE_GROUP, rootTimelineGroup);
    } else if (group.id === "/") {
      title = GROUP_PLURAL[groupType];
    } else {
      title = getGroupName(group);
    }
  }

  return (
    <div>
      {/* Metadata */}
      {title && (
        <Helmet>
          <title>{makeTitleString(titleCase(title))}</title>
          {group && group.metaDescription && (
            <meta name="description" content={group.metaDescription} />
          )}
        </Helmet>
      )}

      {/* Control bar */}
      <ControlBar sticky left={cbLeft} right={cbRight}>
        <ControlBarGroup>{cbViewGroup}</ControlBarGroup>
        {currentView === VIEW_CHILDREN && cbChildrenCustom && (
          <ControlBarGroup>{cbChildrenCustom}</ControlBarGroup>
        )}
        {currentView === VIEW_PHOTOS && !showComplexControls && (
          // Show first photo group but the rest are hidden behind as "complex controls"
          <>
            {cbPhotoGroup.length > 0 && (
              <ControlBarGroup>{cbPhotoGroup[0]}</ControlBarGroup>
            )}
            <ControlBarGroup>{btnComplexControls}</ControlBarGroup>
          </>
        )}
        {currentView === VIEW_PHOTOS &&
          showComplexControls &&
          cbPhotoGroup.map((items, i) => (
            <ControlBarGroup key={`photo${i}`}>
              {i === 1 ? btnComplexControls : null}
              {items}
            </ControlBarGroup>
          ))}
        <ControlBarGroup>{cbAdminGroup}</ControlBarGroup>
      </ControlBar>

      {/* Views */}
      {infoElem}
      {timelineElem}
      {childrenElem}
      {mapElem}
      {customViewElems}
      {photoGalleryElem}

      {/* Settings */}
      {showSettings && (
        <PhotoGroupSettings
          groupType={groupType}
          groupInfo={group!}
          onClose={() => setShowSettings(false)}
          refresh={refreshGroupInfo}
          Extension={SettingsExtension}
        />
      )}
    </div>
  );
}
