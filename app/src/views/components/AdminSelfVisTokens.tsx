// SPDX-License-Identifier: MIT

import React, { useCallback, useMemo, useState } from "react";
import { RenderProps } from "../../components/Dialog";
import { TokenMap } from "./PhotoGroupSettings";
import {
  DividedRows,
  FormButton,
  FormGrid,
  IndentedFlexRow,
  Row,
} from "../../components/Form";
import { CircleIconButton } from "../../components/CircleIconButton";
import { IconAdd, IconRemove } from "../../components/Icons";

interface Props<TokenParams> {
  // Self vis tokens currently on the group.
  selfVisTokens: string[];

  // Dialog render props.
  rprops: RenderProps;

  // Map of all available tokens to their display names.
  tokenMap: TokenMap;

  // Callback to refresh group info (e.g. when tokens have been changed by this dialog).
  refresh: () => void;

  // Token params (additional parameters for add/delete token).

  // Initial token params value.
  initTokenParams: TokenParams;

  // Validate if the given parameters are valid.
  validateParams?: (params: TokenParams) => boolean;

  // Optional render function to show token params in the form dialog.
  // Rendering happens within a FormGrid.
  //
  // `idPrefix`: string to uniquely identify the token (a combination of "add/delete" + token id)
  renderTokenParams?: (
    idPrefix: string,
    rprops: RenderProps,
    params: TokenParams,
    onParamsChange: React.Dispatch<React.SetStateAction<TokenParams>>
  ) => React.ReactNode;

  // Callbacks to delete / add self vis token.
  // Params are guaranteed to be valid (via validateParams).
  deleteToken: (token: string, params: TokenParams) => Promise<boolean>;
  addToken: (token: string, params: TokenParams) => Promise<boolean>;
}

// Component that renders an interface to admin self-vis tokens for a group.
// Intended to be used in a dialog form.
export function AdminSelfVisTokens<TokenParams>(props: Props<TokenParams>) {
  const {
    selfVisTokens,
    rprops,
    tokenMap,
    refresh,
    initTokenParams,
    validateParams,
    renderTokenParams,
    deleteToken,
    addToken,
  } = props;

  return (
    <>
      <Row>
        <b>Self visibility tokens:</b>
      </Row>
      <DividedRows>
        {/* One row for each self visibility token and option to delete it */}
        {selfVisTokens.map((token) => (
          <Token
            key={token}
            rprops={rprops}
            token={token}
            tokenDisplay={tokenMap.get(token)?.display}
            refresh={refresh}
            initTokenParams={initTokenParams}
            validateParams={validateParams}
            renderTokenParams={renderTokenParams}
            deleteToken={deleteToken}
          />
        ))}
        {/* Row for adding a token */}
        <AddToken
          rprops={rprops}
          tokenMap={tokenMap}
          refresh={refresh}
          initTokenParams={initTokenParams}
          validateParams={validateParams}
          renderTokenParams={renderTokenParams}
          addToken={addToken}
        />
      </DividedRows>
    </>
  );
}

interface TokenProps<TokenParams>
  extends Pick<
    Props<TokenParams>,
    | "rprops"
    | "refresh"
    | "initTokenParams"
    | "validateParams"
    | "renderTokenParams"
    | "deleteToken"
  > {
  token: string;
  tokenDisplay?: string;
}

function Token<TokenParams>(props: TokenProps<TokenParams>) {
  const {
    rprops,
    token,
    tokenDisplay,
    refresh,
    initTokenParams,
    validateParams,
    renderTokenParams,
    deleteToken,
  } = props;
  const { processing, setProcessing, setError } = rprops;

  const [showDelete, setShowDelete] = useState(false);
  const [extraParams, setExtraParams] = useState(initTokenParams);

  const doDeleteToken = useCallback(async () => {
    if (validateParams && !validateParams(extraParams)) return;

    setProcessing(true);
    setError(null);

    const success = await deleteToken(token, extraParams);

    if (success) {
      // Success. Refresh the timeline group.
      refresh();
    } else {
      // Display error.
      setError("Error: Could not delete visibility token.");
    }
    setShowDelete(false);
    setProcessing(false);
    setExtraParams(initTokenParams);
  }, [
    validateParams,
    extraParams,
    setProcessing,
    setError,
    deleteToken,
    token,
    initTokenParams,
    refresh,
  ]);

  return (
    <>
      <IndentedFlexRow>
        <CircleIconButton
          toggle
          toggleActive={showDelete}
          disabled={processing}
          title="Delete token"
          onClick={() => setShowDelete(!showDelete)}
        >
          <IconRemove />
        </CircleIconButton>
        <FormGrid>
          <span>{tokenDisplay ?? token}</span>
          <span />
          {showDelete && (
            <>
              {renderTokenParams &&
                renderTokenParams(
                  `self-vis-token-delete-${token}`,
                  rprops,
                  extraParams,
                  setExtraParams
                )}
              <FormButton disabled={processing} onClick={doDeleteToken}>
                Delete
              </FormButton>
            </>
          )}
        </FormGrid>
      </IndentedFlexRow>
    </>
  );
}

type AddTokenProps<TokenParams> = Pick<
  Props<TokenParams>,
  | "rprops"
  | "tokenMap"
  | "refresh"
  | "initTokenParams"
  | "validateParams"
  | "renderTokenParams"
  | "addToken"
>;

function AddToken<TokenParams>(props: AddTokenProps<TokenParams>) {
  const {
    rprops,
    tokenMap,
    refresh,
    initTokenParams,
    validateParams,
    renderTokenParams,
    addToken,
  } = props;
  const { processing, setProcessing, setError } = rprops;

  const visTokensToAdd = useMemo(() => {
    const tokens = [...tokenMap.values()];
    tokens.sort((a, b) => a.display.localeCompare(b.display));
    return tokens;
  }, [tokenMap]);

  // Form state
  const [showAdd, setShowAdd] = useState(false);
  const [tokenId, setTokenId] = useState("");
  const [extraParams, setExtraParams] = useState(initTokenParams);

  // Form actions
  const doAddToken = useCallback(async () => {
    if (tokenId === "") return;
    if (validateParams && !validateParams(extraParams)) return;

    setProcessing(true);
    setError(null);

    const success = await addToken(tokenId, extraParams);

    if (success) {
      // Success. Refresh the folder.
      refresh();
    } else {
      // Display error.
      setError("Error: Could not add visibility token.");
    }
    setShowAdd(false);
    setProcessing(false);
    setTokenId("");
    setExtraParams(initTokenParams);
  }, [
    tokenId,
    validateParams,
    extraParams,
    setProcessing,
    setError,
    addToken,
    initTokenParams,
    refresh,
  ]);

  return (
    <IndentedFlexRow>
      <CircleIconButton
        toggle
        toggleActive={showAdd}
        disabled={processing}
        title="Add token"
        onClick={() => setShowAdd(!showAdd)}
      >
        <IconAdd />
      </CircleIconButton>
      {showAdd && (
        <FormGrid>
          <label htmlFor="add-token">Token:</label>
          <select
            id="add-token"
            value={tokenId}
            disabled={processing}
            onChange={(e) => setTokenId(e.target.value)}
          >
            <option value=""></option>
            {visTokensToAdd.map((t) => (
              <option key={t.id} value={t.id}>
                {t.display}
              </option>
            ))}
          </select>
          {renderTokenParams &&
            renderTokenParams(
              `self-vis-token-add-${tokenId}`,
              rprops,
              extraParams,
              setExtraParams
            )}
          <FormButton disabled={processing} onClick={doAddToken}>
            Add
          </FormButton>
        </FormGrid>
      )}
    </IndentedFlexRow>
  );
}

interface RecurseTokenParamProps {
  idPrefix: string;
  rprops: RenderProps;
  params: boolean;
  onParamsChange: (params: boolean) => void;
}

export const RecurseTokenParam: React.FC<RecurseTokenParamProps> = ({
  idPrefix,
  rprops,
  params,
  onParamsChange,
}) => {
  return (
    <>
      <label htmlFor={`${idPrefix}-recurse`}>Recursive:</label>
      <input
        id={`${idPrefix}-recurse`}
        type="checkbox"
        disabled={rprops.processing}
        checked={params}
        onChange={(e) => onParamsChange(e.target.checked)}
      />{" "}
    </>
  );
};
