import React from "react";
import styled from "styled-components";

export const FilterCode = styled.pre`
  margin: 0;
  font-size: 1em;
  line-height: 1.2;
`;

export const FilterTextArea = styled.textarea`
  flex-grow: 1;
  font-family: monospace;
  font-size: 1em;
  line-height: 1.2;
  height: 20em;
  min-width: 150px;
`;

const FilterCodeRefCode = styled(FilterCode)`
  font-size: 0.8em;
`;

export const FilterCodeRef: React.FC = () => {
  return (
    <div>
      <b>Reference:</b>
      <FilterCodeRefCode>{`{
  "op": "and" | "or",
  "exprs": [...],
  "folders": [{ path, fullTree }],
  "ratings": [{ value, cmpOp }],
  "keywords": [...],
  "categories": [...],
  "dates": [{ value, cmpOp }],
  "aspectRatios": [{ value, cmpOp }]
}
cmpOp = eq|ne|gt|gte|lt|lte
`}</FilterCodeRefCode>
    </div>
  );
};
