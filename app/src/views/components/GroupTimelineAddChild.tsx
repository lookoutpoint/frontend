// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import {
  createTimelineGroup,
  GroupType,
  GROUP_SEP,
  syncTimelineGroup,
  TimelineGroup,
} from "../../api/photoGroups";
import { Dialog, RenderProps } from "../../components/Dialog";
import {
  FlexRowNoMargin,
  FormButton,
  FormGrid,
  Row,
} from "../../components/Form";
import { IconAdmin } from "../../components/Icons";
import { FilterCodeRef, FilterTextArea } from "./GroupTimelineAdmin";

interface Props {
  groupType: GroupType;
  groupId: string;
  parentTimelineGroup: TimelineGroup;
  onClose: () => void;
  refresh: () => void;
}

export const GroupTimelineAddChild: React.FC<Props> = ({
  groupType,
  groupId,
  parentTimelineGroup,
  onClose,
  refresh,
}) => {
  return (
    <Dialog
      modalLevel={0}
      title={
        <span>
          <IconAdmin /> Add Child: {parentTimelineGroup.display}
        </span>
      }
      onClose={onClose}
      render={(rprops) => (
        <DialogImpl
          rprops={rprops}
          groupType={groupType}
          groupId={groupId}
          parentTimelineGroup={parentTimelineGroup}
          onClose={onClose}
          refresh={refresh}
        />
      )}
    />
  );
};

interface DialogProps extends Props {
  rprops: RenderProps;
}

const DialogImpl: React.FC<DialogProps> = ({
  rprops,
  groupId,
  parentTimelineGroup,
  onClose,
  refresh,
}) => {
  const { processing, setProcessing, setError } = rprops;

  // Form state
  const [name, setName] = useState("");
  const [filter, setFilter] = useState("{}");

  // Form create action
  const onCreate = useCallback(async () => {
    setProcessing(true);

    try {
      // Name
      const trimmedName = name.trim();
      if (trimmedName.length === 0) {
        setError("Error: No name specified.");
        return;
      }

      if (trimmedName.indexOf(GROUP_SEP[GroupType.TIMELINE_GROUP]) !== -1) {
        setError("Error: Name has group separator character.");
        return;
      }

      // Add parent display name, excluding the folder.
      const groupSep = GROUP_SEP[GroupType.TIMELINE_GROUP];
      const sepIdx = parentTimelineGroup.display.indexOf(groupSep);

      let fullName = trimmedName;
      if (sepIdx !== -1) {
        // Prepend the ancestor display path.
        fullName =
          parentTimelineGroup.display.slice(sepIdx + 1) +
          GROUP_SEP[GroupType.TIMELINE_GROUP] +
          fullName;
      }

      // Filter
      let parsedFilter: unknown;
      if (filter.trim().length > 0) {
        try {
          parsedFilter = JSON.parse(filter);
        } catch (e) {
          // Invalid JSON.
          setError(`Error: Invalid filter JSON. ${e}`);
          return;
        }
      }

      // Try to create
      const [success, idOrError] = await createTimelineGroup(
        groupId,
        fullName,
        parsedFilter
      );
      if (success) {
        // Success, so sync, then refresh and set success flag (which will close the dialog).
        const success = await syncTimelineGroup(idOrError!);
        if (!success) {
          // Don't hard fail here. Put a warning, wait a bit, then continue.
          setError("Warning: Could not do initial sync.");
          await new Promise((resolve) => setTimeout(resolve, 2000));
        }

        // Timeline group added successfully. Refresh and close this dialog.
        refresh();
        onClose();
      } else {
        setError(
          `Error: Could not create new timeline group. ${idOrError ?? ""}`
        );
      }
    } finally {
      setProcessing(false);
    }
  }, [
    filter,
    groupId,
    name,
    onClose,
    parentTimelineGroup.display,
    refresh,
    setError,
    setProcessing,
  ]);

  return (
    <Row>
      <FormGrid>
        <label htmlFor="name">Name:</label>
        <FlexRowNoMargin>
          <span>{parentTimelineGroup.display}|</span>
          <input
            id="name"
            placeholder="name"
            disabled={processing}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FlexRowNoMargin>
        <label htmlFor="filter">Filter:</label>
        <FlexRowNoMargin>
          <FilterTextArea
            id="filter"
            disabled={processing}
            value={filter}
            onChange={(e) => setFilter(e.target.value)}
          />
          <FilterCodeRef />
        </FlexRowNoMargin>
        <FormButton disabled={processing} onClick={onCreate}>
          Create
        </FormButton>
      </FormGrid>
    </Row>
  );
};
