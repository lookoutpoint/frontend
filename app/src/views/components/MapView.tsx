// SPDX-License-Identifier: MIT

import React, {
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Group } from "../../api/photoGroups";
import { mapTilerApiKey } from "../../api/maps";
import {
  GeoJSONSource,
  LngLatBounds,
  Map as MapTilerMap,
  MapGeoJSONFeature,
  MapStyle,
  config as mapTilerConfig,
  Popup as MapTilerPopup,
} from "@maptiler/sdk";
import { PhotoListFilterExpr, PhotoMapListPhoto } from "../../api/photos";
import { useTheme } from "../../hooks/useTheme";
import { ThemeType } from "../../helpers/Contexts";

import "@maptiler/sdk/dist/maptiler-sdk.css";
import { useMappedPhotos } from "../../hooks/useMappedPhotos";
import { createPortal } from "react-dom";
import { ConstrainedPhoto } from "../../components/ConstrainedPhoto";

interface Props<G extends Group> {
  group: G;
  hidden: boolean;
  buildPhotoListFilter: (groupId: string) => PhotoListFilterExpr;
}

mapTilerConfig.apiKey = mapTilerApiKey;

const SOURCE_PHOTOS = "photos";
const LAYER_PREFIX = "lookoutpoint.";
const LAYER_PHOTO_CLUSTERS = LAYER_PREFIX + "photos.clusters";
const LAYER_PHOTO_CLUSTERS_COUNT = LAYER_PREFIX + "photos.clusters.count";
const LAYER_PHOTO_UNCLUSTERED = LAYER_PREFIX + "photos.unclustered";

function getPointCoordinates(feature: GeoJSON.Feature): [number, number] {
  if (feature.geometry.type === "Point") {
    return feature.geometry.coordinates as [number, number];
  } else {
    throw new Error("Expected Point");
  }
}

function getMapStyle(theme: ThemeType) {
  if (theme === ThemeType.LIGHT) {
    return MapStyle.OUTDOOR.DEFAULT;
  } else {
    return MapStyle.OUTDOOR.DARK;
  }
}

function addMapPhotoSource(
  map: MapTilerMap,
  data?: GeoJSON.GeoJSON<GeoJSON.Geometry, GeoJSON.GeoJsonProperties>
) {
  data = data ?? { type: "FeatureCollection", features: [] };

  map.addSource(SOURCE_PHOTOS, {
    type: "geojson",
    cluster: true,
    clusterRadius: 40,
    clusterMaxZoom: 15,
    data,
  });
}

// Add map layers.
//
// For layers that do not depend on theme, only added if layer does not already exist.
// For layers that depend on theme, layer is removed if existing and then readded.
// This seems like the most reliable way to get layers to repaint.
function addMapLayers(map: MapTilerMap, theme: ThemeType) {
  // Unthemed layer.
  if (!map.getLayer(LAYER_PHOTO_CLUSTERS)) {
    map.addLayer({
      id: LAYER_PHOTO_CLUSTERS,
      type: "circle",
      source: SOURCE_PHOTOS,
      filter: ["has", "point_count"],
      paint: {
        "circle-color": [
          "step",
          ["get", "point_count"],
          "#ff8800",
          100,
          "#ff6200",
        ],
        "circle-radius": ["step", ["get", "point_count"], 20, 100, 25],
        "circle-stroke-width": 1,
        "circle-stroke-color": [
          "step",
          ["get", "point_count"],
          "#ff6200",
          100,
          "#913c00",
        ],
      },
    });
  }

  // Unthemed layer.
  if (!map.getLayer(LAYER_PHOTO_CLUSTERS_COUNT)) {
    map.addLayer({
      id: LAYER_PHOTO_CLUSTERS_COUNT,
      type: "symbol",
      source: SOURCE_PHOTOS,
      filter: ["has", "point_count"],
      layout: {
        "text-field": "{point_count_abbreviated}",
        "text-font": ["Lato"],
        "text-size": 12,
      },
    });
  }

  // Themed layer.
  if (map.getLayer(LAYER_PHOTO_UNCLUSTERED)) {
    map.removeLayer(LAYER_PHOTO_UNCLUSTERED);
  }
  map.addLayer({
    id: LAYER_PHOTO_UNCLUSTERED,
    type: "circle",
    source: SOURCE_PHOTOS,
    filter: ["!", ["has", "point_count"]],
    paint: {
      "circle-color": "#ffd500",
      "circle-radius": 5,
      "circle-stroke-width": 1,
      "circle-stroke-color": theme === ThemeType.DARK ? "#fff" : "#000",
    },
  });
}

function updatePhotoSource(map: MapTilerMap, photos: PhotoMapListPhoto[]) {
  map.getSource<GeoJSONSource>(SOURCE_PHOTOS)!.setData({
    type: "FeatureCollection",
    features: photos.map((photo) => ({
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: photo.gpsLocation.coordinates,
      },
      properties: { photoId: photo.id },
    })),
  });
}

interface PopupState {
  container: HTMLElement;
  photos: PhotoMapListPhoto[];
}

export function MapView<G extends Group>(props: Props<G>) {
  const { group, hidden, buildPhotoListFilter } = props;

  const theme = useTheme();

  const photoFilter = useMemo(
    () => buildPhotoListFilter(group.id),
    [buildPhotoListFilter, group.id]
  );
  const mappablePhotos = useMappedPhotos(photoFilter, { enabled: !hidden });
  const mappablePhotosMap = useMemo(() => {
    const map = new Map<string, PhotoMapListPhoto>();
    for (const photo of mappablePhotos) {
      map.set(photo.id, photo);
    }
    return map;
  }, [mappablePhotos]);
  const mappablePhotosMapRef = useRef(mappablePhotosMap);
  useEffect(() => {
    mappablePhotosMapRef.current = mappablePhotosMap;
  }, [mappablePhotosMap]);

  const containerRef = useRef<HTMLDivElement>(null);
  const [containerTop, setContainerTop] = useState<number | null>(null);

  useLayoutEffect(() => {
    const rect = containerRef.current!.getBoundingClientRect();
    setContainerTop(rect.top);
  }, []);

  const mapRef = useRef<MapTilerMap | null>(null);
  const [mapLoaded, setMapLoaded] = useState(false);

  const [popupState, setPopupState] = useState<PopupState | undefined>(
    undefined
  );

  useEffect(() => {
    // Create the map at most once.
    if (mapRef.current) return;

    const map = new MapTilerMap({
      container: containerRef.current!,
      style: getMapStyle(theme.type),
      projection: "mercator",
    });
    mapRef.current = map;

    map.on("load", () => {
      addMapPhotoSource(map);
      addMapLayers(map, theme.type);
      setMapLoaded(true);
    });

    // Zoom into cluster on click.
    map.on("click", LAYER_PHOTO_CLUSTERS, async (e) => {
      const features = map.queryRenderedFeatures(e.point, {
        layers: [LAYER_PHOTO_CLUSTERS],
      });
      const feature = features[0];
      const clusterId = feature.properties.cluster_id;
      const pointCount = feature.properties.point_count;
      const clusterFeatures = await map
        .getSource<GeoJSONSource>(SOURCE_PHOTOS)!
        .getClusterLeaves(clusterId, pointCount, 0);

      const bounds = new LngLatBounds(
        getPointCoordinates(clusterFeatures[0]),
        getPointCoordinates(clusterFeatures[0])
      );

      for (let i = 1; i < clusterFeatures.length; ++i) {
        bounds.extend(getPointCoordinates(clusterFeatures[i]));
      }

      map.fitBounds(bounds, { padding: 50, animate: true, duration: 1000 });
    });

    // Center map on unclustered photo on click.
    map.on("click", LAYER_PHOTO_UNCLUSTERED, async (e) => {
      const features = map.queryRenderedFeatures(e.point, {
        layers: [LAYER_PHOTO_UNCLUSTERED],
      });
      const feature = features[0];

      map.panTo(getPointCoordinates(feature));
    });

    // Change cursor for layers with click handlers.
    map.on("mouseenter", LAYER_PHOTO_CLUSTERS, () => {
      map.getCanvas().style.cursor = "pointer";
    });
    map.on("mouseleave", LAYER_PHOTO_CLUSTERS, () => {
      map.getCanvas().style.cursor = "";
    });
    map.on("mouseenter", LAYER_PHOTO_UNCLUSTERED, () => {
      map.getCanvas().style.cursor = "pointer";
    });
    map.on("mouseleave", LAYER_PHOTO_UNCLUSTERED, () => {
      map.getCanvas().style.cursor = "";
    });

    // Hover over unclustered photo shows preview of photo.
    let popup: MapTilerPopup | undefined;
    let popupDiv: HTMLElement | undefined;
    let popupFeatures: MapGeoJSONFeature[] = [];

    map.on("mousemove", LAYER_PHOTO_UNCLUSTERED, async (e) => {
      const features = map.queryRenderedFeatures(e.point, {
        layers: [LAYER_PHOTO_UNCLUSTERED],
      });

      console.log(typeof features[0].properties);

      // Check if this is the same set of features, in which case do nothing
      // (keep current popup).
      if (popupFeatures.length === features.length) {
        let equalFeatures = true;
        for (let i = 0; i < popupFeatures.length; ++i) {
          if (
            popupFeatures[i].properties["photoId"] !==
            features[i].properties["photoId"]
          ) {
            equalFeatures = false;
            break;
          }
        }
        if (equalFeatures) return;
      }
      popupFeatures = features;

      if (!popup) {
        popup = new MapTilerPopup();
        popup.addTo(map);
      }

      if (!popupDiv) {
        popupDiv = document.createElement("div");
        popup.setDOMContent(popupDiv);
      }

      popup.setLngLat(getPointCoordinates(features[0]));

      setPopupState({
        container: popupDiv,
        photos: features.map(
          (f) => mappablePhotosMapRef.current.get(f.properties["photoId"])!
        ),
      });
    });
    map.on("mouseleave", LAYER_PHOTO_UNCLUSTERED, async () => {
      if (popup) {
        popup.remove();
        popup = undefined;
        popupDiv!.remove();
        popupDiv = undefined;
        popupFeatures = [];

        setPopupState(undefined);
      }
    });

    // The initial size of the container may not be up-to-date, so force a resize one frame later.
    requestAnimationFrame(() => {
      map.resize();
    });

    // Map is only created once.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Update cluster of mapped photo points.
  useEffect(() => {
    if (!mapLoaded) return;
    const map = mapRef.current!;

    if (mappablePhotos.length > 0) {
      // Update source.
      updatePhotoSource(map, mappablePhotos);

      // Zoom to fit.
      const bounds = new LngLatBounds(
        mappablePhotos[0].gpsLocation.coordinates,
        mappablePhotos[0].gpsLocation.coordinates
      );

      for (let i = 1; i < mappablePhotos.length; ++i) {
        bounds.extend(mappablePhotos[i].gpsLocation.coordinates);
      }

      map.fitBounds(bounds, { padding: 50, animate: false });
    }
  }, [mapLoaded, mappablePhotos]);

  // Update map style on theme changes.
  useEffect(() => {
    const map = mapRef.current;
    if (!map) return;

    // Re-add layers after style data has loaded.
    const styleData = () => {
      addMapLayers(map, theme.type);
      map.off("styledata", styleData);
    };
    map.on("styledata", styleData);

    map.setStyle(getMapStyle(theme.type), {
      // Bring over custom sources and layers.
      transformStyle: (prev, next) => {
        const merged = { ...next };

        if (prev?.sources[SOURCE_PHOTOS]) {
          merged.sources[SOURCE_PHOTOS] = prev.sources[SOURCE_PHOTOS];
        }

        // Layers will be re-added.

        return merged;
      },
    });
  }, [theme.type]);

  return (
    <>
      <div
        ref={containerRef}
        style={{ height: `calc(100vh - ${containerTop}px)` }}
      ></div>
      {popupState &&
        createPortal(<PopupView {...popupState} />, popupState.container)}
    </>
  );
}

const PopupView: React.FC<PopupState> = ({ photos }) => {
  return (
    <ConstrainedPhoto
      photo={photos[0]}
      constrainWidth
      constrainHeight
      maxWidth={300}
      maxHeight={300}
      // fixWidth={
      //   photos[0].width >= photos[0].height
      //     ? Math.min(300, photos[0].width)
      //     : undefined
      // }
      // fixHeight={
      //   photos[0].width < photos[0].height
      //     ? Math.min(300, photos[0].height)
      //     : undefined
      // }
    />
  );
};
