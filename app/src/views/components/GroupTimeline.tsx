// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useState,
} from "react";
import styled from "styled-components";
import {
  createTimelineGroup,
  getGroupName,
  getTimelineGroupChildren,
  getTimelineGroupId,
  GroupType,
  syncTimelineGroup,
  TimelineGroup,
} from "../../api/photoGroups";
import { CircleIconButton } from "../../components/CircleIconButton";
import { NotPublicIcon } from "../../components/NotPublicIcon";
import { RichText } from "../../components/RichText";
import { useUserSession } from "../../hooks/useUserSession";
import { GroupTimelineAddChild } from "./GroupTimelineAddChild";
import { TimelineContainer } from "../../components/TimelineContainer";
import { NotFound } from "../NotFound";
import { PhotoListQuery } from "../../helpers/PhotoListQuery";
import { listPhotos } from "../../api/photos";
import { PhotoGalleryViewWrapper } from "./PhotoGalleryViewWrapper";
import {
  orderTimelineGroups,
  TimelineGroupOrderInfo,
} from "../../helpers/TimelineGroup";
import {
  localStorageGetBool,
  LocalStorageKey,
  localStorageSetBool,
} from "../../helpers/localStorage";
import { Helmet } from "react-helmet-async";
import { GroupTextEdit } from "./GroupTextEdit";
import { AppEvent, makeEventGroupID } from "../../api/events";
import { LinkWithEvent } from "../../components/LinkWithEvent";
import {
  IconAdd,
  IconAdmin,
  IconAngleLeft,
  IconAngleRight,
  IconEdit,
} from "../../components/Icons";
import { CenteredText, Error } from "../../components/Utility";
import { lazily } from "react-lazily";
import { useQueryClient } from "@tanstack/react-query";

const { PhotoGroupSettings } = lazily(() => import("./PhotoGroupSettings"));
const { GroupTimelineSettingsExtension } = lazily(
  () => import("./GroupTimelineSettingsExtension")
);

// ============================================================================
// Styled components
// ============================================================================

const StyledTimelineContainer = styled(TimelineContainer)`
  /* line up against control bar */
  margin-top: -0.5em;
  border-top: 0;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5em 0;
  margin-top: -0.5em; /* to be right up against bottom of timeline */
  background-color: var(--timeline-title-bg-color);

  & > * + * {
    margin-left: 0.5em;
  }
`;

const Title = styled.h1`
  font-size: 1.5em;
  margin: 0;
  font-weight: normal;
  text-align: center;
`;

const PrivateIcon = styled(NotPublicIcon)`
  color: var(--lesser-text-color);
`;

const PhotosTitle = styled.div`
  padding-top: 0.5em;
  font-size: calc(1.25 * var(--fancy-title-size));
  font-style: italic;
  font-family: var(--fancy-font), "cursive";
  text-align: center;
  border-top: 1px solid var(--timeline-border-color);
`;

const PhotoRichText = styled.div`
  padding-top: 0.5em;
  padding-bottom: 1em;
  background-color: var(--text-bg-color);
`;

const GroupNavDiv = styled.div`
  max-width: var(--max-text-width);
  margin: 1em auto 0 auto;
  padding: 1em 0.5em 0 0.5em;
  border-top: 1px solid var(--timeline-border-color);

  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto;
`;

const GroupNavLink = styled(LinkWithEvent)`
  text-decoration: none;
  color: var(--link-color);

  &:hover {
    color: var(--link-color);
  }
  &:hover span {
    text-decoration: underline;
  }

  display: flex;
  align-items: center;
  > * + * {
    margin-left: 0.25em;
  }
`;

const GroupNavPrevLink = styled(GroupNavLink)`
  grid-column: 1 / 1;
  justify-self: start;
`;

const GroupNavNextLink = styled(GroupNavLink)`
  grid-column: 2 / 2;
  justify-self: end;
  text-align: right;
`;

// ============================================================================
// GroupTimeline: Main component
// ============================================================================

interface Props {
  groupType: GroupType;
  groupId: string;
  rootTimelineGroup?: TimelineGroup;
  displayTimelineGroup?: TimelineGroup;
  rootTimelineUrlPrefix: string;
  timelineGroupPath?: string;
  lightboxPhotoId?: string;
  hidden?: boolean;
  refresh: () => void;
  onGroupNavInfo?: (
    prevGroup: TimelineGroup | null,
    nextGroup: TimelineGroup | null
  ) => void;
}

export const GroupTimeline: React.FC<Props> = (props) => {
  const { groupId, rootTimelineGroup, refresh, onGroupNavInfo } = props;

  const { isAdmin } = useUserSession();

  // If there's no root timeline group, reset nav info.
  useEffect(() => {
    if (rootTimelineGroup !== null || !onGroupNavInfo) return;
    onGroupNavInfo(null, null);
  }, [onGroupNavInfo, rootTimelineGroup]);

  if (rootTimelineGroup) {
    return (
      <GroupTimelineImpl {...props} rootTimelineGroup={rootTimelineGroup} />
    );
  } else if (isAdmin) {
    return <NoTimelineGroup groupId={groupId} refresh={refresh} />;
  } else {
    return <NotFound what="Timeline" />;
  }
};

// Implementation with a non-null root timeline group.
interface ImplProps extends Omit<Props, "rootTimelineGroup"> {
  rootTimelineGroup: TimelineGroup;
}

const GroupTimelineImpl: React.FC<ImplProps> = ({
  groupType,
  groupId,
  rootTimelineGroup,
  displayTimelineGroup,
  rootTimelineUrlPrefix,
  timelineGroupPath,
  lightboxPhotoId,
  hidden,
  refresh,
  onGroupNavInfo,
}) => {
  const queryClient = useQueryClient();
  const session = useUserSession();

  const isChildGroup = timelineGroupPath && timelineGroupPath.length > 1;

  const timelineGroup = isChildGroup ? displayTimelineGroup : rootTimelineGroup;
  const timelineGroupUrl = timelineGroup
    ? `${rootTimelineUrlPrefix}${timelineGroup.id.substring(
        timelineGroup.id.indexOf("/", 1)
      )}`
    : undefined;

  const rootEventContext = makeEventGroupID(
    GroupType.TIMELINE_GROUP,
    rootTimelineGroup
  );
  const storyEventContext = timelineGroup
    ? makeEventGroupID(GroupType.TIMELINE_GROUP, timelineGroup, "story")
    : undefined;

  // Child timeline groups of the root group.
  const [childrenGroups, setChildrenGroups] = useState<
    TimelineGroupOrderInfo[]
  >([]);

  useEffect(() => {
    (async () => {
      // Fetch all child timeline groups.
      const children = await getTimelineGroupChildren(session, {
        id: rootTimelineGroup.id,
        coverPhoto: true,
        maxRelDepth: -1,
      });

      if (children) {
        setChildrenGroups(
          orderTimelineGroups(rootTimelineGroup, children, session.isAdmin)
        );
      }
    })();
  }, [rootTimelineGroup, session]);

  // Group nav.
  const [navInfo, setRawNavInfo] = useState({
    prev: null as TimelineGroup | null,
    next: null as TimelineGroup | null,
  });

  const setNavInfo = useCallback(
    (prev: TimelineGroup | null, next: TimelineGroup | null) => {
      setRawNavInfo({ prev, next });
      if (onGroupNavInfo) {
        onGroupNavInfo(prev, next);
      }
    },
    [onGroupNavInfo]
  );

  // Update the nav info based on the currently displayed timeline group.
  // For a given root timeline group, this reuses the child groups fetched for the
  // root timeline group.
  useEffect(() => {
    if (!childrenGroups || childrenGroups.length === 0) return;

    if (
      !displayTimelineGroup ||
      displayTimelineGroup.id === rootTimelineGroup.id
    ) {
      // Root group, so nav is the first child group.
      setNavInfo(null, childrenGroups[0].group);
      return;
    }

    // Find display timeline group in the ordered list.
    for (let i = 0; i < childrenGroups.length; ++i) {
      if (childrenGroups[i].group.id === displayTimelineGroup.id) {
        setNavInfo(
          i > 0 ? childrenGroups[i - 1].group : rootTimelineGroup,
          i < childrenGroups.length - 1 ? childrenGroups[i + 1].group : null
        );
        return;
      }
    }
  }, [childrenGroups, displayTimelineGroup, rootTimelineGroup, setNavInfo]);

  // Photo list query
  const [photoListQuery, setPhotoListQuery] =
    useState<PhotoListQuery<"sortKey"> | null>(null);

  useEffect(() => {
    if (timelineGroup) {
      setPhotoListQuery(
        new PhotoListQuery("sortKey", async (limit, refKeyValue, refOp) => {
          return await listPhotos(queryClient, {
            limit,
            refKeyValue,
            refOp,
            filter: timelineGroup.filter!,
          });
        })
      );
    }
  }, [queryClient, session, timelineGroup]);

  // Timeline size state
  const [timelineCollapsed, setTimelineCollapsed] = useState(false);

  // Use useLayoutEffect as the collapsed state affects rendering of the timeline.
  useLayoutEffect(() => {
    setTimelineCollapsed(
      localStorageGetBool(
        isChildGroup
          ? LocalStorageKey.TIMELINE_CHILD_COLLAPSED
          : LocalStorageKey.TIMELINE_ROOT_COLLAPSED
      )
    );
  }, [isChildGroup]);

  const toggleTimelineCollapsed = useCallback(() => {
    const newState = !timelineCollapsed;
    setTimelineCollapsed(newState);
    localStorageSetBool(
      isChildGroup
        ? LocalStorageKey.TIMELINE_CHILD_COLLAPSED
        : LocalStorageKey.TIMELINE_ROOT_COLLAPSED,
      newState
    );
  }, [isChildGroup, timelineCollapsed]);

  // Dialogs
  const [showSettings, setShowSettings] = useState(false);
  const [showAddChild, setShowAddChild] = useState(false);

  // Text edit state
  const [editText, setEditText] = useState(false);

  return (
    <div>
      {/* Metadata */}
      {timelineGroup && timelineGroup.metaDescription && (
        <Helmet>
          <meta name="description" content={timelineGroup.metaDescription} />
        </Helmet>
      )}

      {/* Timeline */}
      <StyledTimelineContainer
        rootGroup={rootTimelineGroup}
        groups={childrenGroups}
        timelineUrlPrefix={rootTimelineUrlPrefix}
        highlightGroupId={
          rootTimelineGroup.id + (timelineGroupPath ?? "/").slice(1)
        }
        collapsed={timelineCollapsed}
        toggleCollapsed={toggleTimelineCollapsed}
        // Since the timeline container is really a property of the root timeline group, use that as the context.
        // This reduces event noise.
        eventContext={rootEventContext}
      />

      {/* Title */}
      <TitleWrapper>
        <Title>
          {timelineGroup &&
            getGroupName(GroupType.TIMELINE_GROUP, timelineGroup)}
        </Title>
        {timelineGroup && !timelineGroup.public && <PrivateIcon />}
        {session.isAdmin && timelineGroup && (
          <>
            <CircleIconButton
              title="Admin"
              onClick={() => setShowSettings(true)}
            >
              <IconAdmin />
            </CircleIconButton>
            <CircleIconButton
              title="Add child timeline group"
              onClick={() => setShowAddChild(true)}
            >
              <IconAdd />
            </CircleIconButton>
            <CircleIconButton
              title="Edit text"
              onClick={() => setEditText(true)}
            >
              <IconEdit />
            </CircleIconButton>
          </>
        )}
      </TitleWrapper>

      {/* Rich text */}
      {timelineGroup && editText && (
        <GroupTextEdit
          groupType={GroupType.TIMELINE_GROUP}
          group={timelineGroup}
          refresh={refresh}
          close={() => setEditText(false)}
        />
      )}
      {timelineGroup && timelineGroup.richText && (
        <PhotoRichText>
          <RichText
            text={timelineGroup.richText}
            currentUrl={timelineGroupUrl!}
            eventContext={storyEventContext}
          />
          {(navInfo.prev || navInfo.next) && (
            <GroupNavDiv>
              {navInfo.prev && (
                <GroupNavPrevLink
                  to={rootTimelineUrlPrefix + getTimelineGroupId(navInfo.prev)}
                  event={{
                    event: AppEvent.GROUP_CLICK,
                    object: makeEventGroupID(
                      GroupType.TIMELINE_GROUP,
                      navInfo.prev
                    ),
                    context: storyEventContext,
                  }}
                >
                  <IconAngleLeft />
                  <span>
                    {getGroupName(GroupType.TIMELINE_GROUP, navInfo.prev)}
                  </span>
                </GroupNavPrevLink>
              )}
              {navInfo.next && (
                <GroupNavNextLink
                  to={rootTimelineUrlPrefix + getTimelineGroupId(navInfo.next)}
                  event={{
                    event: AppEvent.GROUP_CLICK,
                    object: makeEventGroupID(
                      GroupType.TIMELINE_GROUP,
                      navInfo.next
                    ),
                    context: storyEventContext,
                  }}
                >
                  <span>
                    {getGroupName(GroupType.TIMELINE_GROUP, navInfo.next)}
                  </span>
                  <IconAngleRight />
                </GroupNavNextLink>
              )}
            </GroupNavDiv>
          )}
        </PhotoRichText>
      )}

      {/* Photos */}
      {timelineGroup && photoListQuery && (
        <>
          <PhotosTitle>Photos</PhotosTitle>
          <PhotoGalleryViewWrapper
            photoListQuery={photoListQuery}
            photoListQueryForward
            showGalleryOverlay={false}
            hidden={hidden}
            rootUrl={`${timelineGroupUrl}@/`}
            closeUrl={timelineGroupUrl}
            lightboxPhotoId={lightboxPhotoId}
            eventContext={makeEventGroupID(
              GroupType.TIMELINE_GROUP,
              timelineGroup,
              "photos"
            )}
          />
        </>
      )}

      {/* Dialogs */}
      {showSettings && timelineGroup && (
        <PhotoGroupSettings
          groupType={GroupType.TIMELINE_GROUP}
          groupInfo={timelineGroup}
          onClose={() => setShowSettings(false)}
          refresh={refresh}
          Extension={GroupTimelineSettingsExtension}
        />
      )}
      {showAddChild && timelineGroup && (
        <GroupTimelineAddChild
          groupType={groupType}
          groupId={groupId}
          parentTimelineGroup={timelineGroup}
          onClose={() => setShowAddChild(false)}
          refresh={refresh}
        />
      )}
    </div>
  );
};

// ============================================================================
// UI for groups without a timeline group.
// ============================================================================

interface NoTimelineGroupProps {
  groupId: string;
  refresh: () => void;
}

const NoTimelineGroup: React.FC<NoTimelineGroupProps> = ({
  groupId,
  refresh,
}) => {
  const [createError, setCreateError] = useState<string | null>(null);
  const [processing, setProcessing] = useState(false);

  const create = useCallback(async () => {
    setProcessing(true);

    try {
      const [success, id] = await createTimelineGroup(groupId);
      if (!success || id === undefined) {
        setCreateError("Error: Could not create timeline group.");
        return;
      }

      if (!(await syncTimelineGroup(id))) {
        setCreateError("Error: Could not sync timeline group.");
        return;
      }

      // Timeline group has changed.
      refresh();
    } finally {
      setProcessing(false);
    }
  }, [groupId, refresh]);

  return (
    <CenteredText>
      <p>No timeline group.</p>
      <button disabled={processing} onClick={create}>
        Create
      </button>
      {createError && <Error>{createError}</Error>}
    </CenteredText>
  );
};
