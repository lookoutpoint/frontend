// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useMemo, useState } from "react";
import {
  Group,
  GroupType,
  GroupUpdateParams,
  GROUP_SEP,
  updateGroup,
} from "../../api/photoGroups";
import { Dialog, RenderProps } from "../../components/Dialog";
import {
  listVisibilityTokens,
  VisibilityToken,
  VIS_TOKEN_ALL,
  VIS_TOKEN_PUBLIC,
} from "../../api/visibility";
import { FlexDiv, FlexRow } from "../../components/Form";
import { EditableField } from "../../components/EditableField";
import { titleCase } from "../../helpers/titleCase";
import { IconAdmin } from "../../components/Icons";

interface Props<G extends Group> {
  groupType: GroupType;
  groupInfo: G;
  onClose: () => void;
  refresh: () => void;
  Extension?: React.FC<SettingsExtensionProps<G>>;
}

export function PhotoGroupSettings<G extends Group>(props: Props<G>) {
  const { groupType, groupInfo, onClose, refresh, Extension } = props;

  return (
    <Dialog
      modalLevel={0}
      title={
        <span>
          <IconAdmin /> {titleCase(groupType)}: {groupInfo.display}
        </span>
      }
      onClose={onClose}
      render={(rprops) => (
        <DialogImpl
          rprops={rprops}
          groupType={groupType}
          groupInfo={groupInfo}
          refresh={refresh}
          onClose={onClose}
          Extension={Extension}
        />
      )}
    />
  );
}

export type TokenMap = Map<string, VisibilityToken>;

export interface SettingsExtensionProps<G extends Group> {
  rprops: RenderProps;
  groupInfo: G;
  tokenMap: TokenMap;
  refresh: () => void;
  close: () => void;
}

interface DialogProps<G extends Group> {
  rprops: RenderProps;
  groupType: GroupType;
  groupInfo: G;
  refresh: () => void;
  onClose: () => void;
  Extension?: React.FunctionComponent<SettingsExtensionProps<G>>;
}

function DialogImpl<G extends Group>(props: DialogProps<G>) {
  const { rprops, groupType, groupInfo, refresh, onClose, Extension } = props;
  const { processing, setProcessing, setError } = rprops;

  const [tokenMap, setTokenMap] = useState<TokenMap>(new Map());

  useEffect(() => {
    let unmounted = false;

    (async () => {
      // Fetch visibility tokens.
      const tokens = await listVisibilityTokens();
      if (!unmounted && tokens) {
        const map = new Map<string, VisibilityToken>();
        map.set(VIS_TOKEN_PUBLIC, { id: VIS_TOKEN_PUBLIC, display: "#Public" });
        for (const token of tokens) {
          map.set(token.id, token);
        }
        setTokenMap(map);
      }
    })();

    return () => {
      unmounted = true;
    };
  }, []);

  const visTokens = (groupInfo.visTokens ?? [])
    .filter((t) => t !== VIS_TOKEN_ALL)
    .sort();

  // Display path parts.
  const { display } = groupInfo;
  const groupSep = GROUP_SEP[groupType];
  const dpSep = groupSep ? display.lastIndexOf(groupSep) : -1;
  const dpPrefix = display.substring(0, dpSep + 1);
  const thisDp = display.substring(dpSep + 1);

  const doUpdate = useCallback(
    (opts: GroupUpdateParams) => {
      const displayChanged =
        opts.display !== undefined && opts.display !== thisDp;
      const sortOrderChanged =
        opts.sortOrder !== undefined && opts.sortOrder !== groupInfo.sortOrder;
      const metaDescChanged =
        opts.metaDescription !== undefined &&
        opts.metaDescription !== groupInfo.metaDescription;
      if (!displayChanged && !sortOrderChanged && !metaDescChanged) {
        return;
      }

      (async () => {
        setProcessing(true);

        const success = await updateGroup(groupType, groupInfo.id, opts);

        if (success) {
          refresh();
        } else {
          setError(`Error: Could not update ${groupType.toLowerCase()}.`);
        }
        setProcessing(false);
      })();
    },
    [
      groupInfo.id,
      groupInfo.metaDescription,
      groupInfo.sortOrder,
      groupType,
      refresh,
      setError,
      setProcessing,
      thisDp,
    ]
  );

  const extension = useMemo(() => {
    if (!Extension) return null;
    return (
      <Extension
        rprops={rprops}
        groupInfo={groupInfo}
        tokenMap={tokenMap}
        refresh={refresh}
        close={onClose}
      />
    );
  }, [Extension, groupInfo, onClose, refresh, rprops, tokenMap]);

  // Date group does not allow editing of display path or sort order.
  const readonly = groupType === GroupType.DATE;

  const onDisplayPathChange = useCallback(
    (display: string) => {
      doUpdate({ display });
    },
    [doUpdate]
  );

  const onSortOrderChange = useCallback(
    (sortOrderStr: string) => {
      // Must be an integer value.
      const sortOrderNum = Math.floor(Number(sortOrderStr));
      if (String(sortOrderNum) === sortOrderStr) {
        // Sort order is a number.
        doUpdate({ sortOrder: sortOrderNum });
      } else {
        // Sort order is a string.
        doUpdate({ sortOrder: sortOrderStr });
      }
    },
    [doUpdate]
  );

  const onMetaDescChange = useCallback(
    (metaDescription: string) => {
      doUpdate({ metaDescription });
    },
    [doUpdate]
  );

  return (
    <div>
      <FlexRow>
        <label htmlFor="display-path">
          <b>Display path:</b>
        </label>
        <FlexDiv>
          {dpPrefix}
          <EditableField
            id="display-path"
            disabled={processing}
            readonly={readonly}
            curValue={thisDp}
            onChange={onDisplayPathChange}
          />
        </FlexDiv>
      </FlexRow>
      <FlexRow>
        <label htmlFor="sort-order">
          <b>Sort order:</b>
        </label>
        <FlexDiv>
          <EditableField
            id="sort-order"
            disabled={processing}
            readonly={readonly}
            curValue={`${groupInfo.sortOrder ?? ""}`}
            onChange={onSortOrderChange}
          />
        </FlexDiv>
      </FlexRow>
      <FlexRow>
        <label htmlFor="meta-desc">
          <b>Meta description:</b>
        </label>
        <FlexDiv>
          <EditableField
            id="meta-desc"
            disabled={processing}
            readonly={readonly}
            curValue={`${groupInfo.metaDescription ?? ""}`}
            onChange={onMetaDescChange}
            multiLine={true}
          />
        </FlexDiv>
      </FlexRow>
      <FlexRow>
        <b>Visibility tokens:</b>
        <span>
          {visTokens.map((t) => tokenMap.get(t)?.display ?? t).join(", ")}
        </span>
      </FlexRow>
      {extension}
    </div>
  );
}
