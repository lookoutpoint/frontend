// SPDX-License-Identifier: MIT
import React from "react";
import { Folder, folderVisibilityToken } from "../../api/photoGroups";
import { RenderProps } from "../../components/Dialog";
import { VIS_TOKEN_ALL } from "../../api/visibility";
import { SettingsExtensionProps } from "./PhotoGroupSettings";
import { AdminSelfVisTokens } from "./AdminSelfVisTokens";

export const PhotoFolderSettingsExtension: React.FC<
  SettingsExtensionProps<Folder>
> = ({ rprops, groupInfo: folderInfo, tokenMap, refresh }) => {
  const selfVisTokens = (folderInfo.selfVisTokens ?? [])
    .filter((t) => t !== VIS_TOKEN_ALL)
    .sort();

  return (
    <>
      <AdminSelfVisTokens
        selfVisTokens={selfVisTokens}
        rprops={rprops}
        tokenMap={tokenMap}
        refresh={refresh}
        initTokenParams={/*target*/ ""}
        validateParams={(target: string) => {
          return target !== "";
        }}
        renderTokenParams={(idPrefix, rprops, params, onParamsChange) => (
          <TokenParams
            idPrefix={idPrefix}
            rprops={rprops}
            params={params}
            onParamsChange={onParamsChange}
          />
        )}
        deleteToken={(token, target) => {
          return folderVisibilityToken("delete", folderInfo.id, target, token);
        }}
        addToken={(token, target) => {
          return folderVisibilityToken("add", folderInfo.id, target, token);
        }}
      />
    </>
  );
};

interface TokenParamsProps {
  idPrefix: string;
  rprops: RenderProps;
  params: string;
  onParamsChange: (params: string) => void;
}

const TokenParams: React.FC<TokenParamsProps> = ({
  idPrefix,
  rprops,
  params,
  onParamsChange,
}) => {
  return (
    <>
      <label htmlFor={`${idPrefix}-target`}>Target:</label>
      <select
        id={`${idPrefix}-target`}
        value={params}
        disabled={rprops.processing}
        onChange={(e) => onParamsChange(e.target.value)}
      >
        <option value=""></option>
        <option value="folder">folder</option>
        <option value="photos">photos</option>
        <option value="fulltree">full tree</option>
      </select>
    </>
  );
};
