// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import {
  TimelineGroup,
  syncTimelineGroup,
  timelineGroupVisibilityToken,
  updateTimelineGroupFilter,
  deleteTimelineGroup,
} from "../../api/photoGroups";
import { CircleIconButton } from "../../components/CircleIconButton";
import { VIS_TOKEN_ALL } from "../../api/visibility";
import {
  FlexRow,
  FormButton,
  FormGrid,
  IndentedFlexRow,
  Row,
} from "../../components/Form";
import { SettingsExtensionProps } from "./PhotoGroupSettings";
import {
  FilterCode,
  FilterCodeRef,
  FilterTextArea,
} from "./GroupTimelineAdmin";
import { IconEdit } from "../../components/Icons";
import { AdminSelfVisTokens, RecurseTokenParam } from "./AdminSelfVisTokens";

function getFilterCode(timelineGroup: TimelineGroup): string {
  if (timelineGroup.unwrappedFilter) {
    return JSON.stringify(timelineGroup.unwrappedFilter, undefined, 2);
  } else {
    return "{}";
  }
}

export const GroupTimelineSettingsExtension: React.FC<
  SettingsExtensionProps<TimelineGroup>
> = ({ rprops, groupInfo: timelineGroup, tokenMap, refresh, close }) => {
  const selfVisTokens = (timelineGroup.selfVisTokens ?? [])
    .filter((t) => t !== VIS_TOKEN_ALL)
    .sort();

  const { processing, setProcessing, setError } = rprops;

  // Filter edit
  const [editFilterCode, setEditFilterCode] = useState(false);
  const [filterCode, setFilterCode] = useState(() =>
    getFilterCode(timelineGroup)
  );

  const onFilterTextAreaChange = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      setFilterCode(e.target.value);
    },
    []
  );

  const doFilterEditSave = useCallback(async () => {
    setProcessing(true);

    try {
      // Parse code as JSON.
      let newFilter: unknown;
      if (filterCode.trim().length > 0) {
        try {
          newFilter = JSON.parse(filterCode);
        } catch (e) {
          // Invalid JSON.
          setError(`Error: Invalid filter JSON. ${e}`);
          return;
        }
      }

      // Update filter.
      const status = await updateTimelineGroupFilter(
        timelineGroup.id,
        newFilter
      );
      if (status === true) {
        // Success. Timeline group has also been synced with new filter, so refresh.
        refresh();
        setError(null);
        setEditFilterCode(false);
      } else {
        // Error
        setError(`Error: Could not update filter. ${status}`);
      }
    } finally {
      setProcessing(false);
    }
  }, [filterCode, refresh, setError, setProcessing, timelineGroup.id]);

  const doFilterEditCancel = useCallback(() => {
    setFilterCode(getFilterCode(timelineGroup));
    setError(null);
    setEditFilterCode(false);
  }, [setError, timelineGroup]);

  // Timeline group sync
  const doSync = useCallback(async () => {
    setProcessing(true);

    const success = await syncTimelineGroup(timelineGroup.id);
    if (success) {
      refresh();
    } else {
      setError("Error: Could not sync.");
    }
    setProcessing(false);
  }, [refresh, setError, setProcessing, timelineGroup.id]);

  const doDelete = useCallback(async () => {
    setProcessing(true);

    const success = await deleteTimelineGroup(timelineGroup.id);
    if (success) {
      refresh();
      close();
    } else {
      setError("Error: Could not delete.");
    }
    setProcessing(false);
  }, [close, refresh, setError, setProcessing, timelineGroup.id]);

  return (
    <>
      {/* Filter */}
      <FlexRow>
        <b>Filter:</b>
        {!editFilterCode && (
          <CircleIconButton
            title="Edit"
            onClick={() => setEditFilterCode(true)}
          >
            <IconEdit />
          </CircleIconButton>
        )}
      </FlexRow>
      <IndentedFlexRow>
        {!editFilterCode && <FilterCode>{filterCode}</FilterCode>}
        {editFilterCode && (
          <>
            <FilterTextArea
              onChange={onFilterTextAreaChange}
              value={filterCode}
            />
            <FilterCodeRef />
          </>
        )}
      </IndentedFlexRow>
      {editFilterCode && (
        <IndentedFlexRow>
          <button disabled={processing} onClick={doFilterEditSave}>
            Save
          </button>
          <button disabled={processing} onClick={doFilterEditCancel}>
            Cancel
          </button>
        </IndentedFlexRow>
      )}
      {/* Date range and sync */}
      <Row>
        <b>Date range:</b>
      </Row>
      <IndentedFlexRow>
        <FormGrid>
          <span>Start date:</span>
          <span>{timelineGroup.startDate ?? "N/A"}</span>
          <span>End date:</span>
          <span>{timelineGroup.endDate ?? "N/A"}</span>
          <FormButton disabled={processing} onClick={doSync}>
            Sync
          </FormButton>
        </FormGrid>
      </IndentedFlexRow>
      <AdminSelfVisTokens
        selfVisTokens={selfVisTokens}
        rprops={rprops}
        tokenMap={tokenMap}
        refresh={refresh}
        initTokenParams={/*recurse*/ false}
        renderTokenParams={(idPrefix, rprops, params, onParamsChange) => (
          <RecurseTokenParam
            idPrefix={idPrefix}
            rprops={rprops}
            params={params}
            onParamsChange={onParamsChange}
          />
        )}
        deleteToken={(token, recurse) => {
          return timelineGroupVisibilityToken(
            "delete",
            timelineGroup.id,
            token,
            recurse
          );
        }}
        addToken={(token, recurse) => {
          return timelineGroupVisibilityToken(
            "add",
            timelineGroup.id,
            token,
            recurse
          );
        }}
      />
      <Row>
        <b>Delete:</b>
      </Row>
      <IndentedFlexRow>
        <button disabled={processing} onClick={doDelete}>
          Delete group (must have no children)
        </button>
      </IndentedFlexRow>
    </>
  );
};
