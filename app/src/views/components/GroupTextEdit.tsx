// SPDX-License-Identifier: MIT
import React, { useCallback, useLayoutEffect, useState } from "react";
import styled from "styled-components";
import { Group, GroupType, updateGroup } from "../../api/photoGroups";
import { parseRichText } from "../../helpers/richText";

const TextEditDiv = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;

  textarea {
    margin: 1em;
    max-width: var(--max-text-width);
    width: 100%;
    height: 20em;
  }

  padding: 0.5em 0;
  border-bottom: 1px solid var(--richtext-divider-color);
`;

const TextEditPadding = styled.div`
  background-color: var(--text-bg-color);
  // To compensate for the negative margin of the rich text display.
  padding-bottom: 1.5em;
`;

const TextEditButtons = styled.div`
  display: flex;
  justify-items: center;

  button {
    margin: 0 1em;
  }
`;

const TextEditError = styled.div`
  border: 1px solid var(--error-border-color);
  background-color: var(--error-bg-color);
  color: var(--error-text-color);
  padding: 0.5em;
`;

const Syntax = styled.div`
  line-height: 1.2;
  margin: 1em;
  max-width: var(--max-text-width);
`;

interface TextEditProps {
  groupType: GroupType;
  group: Group;
  close: () => void;
  refresh: () => void;
}

export const GroupTextEdit: React.FC<TextEditProps> = ({
  groupType,
  group,
  refresh,
  close,
}) => {
  const [text, setText] = useState(group.richText ?? "");
  const [processing, setProcessing] = useState(false);
  const [error, setError] = useState<string | null>(null);

  useLayoutEffect(() => {
    setText(group.richText ?? "");
  }, [group.richText]);

  const onChange = useCallback((e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value);
  }, []);

  const onApply = useCallback(async () => {
    setProcessing(true);
    setError(null);

    try {
      // Try to parse
      const doc = parseRichText(text);
      if (typeof doc === "string") {
        // Error
        setError(`Error: Parsing failed. ${doc}`);
        return;
      }

      const status = await updateGroup(groupType, group.id, {
        richText: text,
      });
      if (!status) {
        setError("Error: Could not update rich text.");
      } else {
        refresh();
      }
    } finally {
      setProcessing(false);
    }
  }, [group.id, groupType, refresh, text]);

  return (
    <TextEditPadding>
      <TextEditDiv>
        {error && <TextEditError>{error}</TextEditError>}
        <textarea value={text} onChange={onChange} />
        <Syntax>
          Syntax: <code>===== {"<header>"}</code>,{" "}
          <code>[[grid=#x#, ratio=#/#, maxw=#, maxh=#:photoIds...]]</code>,{" "}
          <code>* {"<paragraph>"}</code>,{" "}
          <code>|!caption=..., |!highlight, |...|...|</code>,{" "}
          <code>{"{{<url>:<desc>}}"}</code>
        </Syntax>
        <TextEditButtons>
          <button disabled={processing} onClick={onApply}>
            Apply
          </button>
          <button disabled={processing} onClick={close}>
            Close
          </button>
        </TextEditButtons>
      </TextEditDiv>
    </TextEditPadding>
  );
};
