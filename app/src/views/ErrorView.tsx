// SPDX-License-Identifier: MIT
import React, { useEffect, useRef } from "react";
import { Helmet } from "react-helmet-async";
import styled from "styled-components";
import { titleCase } from "../helpers/titleCase";
import { useErrorBoundary } from "react-error-boundary";
import { useLocation } from "react-router-dom";

export class NotFoundError {
  public what = "";

  constructor(what: string) {
    this.what = what;
  }
}

const Message = styled.div`
  text-align: center;
`;

interface Props {
  error: unknown;
}

export const ErrorView: React.FC<Props> = ({ error }) => {
  const location = useLocation();
  const { resetBoundary } = useErrorBoundary();

  const initialLoc = useRef(location);

  // If the location changes, reset the error boundary so that user can try to resume normal navigation (e.g. click Home logo).
  useEffect(() => {
    if (location !== initialLoc.current) {
      resetBoundary();
    }
  }, [location, resetBoundary]);

  let content: React.ReactNode;

  if (error instanceof NotFoundError) {
    content = (
      <>
        <p>Oops! Nothing to see here.</p>
        {error.what && <p>{titleCase(error.what)} not found.</p>}
      </>
    );
  } else {
    content = (
      <>
        <p>Oops! Something went wrong.</p>
        <p>{`${error}`}</p>
      </>
    );
  }

  return (
    <>
      <Helmet>
        <meta name="robots" content="noindex" />
      </Helmet>
      <Message>{content}</Message>
    </>
  );
};
