// SPDX-License-Identifier: MIT
import React, { useEffect, useRef, useState } from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { Dialog } from "../components/Dialog";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import { activateVisTokenLink } from "../api/visibility";
import { useVisibilityTokens } from "../hooks/useVisibilityTokens";

interface RouteParams {
  actValue: string;
  [key: string]: string | undefined;
}

const Message = styled.div`
  text-align: center;
`;

// Expected path: /vtactivate/<actValue>
export const VisTokenActivate: React.FC = () => {
  const { actValue } = useParams<RouteParams>();
  const vtokens = useVisibilityTokens();
  const navigate = useNavigate();

  const [message, setMessage] = useState("Validating...");

  const activated = useRef(false);

  useEffect(() => {
    // Only try to activate once.
    if (activated.current) return;

    let unmounted = false;

    (async () => {
      setMessage("Validating...");
      const redirect = await activateVisTokenLink(actValue!);
      if (unmounted) return;

      // Regardless of success or failure, consider activated so that we don't trigger the validation again
      // on the next render (which will happen because vtokens will change below).
      activated.current = true;

      if (redirect === null) {
        setMessage("Uh-oh - could not validate link.");
      } else {
        setMessage("Success! Redirecting in a moment...");

        // Redirect in one second
        setTimeout(() => {
          vtokens.onTokenActivated();
          navigate(redirect);
        }, 1000);
      }
    })();

    return () => {
      unmounted = true;
    };
  }, [actValue, navigate, vtokens]);

  return (
    <div>
      <Helmet>
        <title>{makeTitleString()}</title>
      </Helmet>
      <Dialog
        render={() => {
          return <Message>{message}</Message>;
        }}
      />
    </div>
  );
};
