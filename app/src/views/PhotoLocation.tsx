// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import {
  AppEvent,
  EventButton,
  EventContext,
  makeEventGroupID,
} from "../api/events";
import {
  Location,
  getGroupName,
  getLocationChildren,
  Group,
  GroupType,
  GROUP_PLURAL,
  GROUP_SEP,
} from "../api/photoGroups";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { useEventTracker } from "../hooks/useEventTracker";
import { GroupInfiniteScroll } from "../components/GroupInfiniteScroll";
import {
  IconCategoryHike,
  IconCategoryPark,
  IconLocations,
} from "../components/Icons";
import { useUserSession } from "../hooks/useUserSession";
import { GroupListQuery } from "../helpers/GroupListQuery";
import {
  LocationType,
  getLocFancyGroupName,
  stripAndGetLocationType,
  stripLocationPrefix,
} from "../helpers/locations";
import { regexRoute } from "../helpers/route";
import { titleCase } from "../helpers/titleCase";
import { CustomView, PhotoGroupList } from "./components/PhotoGroupList";
import { usePhotoGroupNavState } from "../hooks/usePhotoGroupNavState";
import { usePhotoListSizes } from "../hooks/usePhotoListSizes";
import styled from "styled-components";
import { LinkWithEvent } from "../components/LinkWithEvent";

const ParkLabelDiv = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
  padding: 1em 0;
  width: 100%;
  text-align: center;
  background-color: var(--park-label-bg-color);
`;

const ParkLabelLink = styled(LinkWithEvent)`
  color: var(--text-color);
  text-decoration: underline;
`;

const ParkLabelSubLinks = styled.div`
  padding-top: 0.2em;
  font-size: 0.9em;
  color: var(--lesser-text-color);

  ${ParkLabelLink} {
    color: var(--lesser-text-color);
  }
`;

export const PhotoLocation: React.FC<{ root?: boolean }> = ({ root }) => {
  const routeParams = useParams();
  const url = routeParams["*"] ?? "";

  if (root) {
    // Root folder.
    return regexRoute(url, [
      {
        // URLs: <empty>, @children/
        regex: /^(?:|@children\/|@parks\/|@hikes\/)$/,
        render: () => <PhotoLocationImpl locationSlug="/" />,
      },
    ]);
  } else {
    // Child location.
    return regexRoute(url, [
      {
        // URL: :locationSlug+/<more>
        // <more>:
        // <empty>
        // @children/
        // @info/
        // @map/
        // @parks/
        // @hikes/
        // @photos/:photoTitleSlug?/:photoId
        regex:
          /^((?:[^/@]+\/)+)(?:@children\/|@info\/|@map\/|@parks\/|@hikes\/|@photos\/(?:(?:[^/]+\/)?(.+))?)?$/,
        render: (m) => {
          const locationSlug = `/${m[1]}`;
          const photoId = m[2];

          return (
            <PhotoLocationImpl locationSlug={locationSlug} photoId={photoId} />
          );
        },
      },
    ]);
  }
};

interface ImplProps {
  locationSlug: string;
  photoId?: string;
}

const PhotoLocationImpl: React.FC<ImplProps> = ({
  locationSlug: locationPathSlug,
  photoId,
}) => {
  const session = useUserSession();

  useEventTracker(makeEventGroupID(GroupType.LOCATION, locationPathSlug));

  const groupUrl = `/${GROUP_PLURAL[GroupType.LOCATION]}/`;
  const [, navState, setNavState, setNavView] = usePhotoGroupNavState(
    {
      groupUrl,
      groupLabel: titleCase(GROUP_PLURAL[GroupType.LOCATION]),
      navUrlPrefix: `/${GroupType.LOCATION}/`,
      displaySep: GROUP_SEP[GroupType.LOCATION],
      adjustView: useCallback(
        (_link: string, view: string | undefined): string | undefined => {
          // These views are guaranteed to be valid on parent links and we want to preserve the view in these cases.
          if (view === "hikes" || view === "parks") return view;

          return undefined;
        },
        []
      ),
    },
    locationPathSlug
  );

  // Callback invoked when the group is available
  const onGroupInfo = useCallback(
    (group: Group) => {
      // Remove all location prefixes in the display string.
      const parts = group.display.split(GROUP_SEP[GroupType.LOCATION]);
      setNavState(
        locationPathSlug,
        parts.map(stripLocationPrefix).join(GROUP_SEP[GroupType.LOCATION])
      );
    },
    [locationPathSlug, setNavState]
  );

  // When the location changes, rebuild approximation.
  useEffect(() => {
    setNavState(locationPathSlug);
  }, [locationPathSlug, setNavState]);

  const getLocGroupName = useCallback(
    (g: Group): string =>
      stripLocationPrefix(getGroupName(GroupType.LOCATION, g)),
    []
  );

  const parentDepth = locationPathSlug.split("/").length - 2;
  const getFancyParkName = useCallback(
    // To give more context, show the parent location as long as that's deeper than the current location.
    (g: Group): JSX.Element => {
      const displayParts = g.display.split(GROUP_SEP[GroupType.LOCATION]);
      // During transition from one location to another, we may be querying a park location that is not a child of
      // the new parent, so in that case fall back to the simple single group name.
      if (displayParts.length <= parentDepth + 1) {
        return getLocFancyGroupName(displayParts[displayParts.length - 1]);
      } else {
        return (
          <>
            {getLocFancyGroupName(displayParts[displayParts.length - 1])}
            {", "}
            {getLocFancyGroupName(displayParts[displayParts.length - 2])}
          </>
        );
      }
    },
    [parentDepth]
  );

  const getFancyHikeName = useCallback(
    // To give more context, show the parent location as long as that's deeper than the current location
    // and the parent is not a park (the hikes view is already showing the park).
    (g: Group): JSX.Element => {
      const displayParts = g.display.split(GROUP_SEP[GroupType.LOCATION]);
      if (displayParts.length > 2 && displayParts.length > parentDepth + 1) {
        const { type } = stripAndGetLocationType(
          displayParts[displayParts.length - 2]
        );
        if (type !== LocationType.PARK) {
          return (
            <>
              {getLocFancyGroupName(displayParts[displayParts.length - 1])}
              {", "}
              {getLocFancyGroupName(displayParts[displayParts.length - 2])}
            </>
          );
        }
      }

      return getLocFancyGroupName(displayParts[displayParts.length - 1]);
    },
    [parentDepth]
  );

  // Custom query for parks.
  const [parksQuery, setParksQuery] = useState<GroupListQuery<Location> | null>(
    null
  );

  useEffect(() => {
    let unmounted = false;

    setParksQuery(null);

    const parksQuery = new GroupListQuery<Location>(
      async (limit, refId, refOp) => {
        return await getLocationChildren(session, {
          id: locationPathSlug,
          limit,
          refId,
          refOp,
          coverPhoto: true,
          maxRelDepth: -1,
          sortIgnoreDepth: true,
          extension: { type: "park" },
        });
      }
    );

    (async () => {
      try {
        const result = await parksQuery.batchAfter(undefined, 1);
        if (!unmounted && result && result.length > 0) {
          // Has parks.
          setParksQuery(parksQuery);
        }
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
      } catch (e) {
        /* empty */
      }
    })();

    return () => {
      unmounted = true;
    };
  }, [locationPathSlug, session]);

  // Custom query for hikes.
  const [hikesQuery, setHikesQuery] = useState<GroupListQuery<Location> | null>(
    null
  );

  useEffect(() => {
    let unmounted = false;

    setHikesQuery(null);

    const hikesQuery = new GroupListQuery<Location>(
      async (limit, refId, refOp) => {
        return await getLocationChildren(session, {
          id: locationPathSlug,
          limit,
          refId,
          refOp,
          coverPhoto: true,
          maxRelDepth: -1,
          sortIgnoreDepth: true,
          extension: { type: "hike", sort: "park" },
        });
      }
    );

    (async () => {
      try {
        const result = await hikesQuery.batchAfter(undefined, 1);
        if (!unmounted && result && result.length > 0) {
          // Has hikes.
          setHikesQuery(hikesQuery);
        }
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
      } catch (e) {
        /* empty */
      }
    })();

    return () => {
      unmounted = true;
    };
  }, [locationPathSlug, session]);

  // Custom views.
  const photoListSizes = usePhotoListSizes();

  const customViews: CustomView[] = useMemo(() => {
    const customViews: CustomView[] = [];

    if (parksQuery) {
      customViews.push({
        label: "Parks",
        urlId: "parks",
        btnContent: <IconCategoryPark />,
        eventObject: EventButton.LOCATION_PARKS,
        render: (hidden) => {
          return (
            <GroupInfiniteScroll
              groupListQuery={parksQuery}
              getGroupName={(g) => getGroupName(GroupType.LOCATION, g)}
              getFancyGroupName={getFancyParkName}
              makeGroupUrl={(g) => `/location${g.id}`}
              photoRatio={photoListSizes.groupPhotoRatio}
              desiredPhotoWidth={photoListSizes.groupDesiredPhotoWidth}
              hidden={hidden}
              events={{
                groupType: GroupType.LOCATION,
                context: EventContext.DEFAULT,
              }}
            />
          );
        },
      });
    }

    if (hikesQuery) {
      customViews.push({
        label: "Hikes",
        urlId: "hikes",
        btnContent: <IconCategoryHike />,
        eventObject: EventButton.LOCATION_HIKES,
        render: (hidden) => {
          return (
            <GroupInfiniteScroll
              groupListQuery={hikesQuery}
              getGroupName={(g) => getGroupName(GroupType.LOCATION, g)}
              getFancyGroupName={getFancyHikeName}
              makeGroupUrl={(g) => `/location${g.id}`}
              photoRatio={photoListSizes.groupPhotoRatio}
              desiredPhotoWidth={photoListSizes.groupDesiredPhotoWidth}
              hidden={hidden}
              getRowGroup={(g) => {
                // Look for a park ancestor in display name. Assume that there is only one.
                const displayParts = g.display.split(
                  GROUP_SEP[GroupType.LOCATION]
                );
                let i = 0;
                for (; i < displayParts.length; i++) {
                  const { type } = stripAndGetLocationType(displayParts[i]);
                  if (type === LocationType.PARK) {
                    break;
                  }
                }

                if (i === displayParts.length) {
                  // No park.
                  return undefined;
                }

                // Extract id.
                const idParts = g.id.split("/");
                const parkId = idParts.slice(0, i + 2).join("/") + "/";

                // If the park location is the same as the current location, no need to start a row group.
                if (locationPathSlug === parkId) return undefined;

                return JSON.stringify({
                  displayParts: displayParts.slice(0, i + 1),
                  idParts: idParts.slice(1, i + 2),
                });
              }}
              renderStartOfRowGroup={(rowGroup) => {
                const { displayParts, idParts } = JSON.parse(rowGroup);
                const elems: JSX.Element[] = [];
                const n = displayParts.length;
                for (let i = 0; i < n; ++i) {
                  const id = `/${idParts.slice(0, n - i).join("/")}/`;
                  if (id === locationPathSlug) break;
                  const display = displayParts[n - i - 1];

                  if (elems.length >= 2) {
                    elems.push(
                      <React.Fragment key={elems.length}>{", "}</React.Fragment>
                    );
                  }

                  elems.push(
                    <ParkLabelLink
                      key={id}
                      to={`/location${id}@hikes/`}
                      event={{
                        event: AppEvent.GROUP_CLICK,
                        object: `/location${id}`,
                        context: EventContext.DEFAULT,
                      }}
                    >
                      {getLocFancyGroupName(display)}
                    </ParkLabelLink>
                  );
                }
                return (
                  <ParkLabelDiv>
                    {elems[0]}
                    {elems.length > 1 && (
                      <ParkLabelSubLinks>{elems.slice(1)}</ParkLabelSubLinks>
                    )}
                  </ParkLabelDiv>
                );
              }}
              events={{
                groupType: GroupType.LOCATION,
                context: EventContext.DEFAULT,
              }}
            />
          );
        },
      });
    }

    return customViews;
  }, [
    getFancyHikeName,
    getFancyParkName,
    hikesQuery,
    locationPathSlug,
    parksQuery,
    photoListSizes.groupDesiredPhotoWidth,
    photoListSizes.groupPhotoRatio,
  ]);

  return (
    <div>
      <BreadcrumbNav
        nav={navState.navInfo}
        activeLabel={navState.activeLabel}
      />
      <PhotoGroupList
        groupId={locationPathSlug}
        groupType={GroupType.LOCATION}
        childrenIcon={<IconLocations />}
        getGroupName={getLocGroupName}
        getFancyGroupName={getFancyParkName}
        buildPhotoListFilter={useCallback(
          (groupId) => ({ locations: [groupId] }),
          []
        )}
        getChildren={getLocationChildren}
        lightboxPhotoId={photoId}
        onGroupInfo={onGroupInfo}
        onView={setNavView}
        customViews={customViews}
      />
    </div>
  );
};
