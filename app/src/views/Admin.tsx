// SPDX-License-Identifier: MIT
import React from "react";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { BreadcrumbNav } from "../components/BreadcrumbNav";
import { Dialog } from "../components/Dialog";
import { Link } from "react-router-dom";
import styled from "styled-components";

const AdminLink = styled(Link)`
  color: var(--text-color);
`;

export const Admin: React.FC = () => {
  return (
    <div>
      <Helmet>
        <title>{makeTitleString("Admin")}</title>
      </Helmet>
      <BreadcrumbNav nav={[]} activeLabel={"Admin"} />
      <Dialog
        render={() => {
          return (
            <>
              <p>
                <AdminLink to="/admin/events">Events</AdminLink>
              </p>
              <p>
                <AdminLink to="/admin/database">Database</AdminLink>
              </p>
              <p>
                <AdminLink to="/admin/groups">Groups</AdminLink>
              </p>
              <p>
                <AdminLink to="/admin/tokens">Tokens</AdminLink>
              </p>
              <p>
                <AdminLink to="/admin/pages/">Pages</AdminLink>
              </p>
            </>
          );
        }}
      />
    </div>
  );
};
