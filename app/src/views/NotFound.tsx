// SPDX-License-Identifier: MIT
import React from "react";
import { NotFoundError } from "./ErrorView";

interface Props {
  what: string;
}

export const NotFound: React.FC<Props> = ({ what }) => {
  throw new NotFoundError(what);
};
