// SPDX-License-Identifier: MIT
import Axios from "axios";

export const SERVICE_ROOT =
  process.env.NODE_ENV === "development"
    ? "https://lookoutpoint-dev.uc.r.appspot.com/s"
    : "/s";

export function makeTitleString(title?: string): string {
  return title ? `${title} - Lookout Point` : "Lookout Point";
}

export const axios = Axios.create({
  baseURL: SERVICE_ROOT,
  withCredentials: true,
});
