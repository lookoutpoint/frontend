// SPDX-License-Identifier: MIT
import React, { useLayoutEffect, useMemo, useRef, useState } from "react";
import styled from "styled-components";
import {
  addDays,
  differenceInCalendarDays,
  eachDayOfInterval,
  format,
  isAfter,
  isBefore,
  isEqual,
  startOfDay,
} from "date-fns";
import { getGroupName, GroupType, TimelineGroup } from "../api/photoGroups";
import { CroppedPhoto } from "./CroppedPhoto";
import { PhotoLink } from "./PhotoLink";
import { TimelineGroupOrderInfo } from "../helpers/TimelineGroup";
import { CircleIconButton } from "./CircleIconButton";
import { NotPublicIcon } from "./NotPublicIcon";
import { EventDesc } from "../helpers/EventTracker";
import { AppEvent, EventButton, makeEventGroupID } from "../api/events";
import { usePhotoViewEventRefCallback } from "../hooks/usePhotoViewEventRefCallback";
import { IconCollapse, IconExpand } from "./Icons";

const Container = styled.div`
  margin: 0.5em 0;
  padding: 0.5em;
  background-color: var(--timeline-bg-color);
  border-top: 1px solid var(--timeline-border-color);
  border-bottom: 1px solid var(--timeline-border-color);
`;

interface LayoutDay {
  date: Date;
  groups: (TimelineGroupOrderInfo | undefined)[];
}

interface LayoutState {
  containerWidth: number;
  maxDayWidth: number;

  // Full date range based on the root group.
  startDate: Date;
  endDate: Date;

  // Start-end date range that is displayed (which may be a subset of the full date
  // range when filtering around a group).
  displayStartDate: Date;
  displayEndDate: Date;
  days: LayoutDay[];

  daysPerRow: number;
  numRowGroups: number;
  rowsInRowGroup: number[];
}

function computeLayoutState(
  rootGroup: TimelineGroup,
  groups: TimelineGroupOrderInfo[],
  containerWidth: number,
  maxDayWidth: number,
  // Group to center the layout around.
  // Only need id (only have id when in the process of loading the group info)
  filterGroupId?: string
): LayoutState | null {
  // Determine date range.
  if (!rootGroup.startDate || !rootGroup.endDate) return null;

  const startDate = startOfDay(new Date(rootGroup.startDate));
  const endDate = startOfDay(new Date(rootGroup.endDate));
  const numDays = differenceInCalendarDays(endDate, startDate) + 1;
  if (numDays < 1) return null; // something is wrong, bail

  const maxDaysPerRow = Math.max(1, Math.round(containerWidth / maxDayWidth));

  // Prepare layout state.
  const layoutState: LayoutState = {
    containerWidth,
    maxDayWidth,
    startDate,
    endDate,
    displayStartDate: startDate,
    displayEndDate: endDate,
    days: [],
    daysPerRow: -1,
    numRowGroups: -1,
    rowsInRowGroup: [],
  };

  if (filterGroupId) {
    // Find filter group.
    const [filterGroupInfo] = groups.filter(
      (g) => g.group.id === filterGroupId
    );

    if (filterGroupInfo) {
      // Try to center filter group.
      let lStartDate = filterGroupInfo.startDate;
      let lEndDate = addDays(lStartDate, filterGroupInfo.numDays - 1);
      let lNumDays = differenceInCalendarDays(lEndDate, lStartDate) + 1;

      if (lNumDays > maxDaysPerRow) {
        // Need to trim.
        lEndDate = addDays(lStartDate, maxDaysPerRow - 1);
        lNumDays = maxDaysPerRow;
      }

      // Try to fill out the row while keeping the filtered group centered.
      while (lNumDays < maxDaysPerRow) {
        let modified = false;

        // Add a day after. Prefer this above a day before.
        if (isBefore(lEndDate, endDate)) {
          lEndDate = addDays(lEndDate, 1);
          lNumDays++;
          modified = true;
          if (lNumDays === maxDaysPerRow) break;
        }

        // Add a day before.
        if (isAfter(lStartDate, startDate)) {
          lStartDate = addDays(lStartDate, -1);
          lNumDays++;
          modified = true;
        }

        if (!modified) {
          // No more dates to add.
          break;
        }
      }

      layoutState.displayStartDate = lStartDate;
      layoutState.displayEndDate = lEndDate;
      layoutState.daysPerRow = lNumDays;
      layoutState.numRowGroups = 1;
    }
  } else {
    layoutState.daysPerRow = Math.min(maxDaysPerRow, numDays);
    layoutState.numRowGroups = Math.ceil(numDays / maxDaysPerRow);
  }

  layoutState.days = eachDayOfInterval({
    start: layoutState.displayStartDate,
    end: layoutState.displayEndDate,
  }).map((date) => ({
    date,
    groups: [],
  }));

  // Add each group to the layout.
  for (const group of groups) {
    addGroupToLayoutState(layoutState, group);
  }

  // Count number of rows in each row group.
  countRowsInRowGroups(layoutState);

  return layoutState;
}

function addGroupToLayoutState(
  layoutState: LayoutState,
  group: TimelineGroupOrderInfo
) {
  // Find group start offset from layout state display start date
  const dayOffset = differenceInCalendarDays(
    group.startDate,
    layoutState.displayStartDate
  );

  // Add group at the same index for each day, so find the max index across all days.
  let index = 0;
  for (let i = 0; i < group.numDays; ++i) {
    const di = dayOffset + i;
    if (di < 0 || di >= layoutState.days.length) continue;

    index = Math.max(index, layoutState.days[di].groups.length);
  }

  // Add group to each day at the found index.
  for (let i = 0; i < group.numDays; ++i) {
    const di = dayOffset + i;
    if (di < 0 || di >= layoutState.days.length) continue;

    const { groups } = layoutState.days[di];
    while (groups.length < index) {
      groups.push(undefined);
    }
    groups.push(group);
  }
}

function countRowsInRowGroups(layoutState: LayoutState) {
  let dayIndex = 0;
  for (let rgIdx = 0; rgIdx < layoutState.numRowGroups; ++rgIdx) {
    let maxRows = 0;
    for (
      let i = 0;
      i < layoutState.daysPerRow && dayIndex < layoutState.days.length;
      ++i, ++dayIndex
    ) {
      maxRows = Math.max(maxRows, layoutState.days[dayIndex].groups.length);
    }
    layoutState.rowsInRowGroup.push(maxRows);
  }
}

function collapse(layoutState: LayoutState, highlightGroupId?: string) {
  if (layoutState.numRowGroups === 0) return;

  const { daysPerRow } = layoutState;

  // If there is a highlight group, find the row that contains it. Otherwise,
  // filter for the first row.
  let targetRow = 0;

  if (highlightGroupId) {
    for (; targetRow < layoutState.numRowGroups; ++targetRow) {
      // Search days of this row.
      let found = false;
      for (
        let dayOff = 0, dayIdx = targetRow * daysPerRow;
        !found && dayOff < daysPerRow && dayIdx < layoutState.days.length;
        ++dayOff, ++dayIdx
      ) {
        for (const group of layoutState.days[dayIdx].groups) {
          if (!group) continue;
          if (group.group.id === highlightGroupId) {
            found = true;
            break;
          }
        }
      }

      if (found) {
        break;
      }
    }

    if (targetRow === layoutState.numRowGroups) {
      throw new Error("Highlight group not found");
    }
  }

  // Filter for target row.
  layoutState.days = layoutState.days.slice(
    targetRow * daysPerRow,
    Math.min(layoutState.days.length, (targetRow + 1) * daysPerRow)
  );
  layoutState.numRowGroups = 1;
  layoutState.rowsInRowGroup = layoutState.rowsInRowGroup.slice(
    targetRow,
    targetRow + 1
  );
}

interface Props {
  rootGroup: TimelineGroup;
  groups: TimelineGroupOrderInfo[];
  highlightGroupId?: string;
  timelineUrlPrefix: string;
  collapsed?: boolean;
  toggleCollapsed?: () => void;
  className?: string;
  eventContext?: string;
}

export const TimelineContainer: React.FC<Props> = (props) => {
  const { className, rootGroup, groups, toggleCollapsed, ...otherProps } =
    props;
  const { collapsed, highlightGroupId } = otherProps;

  // Basic layout state to determine the number of days per row.
  const [containerWidth, setContainerWidth] = useState(0);
  const containerRef = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    const onResize = () => {
      setContainerWidth(containerRef.current!.clientWidth);
    };

    // Initial
    onResize();

    const obsv = new ResizeObserver(onResize);
    obsv.observe(containerRef.current!);

    return () => obsv.disconnect();
  }, []);

  // Maximum size of day is derived from container width.
  // Goal is to try to fit at least three days in a row
  // (to enable prev, current, nav navigation).
  const maxDayWidth = Math.max(
    100,
    Math.min(175, Math.floor(containerWidth / 3))
  );

  // Layout of the timeline.
  const [layoutState, setLayoutState] = useState<LayoutState | null>(null);
  const [layoutCollapsable, setLayoutCollapsible] = useState<boolean>(false);
  useLayoutEffect(() => {
    if (containerWidth === 0) {
      // Nothing to do yet.
      setLayoutState(null);
      setLayoutCollapsible(false);
      return;
    }

    // The root group cannot actually be highlighted, so if the highlight group is the root group
    // (i.e. showing timeline for the root group), don't try to highlight that group.
    const adjustedHighlightGroupId =
      highlightGroupId === rootGroup.id ? undefined : highlightGroupId;

    // Compute layout state.
    let layoutState = computeLayoutState(
      rootGroup,
      groups,
      containerWidth,
      maxDayWidth,
      undefined
    );
    setLayoutCollapsible(layoutState ? layoutState.numRowGroups > 1 : false);

    if (collapsed && adjustedHighlightGroupId) {
      layoutState = computeLayoutState(
        rootGroup,
        groups,
        containerWidth,
        maxDayWidth,
        adjustedHighlightGroupId
      );
    }

    // Filter by collapsed state.
    if (collapsed && layoutState) {
      collapse(layoutState, adjustedHighlightGroupId);
    }

    setLayoutState(layoutState);
  }, [
    containerWidth,
    groups,
    rootGroup,
    collapsed,
    highlightGroupId,
    maxDayWidth,
  ]);

  return (
    <Container className={className}>
      {/* We want the container width to not include padding */}
      <div ref={containerRef}>
        {layoutState && (
          <TimelineView
            layoutState={layoutState}
            toggleCollapsed={layoutCollapsable ? toggleCollapsed : undefined}
            {...otherProps}
          />
        )}
      </div>
    </Container>
  );
};

function buildGridTemplateRows(
  rowsInRowGroup: number[],
  rowHeight: number
): string {
  const gridRows: string[] = [];

  for (const rowsInGroup of rowsInRowGroup) {
    gridRows.push("auto"); // for date labels

    // At least one row even if the row group is empty.
    const numRows = Math.max(1, rowsInGroup);

    // Give a bit more height if there are multiple rows, but not a full row's worth
    // as that makes the timeline too high.
    const heightPerRow = rowHeight / (numRows - (numRows - 1) * 0.3);

    for (let i = 0; i < Math.max(1, rowsInGroup); i++) {
      gridRows.push(`${heightPerRow}px`);
    }
  }

  return gridRows.join(" ");
}

const TimelineGridWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
`;

const TimelineGrid = styled.div<{
  $gridCols: number;
  $rowsInRowGroup?: number[];
  $daySizePx: number;
}>`
  display: grid;
  grid-template-columns: repeat(
    ${(p) => p.$gridCols},
    ${(p) => p.$daySizePx}px
  );
  grid-template-rows: ${(p) =>
    p.$rowsInRowGroup
      ? buildGridTemplateRows(p.$rowsInRowGroup, p.$daySizePx)
      : "auto"};
  row-gap: 0.5em;
`;

interface GridTitleProps {
  $numCols: number;
}

const GridTitle = styled.div<GridTitleProps>`
  grid-row: 1;
  grid-column: 1 / span ${(p) => p.$numCols};
  font-size: var(--fancy-title-size);
  font-style: italic;
  font-family: var(--fancy-font), "cursive";
  text-align: center;
`;

interface GridPosition {
  $gridRow: number;
  $gridCol: number;
}

const GridToggleSizeBtn = styled(CircleIconButton)<
  Pick<GridPosition, "$gridCol">
>`
  grid-row: 1;
  grid-column: ${(p) => `${p.$gridCol} / ${p.$gridCol}`};
  justify-self: end;
`;

const DateLabel = styled.div.attrs<GridPosition>((props) => ({
  style: {
    gridRow: props.$gridRow,
    gridColumn: props.$gridCol,
  },
}))<GridPosition>`
  padding: 0.25em;
  border-bottom: 2px solid var(--text-color);
`;

const DateCircle = styled.div.attrs<GridPosition>((props) => ({
  style: {
    gridRow: props.$gridRow,
    gridColumn: props.$gridCol,
  },
}))<GridPosition>`
  position: relative;
  left: -4px;
  top: calc(100% - 5px);
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background-color: var(--text-color);
`;

const DateCircleEnd = styled.div.attrs<GridPosition>((props) => ({
  style: {
    gridRow: props.$gridRow,
    gridColumn: props.$gridCol,
  },
}))<GridPosition>`
  position: relative;
  left: calc(100% - 4px);
  top: calc(100% - 5px);
  width: 8px;
  height: 8px;
  border-radius: 50%;
  border: 2px solid var(--text-color);
  background-color: var(--timeline-bg-color);
  box-sizing: border-box;
`;

const DateMoreNext = styled.div.attrs<GridPosition>((props) => ({
  style: {
    gridRow: props.$gridRow,
    gridColumn: props.$gridCol,
  },
}))<GridPosition>`
  position: relative;
  left: calc(100% - 8px);
  top: calc(100% - 5px);
  width: 8px;
  height: 8px;
  border: 2px solid var(--text-color);
  border-left: 0;
  border-bottom: 0;
  transform: rotate(45deg);
  box-sizing: border-box;
`;

const DateMorePrev = styled.div.attrs<GridPosition>((props) => ({
  style: {
    gridRow: props.$gridRow,
    gridColumn: props.$gridCol,
  },
}))<GridPosition>`
  position: relative;
  left: 0px;
  top: calc(100% - 5px);
  width: 8px;
  height: 8px;
  border: 2px solid var(--text-color);
  border-top: 0;
  border-right: 0;
  transform: rotate(45deg);
  box-sizing: border-box;
`;

interface TimelineGridProps extends Omit<Props, "rootGroup" | "groups"> {
  layoutState: LayoutState;
}

const TimelineView: React.FC<TimelineGridProps> = ({
  layoutState,
  timelineUrlPrefix,
  highlightGroupId,
  collapsed,
  toggleCollapsed,
  eventContext,
}) => {
  // Track cover photo visibility.
  const photoViewRefCallback = usePhotoViewEventRefCallback(
    AppEvent.PHOTO_VIEW_COVER,
    eventContext
  );

  const gridElems: JSX.Element[] = [];

  const numDays = layoutState.days.length;

  // For each row group.
  let gridRow = 1;
  for (let rgIdx = 0; rgIdx < layoutState.numRowGroups; ++rgIdx) {
    const startDayIdx = rgIdx * layoutState.daysPerRow;
    const daysInRow = Math.min(layoutState.daysPerRow, numDays - startDayIdx);

    // Generate date labels.
    for (
      let dayOff = 0, dayIdx = startDayIdx;
      dayOff < daysInRow;
      ++dayOff, ++dayIdx
    ) {
      const { date } = layoutState.days[dayIdx];

      let fmt: string;
      if (
        (rgIdx === 0 && dayOff === 0) ||
        (date.getMonth() === 0 && date.getDate() === 1)
      ) {
        // First date of the timeline or first day of new year
        fmt = "LLL do, yyyy";
      } else if (date.getDate() === 1) {
        // First date of a new month
        fmt = "LLL do";
      } else {
        fmt = "do";
      }

      // Figure out the appropriate begin and end type on the timeline.
      const circleEnd = isEqual(date, layoutState.endDate);
      const moreNext = dayOff === daysInRow - 1 && !circleEnd;
      const morePrev = dayOff === 0 && !isEqual(date, layoutState.startDate);
      const circle = !morePrev;

      gridElems.push(
        <React.Fragment key={`date${dayIdx}`}>
          <DateLabel $gridRow={gridRow} $gridCol={dayOff + 1}>
            {format(layoutState.days[dayIdx].date, fmt)}
          </DateLabel>
          {circle && <DateCircle $gridRow={gridRow} $gridCol={dayOff + 1} />}
          {circleEnd && (
            <DateCircleEnd $gridRow={gridRow} $gridCol={dayOff + 1} />
          )}
          {moreNext && (
            <DateMoreNext $gridRow={gridRow} $gridCol={dayOff + 1} />
          )}
          {morePrev && (
            <DateMorePrev $gridRow={gridRow} $gridCol={dayOff + 1} />
          )}
        </React.Fragment>
      );
    }
    gridRow++;

    // Generate timeline groups for this row group.
    const rowsInRowGroup = Math.max(1, layoutState.rowsInRowGroup[rgIdx]); // at least one row even if it is empty
    for (let rIdx = 0; rIdx < rowsInRowGroup; ++rIdx, ++gridRow) {
      const addGroup = (
        group: TimelineGroupOrderInfo,
        row: number,
        rowSpan: number,
        col: number,
        colSpan: number
      ) => {
        gridElems.push(
          <TimelineGroupBar
            key={`rg-${rgIdx}-bar-${group.group.id}`}
            $gridRow={row}
            rowSpan={rowSpan}
            $gridCol={col}
            colSpan={colSpan}
            group={group}
            urlPrefix={timelineUrlPrefix}
            highlight={group.group.id === highlightGroupId}
            photoViewRefCallback={photoViewRefCallback}
            eventContext={eventContext}
          />
        );
      };

      // Process the row for this row group.
      let prevGroup: TimelineGroupOrderInfo | undefined;
      let prevGroupOff = 0;
      let prevGroupRowSpan = 0;
      for (
        let dayOff = 0, dayIdx = startDayIdx;
        dayOff < daysInRow;
        ++dayOff, ++dayIdx
      ) {
        const group = layoutState.days[dayIdx].groups[rIdx];

        if (group !== prevGroup) {
          if (prevGroup !== undefined) {
            // Generate the element for the previous group.
            addGroup(
              prevGroup,
              gridRow,
              prevGroupRowSpan,
              prevGroupOff + 1,
              dayOff - prevGroupOff
            );
          }

          prevGroup = group;
          prevGroupOff = dayOff;
          prevGroupRowSpan = 1;

          let ri = rIdx + 1;
          for (
            ;
            ri < layoutState.days[dayIdx].groups.length &&
            layoutState.days[dayIdx].groups[ri] === undefined;
            ++ri
          );

          if (ri === layoutState.days[dayIdx].groups.length) {
            ri = rowsInRowGroup;
          }
          prevGroupRowSpan = ri - rIdx;
        }

        // If this day has no entries, insert empty day which will span all rows
        // (so only do this for the first row in the row group).
        if (layoutState.days[dayIdx].groups.length === 0 && rIdx === 0) {
          gridElems.push(
            <TimelineGroupEmptyDay
              key={`rg-${rgIdx}-day-${dayIdx}`}
              $gridRow={gridRow}
              rowSpan={rowsInRowGroup}
              $gridCol={dayOff + 1}
            />
          );
        }
      }

      if (prevGroup !== undefined) {
        // Generate the element for the last group.
        addGroup(
          prevGroup,
          gridRow,
          prevGroupRowSpan,
          prevGroupOff + 1,
          daysInRow - prevGroupOff
        );
      }
    }
  }

  // Determine size of each day in the grid.
  const daySizePx = Math.min(
    layoutState.containerWidth / layoutState.daysPerRow,
    layoutState.maxDayWidth
  );

  const sizeEventDesc = useMemo<EventDesc | undefined>(() => {
    if (eventContext === undefined) return undefined;
    return {
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.TIMELINE_SIZE,
      context: eventContext,
    };
  }, [eventContext]);

  return (
    <TimelineGridWrapper>
      {/* Use separate grid for title and elements so that there's no row gap after the title */}
      <TimelineGrid $gridCols={layoutState.daysPerRow} $daySizePx={daySizePx}>
        <GridTitle $numCols={layoutState.daysPerRow}>Timeline</GridTitle>
        {toggleCollapsed && (
          <GridToggleSizeBtn
            $gridCol={layoutState.daysPerRow}
            sizeEm={1.5}
            fontSizeEm={0.8}
            onClick={toggleCollapsed}
            title={collapsed ? "Expand" : "Collapse"}
            eventDesc={sizeEventDesc}
          >
            {collapsed ? <IconExpand /> : <IconCollapse />}
          </GridToggleSizeBtn>
        )}
      </TimelineGrid>
      <TimelineGrid
        $gridCols={layoutState.daysPerRow}
        $rowsInRowGroup={layoutState.rowsInRowGroup}
        $daySizePx={daySizePx}
      >
        {gridElems}
      </TimelineGrid>
    </TimelineGridWrapper>
  );
};

const TimelineGroupBarLink = styled(PhotoLink)`
  position: relative;
  margin: 0 0.25em;
  background-color: var(--caption-bg-color);
  display: block;
  overflow: hidden;

  &:hover {
    background-color: var(--caption-hover-bg-color);
  }
`;

const TimelineGroupBarHighlight = styled.div`
  position: relative;
  margin: 0 0.25em;
  background-color: var(--caption-bg-color);
  display: flex;
  flex-flow: column;
  border-radius: 0.5em;
  overflow: hidden;
`;

const TimelineGroupBarHighlightBorder = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  border-radius: 0.5em;
  overflow: hidden;
  /* Important so that it doesn't get clipped or obscure things underneath (from an events perspective). */
  pointer-events: none;
  z-index: 2;
  border: 3px solid var(--photolink-hover-border-color);
`;

const BarLabelWrapper = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--timelinebar-bg-color);
  z-index: 1;

  ${TimelineGroupBarLink}:hover &,
  ${TimelineGroupBarHighlight} & {
    background-color: var(--timelinebar-hover-bg-color);
  }
`;

const BarLabel = styled.div`
  padding: 0.5em;
  color: var(--timelinebar-color);
  text-align: center;

  ${TimelineGroupBarLink}:hover &,
  ${TimelineGroupBarHighlight} & {
    color: var(--timelinebar-hover-color);
  }

  @media (max-width: 500px) {
    padding: 0.278em;
    font-size: 0.9em;
  }
`;

const PrivateIcon = styled(NotPublicIcon)`
  color: var(--lesser-text-color);
  font-size: 0.8em;
  padding-right: 0.25em;

  ${TimelineGroupBarLink}:hover &,
  ${TimelineGroupBarHighlight} & {
    color: var(--timelinebar-hover-color);
  }
`;

interface TimelineGroupBarProps extends GridPosition {
  rowSpan: number;
  colSpan: number;
  group: TimelineGroupOrderInfo;
  urlPrefix: string;
  highlight?: boolean;
  photoViewRefCallback?: (elem: HTMLElement | null) => void;
  eventContext?: string;
}

const TimelineGroupBar: React.FC<TimelineGroupBarProps> = ({
  $gridRow: gridRow,
  $gridCol: gridCol,
  rowSpan,
  colSpan,
  group,
  urlPrefix,
  highlight,
  photoViewRefCallback,
  eventContext,
}) => {
  const timelineGroup = group.group;

  // Strip the first component of the id as that is the folder path.
  const groupPath = timelineGroup.id.slice(timelineGroup.id.indexOf("/", 1));

  const contents = (
    <>
      <BarLabelWrapper>
        <BarLabel>
          {getGroupName(GroupType.TIMELINE_GROUP, group.group)}
        </BarLabel>
        {!group.group.public && <PrivateIcon />}
      </BarLabelWrapper>

      {timelineGroup.coverPhoto && (
        <CroppedPhoto photo={timelineGroup.coverPhoto} autoResize />
      )}
    </>
  );
  const gridPos = {
    gridRow: `${gridRow} / span ${rowSpan}`,
    gridColumn: `${gridCol} / span ${colSpan}`,
  };

  const linkEvent = useMemo<EventDesc | undefined>(() => {
    if (eventContext === undefined) return undefined;
    return {
      event: AppEvent.GROUP_CLICK,
      object: makeEventGroupID(GroupType.TIMELINE_GROUP, timelineGroup),
      context: eventContext,
    };
  }, [eventContext, timelineGroup]);

  if (highlight) {
    return (
      <TimelineGroupBarHighlight
        style={gridPos}
        ref={photoViewRefCallback}
        data-photo-id={timelineGroup.coverPhoto?.id}
      >
        <TimelineGroupBarHighlightBorder />
        {contents}
      </TimelineGroupBarHighlight>
    );
  } else {
    return (
      <TimelineGroupBarLink
        to={`${urlPrefix}${groupPath}`}
        style={gridPos}
        event={linkEvent}
        refCallback={photoViewRefCallback}
        data-photo-id={timelineGroup.coverPhoto?.id}
      >
        {contents}
      </TimelineGroupBarLink>
    );
  }
};

const TimelineGroupBarEmpty = styled.div`
  margin: 0 0.25em;
  border: 1px solid var(--photolink-border-color);
  background-color: var(--caption-bg-color);
`;

interface TimelineGroupEmptyDayProps extends GridPosition {
  rowSpan: number;
}

const TimelineGroupEmptyDay: React.FC<TimelineGroupEmptyDayProps> = ({
  $gridRow: gridRow,
  $gridCol: gridCol,
  rowSpan,
}) => {
  return (
    <TimelineGroupBarEmpty
      style={{ gridRow: `${gridRow} / span ${rowSpan}`, gridColumn: gridCol }}
    />
  );
};
