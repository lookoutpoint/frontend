// SPDX-License-Identifier: MIT
import React, {
  CSSProperties,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { SERVICE_ROOT } from "../util";
import { FullPhoto } from "../api/photos";
import { isEqual } from "lodash-es";

interface Props extends React.ImgHTMLAttributes<HTMLImageElement> {
  photo: Pick<FullPhoto, "id" | "storeGen" | "width" | "height" | "title">;
  constrainWidth?: boolean;
  constrainHeight?: boolean;
  // Size constraints. In order of precendence.
  // Fix width/height: image will have the given size.
  fixWidth?: number;
  fixHeight?: number;
  // Max width/height: image will fit within the max size, irrespective of parent
  maxWidth?: number;
  maxHeight?: number;
  // Max parent width/height: will query parent and apply max limits
  maxParentWidth?: number;
  maxParentHeight?: number;
  parentLevel?: number; // defaults to 1 (look at immediate parent for constraints)

  autoResize?: boolean;
  resizedCallback?: (
    width: number,
    height: number,
    parentWidth: number,
    parentHeight: number
  ) => void;

  // Multiplies the computed image size by the given factor. Must be >= 1. Will also
  // not resize beyond the original image size. Defaults to 1.
  sizeMultiplier?: number;

  className?: string;
  style?: CSSProperties;
}

// Needs to be in sync with the backend.
const maxLengths = [300, 600, 1200, 2400, 4800];

const Img = styled.img`
  display: block;
`;

interface ResizeState {
  width: number;
  height: number;
  rzMaxLength: number;
}

function getParentElement(elem: HTMLElement, level: number): HTMLElement {
  for (let i = 0; i < level; i++) {
    elem = elem.parentElement!;
  }
  return elem;
}

export function ConstrainedPhoto(props: Props) {
  const {
    photo,
    constrainWidth,
    constrainHeight,
    fixWidth,
    fixHeight,
    maxWidth,
    maxHeight,
    maxParentWidth,
    maxParentHeight,
    autoResize = true,
    className,
    style,
    resizedCallback: onResize,
    sizeMultiplier = 1,
    parentLevel = 1,
    ...imgProps
  } = props;

  const imgRef = useRef<HTMLImageElement>(null);
  const [state, setState] = useState<ResizeState | null>(null);

  // Use a ref to track the onResize callback as changing the callback is not intended
  // to trigger a render.
  const onResizeRef = useRef(onResize);
  useEffect(() => {
    onResizeRef.current = onResize;
  }, [onResize]);

  // Track last onResize callback values so that callback is only invoked when values change.
  const lastOnResize = useRef({
    width: 0,
    height: 0,
    parentWidth: 0,
    parentHeight: 0,
  });

  const resize = useCallback(() => {
    // This can happen in some corner cases...
    if (!imgRef.current) return;

    // Master dimensions and ratio
    const [mWidth, mHeight] = [photo.width, photo.height];
    const mRatio = mWidth / mHeight;

    // Target dimensions starts off with the parent dimensions.
    const parentWidth = getParentElement(
      imgRef.current!,
      parentLevel
    ).clientWidth;
    const parentHeight = getParentElement(
      imgRef.current!,
      parentLevel
    ).clientHeight;

    let [tWidth, tHeight] = [
      fixWidth ?? maxWidth ?? Math.min(parentWidth, maxParentWidth ?? Infinity),
      fixHeight ??
        maxHeight ??
        Math.min(parentHeight, maxParentHeight ?? Infinity),
    ];

    // Increase target size by size multiplier.
    tWidth *= sizeMultiplier;
    tHeight *= sizeMultiplier;

    const tRatio = tWidth / tHeight;

    // Compute resized dimensions to fit target
    let rWidth: number, rHeight: number;
    if (constrainWidth && constrainHeight) {
      // Fit image into target width and height
      if (tRatio <= mRatio) {
        rWidth = Math.min(mWidth, tWidth);
        rHeight = rWidth / mRatio;
      } else {
        rHeight = Math.min(mHeight, tHeight);
        rWidth = rHeight * mRatio;
      }
    } else if (constrainWidth) {
      // Fit image into target width
      rWidth = Math.min(mWidth, tWidth);
      rHeight = tWidth / mRatio;
    } else if (constrainHeight) {
      // Fit image into target height
      rHeight = Math.min(mHeight, tHeight);
      rWidth = tHeight * mRatio;
    } else {
      throw new Error("No width or height constraint specified");
    }

    // Take longest length.
    const rMaxLength = Math.max(rWidth, rHeight);

    // Account for device pixel ratio.
    const dpr = window.devicePixelRatio || 1;
    const tMaxLength = rMaxLength * dpr;

    // Find the smallest max length >= rMaxLength.
    let maxLength: number | undefined;
    for (const ml of maxLengths) {
      if (ml >= tMaxLength) {
        maxLength = ml;
        break;
      }
    }

    if (maxLength === undefined) {
      // Use infinity to indicate use original image size.
      maxLength = Infinity;
    }

    setState((prevState) => {
      const newState = {
        width: rWidth,
        height: rHeight,
        // Don't downsize max length (let the browser resize down the current image).
        rzMaxLength: prevState
          ? Math.max(prevState.rzMaxLength, maxLength!)
          : maxLength!,
      };

      // If state is unchanged, return prevState to reflect no changes.
      if (isEqual(prevState, newState)) return prevState;
      return newState;
    });

    if (onResizeRef.current) {
      // Invoke onResize callback if the values are different.
      const newValues = {
        width: rWidth,
        height: rHeight,
        parentWidth,
        parentHeight,
      };
      if (!isEqual(lastOnResize.current, newValues)) {
        onResizeRef.current(
          newValues.width,
          newValues.height,
          newValues.parentWidth,
          newValues.parentHeight
        );
        lastOnResize.current = newValues;
      }
    }
  }, [
    photo.width,
    photo.height,
    fixWidth,
    maxWidth,
    parentLevel,
    maxParentWidth,
    fixHeight,
    maxHeight,
    maxParentHeight,
    sizeMultiplier,
    constrainWidth,
    constrainHeight,
  ]);

  // Auto-resize: use ResizeObserver to listen for resizing events on the relevant element.
  useEffect(() => {
    if (!autoResize) return;

    let rafId: number | undefined;

    const resizeObsv = new ResizeObserver(() => {
      // Only trigger resizing at most once per frame
      if (rafId === undefined) {
        rafId = window.requestAnimationFrame(() => {
          resize();
          rafId = undefined;
        });
      }
    });
    resizeObsv.observe(getParentElement(imgRef.current!, parentLevel));

    return () => {
      resizeObsv.disconnect();
    };
  }, [autoResize, parentLevel, resize]);

  // Initial resize. Use useLayoutEffect so to update before paint.
  useLayoutEffect(() => {
    resize();
  }, [resize]);

  const width = state?.width;
  const height = state?.height;
  const rzMaxLength = state?.rzMaxLength;

  return (
    <Img
      {...imgProps}
      ref={imgRef}
      className={className}
      style={style}
      width={fixWidth ?? width}
      height={fixHeight ?? height}
      src={
        rzMaxLength
          ? `${SERVICE_ROOT}/photos/photo/rz/${
              rzMaxLength === Infinity ? 0 : rzMaxLength
            }/${photo.id}/${photo.storeGen}.jpg`
          : undefined
      }
      alt={photo.title}
    />
  );
}
