// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { AppEvent, EventButton, EventContext, EventLink } from "../api/events";
import { GroupType, GROUP_PLURAL } from "../api/photoGroups";
import { EventDesc } from "../helpers/EventTracker";
import { titleCase } from "../helpers/titleCase";
import { CircleIconButton } from "./CircleIconButton";
import { useEventTracker } from "../hooks/useEventTracker";
import {
  IconBlog,
  IconCategories,
  IconCategoryAnimal,
  IconCategoryFlower,
  IconCategoryHike,
  IconCategoryPark,
  IconClose,
  IconCollections,
  IconDarkMode,
  IconDates,
  IconHome,
  IconKeywords,
  IconLicense,
  IconLightMode,
  IconLocal,
  IconLocations,
  IconLock,
  IconMenu,
  IconPerson,
  IconRecent,
  IconTravel,
} from "./Icons";
import { LinkWithEvent } from "./LinkWithEvent";
import { useTheme } from "../hooks/useTheme";
import { ThemeType } from "../helpers/Contexts";
import { useUserSession } from "../hooks/useUserSession";
import { useVisibilityTokens } from "../hooks/useVisibilityTokens";

const WrappedHeader = styled.header`
  text-align: center;
`;

const RootLink = styled(LinkWithEvent)`
  display: inline-block;
  font-size: 2em;
  font-family: var(--fancy-font), cursive;
  text-decoration: none;
  color: var(--text-color);
  padding: 0.25em 0;

  &:visited {
  }
  &:hover {
  }

  @media (max-device-height: 600px) {
    font-size: 25px;
  }
`;

const MenuButton = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  padding: 0.5em;
`;

const UserLinks = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  padding: 0.5em;
  display: flex;
  align-items: center;
  column-gap: 1em;
`;

const UserLink = styled(Link)`
  text-decoration: none;
  color: var(--text-color);

  &:visited,
  &:hover {
  }
`;

export const Header = React.memo(() => {
  const session = useUserSession();
  const theme = useTheme();
  const vtokens = useVisibilityTokens();
  const eventTracker = useEventTracker();

  const toggleTheme = useCallback(() => {
    eventTracker.addUXEvent({
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.SITE_THEME,
      context: EventContext.DEFAULT,
    });
    if (theme.type === ThemeType.DARK) {
      theme.setType(ThemeType.LIGHT);
    } else {
      theme.setType(ThemeType.DARK);
    }
  }, [eventTracker, theme]);

  const onShowOnlyPublic = useCallback(() => {
    vtokens.clearTokens();
  }, [vtokens]);

  return (
    <WrappedHeader>
      <Menu />
      <RootLink
        to="/"
        event={useMemo(
          () => ({
            event: AppEvent.LINK_CLICK,
            object: EventLink.HEADER_LOGO,
            context: EventContext.DEFAULT,
          }),
          []
        )}
      >
        lookout point
      </RootLink>
      <UserLinks>
        {session.isLoggedIn && (
          <>
            {session.isAdmin && <UserLink to="/admin">Admin</UserLink>}
            <UserLink to="/user/logout">Logout</UserLink>
          </>
        )}
        {vtokens.hasTokens && (
          <CircleIconButton
            title="Only show public content"
            onClick={onShowOnlyPublic}
          >
            <IconLock />
          </CircleIconButton>
        )}
        <CircleIconButton title="Toggle theme" onClick={toggleTheme}>
          {theme.type === ThemeType.DARK ? <IconDarkMode /> : <IconLightMode />}
        </CircleIconButton>
      </UserLinks>
    </WrappedHeader>
  );
});

const MenuWrapper = styled.div`
  position: absolute;
  left: 0.5em;
  top: 3em;
  background-color: var(--bg-color);
  border-radius: 10px;
  z-index: 100;
  display: grid;
  grid-template-columns: 1fr 1fr;
  box-shadow: 0 0 10px #888;
  // For border-radius
  overflow: hidden;

  & > *:nth-child(even) {
    border-left: 1px solid var(--text-color);
  }
`;

const Menu: React.FC = () => {
  const [showMenu, setShowMenu] = useState(false);

  // After menu is shown, click anywhere closes the menu.
  const clickListener = useRef<(() => void) | undefined>(undefined);
  useEffect(() => {
    if (showMenu && clickListener.current === undefined) {
      clickListener.current = () => {
        setShowMenu(false);
      };

      // Use RAF to register otherwise the click event that opens the menu can trigger the the
      // click listener and immediately hide the menu.
      window.requestAnimationFrame(() => {
        if (clickListener.current) {
          window.addEventListener("click", clickListener.current);
        }
      });
    } else if (!showMenu && clickListener.current) {
      window.removeEventListener("click", clickListener.current);
      clickListener.current = undefined;
    }

    return () => {
      if (clickListener.current) {
        window.removeEventListener("click", clickListener.current);
        clickListener.current = undefined;
      }
    };
  }, [showMenu]);

  useEffect(() => {
    if (showMenu) {
      const listener = (e: KeyboardEvent) => {
        if (showMenu && e.key === "Escape") {
          setShowMenu(false);
        }
      };

      window.addEventListener("keydown", listener);

      return () => window.removeEventListener("keydown", listener);
    }
  }, [showMenu]);

  const menuButtonEvent = useMemo<EventDesc>(
    () => ({
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.MENU,
      context: EventContext.DEFAULT,
    }),
    []
  );

  return (
    <>
      <MenuButton>
        <CircleIconButton
          title="Menu"
          onClick={() => setShowMenu(!showMenu)}
          eventDesc={menuButtonEvent}
        >
          {showMenu ? <IconClose /> : <IconMenu />}
        </CircleIconButton>
      </MenuButton>
      {showMenu && (
        <MenuWrapper>
          <MenuLink
            title="Home"
            link="/"
            icon={<IconHome />}
            event={AppEvent.LINK_CLICK}
          />
          <MenuLink
            title="Recent"
            link="/recent/"
            icon={<IconRecent />}
            event={AppEvent.LINK_CLICK}
          />
          <MenuLink
            title="Blogs"
            link="/blogs/"
            icon={<IconBlog />}
            event={AppEvent.LINK_CLICK}
          />
          <MenuLink
            title={titleCase(GROUP_PLURAL[GroupType.COLLECTION])}
            link={`/${GROUP_PLURAL[GroupType.COLLECTION]}/`}
            icon={<IconCollections />}
            event={AppEvent.GROUP_CLICK}
            eventObject={`/${GroupType.COLLECTION}/`}
          />
          <MenuLink
            title={titleCase(GROUP_PLURAL[GroupType.LOCATION])}
            link={`/${GROUP_PLURAL[GroupType.LOCATION]}/`}
            icon={<IconLocations />}
            event={AppEvent.GROUP_CLICK}
            eventObject={`/${GroupType.LOCATION}/`}
          />
          <MenuLink
            title={titleCase(GROUP_PLURAL[GroupType.CATEGORY])}
            link={`/${GROUP_PLURAL[GroupType.CATEGORY]}/`}
            icon={<IconCategories />}
            event={AppEvent.GROUP_CLICK}
            eventObject={`/${GroupType.CATEGORY}/`}
          />
          <MenuLink
            title={titleCase(GROUP_PLURAL[GroupType.KEYWORD])}
            link={`/${GROUP_PLURAL[GroupType.KEYWORD]}/`}
            icon={<IconKeywords />}
            event={AppEvent.GROUP_CLICK}
            eventObject={`/${GroupType.KEYWORD}/`}
          />
          {/* Column 2 */}
          <MenuLink
            title="Local"
            link={`/${GroupType.COLLECTION}/Local/`}
            icon={<IconLocal />}
            event={AppEvent.GROUP_CLICK}
          />
          <MenuLink
            title="Travel"
            link={`/${GroupType.COLLECTION}/Travel/`}
            icon={<IconTravel />}
            event={AppEvent.GROUP_CLICK}
          />
          <MenuLink
            title="Hikes"
            link={`/${GROUP_PLURAL[GroupType.LOCATION]}/@hikes/`}
            icon={<IconCategoryHike />}
            event={AppEvent.LINK_CLICK}
          />
          <MenuLink
            title="Parks"
            link={`/${GROUP_PLURAL[GroupType.LOCATION]}/@parks/`}
            icon={<IconCategoryPark />}
            event={AppEvent.LINK_CLICK}
          />
          <MenuLink
            title="Animals"
            link={`/${GroupType.CATEGORY}/animal/`}
            icon={<IconCategoryAnimal />}
            event={AppEvent.GROUP_CLICK}
          />
          <MenuLink
            title="Flowers"
            link={`/${GroupType.CATEGORY}/flower/`}
            icon={<IconCategoryFlower />}
            event={AppEvent.GROUP_CLICK}
          />
          <MenuLink
            title={titleCase(GROUP_PLURAL[GroupType.DATE])}
            link={`/${GROUP_PLURAL[GroupType.DATE]}/`}
            icon={<IconDates />}
            event={AppEvent.GROUP_CLICK}
            eventObject={`/${GroupType.DATE}/`}
          />
          <MenuLink
            title="About"
            link="/about/"
            icon={<IconPerson />}
            event={AppEvent.GROUP_CLICK}
          />
          <MenuLink
            title="Licensing"
            link="/licensing/"
            icon={<IconLicense />}
            event={AppEvent.GROUP_CLICK}
          />
        </MenuWrapper>
      )}
    </>
  );
};

interface MenuLinkProps {
  title: string;
  link: string;
  icon: React.ReactNode;
  event: AppEvent;
  eventObject?: string;
}

const StyledMenuLink = styled(LinkWithEvent)`
  padding: 1em;
  display: flex;
  flex-flow: row;
  column-gap: 0.5em;

  color: var(--text-color);

  &:hover {
    background-color: var(--caption-hover-bg-color);
    color: var(--caption-hover-color);
  }
`;

const MenuLink: React.FC<MenuLinkProps> = ({
  title,
  link,
  icon,
  event,
  eventObject,
}) => {
  const eventDesc = useMemo<EventDesc>(() => {
    return {
      event,
      object: eventObject ?? link,
      context: EventContext.MENU,
    };
  }, [event, eventObject, link]);

  return (
    <StyledMenuLink to={link} event={eventDesc}>
      {icon}
      <span>{title}</span>
    </StyledMenuLink>
  );
};
