// SPDX-License-Identifier: MIT
import React, { useCallback } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { EventDesc } from "../helpers/EventTracker";
import { useUXEventTrackerCallback } from "../hooks/useEventTracker";

export interface Props {
  onClick?: () => void;
  link?: string;
  toggle?: boolean;
  toggleActive?: boolean;
  title?: string;
  disabled?: boolean;
  background?: string;
  fontSizeEm?: number;
  sizeEm?: number; // button size, in em (default 2em)
  className?: string;
  eventDesc?: EventDesc;
  children?: React.ReactNode;
}

// Button dimensions work out be 2em x 2em
const Button = styled.div<{
  $toggle: boolean;
  $toggleActive: boolean;
  $disabled: boolean;
  $fontSizeEm: number;
  $sizeEm: number;
  $background?: string;
}>`
  display: block;
  text-decoration: none;
  text-align: center;
  font-size: ${(p) => p.$fontSizeEm}em;
  width: ${(p) => p.$sizeEm / p.$fontSizeEm}em;
  height: ${(p) => p.$sizeEm / p.$fontSizeEm}em;
  border-radius: 50%;
  line-height: ${(p) => p.$sizeEm / p.$fontSizeEm}em;
  cursor: ${(p) => (p.$disabled ? "auto" : "pointer")};

  &,
  & * {
    color: ${(p) => {
      if (p.$disabled) return "var(--iconbtn-disabled-color)";
      if (p.$toggleActive) return "var(--iconbtn-toggleActive-color)";
      return "var(--iconbtn-color)";
    }};
  }

  background-color: ${(p) => {
    if (p.$toggleActive) return "var(--iconbtn-toggleActive-bg-color)";
    if (p.$background) return p.$background;
    return "var(--iconbtn-bg-color)";
  }};

  &:active {
    background-color: ${(p) => {
      if (p.$disabled) {
        // Same as non-hover background color
        if (p.$background) return p.$background;
        return "var(--iconbtn-bg-color)";
      }
      if (!p.$toggle || !p.$toggleActive)
        return "var(--iconbtn-hover-bg-color)";
      return "var(--iconbtn-toggle-hover-bg-color)";
    }};
  }

  &:active,
  &:active * {
    color: ${(p) =>
      p.$disabled
        ? "var(--iconbtn-disabled-color)"
        : "var(--iconbtn-hover-color)"};
  }

  @media (hover: hover) {
    &:hover {
      background-color: ${(p) => {
        if (p.$disabled) {
          // Same as non-hover background color
          if (p.$background) return p.$background;
          return "var(--iconbtn-bg-color)";
        }
        if (!p.$toggle || !p.$toggleActive)
          return "var(--iconbtn-hover-bg-color)";
        return "var(--iconbtn-toggle-hover-bg-color)";
      }};
    }

    &:hover,
    &:hover * {
      color: ${(p) =>
        p.$disabled
          ? "var(--iconbtn-disabled-color)"
          : "var(--iconbtn-hover-color)"};
    }
  }
`;

const StyledLink = styled(Link)`
  width: 100%;
  height: 100%;
  display: block;
`;

export const DEFAULT_FONT_SIZE_EM = 1.2;
export const DEFAULT_SIZE_EM = 2;

export const CircleIconButton: React.FC<Props> = React.memo(
  ({
    className,
    toggle,
    toggleActive,
    disabled,
    background,
    link,
    title,
    fontSizeEm,
    sizeEm,
    onClick,
    eventDesc,
    children,
  }) => {
    const trackEvent = useUXEventTrackerCallback(eventDesc);

    const onClickWrapper = useCallback(
      (e: React.MouseEvent) => {
        if (onClick) {
          e.preventDefault();
          onClick();
          trackEvent();
        }
      },
      [onClick, trackEvent]
    );

    return (
      <Button
        $disabled={!!disabled}
        $toggle={!!toggle}
        $toggleActive={!!toggleActive}
        className={className}
        title={title}
        $background={background}
        $fontSizeEm={fontSizeEm ?? DEFAULT_FONT_SIZE_EM}
        $sizeEm={sizeEm ?? DEFAULT_SIZE_EM}
        onClick={disabled ? undefined : onClickWrapper}
      >
        {link && (
          <StyledLink to={link} onClick={trackEvent}>
            {children}
          </StyledLink>
        )}
        {!link && children}
      </Button>
    );
  }
);
