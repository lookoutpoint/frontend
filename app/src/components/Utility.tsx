// SPDX-License-Identifier: MIT

import { styled } from "styled-components";

export const CenteredText = styled.div`
  text-align: center;
`;

export const Error = styled.div`
  margin: 0.5em;
  border: 1px solid var(--error-border-color);
  background-color: var(--error-bg-color);
  color: var(--error-text-color);
  padding: 0.5em;
`;
