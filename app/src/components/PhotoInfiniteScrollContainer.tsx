// SPDX-License-Identifier: MIT
import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  useReducer,
  useLayoutEffect,
} from "react";
import styled from "styled-components";
import { NotPublicIcon } from "./NotPublicIcon";
import { ListQuery } from "../helpers/ListQuery";
import { useWindowSize } from "../hooks/useWindowSize";
import { PhotoLink } from "./PhotoLink";
import { ScrollToTop } from "./ScrollToTop";
import {
  OVERLAY_PADDING,
  OVERLAY_ROW_HEIGHT,
  PHOTO_PADDING,
} from "./PhotoConstants";
import { useEventListener } from "../hooks/useEventListener";
import { useProcessingIndicatorActions } from "../hooks/useProcessingIndicator";
import { AppEvent } from "../api/events";
import { useEventTracker } from "../hooks/useEventTracker";
import { usePhotoViewEventRefCallback } from "../hooks/usePhotoViewEventRefCallback";
import { debounce } from "lodash-es";

const Container = styled.div``;

// padding: makes photos at the begin and end of a row have the same total margin look as photos in the middle of the row
// white-space and overflow-x: no wrapping to prevent unwanted jankiness during resizing
// line-height: get precise heights
// display: flexbox seems to calculate stuff differently so that vertical scroll bars don't show up on images with fixed heights
//
// This is separate per container to deal with row groups.
const PhotoContainer = styled.div`
  margin: ${PHOTO_PADDING}px;
  display: flex;
  flex-flow: column nowrap;
  line-height: 1;
  overflow-x: hidden;
  white-space: nowrap;

  a {
    margin: ${PHOTO_PADDING}px;
  }
`;

// Rows get their own container so that when resizing we don't wrap the row into the next. It will get relayouted anyway
// the wrapping is jarring.
const PhotoContainerRow = styled.div`
  display: flex;
  flex-flow: row nowrap;
  overflow: hidden;
  justify-content: center;
`;

// In style below:
// - & img: must be block so that no line-height or anything shows up, which impacts height sizing
const PhotoWrapper = styled.div`
  line-height: 1;
  /* For items without an image */
  background-color: var(--caption-bg-color);
  /* Image may have fractional width or height but the wrapper will have an integer size, so hide any overflow. */
  overflow: hidden;

  & img {
    display: block;
  }
`;

// Wrapper for PhotoWrapper and PhotoOverlay, where they are arranged in column-wise.
export const PhotoAndOverlayWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

interface PhotoOverlayProps {
  $olWidth: number;
  $olRows: number;
}

// Photo overlay shows additional information about a photo.
const PhotoOverlay = styled.div.attrs<PhotoOverlayProps>((props) => ({
  style: {
    width: `calc(${props.$olWidth}px - ${OVERLAY_PADDING * 4}em)`,
    height: `calc(${props.$olRows * OVERLAY_ROW_HEIGHT}em)`,
  },
}))<PhotoOverlayProps>`
  padding: ${OVERLAY_PADDING * 2}em;
  background-color: var(--caption-bg-color);
  color: var(--caption-color);
  position: relative;

  ${PhotoAndOverlayWrapper}:hover & {
    background-color: var(--caption-hover-bg-color);
    color: var(--caption-hover-color);
    border-bottom-color: var(--caption-hover-bg-color);
  }

  border-bottom: 1px solid var(--photolink-border-color);
`;

// A single row of text in the photo overlay never wraps and any overflow should be rendered as "...".
export const PhotoOverlayRow = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  line-height: ${OVERLAY_ROW_HEIGHT};
`;

// Styles for individual rows of the photo overlay (title).
export const PhotoOverlayTitle = styled(PhotoOverlayRow)``;

const NotPublic = styled(NotPublicIcon)`
  position: absolute;
  top: 0.5em;
  right: 0.5em;
  color: var(--lesser-text-color);

  ${PhotoAndOverlayWrapper}:hover & {
    color: var(--caption-hover-color);
  }
`;

function useContainerWidth(
  ref: React.MutableRefObject<HTMLElement | null>,
  hidden: boolean
): number {
  const [containerWidth, setContainerWidth] = useState<number>(0);

  useLayoutEffect(() => {
    let unmounted = false;

    if (hidden || !ref.current) return;

    const container = ref.current;

    // Get the container width. Use getBoundingClientRect as the value may not be an integer
    // and rounding could throw off the photo row layout.
    //
    // Adjust width by the margin amounts.
    let lastW = container.getBoundingClientRect().width - PHOTO_PADDING * 2;
    setContainerWidth(lastW);

    // Debounce width updates because re-layouts are expensive.
    const updateWidth = debounce(() => {
      const w = container.getBoundingClientRect().width - PHOTO_PADDING * 2;
      if (!unmounted && lastW !== w) {
        setContainerWidth(w);
        lastW = w;
      }
    }, 30);

    // Listen for changes to the container width.
    const resizeObsv = new ResizeObserver(() => {
      updateWidth();
    });

    resizeObsv.observe(container);

    return () => {
      resizeObsv.disconnect();
      unmounted = true;
    };
  }, [hidden, ref]);

  return containerWidth;
}

interface Item {
  readonly id: string;
  readonly public: boolean;
}

interface RowInfo<T extends Item> {
  readonly lastIdx: number;
  readonly items: T[];
  readonly full: boolean;
  readonly startOfRowGroup?: string;
}

export interface PhotoInfiniteScrollContainerEvents<T extends Item> {
  context: string;
  photoView?: AppEvent;
  itemClick?: {
    event: AppEvent;
    getEventObject: (item: T) => string;
  };
}

interface Props<R extends RowInfo<T>, T extends Item, LQK extends keyof T> {
  hidden: boolean;
  listQuery: ListQuery<T, LQK>;
  queryForward: boolean;
  showOverlay: boolean;
  rowLimit?: number;
  layoutRows: (idxStart: number, items: T[], containerWidth: number) => R[];
  getOverlayRowCount: (item: T) => number;
  getRowHeight: (row: R, containerWidth: number) => number;
  getPhotoWidth: (row: R, idx: number, rowHeight: number) => number;
  buildOverlayContent: (item: T) => JSX.Element;
  renderPhoto: (
    row: R,
    idx: number,
    rowHeight: number
  ) => [JSX.Element, string | undefined];
  renderStartOfRowGroup?: (row: R) => JSX.Element | undefined;
  makeUrl?: (item: T) => string;

  // Return true to do default mouse click processing, false to prevent.
  onItemClick?: (item: T) => boolean;

  // Events
  events?: PhotoInfiniteScrollContainerEvents<T>;
}

export function PhotoInfiniteScrollContainer<
  R extends RowInfo<T>,
  T extends Item,
  LQK extends keyof T
>(props: Props<R, T, LQK>): JSX.Element {
  const {
    hidden,
    listQuery,
    queryForward,
    showOverlay,
    rowLimit,

    layoutRows,
    getOverlayRowCount,
    getRowHeight,
    getPhotoWidth,
    buildOverlayContent,
    renderPhoto,
    renderStartOfRowGroup,
    makeUrl,
    onItemClick: onItemClickImpl,
    events,
  } = props;

  const windowSize = useWindowSize();
  const procIndicatorActions = useProcessingIndicatorActions();
  const eventTracker = useEventTracker();

  // Reference to container element.
  const containerRef = useRef<HTMLDivElement | null>(null);
  const containerWidth = useContainerWidth(containerRef, hidden ?? false);

  // State holding all raw item data. A ref because it doesn't directly impact renders (need rows for that).
  const itemsRef = useRef<T[]>([]);

  // State holding the row layout. A ref because we want to control when rows are rendered (specifically
  // on listQuery change and on new rows). Also allows certain functions, such as fetchMoreIfNeeded callback,
  // to be independent of the row state.
  const rowsRef = useRef<R[]>([]);

  // rowsChanged is used to force a render when rows have been updated.
  const [, rowsChanged] = useReducer((a) => a + 1, 0);

  // If the list changes, reset the photos and rows.
  const prevListQueryAndDir = useRef({ listQuery, queryForward });
  useEffect(() => {
    if (
      listQuery !== prevListQueryAndDir.current.listQuery ||
      queryForward !== prevListQueryAndDir.current.queryForward
    ) {
      prevListQueryAndDir.current = { listQuery, queryForward };

      itemsRef.current = [];
      rowsRef.current = [];
      rowsChanged();
    }
  }, [listQuery, queryForward]);

  const setRows = useCallback(
    (rows: R[]) => {
      // If we have all of the photos, use the rows as-is.
      if (
        listQuery.complete &&
        rows.reduce((sum: number, row) => {
          return sum + row.items.length;
        }, 0) === listQuery.count
      ) {
        rowsRef.current = [...rows];
      }
      // Otherwise if the last row is not full, don't use it. It either needs to be filled
      // before we show it (by fetching more photos) or it is off-screen and we'll deal with filling
      // the row later.
      else {
        if (rows.length > 0 && !rows[rows.length - 1].full) {
          rowsRef.current = rows.slice(0, rows.length - 1);
        } else {
          rowsRef.current = [...rows];
        }
      }

      if (rowLimit && rowsRef.current.length > rowLimit) {
        rowsRef.current = rowsRef.current.slice(0, rowLimit);
      }

      rowsChanged();
    },
    [listQuery.complete, listQuery.count, rowLimit]
  );

  // Redo entire layout if certain props change.
  useLayoutEffect(() => {
    if (containerWidth <= 0) return;

    const rows = layoutRows(0, itemsRef.current, containerWidth);
    setRows(rows);
  }, [containerWidth, setRows, layoutRows]);

  /**
   * Infinite scroll and fetching.
   */

  // Variable to track if there is an in-flight request to fetch more photos.
  // Track which list query the in-flight request is for as the list query may change.
  const fetchInFlightQueryRef = useRef<ListQuery<T, LQK> | null>(null);

  // Track the container width via a ref, which is used by the async doFetch below.
  // This allows it to get the latest container width when the fetch is completed, at which point
  // the container width may have changed.
  const containerWidthRef = useRef(containerWidth);
  useLayoutEffect(() => {
    containerWidthRef.current = containerWidth;
  }, [containerWidth]);

  // Callback that checks if more photos should be fetched based on the current viewport and photos loaded
  // into the container.
  const fetchMoreIfNeeded = useCallback(() => {
    // If hidden or no width, don't do anything.
    if (hidden || containerWidth <= 0) return;

    // If the list query is complete and the gallery has all the photos, nothing to do.
    if (listQuery.complete && itemsRef.current.length === listQuery.count) {
      return;
    }

    const photoRows = rowsRef.current;

    // If the row limit has been reached, then we're done.
    if (rowLimit && photoRows.length >= rowLimit) {
      return;
    }

    // Check if the bottom of the container is below the viewport. We want sufficient margin to cover the case
    // where the last row is partially visible in the viewport.
    const container = containerRef.current!;
    const rect = container.getBoundingClientRect();
    const viewportHeight = windowSize.height;

    // We want the container bottom to exceed the viewport by at least the height of the last row.
    // For margin and pre-fetching purposes, we also want to pre-fetch the next screen's worth of photos,
    // which is based on the viewport height and the standard photo height.
    const lastRow =
      photoRows.length > 0 ? photoRows[photoRows.length - 1] : null;
    if (lastRow) {
      const delta = rect.top + rect.height - viewportHeight;
      if (
        delta > viewportHeight &&
        delta >= getRowHeight(lastRow, containerWidth)
      )
        return;
    }

    // Fetch more photos!
    // The async function takes the listQuery as a parameter as it'll fetch based on that
    // query and will only update if that remains the active query when the results are
    // returned.
    //
    // This is necessary to handle the case where the list query changes after a fetch
    // has been initiated. In such a scenario:
    // 1. We want to ignore the results of the fetch on the old list query.
    // 2. We want to fetch results based on the new query.
    const doFetch = async (listQuery: ListQuery<T, LQK>) => {
      const items = itemsRef.current;
      procIndicatorActions.start();

      const newItems = await listQuery[
        queryForward ? "batchAfter" : "batchBefore"
      ](
        items.length > 0
          ? items[items.length - 1][listQuery.sortKey]
          : undefined,
        listQuery.minFetchCount
      );

      // If the in-flight query tracking value is not the same as the list query
      // used in the fetch above, then discard the results as the list query has changed
      // (and there's another in-flight fetch happening for that).
      if (fetchInFlightQueryRef.current !== listQuery) {
        procIndicatorActions.stop();
        return;
      }

      // Fetch complete. Toggle flag. The setRows below may trigger an immediate render,
      // which will try to fetch more things and should be allowed to proceed.
      fetchInFlightQueryRef.current = null;
      if (newItems && newItems.length > 0) {
        // Append newly fetched items to existing groups array.
        items.push(...newItems);

        // Layout the new rows. Start with the item immediately after the last item in the last row
        // (which may be fetched from before but did not form a complete row).
        const newRows = layoutRows(
          lastRow ? lastRow.lastIdx + 1 : 0,
          items,
          // Important: use the ref to get the latest container width as this code is async
          // so the container width may have changed.
          containerWidthRef.current
        );
        setRows([...photoRows, ...newRows]);
      }

      procIndicatorActions.stop();
    };

    // If the list query is different (including if there is no in-flight query),
    // proceed with the fetch. If the query is different it will end up over-writing.
    if (fetchInFlightQueryRef.current !== listQuery) {
      fetchInFlightQueryRef.current = listQuery;
      doFetch(listQuery);
    }
  }, [
    hidden,
    containerWidth,
    listQuery,
    rowLimit,
    windowSize.height,
    getRowHeight,
    procIndicatorActions,
    queryForward,
    layoutRows,
    setRows,
  ]);

  // After every render, check if we should fetch more.
  useEffect(() => {
    // Fetch more photos, if needed.
    fetchMoreIfNeeded();
  });

  // Fetch more on scroll or resize.
  useEventListener(window, "scroll", fetchMoreIfNeeded);

  // On click handler for photos.
  const onItemClick = useCallback(
    (e: React.MouseEvent) => {
      // Use currentTarget to get the <a> (the target is the <img>)
      const { currentTarget } = e;
      const id = (currentTarget as HTMLElement).dataset.id!;
      const item = listQuery.getByIdSync(id)!;

      // Item click.
      if (onItemClickImpl) {
        if (!onItemClickImpl(item)) {
          e.preventDefault();
        }
      }

      // Click event.
      if (events && events.itemClick) {
        eventTracker.addUXEvent({
          event: events.itemClick.event,
          object: events.itemClick.getEventObject(item),
          context: events.context,
        });
      }
    },
    [listQuery, onItemClickImpl, events, eventTracker]
  );

  // Photo view event ref callback.
  const onItemRef = usePhotoViewEventRefCallback(
    events?.photoView,
    events?.context
  );

  const elems: JSX.Element[] = [];

  let curItemElems: JSX.Element[] = [];
  const wrapAndAddCurItemElems = () => {
    if (curItemElems.length === 0) return;
    elems.push(
      <PhotoContainer key={`pc-${elems.length}`}>{curItemElems}</PhotoContainer>
    );
    curItemElems = [];
  };

  for (const row of rowsRef.current) {
    if (renderStartOfRowGroup && row.startOfRowGroup !== undefined) {
      // Starting a new row group, so wrap the current set of items into a photo container and start a new one.
      wrapAndAddCurItemElems();

      elems.push(
        <React.Fragment key={`rg-${elems.length}`}>
          {renderStartOfRowGroup(row)}
        </React.Fragment>
      );
    }

    // Figure out the number of overlay rows in this row of photos, if overlay is being shown.
    let maxOverlayRows = 0;
    if (showOverlay) {
      for (const item of row.items) {
        const numOverlayRows = getOverlayRowCount(item);
        if (numOverlayRows > maxOverlayRows) {
          maxOverlayRows = numOverlayRows;
        }
      }
    }

    const overlayHeight =
      maxOverlayRows > 0
        ? OVERLAY_PADDING * 4 + maxOverlayRows * OVERLAY_ROW_HEIGHT
        : 0;
    const rowHeight = getRowHeight(row, containerWidth);

    const rowItems: React.ReactElement[] = [];
    for (let i = 0; i < row.items.length; i++) {
      const item = row.items[i];
      const photoWidth = getPhotoWidth(row, i, rowHeight);
      const wrapperStyle = {
        width: `${photoWidth}px`,
        height: `${rowHeight}px`,
      };

      let photoOverlay: JSX.Element | null = null;
      if (showOverlay) {
        photoOverlay = (
          <PhotoOverlay $olWidth={photoWidth} $olRows={maxOverlayRows}>
            {buildOverlayContent(item)}
            {!item.public && <NotPublic />}
          </PhotoOverlay>
        );
      }

      // Build photo item.
      const [renderedPhoto, renderedPhotoID] = renderPhoto(row, i, rowHeight);
      rowItems.push(
        <PhotoLink
          key={item.id}
          to={makeUrl ? makeUrl(item) : undefined}
          data-id={item.id}
          data-photo-id={renderedPhotoID}
          onClick={onItemClick}
          refCallback={onItemRef}
          style={{
            height:
              "calc(" +
              (Math.floor(rowHeight) + 1) + // +1 for overlay bottom border
              `px + ${overlayHeight}em)`,
          }}
        >
          <PhotoAndOverlayWrapper>
            {photoOverlay}
            <PhotoWrapper style={wrapperStyle}>{renderedPhoto}</PhotoWrapper>
          </PhotoAndOverlayWrapper>
        </PhotoLink>
      );
    }

    curItemElems.push(
      <PhotoContainerRow key={`row-${curItemElems.length}`}>
        {rowItems}
      </PhotoContainerRow>
    );
  }
  wrapAndAddCurItemElems();

  return (
    <Container ref={containerRef}>
      {elems}
      {/* If row limit is set, assume we don't want scroll to top */}
      {rowLimit === undefined && <ScrollToTop eventContext={events?.context} />}
    </Container>
  );
}
