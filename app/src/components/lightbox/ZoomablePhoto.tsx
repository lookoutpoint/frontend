// SPDX-License-Identifier: MIT
import React, {
  CSSProperties,
  useCallback,
  useImperativeHandle,
  useLayoutEffect,
  useState,
} from "react";
import { AppEvent, EventContext, makeEventPhotoID } from "../../api/events";
import { FullPhoto } from "../../api/photos";
import { ConstrainedPhoto } from "../ConstrainedPhoto";
import { useEventTracker } from "../../hooks/useEventTracker";
import { useWindowSize } from "../../hooks/useWindowSize";
import { isEqual } from "lodash-es";
import { ZoomablePhotoState, ZoomablePhotoActions } from "./ExternalInterface";

// Internal state with sizes of photo and parent container.
interface SizeState {
  width: number;
  height: number;
  parentWidth: number;
  parentHeight: number;
}

interface PanPoint {
  x: number;
  y: number;
  t: number; // milliseconds
}

interface PanOrigin {
  x: number;
  y: number;
  offsetX: number;
  offsetY: number;
}

function addAndTrimPanPoints(
  pts: PanPoint[],
  x: number,
  y: number
): PanPoint[] {
  const TIME_THRESHOLD = 250; // milliseconds
  const now = performance.now();
  let i = 0;
  while (i < pts.length && now - pts[i].t > TIME_THRESHOLD) {
    i++;
  }

  pts = pts.slice(i);
  pts.push({ x, y, t: now });
  return pts;
}

interface Props extends React.ImgHTMLAttributes<HTMLImageElement> {
  photo: Pick<FullPhoto, "id" | "storeGen" | "width" | "height" | "title">;
  // External state.
  state: ZoomablePhotoState;
  // External state modifiers.
  setState: React.Dispatch<React.SetStateAction<ZoomablePhotoState>>;

  // Additional styling.
  className?: string;
  style?: CSSProperties;
}

export const ZoomablePhoto = React.forwardRef<ZoomablePhotoActions, Props>(
  (
    { photo, state, setState, className, style: styleProp, ...imgProps },
    ref
  ) => {
    // Track zoom event.
    const eventTracker = useEventTracker();

    // Make a copy for custom modifications.
    const style = { ...styleProp };

    // Track photo and parent sizes.
    const [sizes, setSizes] = useState<SizeState>({
      width: 0,
      height: 0,
      parentWidth: 0,
      parentHeight: 0,
    });

    const updateState = useCallback(
      (sizes: SizeState, newState?: ZoomablePhotoState) => {
        if (sizes.width === 0 || sizes.height === 0) {
          if (newState) setState(newState);
          return;
        }

        setState((curState) => {
          // Legalize offsets based on the new state.
          if (newState === undefined) newState = { ...curState };

          // Change in zoom.
          let zoomRatio = newState.zoom / curState.zoom;

          // Compute new photo size based on change in zoom.
          if (sizes.width * zoomRatio > photo.width) {
            // Can only zoom to full photo width.
            zoomRatio = photo.width / sizes.width;
          }
          if (sizes.height * zoomRatio > photo.height) {
            // Can only zoom to full photo height.
            zoomRatio = photo.height / sizes.height;
          }

          const newPhotoWidth = sizes.width * zoomRatio;
          const newPhotoHeight = sizes.height * zoomRatio;

          const maxOffsetX = Math.max(
            0,
            (newPhotoWidth - sizes.parentWidth) / (2 * newPhotoWidth)
          );
          const maxOffsetY = Math.max(
            0,
            (newPhotoHeight - sizes.parentHeight) / (2 * newPhotoHeight)
          );

          newState.offsetX = Math.max(
            -maxOffsetX,
            Math.min(maxOffsetX, newState.offsetX)
          );
          newState.offsetY = Math.max(
            -maxOffsetY,
            Math.min(maxOffsetY, newState.offsetY)
          );

          if (!isEqual(curState, newState)) {
            return newState;
          } else {
            return curState;
          }
        });

        // Track zoom event.
        if (newState && newState.zoom > 1) {
          eventTracker.addEvent({
            event: AppEvent.PHOTO_VIEW_ZOOM,
            object: makeEventPhotoID(photo),
            context: EventContext.DEFAULT,
          });
        }
      },
      [eventTracker, photo, setState]
    );

    const onResize = useCallback(
      (
        width: number,
        height: number,
        parentWidth: number,
        parentHeight: number
      ) => {
        const newSizes: SizeState = {
          width,
          height,
          parentWidth,
          parentHeight,
        };
        setSizes(newSizes);
      },
      []
    );

    // Update state and offsets when sizes change.
    useLayoutEffect(() => {
      updateState(sizes);
    }, [sizes, updateState]);

    // At max zoom when photo size is the same as the photo.
    const atMaxZoom =
      sizes.width === photo.width && sizes.height === photo.height;
    if (state.atMaxZoom !== atMaxZoom) {
      setState({ ...state, atMaxZoom });
    }

    // Pan.
    const [panOrigin, setPanOrigin] = useState<PanOrigin | undefined>(
      undefined
    );
    const [panPoints, setPanPoints] = useState<PanPoint[]>([]);

    const windowSize = useWindowSize();

    const addPanPointToState = useCallback((x: number, y: number) => {
      setPanPoints((pts) => {
        return addAndTrimPanPoints(pts, x, y);
      });
    }, []);

    const onPointerDown = useCallback(
      (e: React.PointerEvent) => {
        if (e.button !== 0) return;

        if (state.zoom > 1) {
          setPanOrigin({
            x: e.screenX,
            y: e.screenY,
            offsetX: state.offsetX,
            offsetY: state.offsetY,
          });
          addPanPointToState(e.screenX, e.screenY);
          setState((s) => ({ ...s, animateOffsetDuration: undefined }));
        }
        (e.target as HTMLElement).setPointerCapture(e.pointerId);
        e.preventDefault();
      },
      [addPanPointToState, setState, state.offsetX, state.offsetY, state.zoom]
    );

    const onPointerUp = useCallback(
      (e: React.PointerEvent) => {
        if (e.button !== 0) return;
        if (panOrigin === undefined) return;

        // Momentum scroll. Determine velocity of the scroll.
        const momentumPts = addAndTrimPanPoints(
          panPoints,
          e.screenX,
          e.screenY
        );
        if (momentumPts.length >= 2) {
          // Determine velocity of the panning operation.
          const first = momentumPts[0];
          const last = momentumPts[momentumPts.length - 1];
          let dx = last.x - first.x;
          let dy = last.y - first.y;
          const dt = (last.t - first.t) / 1000; // seconds
          const velocity = Math.sqrt(dx * dx + dy * dy) / dt;
          const minVelocity = Math.min(windowSize.width, windowSize.height);

          if (velocity > minVelocity) {
            // Compute additional momentum scroll amount.
            dx = Math.pow(Math.abs(dx), 1.2) * Math.sign(dx);
            dy = Math.pow(Math.abs(dy), 1.2) * Math.sign(dy);

            // Convert to offset percentage.
            dx = dx / sizes.width;
            dy = dy / sizes.height;

            // Don't want duration to last too long.
            const duration =
              Math.log(velocity / minVelocity) / Math.log(40) + 0.3;

            updateState(sizes, {
              ...state,
              offsetX: state.offsetX + dx,
              offsetY: state.offsetY + dy,
              // Animate to make the panning feel natural.
              animateOffsetDuration: duration,
            });

            // Clear the animate flag once the transition is complete.
            window.setTimeout(() => {
              setState((s) => ({ ...s, animateOffsetDuration: undefined }));
            }, duration * 1000 + 100);
          }
        }

        setPanOrigin(undefined);
        (e.target as HTMLElement).releasePointerCapture(e.pointerId);
      },
      [
        panOrigin,
        panPoints,
        setState,
        sizes,
        state,
        updateState,
        windowSize.height,
        windowSize.width,
      ]
    );

    const onPointerMove = useCallback(
      (e: React.PointerEvent) => {
        if (panOrigin === undefined) return;

        // Delta in pixels.
        let dx = e.screenX - panOrigin.x;
        let dy = e.screenY - panOrigin.y;

        // As offset percentage.
        dx = dx / sizes.width;
        dy = dy / sizes.height;

        updateState(sizes, {
          ...state,
          offsetX: panOrigin.offsetX + dx,
          offsetY: panOrigin.offsetY + dy,
        });
        addPanPointToState(e.screenX, e.screenY);

        e.preventDefault();
      },
      [addPanPointToState, panOrigin, sizes, state, updateState]
    );

    useImperativeHandle(
      ref,
      () => ({
        zoomIn: () => {
          updateState(sizes, { ...state, zoom: state.zoom * 2 });
        },

        zoomOut: () => {
          updateState(sizes, { ...state, zoom: Math.max(1, state.zoom / 2) });
        },
      }),
      [sizes, state, updateState]
    );

    // Manual style.
    style.transform = `translateX(${state.offsetX * 100 - 50}%) translateY(${
      state.offsetY * 100 - 50
    }%)`;
    if (state.animateOffsetDuration) {
      style.transition = `transform ease-out ${state.animateOffsetDuration}s`;
    }

    if (state.zoom > 1) {
      style.cursor = "grab";
    }

    return (
      <ConstrainedPhoto
        {...imgProps}
        className={className}
        style={style}
        photo={photo}
        sizeMultiplier={state.zoom}
        resizedCallback={onResize}
        onPointerDown={onPointerDown}
        onPointerUp={onPointerUp}
        onPointerMove={onPointerMove}
        parentLevel={1}
        constrainWidth
        constrainHeight
      />
    );
  }
);
