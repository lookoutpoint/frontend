// SPDX-License-Identifier: MIT
import React from "react";
import styled from "styled-components";
import {
  FullPhoto,
  PhotoGroup,
  PhotoGroupParts,
  PHOTO_GROUP_DISPLAY,
  PHOTO_GROUP_ID,
  splitPhotoGroup,
  PartialFullPhoto,
} from "../../api/photos";
import { titleCase } from "../../helpers/titleCase";
import { NotPublicIcon } from "../NotPublicIcon";
import { ExternalLink } from "../ExternalLink";
import { GroupType } from "../../api/photoGroups";
import {
  IconCamera,
  IconCollection,
  IconCopyright,
  IconDate,
  IconDescription,
  IconHike,
  IconKeywords,
  IconLocation,
  IconPark,
} from "../Icons";
import { getCategoryIcon } from "../../helpers/Icons";
import { stripAndGetLocationType, LocationType } from "../../helpers/locations";
import { LinkWithEvent } from "../LinkWithEvent";
import { AppEvent } from "../../api/events";
import { EventDesc } from "../../helpers/EventTracker";

interface Props {
  photo: PartialFullPhoto;
}

const SidebarGrid = styled.div`
  display: grid;
  grid-template-columns: min-content minmax(0, 1fr);
  grid-template-rows: auto;
  row-gap: 1em;
  column-gap: 0.5em;

  line-height: 1.7;
  i {
    line-height: 1.7;
  }

  a {
    color: var(--text-color);
    text-decoration: underline;
    text-decoration-color: var(--lesser-text-color);
  }
  a:hover {
    text-decoration-color: var(--text-color);
  }
`;

const FlexList = styled.span`
  display: inline-flex;
  flex-flow: row wrap;
  column-gap: 0.75em;
`;

export const Sidebar: React.FC<Props> = ({ photo }) => {
  return (
    <SidebarGrid>
      <Location photo={photo} />
      <Description photo={photo} />
      <Categories photo={photo} />
      <Folder photo={photo} />
      <Keywords photo={photo} />
      <DateTime photo={photo} />
      <CameraInfo photo={photo} />
      <CopyrightUsage photo={photo} />
      <NotPublic photo={photo} />
    </SidebarGrid>
  );
};

// Uniqifies the list of PhotoGroups by id, maintaining order of first appearance.
function uniqifyPhotoGroupList(list: PhotoGroup[]): PhotoGroup[] {
  const uniqList: PhotoGroup[] = [];
  const idSet = new Set<string>();

  for (const item of list) {
    if (idSet.has(item[PHOTO_GROUP_ID])) continue;
    idSet.add(item[PHOTO_GROUP_ID]);
    uniqList.push(item);
  }

  return uniqList;
}

const Location: React.FC<{
  photo: Pick<PartialFullPhoto, "id" | "locations">;
}> = ({ photo }) => {
  if (photo.locations === undefined || photo.locations.length === 0)
    return null;

  // Location is a comma-separated string. May have hikes and parks embedded.
  let hikes: PhotoGroup[] = [];
  let parks: PhotoGroup[] = [];

  // Strip off any trailing parks or hikes, which are added to their own respective lists.
  const stripTrailingSpecialLocs = (
    partsIn: PhotoGroupParts
  ): PhotoGroupParts => {
    const parts = structuredClone(partsIn);

    while (parts.display.length > 0) {
      const i = parts.display.length - 1;
      const { display, type } = stripAndGetLocationType(parts.display[i]);
      if (type === LocationType.OTHER) {
        // Stop, no more trailing special locs.
        break;
      }

      // Special. Trim and store in special category.
      if (type === LocationType.PARK) {
        parks.push([parts.slug.slice(0, i + 1).join("/"), display]);
      } else if (type === LocationType.HIKE) {
        hikes.push([parts.slug.slice(0, i + 1).join("/"), display]);
      } else {
        throw new Error("Unrecognized location type");
      }

      parts.display.pop();
      parts.slug.pop();
    }

    return parts;
  };

  const partsToGroups = (
    parts: PhotoGroupParts,
    startIdx: number
  ): PhotoGroup[] => {
    // For each part, build the id and display. Look for any special location types.
    const group: PhotoGroup[] = [];
    for (let i = startIdx; i < parts.slug.length; ++i) {
      const { display, type } = stripAndGetLocationType(parts.display[i]);
      const id = parts.slug.slice(0, i + 1).join("/");
      group.push([id, display]);

      if (type === LocationType.PARK) {
        parks.push([id, display]);
      } else if (type === LocationType.HIKE) {
        hikes.push([id, display]);
      }
    }
    return group;
  };

  // First location is primary and will be shown in full.
  // primarySplitLoc will be the unmodified primary location, used as a reference for secondary locations.
  const primaryParts = splitPhotoGroup(photo.locations[0], ",");

  // Strip trailing special locs. Then prepare for display.
  const primaryGroups = partsToGroups(
    stripTrailingSpecialLocs(primaryParts),
    0
  );

  // Process additional locations. Only keep the unique parts of additional locations relative
  // to the primary location. Also identify any special location types.
  const secondaryGroups: PhotoGroup[][] = [];

  for (let locIdx = 1; locIdx < photo.locations.length; ++locIdx) {
    // Split and strip trailing special locations.
    const parts = stripTrailingSpecialLocs(
      splitPhotoGroup(photo.locations[locIdx], ",")
    );

    // Find the first index that differs from the primary location.
    let i = 0;
    while (
      i < parts.slug.length &&
      i < primaryParts.slug.length &&
      parts.slug[i] === primaryParts.slug[i]
    ) {
      i++;
    }

    // Skip if everything has already been captured elsewhere (primary location and/or special locations).
    if (i === parts.slug.length) continue;

    secondaryGroups.push(partsToGroups(parts, i));
  }

  // Uniqify special location lists.
  hikes = uniqifyPhotoGroupList(hikes);
  parks = uniqifyPhotoGroupList(parks);

  // Build React fragments for the main locations, secondary locations, and special locations.
  const makeLinkEvent = (id: string): EventDesc => ({
    event: AppEvent.GROUP_CLICK,
    object: `/location${id}`,
    context: `/photo/${photo.id}`,
    unique: false,
  });

  const renderLocation = (parts: PhotoGroup[]): JSX.Element => {
    return (
      <span>
        {parts
          .map((part, i) => {
            const id = `/${part[PHOTO_GROUP_ID]}/`;

            return (
              <span key={part[PHOTO_GROUP_ID]}>
                {i < parts.length - 1 ? ", " : null}
                <LinkWithEvent to={`/location${id}`} event={makeLinkEvent(id)}>
                  {titleCase(part[PHOTO_GROUP_DISPLAY])}
                </LinkWithEvent>
              </span>
            );
          })
          // Locations are specified in least to most specific order, want to display in reverse order.
          .reverse()}
      </span>
    );
  };

  const primaryLocationParts = renderLocation(primaryGroups);

  const secondaryLocationParts = secondaryGroups.map((locParts) => {
    return (
      <React.Fragment key={locParts[0][PHOTO_GROUP_ID]}>
        {renderLocation(locParts)}
      </React.Fragment>
    );
  });

  const parkParts = parks.map(([rawId, display]) => {
    const id = `/${rawId}/`;
    return (
      <span key={id}>
        <LinkWithEvent to={`/location${id}`} event={makeLinkEvent(id)}>
          {titleCase(display)}
        </LinkWithEvent>{" "}
      </span>
    );
  });

  const hikeParts = hikes.map(([rawId, display]) => {
    const id = `/${rawId}/`;
    return (
      <span key={id}>
        <LinkWithEvent to={`/location${id}`} event={makeLinkEvent(id)}>
          {titleCase(display)}
        </LinkWithEvent>{" "}
      </span>
    );
  });

  return (
    <>
      {hikeParts.length > 0 && (
        <>
          <IconHike />
          <span>
            Hike{hikeParts.length > 1 ? "s" : ""}:{" "}
            <FlexList>{hikeParts}</FlexList>
          </span>
        </>
      )}
      {parkParts.length > 0 && (
        <>
          <IconPark />
          <span>
            Park{parkParts.length > 1 ? "s" : ""}:{" "}
            <FlexList>{parkParts}</FlexList>
          </span>
        </>
      )}
      <IconLocation />
      <span>
        {primaryLocationParts}
        {secondaryLocationParts.length > 0 && (
          <>
            <br />
            <FlexList>
              <i>also</i>
              {secondaryLocationParts}
            </FlexList>
          </>
        )}
      </span>
    </>
  );
};

const Description: React.FC<{
  photo: Pick<PartialFullPhoto, "description">;
}> = ({ photo }) => {
  if (photo.description === undefined || photo.description.length === 0)
    return null;

  return (
    <>
      <IconDescription />
      <span>{photo.description}</span>
    </>
  );
};

const Keywords: React.FC<{
  photo: Pick<PartialFullPhoto, "id" | "keywords">;
}> = ({ photo }) => {
  if (photo.keywords === undefined || photo.keywords.length === 0) return null;

  const makeLinkEvent = (id: string): EventDesc => ({
    event: AppEvent.GROUP_CLICK,
    object: `/keyword${id}`,
    context: `/photo/${photo.id}`,
    unique: false,
  });

  // Sort keywords by display.
  const keywords = [...photo.keywords];
  keywords.sort((a, b) =>
    a[PHOTO_GROUP_DISPLAY].localeCompare(b[PHOTO_GROUP_DISPLAY])
  );

  const elems = photo.keywords.map((kwGroup, i) => {
    const [id, display] = kwGroup;
    return (
      <React.Fragment key={id}>
        {i > 0 ? ", " : null}
        <LinkWithEvent to={`/keyword${id}`} event={makeLinkEvent(id)}>
          {display}
        </LinkWithEvent>
      </React.Fragment>
    );
  });

  return (
    <>
      <IconKeywords />
      <span>{elems}</span>
    </>
  );
};

const Categories: React.FC<{
  photo: Pick<PartialFullPhoto, "id" | "categories">;
}> = ({ photo }) => {
  if (photo.categories === undefined || photo.categories.length === 0) {
    return null;
  }

  const makeLinkEvent = (id: string): EventDesc => ({
    event: AppEvent.GROUP_CLICK,
    object: `/category${id}`,
    context: `/photo/${photo.id}`,
    unique: false,
  });

  // Build dictionary of category-value pairs. Some categories may have multiple values.
  const catMap = new Map<
    string,
    { catDisplay: string; values: PhotoGroup[] }
  >();

  for (const cvGroup of photo.categories) {
    const { slug, display } = splitPhotoGroup(cvGroup, ":");

    // First part is the category.
    let cat = catMap.get(slug[0]);
    if (cat === undefined) {
      // Initialize category.
      cat = { catDisplay: display[0], values: [] };
      catMap.set(slug[0], cat);
    }

    // Second part is the category-value
    // TODO: extend if categories have more hierarchy levels in the future
    cat.values.push([slug[1], display[1]]);
  }

  const elems: JSX.Element[] = [];

  // Output categories in alphabetical order.
  const catIds = [...catMap.keys()];
  catIds.sort((a, b) => a.localeCompare(b));

  for (const catId of catIds) {
    const catIcon = getCategoryIcon(catId);

    const { catDisplay, values } = catMap.get(catId)!;

    // Sort values in alphabetical order.
    values.sort((a, b) =>
      a[PHOTO_GROUP_DISPLAY].localeCompare(b[PHOTO_GROUP_DISPLAY])
    );
    const valueElems = values.map((value, i) => {
      const [valueId, display] = value;
      const id = `/${catId}/${valueId}/`;
      return (
        <React.Fragment key={id}>
          {i > 0 ? ", " : null}
          <LinkWithEvent to={`/category${id}`} event={makeLinkEvent(id)}>
            {titleCase(display)}
          </LinkWithEvent>
        </React.Fragment>
      );
    });

    elems.push(
      <React.Fragment key={catId}>
        {catIcon}
        <span>
          <LinkWithEvent
            to={`/category/${catId}/`}
            event={makeLinkEvent(`/${catId}/`)}
          >
            {titleCase(catDisplay)}
          </LinkWithEvent>
          {": "}
          {valueElems}
        </span>
      </React.Fragment>
    );
  }

  return <>{elems}</>;
};

const Folder: React.FC<{
  photo: Pick<PartialFullPhoto, "id" | "folder">;
}> = ({ photo }) => {
  if (photo.folder === undefined) return null;

  const { slug, display } = splitPhotoGroup(photo.folder, "/");
  if (slug.length === 0) return null;

  const makeLinkEvent = (id: string): EventDesc => ({
    event: AppEvent.GROUP_CLICK,
    object: `/folder${id}`,
    context: `/photo/${photo.id}`,
    unique: false,
  });

  const elems = slug.map((_, i) => {
    const id = `/${slug.slice(0, i + 1).join("/")}/`;
    return (
      <React.Fragment key={id}>
        {i > 0 ? "/" : null}
        <LinkWithEvent
          to={`/${GroupType.COLLECTION}${id}`}
          event={makeLinkEvent(id)}
        >
          {display[i]}
        </LinkWithEvent>
      </React.Fragment>
    );
  });

  return (
    <>
      <IconCollection />
      <span>{elems}</span>
    </>
  );
};

const monthName = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

function dateSlug(year: number, month?: number, day?: number): string {
  let slug = `/${year}/`;
  if (month !== undefined) {
    slug += `${month < 10 ? "0" : ""}${month}/`;
    if (day !== undefined) {
      slug += `${day < 10 ? "0" : ""}${day}/`;
    }
  }
  return slug;
}

const DateTime: React.FC<{
  photo: Pick<PartialFullPhoto, "id" | "dateTime">;
}> = ({ photo }) => {
  if (!photo.dateTime) return null;

  const makeLinkEvent = (id: string): EventDesc => ({
    event: AppEvent.GROUP_CLICK,
    object: `/date${id}`,
    context: `/photo/${photo.id}`,
    unique: false,
  });

  // Treat date time as in local timezone (although it'll be reported as "UTC")
  const dt = new Date(photo.dateTime);
  const year = dt.getUTCFullYear();
  const month = dt.getUTCMonth();
  const day = dt.getUTCDate();
  const min = dt.getUTCMinutes();

  const monthSlug = monthName[month];

  let hour = dt.getUTCHours();
  const phase = hour >= 12 ? "PM" : "AM";
  hour = ((hour + 11) % 12) + 1; // map 0 -> 12

  const yearId = dateSlug(year);
  const monthId = dateSlug(year, month + 1);
  const dayId = dateSlug(year, month + 1, day);

  return (
    <>
      <IconDate />
      <span>
        <LinkWithEvent to={`/date${yearId}`} event={makeLinkEvent(yearId)}>
          {year}
        </LinkWithEvent>{" "}
        <LinkWithEvent to={`/date${monthId}`} event={makeLinkEvent(monthId)}>
          {monthSlug}
        </LinkWithEvent>{" "}
        <LinkWithEvent to={`/date${dayId}`} event={makeLinkEvent(dayId)}>
          {day}
        </LinkWithEvent>{" "}
        {hour}:{min < 10 ? "0" + min : min} {phase}
      </span>
    </>
  );
};

const CameraInfoGrid = styled.div`
  display: grid;
  grid-template-columns: max-content 1fr;
  grid-auto-rows: auto;

  /* To line up first row (whose cells have top padding) with the icon */
  margin-top: -0.25em;

  div {
    padding: 0.25em;
    border-right: 1px solid var(--richtext-divider-color);
    border-bottom: 1px solid var(--richtext-divider-color);
  }
  div:nth-child(2n) {
    border-right: 0;
  }
  div:last-child,
  div:nth-last-child(2) {
    border-bottom: 0;
  }
`;

const CameraInfo: React.FC<{
  photo: Pick<
    FullPhoto,
    "model" | "lens" | "fNumber" | "focalLength" | "iso" | "exposureTime"
  >;
}> = ({ photo }) => {
  // Skipping a few fields:
  // - make: seems redundant with model
  // - exposureBias: not relevant to exposure, which is already reflected in other values
  if (
    !photo.model &&
    !photo.lens &&
    !photo.fNumber &&
    !photo.focalLength &&
    !photo.iso &&
    !photo.exposureTime
  ) {
    return null;
  }

  return (
    <>
      <IconCamera />
      <CameraInfoGrid>
        {photo.model && (
          <>
            <div>Model</div>
            <div>{photo.model}</div>
          </>
        )}
        {photo.lens && (
          <>
            <div>Lens</div>
            <div>{photo.lens}</div>
          </>
        )}
        {photo.focalLength && (
          <>
            <div>Focal length</div>
            <div>{photo.focalLength.toFixed(0)} mm</div>
          </>
        )}
        {photo.fNumber && (
          <>
            <div>Aperture</div>
            <div>f/{photo.fNumber.toFixed(1)}</div>
          </>
        )}
        {photo.exposureTime && (
          <>
            <div>Exposure time</div>
            <div>
              {photo.exposureTime >= 1
                ? photo.exposureTime.toFixed(0)
                : `1/${(1 / photo.exposureTime).toFixed(0)}`}{" "}
              s
            </div>
          </>
        )}
        {photo.iso && (
          <>
            <div>ISO</div>
            <div>{photo.iso}</div>
          </>
        )}
      </CameraInfoGrid>
    </>
  );
};

const CopyrightUsage: React.FC<{
  photo: Pick<PartialFullPhoto, "copyright" | "usageTerms" | "usageWebUrl">;
}> = ({ photo }) => {
  if (!photo.copyright && !photo.usageTerms && !photo.usageWebUrl) return null;

  // If there is no usage terms but there is a link, use the link.
  const usageDesc = photo.usageTerms ?? photo.usageWebUrl;
  const usage = photo.usageWebUrl ? (
    <ExternalLink href={photo.usageWebUrl}>{usageDesc}</ExternalLink>
  ) : (
    usageDesc
  );

  return (
    <>
      <IconCopyright />
      <span>
        {photo.copyright ?? ""} {usage ?? ""}
      </span>
    </>
  );
};

const NotPublic: React.FC<{ photo: Pick<PartialFullPhoto, "public"> }> = ({
  photo,
}) => {
  if (photo.public) return null;

  return (
    <>
      <NotPublicIcon />
      <span>Not publicly visible</span>
    </>
  );
};
