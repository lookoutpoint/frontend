// SPDX-License-Identifier: MIT

export interface ZoomablePhotoState {
  zoom: number; // >= 1.0
  atMaxZoom: boolean;
  // Offsets: percentage offset from center of image (0% means the picture is centered)
  offsetX: number;
  offsetY: number;
  animateOffsetDuration?: number;
}
export const DEFAULT_ZOOMABLE_PHOTO_STATE: ZoomablePhotoState = {
  zoom: 1,
  atMaxZoom: false,
  offsetX: 0,
  offsetY: 0,
}; // External state.

export interface ZoomablePhotoActions {
  zoomIn: () => void;
  zoomOut: () => void;
}
