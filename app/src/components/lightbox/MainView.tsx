// SPDX-License-Identifier: MIT
import React, { useCallback, useRef, useState } from "react";
import styled, { CSSProperties } from "styled-components";
import { CircleIconButton } from "../CircleIconButton";
import { ConstrainedPhoto } from "../ConstrainedPhoto";
import { PartialFullPhoto, PhotoListPhoto } from "../../api/photos";
import { Sidebar } from "./Sidebar";
import { useWindowSize } from "../../hooks/useWindowSize";
import { swipeAnimationDuration } from "../../helpers/photos";
import {
  IconArrowLeft,
  IconArrowRight,
  IconClose,
  IconPause,
  IconPlay,
  IconSidebarBottom,
  IconSidebarLeft,
  IconZoomIn,
  IconZoomOut,
} from "../Icons";
import { useMediaQuery } from "../../hooks/useMediaQuery";
import { ZoomablePhoto } from "./ZoomablePhoto";
import { ZoomablePhotoActions, ZoomablePhotoState } from "./ExternalInterface";

interface Props {
  photo: PartialFullPhoto;
  prevPhoto: PhotoListPhoto | null;
  nextPhoto: PhotoListPhoto | null;
  showSlideshow: boolean;
  showSidebar: boolean;
  closeUrl: string;
  transitionFromPrev: boolean;
  makePhotoLink?: (photo: PhotoListPhoto) => string;
  onPlay: () => void;
  onSidebar: () => void;
  onClose: () => void;
  onPrev: (swiped?: boolean) => void;
  onNext: (swiped?: boolean) => void;
  mainPhotoZoomState: ZoomablePhotoState;
  setMainPhotoZoomState: React.Dispatch<
    React.SetStateAction<ZoomablePhotoState>
  >;
}

const SMALLER_TITLE_WIDTH = 500;

const MainLayout = styled.div`
  display: grid;
  width: 100%;
  height: 100%;

  grid-template-areas:
    "header prevBtn nextBtn playBtn zoomInBtn zoomOutBtn sidebarBtn closeBtn"
    "photo photo photo photo photo photo photo photo";
  grid-template-rows: 2.5em 1fr;
  grid-template-columns: 1fr 3em 3em 3em 3em 3em 3em 3em;

  &.sidebar {
    grid-template-areas:
      "header header prevBtn nextBtn playBtn zoomInBtn zoomOutBtn sidebarBtn closeBtn"
      "sidebar photo photo photo photo photo photo photo photo";
    grid-template-columns: minmax(20em, 20%) 1fr 3em 3em 3em 3em 3em 3em 3em;
  }

  @media (max-aspect-ratio: 1/1) {
    &.sidebar {
      grid-template-areas:
        "header prevBtn nextBtn playBtn zoomInBtn zoomOutBtn sidebarBtn closeBtn"
        "photo photo photo photo photo photo photo photo"
        "sidebar sidebar sidebar sidebar sidebar sidebar sidebar sidebar";
      grid-template-rows: 2.5em 1fr minmax(20em, 20%);
      grid-template-columns: 1fr 3em 3em 3em 3em 3em 3em 3em;
    }
  }

  @media (max-aspect-ratio: 1/1) and (max-width: ${SMALLER_TITLE_WIDTH}px) {
    grid-template-columns: 1fr 2.1em 2.1em 2.1em 2.1em 2.1em 2.1em 2.1em;

    /* Duplicate to make it specific enough to override above */
    &.sidebar {
      grid-template-columns: 1fr 2.1em 2.1em 2.1em 2.1em 2.1em 2.1em 2.1em;
    }
  }
`;

const Header = styled.div`
  grid-area: header;
  position: relative;
  background-color: var(--control-bar-bg-color);
  color: var(--text-color);
  line-height: 1.667;
  font-size: 1.5em;
  /* Align padding with the sidebar (in landscape layout) */
  padding-left: 0.5em;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;

  @media (max-width: ${SMALLER_TITLE_WIDTH}px) {
    font-size: 1em;
    line-height: 2.5;
  }
`;

const ButtonWrapper = styled.div<{ $area: string }>`
  grid-area: ${(p) => p.$area};
  background-color: var(--control-bar-bg-color);
`;

const NavButton = styled(CircleIconButton)`
  margin: 0.25em auto;

  @media (max-aspect-ratio: 1/1) and (max-width: ${SMALLER_TITLE_WIDTH}px) {
    margin: 0.375em auto;
  }
`;

const SidebarWrapper = styled.div`
  grid-area: sidebar;
  background-color: var(--text-bg-color);
  padding: 0.5em;
  overflow-x: hidden;
  overflow-y: auto;
  /* Needed in some cases e.g. usage URLs might need to be broken in the middle */
  word-break: break-word;
`;

const PhotoWrapper = styled.div`
  grid-row: photo-start;
  grid-column: photo-start / photo-end;
  position: relative;

  // We fully handle touch events.
  touch-action: none;

  // For zoomed-in photos, don't overflow.
  overflow: hidden;

  & img {
    position: absolute;
    top: 50%;
    /* Center horizontally */
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    touch-action: none;
  }

  & img.hidden {
    visibility: hidden;
  }

  & img.fadeIn {
    animation-name: fadeIn;
    animation-duration: 2s;
    animation-fill-mode: both;
  }
  & img.fadeOut {
    animation-name: fadeOut;
    animation-duration: 2s;
    animation-fill-mode: both;
  }

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @keyframes fadeOut {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }

  @keyframes toLeft {
    100% {
      left: calc(-1 * var(--gallery-swipe-gap));
      transform: translateX(-100%) translateY(-50%);
    }
  }

  @keyframes toCenter {
    100% {
      left: 50%;
      transform: translateX(-50%) translateY(-50%);
    }
  }

  @keyframes toRight {
    100% {
      left: calc(100% + var(--gallery-swipe-gap));
      transform: translateY(-50%);
    }
  }
`;

enum TouchMode {
  START,
  MOVE,
  END,
}

interface TouchState {
  mode: TouchMode;
  startX: number;
  curX: number;
  mainAnimation?: string;
  prevAnimation?: string;
  nextAnimation?: string;
}

const TOUCH_THRESHOLD = 10;

export const MainView: React.FC<Props> = ({
  photo,
  prevPhoto,
  nextPhoto,
  showSlideshow,
  showSidebar,
  transitionFromPrev,
  closeUrl,
  makePhotoLink,
  onPlay,
  onSidebar,
  onClose,
  onPrev,
  onNext,
  mainPhotoZoomState,
  setMainPhotoZoomState,
}) => {
  const windowSize = useWindowSize();

  const zoomablePhotoActions = useRef<ZoomablePhotoActions>(null);

  // Touch events
  const [touchState, setTouchState] = useState<TouchState | undefined>(
    undefined
  );

  const onTouchStart = useCallback(
    (e: React.TouchEvent) => {
      // Swipe should only use one touch point.
      if (e.touches.length !== 1) return;

      // No swiping while zoomed in.
      if (mainPhotoZoomState.zoom > 1) return;

      // If already in touch state, don't interrupt (e.g. in END).
      if (touchState) return;

      const { clientX: x } = e.touches[0];
      setTouchState({ mode: TouchMode.START, startX: x, curX: x });
    },
    [mainPhotoZoomState.zoom, touchState]
  );
  const onTouchMove = useCallback(
    (e: React.TouchEvent) => {
      // Don't interrupt END.
      if (!touchState || touchState.mode === TouchMode.END) return;

      const { clientX: x } = e.touches[0];
      setTouchState((s) => {
        return s
          ? { ...s, mode: TouchMode.MOVE, curX: x }
          : { mode: TouchMode.MOVE, startX: x, curX: x }; // this shouldn't happen, but fallback
      });
    },
    [touchState]
  );
  const onTouchEnd = useCallback(() => {
    if (!touchState) return;

    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;
    if (!dragToNext && !dragToPrev) {
      // Nothing to do.
      setTouchState(undefined);
      return;
    }

    // Something is being dragged. Set animation for appropriate photos.
    const duration = swipeAnimationDuration(delta, windowSize.width);

    if (dragToNext) {
      setTouchState({
        ...touchState,
        mode: TouchMode.END,
        mainAnimation: `toLeft ${duration}s forwards`,
        nextAnimation: `toCenter ${duration}s forwards`,
      });
    } else if (dragToPrev) {
      setTouchState({
        ...touchState,
        mode: TouchMode.END,
        mainAnimation: `toRight ${duration}s forwards`,
        prevAnimation: `toCenter ${duration}s forwards`,
      });
    }
  }, [nextPhoto, prevPhoto, touchState, windowSize.width]);
  const onAnimationEnd = useCallback(() => {
    if (!touchState) return;

    // Animation ended. Trigger actual navigation to prev/next photo.
    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;

    if (dragToNext) {
      onNext(/*swiped*/ true);
    } else if (dragToPrev) {
      onPrev(/*swiped*/ true);
    }

    setTouchState(undefined);
  }, [nextPhoto, onNext, onPrev, prevPhoto, touchState]);
  const onTouchCancel = useCallback(() => setTouchState(undefined), []);

  // Styles for images
  let mainPhotoClass: string | undefined;
  let mainPhotoCss: CSSProperties | undefined;

  let prevPhotoClass: string | undefined = "hidden";
  let prevPhotoCss: CSSProperties | undefined;

  let nextPhotoClass: string | undefined = "hidden";
  let nextPhotoCss: CSSProperties | undefined;

  if (transitionFromPrev) {
    mainPhotoClass = "fadeIn";
    prevPhotoClass = "fadeOut"; // not hidden
  } else if (touchState) {
    const delta = touchState.curX - touchState.startX;
    const dragToNext = delta < -TOUCH_THRESHOLD && nextPhoto;
    const dragToPrev = delta > TOUCH_THRESHOLD && prevPhoto;
    if (dragToNext || dragToPrev) {
      mainPhotoCss = {
        left: `calc(50% + ${delta}px)`,
        animation: touchState.mainAnimation,
      };
    }

    if (dragToNext) {
      nextPhotoClass = undefined;
      nextPhotoCss = {
        left: `calc(100% + var(--gallery-swipe-gap) + ${delta}px)`,
        transform: "translateY(-50%)",
        animation: touchState.nextAnimation,
      };
    }

    if (dragToPrev) {
      prevPhotoClass = undefined;
      prevPhotoCss = {
        left: `calc(${delta}px - var(--gallery-swipe-gap))`,
        transform: "translateX(-100%) translateY(-50%)",
        animation: touchState.prevAnimation,
      };
    }
  }

  const onPrevBtn = useCallback(() => onPrev(/*swipe*/ false), [onPrev]);
  const onNextBtn = useCallback(() => onNext(/*swipe*/ false), [onNext]);

  const onZoomIn = useCallback(
    () => zoomablePhotoActions.current?.zoomIn(),
    []
  );
  const onZoomOut = useCallback(
    () => zoomablePhotoActions.current?.zoomOut(),
    []
  );

  const onlyOnePhoto = prevPhoto === null && nextPhoto === null;

  // Aligns with grid layout for when sidebar positioning is changed.
  const maxAspectRatio1to1 = useMediaQuery("(max-aspect-ratio: 1/1)");
  const smallerNavButtons = useMediaQuery(
    `(max-aspect-ratio: 1/1) and (max-width: ${SMALLER_TITLE_WIDTH}px)`
  );
  const navButtonSizeEm = smallerNavButtons ? 1.75 : undefined;

  return (
    <MainLayout className={showSidebar ? "sidebar" : undefined}>
      {/* Header */}
      <Header>{photo.title ?? null}</Header>
      <ButtonWrapper $area="playBtn">
        {!onlyOnePhoto && (
          <NavButton
            toggle
            toggleActive={showSlideshow}
            title="Slideshow"
            onClick={onPlay}
            disabled={nextPhoto === null}
            sizeEm={navButtonSizeEm}
          >
            {showSlideshow ? <IconPause /> : <IconPlay />}
          </NavButton>
        )}
      </ButtonWrapper>
      <ButtonWrapper $area="sidebarBtn">
        <NavButton
          toggle
          toggleActive={showSidebar}
          title="Details"
          onClick={onSidebar}
          sizeEm={navButtonSizeEm}
        >
          {maxAspectRatio1to1 ? <IconSidebarBottom /> : <IconSidebarLeft />}
        </NavButton>
      </ButtonWrapper>
      <ButtonWrapper $area="closeBtn">
        <NavButton
          link={closeUrl}
          title="Close"
          onClick={onClose}
          sizeEm={navButtonSizeEm}
        >
          <IconClose />
        </NavButton>
      </ButtonWrapper>
      {/* Prev/next navigation buttons */}
      <ButtonWrapper $area="prevBtn">
        {!onlyOnePhoto && (
          <NavButton
            disabled={prevPhoto === null}
            title="Previous"
            link={
              prevPhoto && makePhotoLink ? makePhotoLink(prevPhoto) : undefined
            }
            onClick={onPrevBtn}
            sizeEm={navButtonSizeEm}
          >
            <IconArrowLeft />
          </NavButton>
        )}
      </ButtonWrapper>
      <ButtonWrapper $area="nextBtn">
        {!onlyOnePhoto && (
          <NavButton
            disabled={nextPhoto === null}
            title="Next"
            link={
              nextPhoto && makePhotoLink ? makePhotoLink(nextPhoto) : undefined
            }
            onClick={onNextBtn}
            sizeEm={navButtonSizeEm}
          >
            <IconArrowRight />
          </NavButton>
        )}
      </ButtonWrapper>
      <ButtonWrapper $area="zoomInBtn">
        <NavButton
          disabled={showSlideshow || mainPhotoZoomState.atMaxZoom}
          title="Zoom In"
          onClick={onZoomIn}
          sizeEm={navButtonSizeEm}
        >
          <IconZoomIn />
        </NavButton>
      </ButtonWrapper>
      <ButtonWrapper $area="zoomOutBtn">
        <NavButton
          disabled={showSlideshow || mainPhotoZoomState.zoom === 1}
          title="Zoom Out"
          onClick={onZoomOut}
          sizeEm={navButtonSizeEm}
        >
          <IconZoomOut />
        </NavButton>
      </ButtonWrapper>
      {/* Sidebar */}
      {showSidebar && (
        <SidebarWrapper>
          <Sidebar photo={photo} />
        </SidebarWrapper>
      )}
      {/* Main photo */}
      <PhotoWrapper
        onTouchStart={onTouchStart}
        onTouchMove={onTouchMove}
        onTouchEnd={onTouchEnd}
        onTouchCancel={onTouchCancel}
        onAnimationEnd={onAnimationEnd}
      >
        {/* Key by photo id so that components can be reused for the same photo. Necessary for a smooth transition when navigating. */}
        <ZoomablePhoto
          key={photo.id}
          ref={zoomablePhotoActions}
          className={`main ${mainPhotoClass ?? ""}`}
          style={mainPhotoCss}
          photo={photo}
          state={mainPhotoZoomState}
          setState={setMainPhotoZoomState}
          fetchPriority="high"
        />
        {/* Hidden photos for prefetching */}
        {prevPhoto && (
          <ConstrainedPhoto
            key={prevPhoto.id}
            className={`prev ${prevPhotoClass ?? ""}`}
            style={prevPhotoCss}
            photo={prevPhoto}
            constrainWidth
            constrainHeight
          />
        )}
        {nextPhoto && (
          <ConstrainedPhoto
            key={nextPhoto.id}
            className={`next ${nextPhotoClass ?? ""}`}
            style={nextPhotoCss}
            photo={nextPhoto}
            constrainWidth
            constrainHeight
          />
        )}
      </PhotoWrapper>
    </MainLayout>
  );
};
