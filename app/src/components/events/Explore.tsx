// SPDX-License-Identifier: MIT
import React, { useState } from "react";
import { DateSelection } from "../../views/AdminEvents";
import { Panel } from "./Panel";
import { FlexRow } from "./styled";
import Select from "react-select";
import { AppEvent } from "../../api/events";
import {
  EventCountGridChart,
  Props as EventCountGridChartProps,
} from "./EventCountGridChart";
import { NumberInput } from "../NumberInput";

interface Props {
  dateSel: DateSelection;
}

interface BasicOption {
  value: string;
  label: string;
}

interface QueryOption extends BasicOption {
  regex?: boolean;
}

const eventOptions: QueryOption[] = Object.values(AppEvent)
  .sort()
  .map((e) => ({
    value: e,
    label: e,
  }));
eventOptions.push({
  value: "^photo/view/",
  regex: true,
  label: "photo/view/*",
});
eventOptions.sort((a, b) => a.label.localeCompare(b.label));

const groupByOptions: BasicOption[] = [
  "event",
  "object",
  "context",
  "extra",
].map((o) => ({
  value: o,
  label: o[0].toLocaleUpperCase() + o.slice(1),
}));

function toGroupBy(
  options: readonly BasicOption[]
): EventCountGridChartProps["groupBy"] {
  const groupBy: EventCountGridChartProps["groupBy"] = {};
  for (const o of options) {
    switch (o.value) {
      case "event":
        groupBy.event = "Event";
        break;
      case "object":
        groupBy.object = "Object";
        break;
      case "context":
        groupBy.context = "Context";
        break;
      case "extra":
        groupBy.extra = "Extra";
        break;
    }
  }
  return groupBy;
}

export const Explore: React.FC<Props> = ({ dateSel }) => {
  const [event, setEvent] = useState<QueryOption | undefined>(undefined);
  const [groupBy, setGroupBy] = useState<readonly BasicOption[]>([]);

  // Limit cannot be less than 1.
  const [rawLimit, setLimit] = useState<number>(10);
  const limit = Math.max(rawLimit, 1);

  return (
    <>
      <Panel title="Query" collapsable={false}>
        <FlexRow>
          <span>Event:</span>
          <Select
            options={eventOptions}
            value={event}
            onChange={(opt) => setEvent(opt ?? undefined)}
            styles={{
              control: (provided) => ({
                ...provided,
                width: "15em",
              }),
            }}
          />
          <span>Group by:</span>
          <Select
            options={groupByOptions}
            isMulti
            value={groupBy}
            onChange={(opts) => setGroupBy(opts ?? [])}
          />
          <span>Limit:</span>
          <NumberInput value={limit} onValueChange={setLimit} />
        </FlexRow>
      </Panel>
      {event && (
        <Panel title="Results" collapsable={false}>
          <EventCountGridChart
            dateSel={dateSel}
            itemLimit={limit}
            filter={{ event: event.value, eventRegex: event.regex }}
            groupBy={toGroupBy(groupBy)}
          ></EventCountGridChart>
        </Panel>
      )}
    </>
  );
};
