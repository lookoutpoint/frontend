/* SPDX-License-Identifier: MIT */
import styled from "styled-components";

interface Props {
  width: string | number;
  height: string | number;
  className?: string;
  children?: React.ReactNode;
}

const Div = styled.div<{ $width: string | number; $height: string | number }>`
  width: ${(p) => (typeof p.$width === "number" ? `${p.$width}px` : p.$width)};
  height: ${(p) =>
    typeof p.$height === "number" ? `${p.$height}px` : p.$height};
  background-color: #fff;
`;

const Padding = styled.div`
  padding: 0.5em;
  height: calc(100% - 1em);
`;

export const ChartWrapper: React.FC<Props> = ({
  width,
  height,
  className,
  children,
}) => {
  return (
    <Div $width={width} $height={height} className={className}>
      <Padding>{children}</Padding>
    </Div>
  );
};
