// SPDX-License-Identifier: MIT
import React, { useMemo } from "react";
import { AppEvent } from "../../api/events";
import { DateSelection } from "../../views/AdminEvents";
import { DateLineChart } from "./DateLineChart";
import { EventCountGridChart } from "./EventCountGridChart";
import { Panel } from "./Panel";

interface Props {
  dateSel: DateSelection;
}

export const SummaryPanel: React.FC<Props> = ({ dateSel }) => {
  return (
    <>
      <Panel title="Site Visits">
        <DateLineChart
          dateSel={dateSel}
          filter={useMemo(() => ({ event: AppEvent.SITE_VISIT }), [])}
        />
        <p>Top site visit URLs:</p>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={10}
          filter={useMemo(() => ({ event: AppEvent.SITE_VISIT }), [])}
          groupBy={useMemo(() => ({ context: "URL" }), [])}
        />
        <p>Top referrers:</p>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={10}
          filter={useMemo(() => ({ event: AppEvent.SITE_VISIT }), [])}
          groupBy={useMemo(() => ({ extra: "Referrer" }), [])}
        />
      </Panel>
      <Panel title="Page Views" initialCollapsed>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={10}
          filter={useMemo(() => ({ event: AppEvent.PAGE_VIEW }), [])}
          groupBy={useMemo(() => ({ context: "Page" }), [])}
        />
      </Panel>
      <Panel title="Photo Views (Lightbox / Home)" initialCollapsed>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={20}
          filter={useMemo(
            () => ({
              event: "^photo/view/(?:lightbox|home)",
              eventRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ object: "Photo" }), [])}
        />
      </Panel>
      <Panel title="Photo Views (All)" initialCollapsed>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={20}
          filter={useMemo(
            () => ({ event: "^photo/view/", eventRegex: true }),
            []
          )}
          groupBy={useMemo(() => ({ object: "Photo" }), [])}
        />
      </Panel>
    </>
  );
};
