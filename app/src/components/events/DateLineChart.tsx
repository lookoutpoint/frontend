// SPDX-License-Identifier: MIT
import React, { useMemo } from "react";
import { EventCountQueryFilter, useCountEvents } from "../../api/events";
import { DateSelection } from "../../views/AdminEvents";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  ChartOptions,
  ChartData,
  Title,
} from "chart.js";
import { Line } from "react-chartjs-2";
import {
  buildChartDateSelectionNumberData,
  useChartDateSelectionLabels,
} from "../../helpers/events";
import { ChartWrapper } from "./ChartWrapper";
import { CHART_COLOURS } from "../../helpers/colours";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Title
);

export interface Props {
  dateSel: DateSelection;
  filter: EventCountQueryFilter;
  title?: string;
  width?: number | string; // defaults to 100%
  height?: number | string; // defaults to 25vh
}

export const DateLineChart: React.FC<Props> = ({
  dateSel,
  filter,
  title,
  width,
  height,
}) => {
  const chartLabels = useChartDateSelectionLabels(dateSel);

  // Query for chart data.
  const rawData = useCountEvents({ ...dateSel, ...filter });

  const countData = buildChartDateSelectionNumberData(
    dateSel,
    rawData ? rawData.length : 0,
    (i) => rawData![i].id!.date!,
    (i) => rawData![i].count
  );

  const chartOptions = useMemo<ChartOptions<"line">>(() => {
    const options: ChartOptions<"line"> = {
      maintainAspectRatio: false,
      scales: {
        y: {
          min: 0,
        },
      },
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: title !== undefined,
          text: title,
        },
      },
    };
    return options;
  }, [title]);

  const chartData = useMemo<ChartData<"line">>(
    () => ({
      labels: chartLabels,
      datasets: [{ data: countData, borderColor: CHART_COLOURS[0] }],
    }),
    [chartLabels, countData]
  );

  return (
    <ChartWrapper width={width ?? "100%"} height={height ?? "25vh"}>
      <Line options={chartOptions} data={chartData} />
    </ChartWrapper>
  );
};
