// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo } from "react";
import DatePicker from "react-datepicker";
import { DateSelection } from "../../views/AdminEvents";

import "react-datepicker/dist/react-datepicker.css";
import { endOfMonth, endOfYear, max, min, subDays, subMonths } from "date-fns";
import Select from "react-select";
import { EventGranularity } from "../../api/events";
import { ToggleButton } from "../ToggleButton";
import { Panel } from "./Panel";
import { UTCDate } from "../../api/date";
import { FlexRow } from "./styled";
import { isEqual } from "lodash-es";

const GRANULARITY_OPTIONS: { [key: string]: string } = {};
GRANULARITY_OPTIONS[`${EventGranularity.DAY}`] = "Day";
GRANULARITY_OPTIONS[`${EventGranularity.MONTH}`] = "Month";
GRANULARITY_OPTIONS[`${EventGranularity.YEAR}`] = "Year";
GRANULARITY_OPTIONS[`${EventGranularity.ALL_TIME}`] = "All Time";

interface Preset {
  label: string;
  dateSel: DateSelection;
}

function computePresets(today: UTCDate): Preset[] {
  const todayBeginMonth = new Date(
    today.date.getFullYear(),
    today.date.getMonth()
  );
  return [
    {
      label: "1 day",
      dateSel: {
        startDate: today,
        endDate: today,
        granularity: EventGranularity.DAY,
      },
    },
    {
      label: "7 days",
      dateSel: {
        startDate: new UTCDate(subDays(today.date, 6)),
        endDate: today,
        granularity: EventGranularity.DAY,
      },
    },
    {
      label: "30 days",
      dateSel: {
        startDate: new UTCDate(subDays(today.date, 29)),
        endDate: today,
        granularity: EventGranularity.DAY,
      },
    },
    {
      label: "90 days",
      dateSel: {
        startDate: new UTCDate(subDays(today.date, 89)),
        endDate: today,
        granularity: EventGranularity.DAY,
      },
    },
    {
      label: "6 months",
      dateSel: {
        startDate: new UTCDate(subMonths(todayBeginMonth, 5)),
        endDate: today,
        granularity: EventGranularity.MONTH,
      },
    },
    {
      label: "12 months",
      dateSel: {
        startDate: new UTCDate(subMonths(todayBeginMonth, 11)),
        endDate: today,
        granularity: EventGranularity.MONTH,
      },
    },
    {
      label: "All time",
      dateSel: {
        startDate: new UTCDate(new Date(2022, 1, 1)),
        endDate: today,
        granularity: EventGranularity.ALL_TIME,
      },
    },
  ];
}

interface Props {
  today: UTCDate;
  dateSel: DateSelection;
  setDateSelection: (dateSel: DateSelection) => void;
}

export const DateSelectionPanel: React.FC<Props> = (props) => {
  const { today, dateSel, setDateSelection } = props;

  const presets = useMemo(() => computePresets(today), [today]);

  const onStartDateChange = useCallback(
    (date: Date | null) => {
      if (!date) return;
      const { endDate } = dateSel;
      setDateSelection({
        ...dateSel,
        startDate: new UTCDate(date),
        endDate: new UTCDate(max([endDate.date, date])),
      });
    },
    [dateSel, setDateSelection]
  );

  const onEndDateChange = useCallback(
    (date: Date | null) => {
      if (!date) return;
      const { startDate, granularity } = dateSel;

      // Round up end date based on granularity.
      if (granularity === EventGranularity.MONTH) {
        date = endOfMonth(date);
      } else if (granularity === EventGranularity.YEAR) {
        date = endOfYear(date);
      }

      setDateSelection({
        ...dateSel,
        endDate: new UTCDate(date),
        startDate: new UTCDate(min([startDate.date, date])),
      });
    },
    [dateSel, setDateSelection]
  );

  const granularityOptions = useMemo(() => {
    const opts: { value: string; label: string }[] = [];
    for (const key in GRANULARITY_OPTIONS) {
      opts.push({ value: key, label: GRANULARITY_OPTIONS[key] });
    }
    return opts;
  }, []);

  const curGranularityOption = useMemo(() => {
    const key = `${dateSel.granularity}`;
    return { value: `${key}`, label: GRANULARITY_OPTIONS[key] };
  }, [dateSel.granularity]);

  const onGranularityChange = useCallback(
    (newValue: { value: string } | null) => {
      if (!newValue) return;
      setDateSelection({
        ...dateSel,
        granularity: parseInt(newValue.value),
      });
    },
    [dateSel, setDateSelection]
  );

  const dateFormat = useMemo<string>(() => {
    switch (dateSel.granularity) {
      case EventGranularity.DAY:
      case EventGranularity.ALL_TIME:
        return "yyyy-MM-dd";
      case EventGranularity.MONTH:
        return "yyyy-MM";
      case EventGranularity.YEAR:
        return "yyyy";
    }
  }, [dateSel.granularity]);

  return (
    <Panel title="Date" collapsable={false}>
      <FlexRow>
        <span>Presets:</span>
        {presets.map((p) => (
          <ToggleButton
            key={p.label}
            toggled={isEqual(p.dateSel, dateSel)}
            onClick={() => setDateSelection(p.dateSel)}
          >
            {p.label}
          </ToggleButton>
        ))}
      </FlexRow>
      <FlexRow>
        <span>Start:</span>
        <DatePicker
          selected={dateSel.startDate.date}
          startDate={dateSel.startDate.date}
          endDate={dateSel.endDate.date}
          selectsStart
          onChange={onStartDateChange}
          dateFormat={dateFormat}
          showMonthYearPicker={dateSel.granularity === EventGranularity.MONTH}
        />
        <span>End:</span>
        <DatePicker
          selected={dateSel.endDate.date}
          startDate={dateSel.startDate.date}
          endDate={dateSel.endDate.date}
          selectsEnd
          onChange={onEndDateChange}
          dateFormat={dateFormat}
          showMonthYearPicker={dateSel.granularity === EventGranularity.MONTH}
        />
        <span>Granularity:</span>
        <Select
          options={granularityOptions}
          value={curGranularityOption}
          onChange={onGranularityChange}
        />
      </FlexRow>
    </Panel>
  );
};
