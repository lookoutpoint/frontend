// SPDX-License-Identifier: MIT
import React, { ReactElement, useState } from "react";
import styled from "styled-components";
import { CircleIconButton } from "../CircleIconButton";
import { IconCollapse, IconExpand } from "../Icons";

const PanelDiv = styled.div`
  margin: 0.5em;
  padding: 0.5em;
  border: 1px solid var(--dialog-border-color);
`;

const PanelHeader = styled.div<{ $collapsed: boolean }>`
  margin: -0.5em;
  padding: 0.5em;
  margin-bottom: ${(p) => (p.$collapsed ? -0.5 : 0.5)}em;
  background-color: var(--caption-bg-color);
  color: var(--caption-color);
  border-bottom: ${(p) => (p.$collapsed ? 0 : 1)}px solid
    var(--dialog-border-color);
  // Left and right "columns"
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
`;

const CollapseButton = styled(CircleIconButton)``;

interface Props {
  title: string;
  collapsable?: boolean; // defaults to true
  initialCollapsed?: boolean; // defaults to false
  children?: React.ReactNode;
}

export const Panel: React.FC<Props> = ({
  title,
  collapsable = true,
  initialCollapsed = false,
  children,
}) => {
  const [collapsed, setCollapsed] = useState<boolean>(
    !!collapsable && !!initialCollapsed
  );

  let collapseBtn: ReactElement | undefined;
  if (collapsable) {
    collapseBtn = (
      <CollapseButton onClick={() => setCollapsed(!collapsed)}>
        {collapsed ? <IconExpand /> : <IconCollapse />}
      </CollapseButton>
    );
  }

  return (
    <PanelDiv>
      <PanelHeader $collapsed={collapsed}>
        {title}
        {collapseBtn}
      </PanelHeader>
      {!collapsed && children}
    </PanelDiv>
  );
};
