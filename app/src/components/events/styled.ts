// SPDX-License-Identifier: MIT
import styled from "styled-components";

export const FlexRow = styled.div`
  padding: 0.25em 0;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  gap: 0.5em;
  > .react-datepicker-wrapper {
    width: auto;
  }
`;
