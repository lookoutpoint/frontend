// SPDX-License-Identifier: MIT
import React, { useMemo, useState } from "react";
import { AppEvent } from "../../api/events";
import { DateSelection } from "../../views/AdminEvents";
import { NumberInput } from "../NumberInput";
import { EventCountGridChart } from "./EventCountGridChart";
import { Panel } from "./Panel";
import { FlexRow } from "./styled";

interface Props {
  dateSel: DateSelection;
}

export const Groups: React.FC<Props> = ({ dateSel }) => {
  // Limit cannot be less than 1.
  const [rawLimit, setLimit] = useState<number>(10);
  const limit = Math.max(rawLimit, 1);

  return (
    <>
      <Panel title="Options" collapsable={false}>
        <FlexRow>
          <span>Limit:</span>
          <NumberInput value={limit} onValueChange={setLimit} />
        </FlexRow>
      </Panel>
      <Panel title="Collections" initialCollapsed={false}>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={limit}
          filter={useMemo(
            () => ({
              event: AppEvent.PAGE_VIEW,
              context: "^/folder/", // backend uses folder instead of collection
              contextRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ context: "Folder" }), [])}
        />
      </Panel>
      <Panel title="Timelines" initialCollapsed={false}>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={limit}
          filter={useMemo(
            () => ({
              event: AppEvent.PAGE_VIEW,
              context: "^/timeline/",
              contextRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ context: "Timeline" }), [])}
        />
      </Panel>
      <Panel title="Locations" initialCollapsed={false}>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={limit}
          filter={useMemo(
            () => ({
              event: AppEvent.PAGE_VIEW,
              context: "^/location/",
              contextRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ context: "Location" }), [])}
        />
      </Panel>
      <Panel title="Categories" initialCollapsed={false}>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={limit}
          filter={useMemo(
            () => ({
              event: AppEvent.PAGE_VIEW,
              context: "^/category/",
              contextRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ context: "Category" }), [])}
        />
      </Panel>
      <Panel title="Keywords" initialCollapsed={false}>
        <EventCountGridChart
          dateSel={dateSel}
          itemLimit={limit}
          filter={useMemo(
            () => ({
              event: AppEvent.PAGE_VIEW,
              context: "^/keyword/",
              contextRegex: true,
            }),
            []
          )}
          groupBy={useMemo(() => ({ context: "Keyword" }), [])}
        />
      </Panel>
    </>
  );
};
