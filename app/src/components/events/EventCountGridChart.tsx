// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo, useState } from "react";
import {
  EventGranularity,
  EventCountQuery,
  EventCountResult,
  EventCountQueryFilter,
  useCountEvents,
} from "../../api/events";
import { DateSelection } from "../../views/AdminEvents";
import DataGrid, { ColSpanArgs, Column, SortColumn } from "react-data-grid";
import { Pie } from "react-chartjs-2";
import { ChartWrapper } from "./ChartWrapper";
import {
  Chart as ChartJS,
  Tooltip,
  ChartOptions,
  ChartData,
  ArcElement,
  Legend,
} from "chart.js";
import { CHART_COLOURS } from "../../helpers/colours";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ConstrainedPhoto } from "../ConstrainedPhoto";
import { useWindowSize } from "../../hooks/useWindowSize";
import { DateLineChart, Props as DateLineChartProps } from "./DateLineChart";
import { Dialog } from "../Dialog";
import { GroupType } from "../../api/photoGroups";
import { IconLineChart } from "../Icons";
import { getPhotoMetadataQueryOptions } from "../../api/photos";
import { useQuery } from "@tanstack/react-query";
import { PhotoLink } from "../PhotoLink";
import { makeCanonicalPhotoUrl } from "../../helpers/photos";

import "react-data-grid/lib/styles.css";

ChartJS.register(ArcElement, Tooltip, Legend);

interface Item {
  result?: EventCountResult;
  event: string;
  object: string;
  context: string;
  extra: string;
  label?: string;
  count: number;
  percent: number;
}

type Comparator = (a: Item, b: Item) => number;
function getComparator(col: string): Comparator {
  switch (col) {
    case "event":
      return (a, b) => a.event.localeCompare(b.event);
    case "object":
      return (a, b) => a.object.localeCompare(b.object);
    case "context":
      return (a, b) => a.context.localeCompare(b.context);
    case "extra":
      return (a, b) => a.extra.localeCompare(b.extra);
    case "count":
    case "percent":
      return (a, b) => a.count - b.count;
    default:
      throw new Error(`unsupported column ${col}`);
  }
}

const GridChartWrapper = styled.div`
  display: grid;
  width: 100%;
  overflow: hidden;

  grid-template-columns: 50% 50%;
  grid-template-rows: min-content;
  @media (max-width: 1000px) {
    grid-template-columns: 100%;
    grid-template-rows: auto auto;
  }
`;
const GridWrapper = styled.div`
  & > div {
    // Removes scroll bar from data grid.
    block-size: auto;
    overflow: hidden;
  }
`;
const PieChartWrapper = styled(ChartWrapper)``;

const RightAlignedNumber = styled.div`
  text-align: right;
  font-family: monospace;
`;

const ChartButton = styled(IconLineChart)`
  cursor: pointer;
`;

function filterToTitle(filter: EventCountQueryFilter): string {
  const parts: string[] = [];
  if (filter.event) {
    parts.push(filter.event);
  }
  if (filter.object) {
    parts.push(filter.object);
  }
  if (filter.context) {
    parts.push(filter.context);
  }
  if (filter.extra) {
    parts.push(filter.extra);
  }
  return parts.join(" - ");
}

const DEFAULT_ROW_HEIGHT = 35;

export interface Props {
  dateSel: DateSelection;
  itemLimit: number;
  filter: EventCountQueryFilter;
  groupBy: {
    // Column names.
    event?: string;
    object?: string;
    context?: string;
    extra?: string;
  };
}

export const EventCountGridChart: React.FC<Props> = ({
  dateSel,
  itemLimit,
  filter,
  groupBy,
}) => {
  const rowHeight = useCallback((item: Item): number => {
    if (
      !item.object.startsWith("/photo/") &&
      !item.context.startsWith("/photo/")
    )
      return DEFAULT_ROW_HEIGHT;

    // Photo height.
    return 150;
  }, []);

  // Per-item data.
  const itemQueryParams: EventCountQuery = {
    ...dateSel,
    ...filter,
    // Want a total, so group for "all time"
    granularity: EventGranularity.ALL_TIME,
    limit: itemLimit,
  };

  if (groupBy.event) {
    itemQueryParams.groupByEvent = true;
  }
  if (groupBy.object) {
    itemQueryParams.groupByObject = true;
  }
  if (groupBy.context) {
    itemQueryParams.groupByContext = true;
  }
  if (groupBy.extra) {
    itemQueryParams.groupByExtra = true;
  }

  const eventCountResults = useCountEvents(itemQueryParams);

  // Total data.
  const totalQueryParams: EventCountQuery = {
    ...dateSel,
    ...filter,
    granularity: EventGranularity.ALL_TIME,
  };

  const totalResult = useCountEvents(totalQueryParams);
  const total = totalResult.length > 0 ? totalResult[0].count : 0;

  // Grid sort state.
  const [sortColumns, setSortColumns] = useState<SortColumn[]>([]);

  // Convert query results into items.
  const items = useMemo<Item[]>(() => {
    return eventCountResults.map((result) => ({
      result,
      event: result.id?.event ?? "",
      object: result.id?.object ?? "",
      context: result.id?.context ?? "",
      extra: result.id?.extra ?? "",
      count: result.count,
      percent: (result.count / total) * 100,
    }));
  }, [eventCountResults, total]);

  // Sort items for table and chart.
  const sortedItems = useMemo<Item[]>(() => {
    const sorted = [...items];

    // Add Other row, if any.
    let other = total;
    sorted.forEach((i) => (other -= i.count));

    if (other > 0) {
      sorted.push({
        event: "Other",
        object: "Other",
        context: "Other",
        extra: "Other",
        label: "Other",
        count: other,
        percent: (other / total) * 100,
      });
    }

    if (sortColumns.length > 0) {
      sorted.sort((a, b) => {
        for (const sort of sortColumns) {
          const comparator = getComparator(sort.columnKey);
          const cmp = comparator(a, b);
          if (cmp !== 0) {
            return sort.direction === "ASC" ? cmp : -cmp;
          }
        }
        return 0;
      });
    }

    // Add total row.
    sorted.push({
      event: "TOTAL",
      object: "TOTAL",
      context: "TOTAL",
      extra: "TOTAL",
      label: "TOTAL",
      count: total,
      percent: 100,
    });

    return sorted;
  }, [items, sortColumns, total]);

  // For modal line chart.
  const [lineChartItem, setLineChartItem] = useState<Item | undefined>(
    undefined
  );

  // Grid columns.
  const gridColumns = useMemo<Column<Item>[]>(() => {
    const cols: Column<Item>[] = [
      {
        key: "chart",
        name: <IconLineChart />,
        width: 20,
        renderCell: ({ row }) =>
          row.label === "Other" ? (
            <></>
          ) : (
            <ChartButton onClick={() => setLineChartItem(row)} />
          ),
      },
    ];

    const numKeyCols =
      (groupBy.event ? 1 : 0) +
      (groupBy.object ? 1 : 0) +
      (groupBy.context ? 1 : 0) +
      (groupBy.extra ? 1 : 0);
    let keyColIdx = 0;

    const colSpan = (csa: ColSpanArgs<Item, unknown>): number | undefined => {
      if (csa.type !== "ROW") return undefined;
      if (csa.row.label) return numKeyCols;
      return 1;
    };

    if (groupBy.event) {
      cols.push({
        key: "event",
        name: groupBy.event,
        sortable: true,
        resizable: true,
        width: "1fr",
        colSpan: keyColIdx === 0 ? colSpan : undefined,
      });
      keyColIdx++;
    }

    if (groupBy.object) {
      cols.push({
        key: "object",
        name: groupBy.object,
        sortable: true,
        resizable: true,
        renderCell: ObjectFormatter,
        colSpan: keyColIdx === 0 ? colSpan : undefined,
      });
      keyColIdx++;
    }

    if (groupBy.context) {
      cols.push({
        key: "context",
        name: groupBy.context,
        sortable: true,
        resizable: true,
        renderCell: ContextFormatter,
        colSpan: keyColIdx === 0 ? colSpan : undefined,
      });
      keyColIdx++;
    }

    if (groupBy.extra) {
      cols.push({
        key: "extra",
        name: groupBy.extra,
        sortable: true,
        resizable: true,
        colSpan: keyColIdx === 0 ? colSpan : undefined,
      });
      keyColIdx++;
    }

    cols.push({
      key: "count",
      name: "Count",
      sortable: true,
      resizable: true,
      sortDescendingFirst: true,
      width: 40,
      renderCell: ({ row }) => (
        <RightAlignedNumber>{row.count}</RightAlignedNumber>
      ),
    });
    cols.push({
      key: "percent",
      name: "%",
      sortable: true,
      resizable: true,
      sortDescendingFirst: true,
      width: 40,
      renderCell: ({ row }) => (
        <RightAlignedNumber>
          {Math.round(row.percent * 10) / 10}%
        </RightAlignedNumber>
      ),
    });

    return cols;
  }, [groupBy.context, groupBy.event, groupBy.extra, groupBy.object]);

  // Chart labels.
  const chartLabels = useMemo<string[]>(() => {
    if (!sortedItems) return [];
    const labels = sortedItems.map((item) => {
      if (item.label) return item.label;

      const parts: string[] = [];
      if (groupBy.event) {
        parts.push(item.event);
      }
      if (groupBy.object) {
        parts.push(item.object);
      }
      if (groupBy.context) {
        parts.push(item.context);
      }
      if (groupBy.extra) {
        parts.push(item.extra);
      }
      return parts.join(" - ");
    });
    // Remove last label, which is the total.
    return labels.slice(0, labels.length - 1);
  }, [
    groupBy.context,
    groupBy.event,
    groupBy.extra,
    groupBy.object,
    sortedItems,
  ]);

  const countData = useMemo<number[]>(() => {
    const data = sortedItems.map((u) => u.count);
    // Remove last count, which is the total.
    return data.slice(0, data.length - 1);
  }, [sortedItems]);

  const chartOptions = useMemo<ChartOptions<"pie">>(
    () => ({
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: "right",
        },
      },
    }),
    []
  );

  const chartData = useMemo<ChartData<"pie">>(
    () => ({
      labels: chartLabels,
      datasets: [
        {
          data: countData,
          backgroundColor: [
            ...CHART_COLOURS.slice(0, countData.length - 1),
            "#ccc",
          ],
        },
      ],
    }),
    [chartLabels, countData]
  );

  // Chart height: size it based on the table height, but no more than 2/3 of window height.
  const windowSize = useWindowSize();
  const chartHeight = useMemo(() => {
    let total = 0;
    sortedItems.forEach((item) => (total += rowHeight(item)));
    // Add additional row for header.
    return Math.min(total + DEFAULT_ROW_HEIGHT, (windowSize.height * 2) / 3);
  }, [rowHeight, sortedItems, windowSize.height]);

  // Modal chart config.
  const lineChartFilter = useMemo<
    DateLineChartProps["filter"] | undefined
  >(() => {
    if (!lineChartItem) return undefined;
    const lineFilter: DateLineChartProps["filter"] = { ...filter };
    if (lineChartItem.result?.id?.event !== undefined) {
      lineFilter.event = lineChartItem.result.id.event;
    }
    if (lineChartItem.result?.id?.object !== undefined) {
      lineFilter.object = lineChartItem.result.id.object;
    }
    if (lineChartItem.result?.id?.context !== undefined) {
      lineFilter.context = lineChartItem.result.id.context;
    }
    if (lineChartItem.result?.id?.extra !== undefined) {
      lineFilter.extra = lineChartItem.result.id.extra;
    }
    return lineFilter;
  }, [filter, lineChartItem]);

  return (
    <>
      {sortedItems && (
        <GridChartWrapper>
          <GridWrapper>
            <DataGrid
              columns={gridColumns}
              rows={sortedItems}
              sortColumns={sortColumns}
              rowHeight={rowHeight}
              onSortColumnsChange={setSortColumns}
            />
          </GridWrapper>
          <PieChartWrapper width="auto" height={chartHeight}>
            <Pie options={chartOptions} data={chartData} />
          </PieChartWrapper>
        </GridChartWrapper>
      )}
      {lineChartFilter && (
        <Dialog
          modalLevel={0}
          width="75vw"
          render={() => {
            return (
              <DateLineChart
                dateSel={dateSel}
                filter={lineChartFilter}
                title={filterToTitle(lineChartFilter)}
                height="50vh"
              />
            );
          }}
          onClose={() => setLineChartItem(undefined)}
        />
      )}
    </>
  );
};

const UrlFormatter: React.FC<{ url: string }> = ({ url }) => {
  if (!url.startsWith("/")) {
    // Not a URL, just use return the string.
    return <>{url}</>;
  }
  if (url.match(/^\/(timeline|timeline-story|timeline-photos)\//)) {
    // Timeline group.
    const sepPrefix = url.indexOf("/", 1);
    const sep = url.indexOf("/", sepPrefix + 1);
    const collId = url.slice(sepPrefix + 1, sep).replaceAll(":", "/");
    return (
      <Link
        to={`/${GroupType.COLLECTION}${collId}@timeline/${url.slice(sep + 1)}`}
        target="_blank"
      >
        {url}
      </Link>
    );
  } else if (url.startsWith("/folder/")) {
    // Remap to collection.
    url = "/collection/" + url.slice("/folder/".length);
  }
  return (
    <Link to={url} target="_blank">
      {url}
    </Link>
  );
};

const PhotoFormatter: React.FC<{ id: string }> = ({ id }) => {
  const { data: photo } = useQuery(getPhotoMetadataQueryOptions({ id }));

  if (photo) {
    return (
      <PhotoLink
        to={makeCanonicalPhotoUrl(photo)}
        hoverBorderWidth="0"
        target="_blank"
      >
        <ConstrainedPhoto
          photo={photo}
          constrainWidth
          constrainHeight
          parentLevel={2}
          loading="lazy"
        />
      </PhotoLink>
    );
  } else {
    // If photo is not ready or unavailable, just render the textual id.
    return <>{`/photo/${id}`}</>;
  }
};

const ObjectFormatter: React.FC<{ row: Item }> = ({ row: item }) => {
  if (item.label) return <>{item.label}</>;
  if (!item.object.startsWith("/photo/")) {
    // Object may be a url.
    return <UrlFormatter url={item.object} />;
  }

  // Strip off /photo/ prefix to get photo id.
  const photoId = item.object.slice("/photo/".length);
  return <PhotoFormatter id={photoId} />;
};

const ContextFormatter: React.FC<{ row: Item }> = ({ row: item }) => {
  if (item.label) return <>{item.label}</>;
  if (!item.context.startsWith("/photo/")) {
    // Object may be a url.
    return <UrlFormatter url={item.context} />;
  }

  // Strip off /photo/ prefix to get photo id.
  const photoId = item.context.slice("/photo/".length);
  return <PhotoFormatter id={photoId} />;
};
