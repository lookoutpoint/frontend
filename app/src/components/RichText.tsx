// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo, useState } from "react";
import styled from "styled-components";
import {
  FullPhoto,
  getPhotoMetadataQueryOptions,
  PhotoListPhoto,
} from "../api/photos";
import { ConstrainedPhoto } from "./ConstrainedPhoto";
import { CroppedPhoto } from "./CroppedPhoto";
import { PhotoLinkDiv } from "./PhotoLink";
import { ExternalLink } from "./ExternalLink";
import {
  makeFetchPhotosFromList,
  PhotoListQuery,
} from "../helpers/PhotoListQuery";
import { PhotoLightbox } from "./PhotoLightbox";
import { usePhotoViewEventRefCallback } from "../hooks/usePhotoViewEventRefCallback";
import { AppEvent, EventContext, makeEventPhotoID } from "../api/events";
import { LinkWithEvent } from "./LinkWithEvent";
import { EventDesc } from "../helpers/EventTracker";
import { useLocation } from "react-router-dom";
import { useEventTracker } from "../hooks/useEventTracker";
import { useQueries } from "@tanstack/react-query";
import {
  parseRichText,
  BlockType,
  Section,
  ParagraphBlock,
  isParagraphLink,
  ListBlock,
  TableBlock,
  PhotosBlock,
} from "../helpers/richText";

// ============================================================================
// Main rich text component
// ============================================================================

interface Props {
  text: string;
  currentUrl: string;
  eventContext?: string;
  className?: string;
}

export const RichText: React.FC<Props> = ({
  text,
  currentUrl,
  eventContext = EventContext.DEFAULT,
  className,
}) => {
  const eventTracker = useEventTracker();

  // Parse text into doc
  const doc = useMemo(() => {
    const doc = parseRichText(text);
    // A string indicates an error.
    return typeof doc === "string" ? null : doc;
  }, [text]);

  // Extract ids of photos used in the document.
  const photoIds = useMemo(() => {
    if (!doc) return [];

    // Accumulate all photo ids. Keep them in order in which they appear, except remove duplicates.
    const photoIds: string[] = [];
    const seen = new Set<string>();

    for (const section of doc.sections) {
      for (const block of section.blocks) {
        if (block.type !== BlockType.PHOTOS) continue;
        for (const id of block.photoIds) {
          if (!seen.has(id)) {
            photoIds.push(id);
            seen.add(id);
          }
        }
      }
    }

    return photoIds;
  }, [doc]);

  // Fetch metadata for each photo id.
  const photoQueries = useQueries({
    queries: photoIds.map((id) => getPhotoMetadataQueryOptions({ id })),
  });

  // Photo lightbox
  const photoList = useMemo(() => {
    const validPhotoIds = photoIds.filter(
      (_, i) => photoQueries[i].data !== undefined
    );
    return new PhotoListQuery<"id">(
      "id",
      makeFetchPhotosFromList(
        "id",
        validPhotoIds.map((_id, i) => photoQueries[i].data!)
      )
    );
  }, [photoIds, photoQueries]);
  const [lightboxPhotoId, setLightboxPhotoId] = useState<string | null>(null);

  const onClose = useCallback(() => {
    setLightboxPhotoId(null);
  }, []);
  const onPhotoNav = useCallback((photo: PhotoListPhoto) => {
    setLightboxPhotoId(photo.id);
  }, []);
  const onPhotoClick = useCallback(
    (photo: PhotoListPhoto) => {
      eventTracker.addUXEvent({
        event: AppEvent.PHOTO_CLICK,
        object: makeEventPhotoID(photo),
        context: eventContext,
      });
      onPhotoNav(photo);
    },
    [eventContext, eventTracker, onPhotoNav]
  );

  // Photo view event.
  const photoViewRefCallback = usePhotoViewEventRefCallback(
    AppEvent.PHOTO_VIEW_THUMB,
    eventContext
  );

  return doc ? (
    <div className={className}>
      {doc.sections.map((s, i) => (
        <SectionView
          key={`${i}`}
          section={s}
          onPhotoClick={onPhotoClick}
          photoViewRefCallback={photoViewRefCallback}
          eventContext={eventContext!}
        />
      ))}
      {lightboxPhotoId && (
        <PhotoLightbox
          photoId={{ id: lightboxPhotoId }}
          photoListQuery={photoList}
          queryForward
          closeUrl={currentUrl}
          modifyTitle={false}
          showSidebarOnOpen={false}
          onClose={onClose}
          onPhotoNav={onPhotoNav}
          eventContext={eventContext}
        />
      )}
    </div>
  ) : null;
};

// ============================================================================
// Supporting components
// ============================================================================

interface BaseViewProps {
  onPhotoClick: (photo: FullPhoto) => void;
  eventContext: string;
}

// Section

const SectionDiv = styled.div`
  margin: 0 auto;
  padding: 0 0.5em;
  max-width: var(--max-text-width);

  font-size: 1.1em;
  line-height: 1.7;

  > p:first-child {
    margin-top: 0;
  }
  > p:last-child {
    margin-bottom: 0;
  }
`;

const SectionTitle = styled.h2`
  text-align: center;
  font-size: 1.15em;
  font-weight: normal;
  background-color: var(--richtext-section-title-bg-color);
  padding: 0.5em 0;
  margin: 1em 0;
`;

interface SectionViewProps extends BaseViewProps {
  section: Section;
  photoViewRefCallback?: (elem: HTMLElement | null) => void;
}

const SectionView: React.FC<SectionViewProps> = (props) => {
  const { section, onPhotoClick, photoViewRefCallback, eventContext } = props;

  // Render blocks.
  const elems: JSX.Element[] = [];

  for (let i = 0; i < section.blocks.length; ++i) {
    const block = section.blocks[i];
    const key = `${i}`;

    switch (block.type) {
      case BlockType.PARAGRAPH:
        elems.push(
          <ParagraphBlockView
            key={key}
            block={block}
            eventContext={eventContext}
          />
        );
        break;

      case BlockType.LIST:
        elems.push(
          <ListBlockView key={key} block={block} eventContext={eventContext} />
        );
        break;

      case BlockType.TABLE:
        elems.push(
          <TableBlockView key={key} block={block} eventContext={eventContext} />
        );
        break;

      case BlockType.PHOTOS:
        elems.push(
          <PhotoBlockView
            key={key}
            block={block}
            onPhotoClick={onPhotoClick}
            photoViewRefCallback={photoViewRefCallback}
            eventContext={eventContext}
          />
        );
        break;
    }
  }

  return (
    <section>
      {section.title && <SectionTitle>{section.title}</SectionTitle>}
      <SectionDiv>{elems}</SectionDiv>
    </section>
  );
};

// Paragraph block

const StyledLink = styled(LinkWithEvent)`
  color: var(--richtext-link-color);
  text-decoration: underline;
  text-decoration-color: var(--richtext-link-color);
`;

interface ParagraphBlockViewProps {
  block: ParagraphBlock;
  eventContext: string;
}

const ParagraphBlockViewContents: React.FC<ParagraphBlockViewProps> = (
  props
) => {
  const { block, eventContext } = props;
  const location = useLocation();

  const contents: JSX.Element[] = [];

  for (let i = 0; i < block.parts.length; ++i) {
    const part = block.parts[i];
    if (isParagraphLink(part)) {
      const [url, desc] = part;
      if (url.startsWith("http")) {
        // External link
        contents.push(
          <ExternalLink key={`${i}`} href={url}>
            {desc}
          </ExternalLink>
        );
      } else {
        let eventDesc: EventDesc | undefined;
        if (url.match(/^\/(?:folder|collection|location|category|keyword)\//)) {
          // Group (if it is not valid, it will be ignored)

          // Remap collection to folder.
          const eventUrl = url.replace(/^\/collection\//, "/folder/");

          eventDesc = {
            event: AppEvent.GROUP_CLICK,
            object: eventUrl,
            context: eventContext,
          };
        } else {
          // Try making an event with the link (if it is not valid, it will be ignored)
          eventDesc = {
            event: AppEvent.LINK_CLICK,
            object: url,
            context: eventContext,
          };
        }

        // React Router Link resolves relative URLs in an undesirable manner (relative the path that was matched).
        // Any relative URLs are intended to be resolved versus the current URL.
        let resolvedUrl = url;
        if (resolvedUrl.startsWith(".")) {
          resolvedUrl = location.pathname + resolvedUrl;
        }

        contents.push(
          <StyledLink key={`${i}`} to={resolvedUrl} event={eventDesc}>
            {desc}
          </StyledLink>
        );
      }
    } else {
      contents.push(
        <span key={`${i}`} dangerouslySetInnerHTML={{ __html: part }}></span>
      );
    }
  }

  return <>{contents}</>;
};

const ParagraphBlockView: React.FC<ParagraphBlockViewProps> = (props) => {
  return (
    <p>
      <ParagraphBlockViewContents {...props} />
    </p>
  );
};

// List block

interface ListBlockViewProps {
  block: ListBlock;
  eventContext: string;
}

const ListBlockView: React.FC<ListBlockViewProps> = (props) => {
  const { block, eventContext } = props;

  const items: JSX.Element[] = [];
  for (let i = 0; i < block.items.length; ++i) {
    const item = block.items[i];
    items.push(
      <li key={`${i}`}>
        <ParagraphBlockViewContents
          block={item.contents}
          eventContext={eventContext}
        />
        {item.subList && (
          <ListBlockView block={item.subList} eventContext={eventContext} />
        )}
      </li>
    );
  }

  return <ul>{items}</ul>;
};

// Table block

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;

  &.highlight {
  }

  th,
  td {
    padding: 0.25em;
    border: 1px solid var(--richtext-divider-color);
  }

  th {
    border-top: 0;
    border-left: 0;
    border-right: 0;
  }
  tr td:first-child {
    border-left: 0;
  }
  tr td:last-child {
    border-right: 0;
  }
  tr:last-child td {
    border-bottom: 0;
  }
`;

interface TableBlockViewProps {
  block: TableBlock;
  eventContext: string;
}

const TableBlockView: React.FC<TableBlockViewProps> = (props) => {
  const { block, eventContext } = props;

  const rowElems: JSX.Element[] = [];
  for (const row of block.contents) {
    const colElems = row.map((item, i) => (
      <td key={`${i}`}>
        <ParagraphBlockViewContents block={item} eventContext={eventContext} />
      </td>
    ));
    rowElems.push(<tr key={`${rowElems.length}`}>{colElems}</tr>);
  }

  return (
    <StyledTable className={block.highlight ? "highlight" : undefined}>
      <tbody>
        {block.caption && (
          <tr>
            <th colSpan={block.numCols}>{block.caption}</th>
          </tr>
        )}
        {rowElems}
      </tbody>
    </StyledTable>
  );
};

// Photos block

const ConstrainedPhotoLink = styled(PhotoLinkDiv)<{
  $width?: number;
  $height?: number;
}>`
  margin: 0.5em auto;
  display: block;
  max-width: ${(p) => (p.$width ? p.$width + "px" : "auto")};
  max-height: ${(p) => (p.$height ? p.$height + "px" : "auto")};
`;

const CroppedPhotoLink = styled(PhotoLinkDiv)``;

const PhotoGrid = styled.div<{
  $numCols: number;
  $numRows: number;
}>`
  display: grid;
  grid-template-columns: repeat(${(p) => p.$numCols}, auto);
  grid-template-rows: repeat(${(p) => p.$numRows}, auto);
  grid-column-gap: 0.5em;
  grid-row-gap: 0.5em;
`;

interface PhotoBlockViewProps extends BaseViewProps {
  block: PhotosBlock;
  photoViewRefCallback?: (elem: HTMLElement | null) => void;
}

const PhotoBlockView: React.FC<PhotoBlockViewProps> = (props) => {
  const { block, onPhotoClick, photoViewRefCallback } = props;

  const photoQueries = useQueries({
    queries: block.photoIds.map((id) => getPhotoMetadataQueryOptions({ id })),
  });

  if (!block.grid) {
    const elems: JSX.Element[] = [];

    for (let i = 0; i < block.photoIds.length; ++i) {
      const id = block.photoIds[i];
      const photo = photoQueries[i].data;
      if (!photo) continue;

      elems.push(
        <EmbeddedPhoto
          key={id}
          photo={photo}
          block={block}
          onClick={onPhotoClick}
          photoViewRefCallback={photoViewRefCallback}
        />
      );
    }

    return <>{elems}</>;
  } else {
    const elems: JSX.Element[] = [];

    for (let i = 0; i < block.photoIds.length; ++i) {
      const id = block.photoIds[i];
      const photo = photoQueries[i].data;
      if (!photo) continue;

      elems.push(
        <CroppedPhotoLink
          key={id}
          onClick={() => onPhotoClick(photo)}
          refCallback={photoViewRefCallback}
          data-photo-id={id}
        >
          <CroppedPhoto
            photo={photo}
            ratio={block.grid.ratio ?? 1}
            autoResize
            loading="lazy"
          />
        </CroppedPhotoLink>
      );
    }

    return (
      <PhotoGrid $numCols={block.grid.cols} $numRows={block.grid.rows}>
        {elems}
      </PhotoGrid>
    );
  }
};

interface EmbeddedPhotoProps {
  block: PhotosBlock;
  photo: FullPhoto;
  onClick: (photo: FullPhoto) => void;
  photoViewRefCallback?: (elem: HTMLElement | null) => void;
}

const EmbeddedPhoto: React.FC<EmbeddedPhotoProps> = ({
  block,
  photo,
  onClick,
  photoViewRefCallback,
}) => {
  const onClickHandler = useCallback(() => {
    if (onClick) {
      onClick(photo);
    }
  }, [onClick, photo]);

  const [photoSize, setPhotoSize] = useState<
    { width: number; height: number } | undefined
  >(undefined);
  const onResize = useCallback((width: number, height: number) => {
    setPhotoSize({ width, height });
  }, []);

  return (
    // This div will be sized based on the rich text column width (without padding).
    // Used as the parent container reference for the ConstrainedPhoto.
    <div>
      <ConstrainedPhotoLink
        onClick={onClickHandler}
        refCallback={photoViewRefCallback}
        // Size the link based on photo size. Needed to properly size for photos that
        // don't span the whole width.
        $width={photoSize?.width}
        $height={photoSize?.height}
        data-photo-id={photo.id}
      >
        <ConstrainedPhoto
          photo={photo}
          constrainWidth
          // Size relative to parent.
          maxParentWidth={block.maxWidth}
          maxParentHeight={block.maxHeight}
          constrainHeight={block.maxHeight !== undefined}
          autoResize
          resizedCallback={onResize}
          // For auto-resizing, size versus the outer div above (not the link div).
          parentLevel={2}
          // Lazy load hint for browser
          loading="lazy"
        />
      </ConstrainedPhotoLink>
    </div>
  );
};
