// SPDX-License-Identifier: MIT
import React, { useCallback, useEffect, useRef, useState } from "react";
import { isBot } from "../api/bot";
import { addEvents } from "../api/events";
import { EventTracker, EVENT_ADD_EVENTS } from "../helpers/EventTracker";
import { useEventListener } from "../hooks/useEventListener";
import { useUserSession } from "../hooks/useUserSession";
import { EventTrackerContext } from "../helpers/Contexts";

const SEND_EVENTS_INITIAL_PERIOD = 1000; // 1s
const SEND_EVENTS_PERIOD = 5000; // 5s

export const EventTrackerProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const [eventTracker] = useState(() => new EventTracker());

  const sendPeriod = useRef(SEND_EVENTS_INITIAL_PERIOD);

  // Periodically emit events.
  const timer = useRef<number | undefined>(undefined);
  const onEvent = useCallback(() => {
    if (timer.current !== undefined) return;
    timer.current = window.setTimeout(async () => {
      timer.current = undefined;

      const events = eventTracker.acquirePendingEvents();
      if (events.length === 0) return;

      // Ignore failures.
      await addEvents(events);

      // Next send period is the default (longer) period.
      sendPeriod.current = SEND_EVENTS_PERIOD;
    }, sendPeriod.current);
  }, [eventTracker]);

  useEffect(() => {
    // We may have already missed some events before this effect handler, so trigger event handler on mount.
    onEvent();
    eventTracker.addEventListener(EVENT_ADD_EVENTS, onEvent);
  }, [eventTracker, onEvent]);

  // If/when the page is hidden, send any pending events.
  const onVisibilityChange = useCallback(() => {
    if (document.visibilityState === "hidden") {
      const events = eventTracker.acquirePendingEvents();
      if (events.length === 0) return;

      // Beacon may be blocked by adblockers.
      addEvents(events, /*beacon*/ true);
    }
  }, [eventTracker]);
  useEventListener(document, "visibilitychange", onVisibilityChange);

  // Don't log events in admin or for bots.
  const userSession = useUserSession();
  useEffect(() => {
    const isAdmin = userSession.isLoggedIn && userSession.isAdmin;
    const enabled = !isAdmin && !isBot;
    eventTracker.setEnabled(enabled);
    if (!enabled) {
      // If not enabled, this means we shouldn't be tracking events. So acquire all pending events and do nothing with them.
      eventTracker.acquirePendingEvents();
    }
  }, [eventTracker, userSession.isAdmin, userSession.isLoggedIn]);

  return (
    <EventTrackerContext.Provider value={eventTracker}>
      {children}
    </EventTrackerContext.Provider>
  );
};
