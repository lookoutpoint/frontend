// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";

interface Props {
  value: number;
  onValueChange: (newValue: number) => void;
}

export const NumberInput: React.FC<Props> = ({ value, onValueChange }) => {
  const [rawValue, setRawValue] = useState<string>(`${value}`);

  const tryParseValue = useCallback(
    (e: HTMLInputElement) => {
      const v = +e.value;

      // If failed to parse value, return and do nothing.
      if (isNaN(v)) return;

      onValueChange(v);
    },
    [onValueChange]
  );

  return (
    <input
      type="text"
      value={rawValue}
      onChange={useCallback<React.ChangeEventHandler<HTMLInputElement>>((e) => {
        setRawValue(e.currentTarget.value);
      }, [])}
      onKeyDown={useCallback<React.KeyboardEventHandler<HTMLInputElement>>(
        (e) => {
          if (e.key === "Enter") {
            tryParseValue(e.currentTarget);
          }
        },
        [tryParseValue]
      )}
      onBlur={useCallback<React.FocusEventHandler<HTMLInputElement>>(
        (e) => {
          tryParseValue(e.currentTarget);
        },
        [tryParseValue]
      )}
    />
  );
};
