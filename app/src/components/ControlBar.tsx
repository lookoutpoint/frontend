// SPDX-License-Identifier: MIT
import React, { useEffect } from "react";
import styled from "styled-components";
import { EventDesc } from "../helpers/EventTracker";
import {
  CircleIconButton,
  DEFAULT_FONT_SIZE_EM,
  DEFAULT_SIZE_EM,
  Props as CircleIconButtonProps,
} from "./CircleIconButton";
import { useEventTracker } from "../hooks/useEventTracker";

const TIGHT_THRESHOLD = 400;
const TIGHTER_THRESHOLD = 350;

const ControlBarGroupDiv = styled.div`
  display: flex;
  flex-flow: row wrap;
  border-right: 1px solid var(--control-bar-group-divider-color);
`;

export const ControlBarGroup: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  if (React.Children.count(children) === 0) return null;
  return <ControlBarGroupDiv>{children}</ControlBarGroupDiv>;
};

export const ControlBarItem = styled.div``;

const ControlBarButtonTitle = styled.div`
  margin-top: 0.25em;
  text-align: center;
`;

const ControlBarGrid = styled.div`
  margin: 0.5em 0;
  padding: 0.25em 0;
  display: grid;
  grid-template-columns: 1fr auto 1fr;
  background-color: var(--control-bar-bg-color);
  border-top: 1px solid var(--control-bar-border-color);
  border-bottom: 1px solid var(--control-bar-border-color);

  &.sticky {
    position: sticky;
    top: 0;
    z-index: 10;
  }
`;

const ControlBarLeft = styled.div<{ $titleFontSize: number }>`
  grid-column: 1;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: center;
  padding: 0 0.5em;

  & ${ControlBarButtonTitle} {
    font-size: ${(p) => p.$titleFontSize}em;
  }

  @media (max-width: ${TIGHTER_THRESHOLD}px) {
    & ${ControlBarButtonTitle} {
      font-size: ${(p) => p.$titleFontSize * 0.9}em;
    }
  }
`;

const ControlBarRight = styled.div`
  grid-column: 3;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-end;
  align-items: center;
  padding: 0 0.5em;
`;

const ControlBarCenter = styled.div<{
  $padding: number;
  $titleFontSize: number;
}>`
  grid-column: 2;

  display: flex;
  flex-flow: row wrap;
  justify-content: center;

  /* Don't show divider after last group */
  ${ControlBarGroupDiv}:last-child {
    border-right: 0;
  }

  /* Trim padding on end groups */
  > div:first-of-type {
    padding-left: 0;
  }
  > div:last-child {
    padding-right: 0;
  }

  /* Paddings and title font size: relative to props */
  & ${ControlBarItem} {
    padding: 0 ${(p) => p.$padding}em;
  }
  & ${ControlBarGroupDiv} {
    padding: 0 ${(p) => p.$padding * 0.5}em;
  }
  & ${ControlBarButtonTitle} {
    font-size: ${(p) => p.$titleFontSize}em;
  }

  @media (max-width: ${TIGHT_THRESHOLD}px) {
    & ${ControlBarItem} {
      padding: 0 ${(p) => p.$padding * 0.6}em;
    }
  }
  @media (max-width: ${TIGHTER_THRESHOLD}px) {
    & ${ControlBarItem} {
      padding: 0 ${(p) => p.$padding * 0.3}em;
    }
    & ${ControlBarGroupDiv} {
      padding: 0 ${(p) => p.$padding * 0.3}em;
    }
    & ${ControlBarButtonTitle} {
      font-size: ${(p) => p.$titleFontSize * 0.9}em;
    }
  }
`;

interface ControlBarProps {
  sticky?: boolean;
  padding?: number; // defaults to 0.5em
  titleFontSize?: number; // defauls to 0.65em
  left?: JSX.Element;
  right?: JSX.Element;
  children?: React.ReactNode;
}

const DEFAULT_PADDING = 0.5;
const DEFAULT_TITLE_FONT_SIZE = 0.65;

export const ControlBar: React.FC<ControlBarProps> = ({
  sticky,
  padding,
  titleFontSize,
  left,
  right,
  children,
}) => {
  const classNames: string[] = [];
  if (sticky) {
    classNames.push("sticky");
  }

  padding = padding ?? DEFAULT_PADDING;
  titleFontSize = titleFontSize ?? DEFAULT_TITLE_FONT_SIZE;

  return (
    <ControlBarGrid className={classNames.join(" ")}>
      {left && (
        <ControlBarLeft $titleFontSize={titleFontSize}>{left}</ControlBarLeft>
      )}
      <ControlBarCenter $padding={padding} $titleFontSize={titleFontSize}>
        {children}
      </ControlBarCenter>
      {right && <ControlBarRight>{right}</ControlBarRight>}
    </ControlBarGrid>
  );
};

const ControlBarItemFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ControlBarButtonDiv = styled(ControlBarItemFlex)`
  width: 2em;
`;

const ControlBarTitleShortcut = styled.span`
  ${ControlBarButtonDiv}:hover & {
    text-decoration: underline;
    font-weight: bold;
  }
`;

interface ControlBarButtonProps extends CircleIconButtonProps {
  keyShortcut?: string;
  eventDesc?: EventDesc;
}

export const ControlBarButton: React.FC<ControlBarButtonProps> = (props) => {
  const { title, keyShortcut, ...otherProps } = props;
  const { onClick, eventDesc } = otherProps;

  const eventTracker = useEventTracker();

  useEffect(() => {
    if (keyShortcut && onClick) {
      const listener = (e: KeyboardEvent) => {
        if (e.key !== keyShortcut) return;

        // Don't process input field keydowns.
        const tagName = (e.target as HTMLElement).tagName;
        if (tagName === "INPUT" || tagName === "TEXTAREA") return;

        if (eventDesc) {
          eventTracker.addEvent(eventDesc);
        }

        onClick();
      };

      window.addEventListener("keydown", listener);
      return () => window.removeEventListener("keydown", listener);
    }
  }, [eventDesc, eventTracker, keyShortcut, onClick]);

  let titleElems: JSX.Element[] = [
    <React.Fragment key="">{title}</React.Fragment>,
  ];
  if (keyShortcut && title) {
    const keyIdx1 = title.indexOf(keyShortcut.toLowerCase());
    const keyIdx2 = title.indexOf(keyShortcut.toUpperCase());
    let keyIdx = -1;
    if (keyIdx1 >= 0 && keyIdx2 >= 0) {
      keyIdx = Math.min(keyIdx1, keyIdx2);
    } else if (keyIdx1 >= 0) {
      keyIdx = keyIdx1;
    } else if (keyIdx2 >= 0) {
      keyIdx = keyIdx2;
    }

    if (keyIdx >= 0) {
      titleElems = [];
      if (keyIdx > 0) {
        titleElems.push(
          <React.Fragment key="before">
            {title.substring(0, keyIdx)}
          </React.Fragment>
        );
      }
      titleElems.push(
        <ControlBarTitleShortcut key="shortcut">
          {title[keyIdx]}
        </ControlBarTitleShortcut>
      );
      if (keyIdx < title.length - 1) {
        titleElems.push(
          <React.Fragment key="after">
            {title.substring(keyIdx + 1)}
          </React.Fragment>
        );
      }
    }
  }

  return (
    <ControlBarButtonDiv>
      <CircleIconButton {...otherProps} />
      {title && <ControlBarButtonTitle>{titleElems}</ControlBarButtonTitle>}
    </ControlBarButtonDiv>
  );
};

interface ControlBarLabelProps {
  fontSizeEm?: number;
  sizeEm?: number; // button size, in em (default 2em)
  title?: string;
  children?: React.ReactNode;
}

const ControlBarLabelDiv = styled.div<{ $fontSizeEm: number; $sizeEm: number }>`
  font-size: ${(p) => p.$fontSizeEm}em;
  height: ${(p) => p.$sizeEm / p.$fontSizeEm}em;
  line-height: ${(p) => p.$sizeEm / p.$fontSizeEm}em;
`;

export const ControlBarLabel: React.FC<ControlBarLabelProps> = ({
  fontSizeEm,
  sizeEm,
  title,
  children,
}) => {
  return (
    <ControlBarItemFlex>
      <ControlBarLabelDiv
        $fontSizeEm={fontSizeEm || DEFAULT_FONT_SIZE_EM}
        $sizeEm={sizeEm || DEFAULT_SIZE_EM}
      >
        {children}
      </ControlBarLabelDiv>
      {title && <ControlBarButtonTitle>{title}</ControlBarButtonTitle>}
    </ControlBarItemFlex>
  );
};
