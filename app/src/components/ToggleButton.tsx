// SPDX-License-Identifier: MIT
import React from "react";
import styled from "styled-components";

interface Props {
  toggled: boolean;
  onClick?: () => void;
  children?: React.ReactNode;
}

const Button = styled.button<{ $toggled: boolean }>`
  background-color: var(
    ${(p) =>
      p.$toggled ? "--iconbtn-toggleActive-bg-color" : "--iconbtn-bg-color"}
  );
  color: var(
    ${(p) => (p.$toggled ? "--iconbtn-toggleActive-color" : "--iconbtn-color")}
  );
  border: 1px solid var(--photolink-border-color);

  &:hover {
    cursor: pointer;
  }
`;

export const ToggleButton: React.FC<Props> = (props) => {
  const { toggled, onClick, children } = props;

  return (
    <Button $toggled={toggled} onClick={onClick}>
      {children}
    </Button>
  );
};
