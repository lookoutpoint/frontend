// SPDX-License-Identifier: MIT
import React from "react";
import { IconNotPublic } from "./Icons";

interface Props {
  className?: string;
}

export const NotPublicIcon: React.FC<Props> = ({ className }) => {
  return <IconNotPublic className={className} title="Not public" />;
};
