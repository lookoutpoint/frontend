// SPDX-License-Identifier: MIT
import React, { useEffect, useMemo, useState } from "react";
import { useNavigate } from "react-router";
import { isBot } from "../api/bot";
import { resetCaches } from "../api/cache";
import { axiosPost } from "../helpers/http";
import { VisibilityTokensContext } from "../helpers/Contexts";

export const VisibilityTokensProvider: React.FC<{
  children?: React.ReactNode;
}> = ({ children }) => {
  const navigate = useNavigate();
  const [hasTokens, setHasTokens] = useState(false);

  // Initialization check for tokens.
  useEffect(() => {
    // Don't check if user agent is a bot.
    if (isBot) return;

    (async () => {
      const { success } = await axiosPost(
        "/photos/visibility/has-tokens",
        undefined
      );
      if (success) {
        setHasTokens(true);
      }
    })();
  }, []);

  const provider = useMemo(
    () => ({
      hasTokens,
      clearTokens() {
        (async () => {
          // Clear tokens.
          await axiosPost("/photos/visibility/clear", undefined);
          setHasTokens(false);

          // Reset all caches and reload the current route as it may have non-public content.
          resetCaches();
          navigate(0);
        })();
      },
      onTokenActivated() {
        setHasTokens(true);
      },
    }),
    [hasTokens, navigate]
  );

  return (
    <VisibilityTokensContext.Provider value={provider}>
      {children}
    </VisibilityTokensContext.Provider>
  );
};
