// SPDX-License-Identifier: MIT
import { useCallback, useEffect, useRef, useState } from "react";
import { PhotoListQuery } from "../helpers/PhotoListQuery";
import {
  localStorageGetBool,
  LocalStorageKey,
  localStorageSetBool,
} from "../helpers/localStorage";
import { Modal } from "./Modal";
import {
  getPhotoMetadata,
  PartialFullPhoto,
  PhotoIdGen,
  PhotoListPhoto,
} from "../api/photos";
import { MainView } from "./lightbox/MainView";
import { Helmet } from "react-helmet-async";
import { makeTitleString } from "../util";
import { useEventListener } from "../hooks/useEventListener";
import { isBot } from "../api/bot";
import {
  AppEvent,
  EventButton,
  EventContext,
  makeEventPhotoID,
} from "../api/events";
import { useEventTracker } from "../hooks/useEventTracker";
import { ZoomablePhotoState } from "./lightbox/ExternalInterface";
import { DEFAULT_ZOOMABLE_PHOTO_STATE } from "./lightbox/ExternalInterface";
import { useErrorBoundary } from "react-error-boundary";
import { useQueryClient } from "@tanstack/react-query";

interface Props<K extends keyof PhotoListPhoto> {
  photoId?: PhotoIdGen;
  photoListQuery: PhotoListQuery<K>;
  queryForward: boolean;
  closeUrl: string;
  modifyTitle?: boolean; // defaults to true
  showSidebarOnOpen?: boolean;
  onClose: () => void;
  onPhotoNav: (photo: PhotoListPhoto) => void;
  makePhotoLink?: (photo: PhotoListPhoto) => string;
  eventContext?: string; // defaults to default context
}

// State for main photo and its neighbours. Kept in one state so that we can get atomic updates
// so that we don't run into issues where 'photo' is updated but 'prevPhoto' and 'nextPhoto' are not yet
// when rendering, which can occur photo id conflicts (due to prefetching).
interface PhotoNeighbourhood {
  photo: PartialFullPhoto | null;
  prevPhoto: PartialFullPhoto | null;
  nextPhoto: PartialFullPhoto | null;
}

// Slideshow state.
interface Slideshow {
  show: boolean;

  // Controls if the transition effect should show.
  transition?: boolean;
}

const SLIDESHOW_INTERVAL = 4000; // ms

export function PhotoLightbox<K extends keyof PhotoListPhoto>(props: Props<K>) {
  const {
    photoId,
    photoListQuery,
    queryForward,
    closeUrl,
    modifyTitle = true,
    showSidebarOnOpen,
    onClose: onCloseImpl,
    onPhotoNav: onPhotoNavImpl,
    makePhotoLink,
    eventContext = EventContext.DEFAULT,
  } = props;
  const { sortKey } = photoListQuery;
  const queryClient = useQueryClient();
  const eventTracker = useEventTracker();
  const { showBoundary } = useErrorBoundary();

  const addUXEvent = useCallback(
    (event?: AppEvent, object?: string) => {
      eventTracker.addUXEvent({
        event,
        object,
        context: eventContext,
      });
    },
    [eventContext, eventTracker]
  );

  // Zoomable main photo.
  const [mainPhotoZoomState, setMainPhotoZoomState] =
    useState<ZoomablePhotoState>(DEFAULT_ZOOMABLE_PHOTO_STATE);

  // Helper to generate a PhotoNeighbourhood state from a photo. Fully synchronously
  // and does do any fetches (so its data may be incomplete).
  const getPhotoNeighbourhoodSync = useCallback(
    (photo: PhotoListPhoto | null) => {
      const adjacent = photo
        ? photoListQuery.adjacentSync(photo[sortKey], queryForward)
        : null;

      return {
        photo,
        prevPhoto: adjacent?.prev ?? null,
        nextPhoto: adjacent?.next ?? null,
      };
    },
    [photoListQuery, queryForward, sortKey]
  );

  // Photo neighbourhood state.
  const [{ photo, prevPhoto, nextPhoto }, setPhotoNeighbourhood] =
    useState<PhotoNeighbourhood>(() => {
      // Initialize to the best of our ability by fetching photo and neighbours synchronously.
      const photo: PhotoListPhoto | null = photoId
        ? photoListQuery.getByIdSync(photoId.id) ?? null
        : null;
      return getPhotoNeighbourhoodSync(photo);
    });

  // Wrapper around onPhotoNavImpl. Each time we navigate, also update the
  // photo neighbourhood synchronously. Due to prefetching and PhotoListQuery, a lot of the time
  // the information we need should already be available (especially for adjacent photos).
  // This is necessary to get a smooth transition while navigating.
  const onPhotoNav = useCallback(
    (photo: PhotoListPhoto) => {
      setMainPhotoZoomState(DEFAULT_ZOOMABLE_PHOTO_STATE); // do not preserve zoom on nav
      setPhotoNeighbourhood(getPhotoNeighbourhoodSync(photo));
      onPhotoNavImpl(photo);
    },
    [getPhotoNeighbourhoodSync, onPhotoNavImpl]
  );

  // Sidebar boolean flag state.
  const [showSidebar, rawSetShowSidebar] = useState(() => {
    // Show sidebar for more information if user agent is a bot.
    if (isBot) return true;

    return (
      showSidebarOnOpen ??
      localStorageGetBool(LocalStorageKey.PHOTO_LIGHTBOX_SHOW_SIDEBAR)
    );
  });

  const setShowSidebar = useCallback(
    (show: boolean) => {
      rawSetShowSidebar(show);
      localStorageSetBool(LocalStorageKey.PHOTO_LIGHTBOX_SHOW_SIDEBAR, show);
      addUXEvent(AppEvent.BUTTON_CLICK, EventButton.LIGHTBOX_INFO);
    },
    [addUXEvent]
  );

  // Slideshow state
  const [slideshow, setSlideshow] = useState<Slideshow>({ show: false });
  const slideshowTimer = useRef<number | undefined>();

  // Fetch full photo metadata and adjacent photos.
  useEffect(() => {
    if (!photoId) return;

    const photo = photoListQuery.getByIdSync(photoId.id);

    if (!showSidebar && photo) {
      // Fetch adjacent photos and that's it.
      photoListQuery
        .adjacent(photo[sortKey], queryForward)
        .then(({ prev, next }) => {
          setPhotoNeighbourhood({
            photo,
            prevPhoto: prev,
            nextPhoto: next,
          });
        });
      return;
    }

    // Get full photo metadata.
    getPhotoMetadata(queryClient, photoId).then(
      (photo) => {
        // Try to get adjacent photos if already available.
        const adjacent = photoListQuery.adjacentSync(
          photo[sortKey],
          queryForward
        );
        setPhotoNeighbourhood({
          photo,
          prevPhoto: adjacent?.prev ?? null,
          nextPhoto: adjacent?.next ?? null,
        });

        // Get adjacent photos, fetching async if necessary.
        photoListQuery
          .adjacent(photo[sortKey], queryForward)
          .then(({ prev, next }) => {
            setPhotoNeighbourhood({
              photo,
              prevPhoto: prev,
              nextPhoto: next,
            });

            if (showSidebar) {
              // Prefetch full photo metadata too. Makes slideshow and navigation smoother
              // if the sidebar is showing.
              if (prev) {
                getPhotoMetadata(queryClient, prev).then((prevPhoto) =>
                  setPhotoNeighbourhood((s) => {
                    // Race condition: lightbox may have navigated by the the time
                    // this request was fulfilled, so only update if the previous photo
                    // matches when the request was made
                    if (s.prevPhoto && s.prevPhoto.id === prev.id) {
                      return { ...s, prevPhoto };
                    } else {
                      return s;
                    }
                  })
                );
              }
              if (next) {
                getPhotoMetadata(queryClient, next).then((nextPhoto) =>
                  setPhotoNeighbourhood((s) => {
                    // Race condition: lightbox may have navigated by the the time
                    // this request was fulfilled, so only update if the next photo
                    // matches when the request was made
                    if (s.nextPhoto && s.nextPhoto.id === next.id) {
                      return { ...s, nextPhoto };
                    } else {
                      return s;
                    }
                  })
                );
              }
            }
          });
      },
      (error) => {
        showBoundary(error);
      }
    );
  }, [
    photoId,
    photoListQuery,
    queryClient,
    queryForward,
    showBoundary,
    showSidebar,
    sortKey,
  ]);

  // Slideshow timer callback. This is a ref because it references state that changes
  // during the slidedown and we always want it to reference the latest state.
  const onSlideshowTimer = useRef<() => void>(() => {});
  useEffect(() => {
    onSlideshowTimer.current = () => {
      // Sanity check
      if (!slideshow.show) return;

      // Advance to next photo.
      if (!nextPhoto) {
        // If there is no next photo, stop slideshow.
        setSlideshow({ show: false });
        return;
      }

      // Navigate to next photo and update lightbox state.
      onPhotoNav(nextPhoto);
      addUXEvent(AppEvent.LIGHTBOX_SLIDESHOW_PHOTO);

      // Schedule next timer.
      slideshowTimer.current = window.setTimeout(
        () => onSlideshowTimer.current(),
        SLIDESHOW_INTERVAL
      );

      setSlideshow({ show: true, transition: true });
    };
  }, [addUXEvent, nextPhoto, onPhotoNav, slideshow.show]);

  // On unmount, cancel any pending slideshow timer.
  useEffect(() => {
    return () => {
      clearTimeout(slideshowTimer.current);
      slideshowTimer.current = undefined;
    };
  }, []);

  // Helper to stop showing slideshow.
  const stopSlideshow = useCallback(() => {
    if (slideshow.show) {
      window.clearTimeout(slideshowTimer.current);
      slideshowTimer.current = undefined;

      setSlideshow({ show: false });
      addUXEvent(AppEvent.BUTTON_CLICK, EventButton.LIGHTBOX_SLIDESHOW);
    }
  }, [addUXEvent, slideshow.show]);

  // Helper to toggle slideshow.
  const onPlay = useCallback(() => {
    if (slideshow.show) {
      // Stop slideshow.
      stopSlideshow();
    } else {
      // Start slideshow.
      slideshowTimer.current = window.setTimeout(
        () => onSlideshowTimer.current(),
        SLIDESHOW_INTERVAL / 2 // make initial transition faster
      );
      setMainPhotoZoomState(DEFAULT_ZOOMABLE_PHOTO_STATE); // no zoom during slideshow
      setSlideshow({ show: true, transition: false });
      addUXEvent(AppEvent.BUTTON_CLICK, EventButton.LIGHTBOX_SLIDESHOW);
    }
  }, [addUXEvent, slideshow.show, stopSlideshow]);

  // Navigate to previous photo.
  const onPrev = useCallback(
    (swiped?: boolean) => {
      stopSlideshow();
      if (prevPhoto) {
        onPhotoNav(prevPhoto);

        // Record all prev navigation as a button click.
        addUXEvent(AppEvent.BUTTON_CLICK, EventButton.LIGHTBOX_NAV_PREV);
        if (swiped) {
          // Record subset that is a swipe.
          addUXEvent(AppEvent.LIGHTBOX_SWIPE_PREV);
        }

        // Do a prefetch a few photos before.
        photoListQuery.batchBefore(prevPhoto[sortKey], 2);
      }
    },
    [addUXEvent, onPhotoNav, photoListQuery, prevPhoto, sortKey, stopSlideshow]
  );

  // Navigate to next photo.
  const onNext = useCallback(
    (swiped?: boolean) => {
      stopSlideshow();
      if (nextPhoto) {
        onPhotoNav(nextPhoto);

        // Record all prev navigation as a button click.
        addUXEvent(AppEvent.BUTTON_CLICK, EventButton.LIGHTBOX_NAV_NEXT);
        if (swiped) {
          // Record subset that is a swipe.
          addUXEvent(AppEvent.LIGHTBOX_SWIPE_NEXT);
        }

        // Do a prefetch a few photos ahead.
        photoListQuery.batchAfter(nextPhoto[sortKey], 2);
      }
    },
    [stopSlideshow, nextPhoto, onPhotoNav, addUXEvent, photoListQuery, sortKey]
  );

  // Close lightbox.
  const onClose = useCallback(() => {
    stopSlideshow();
    onCloseImpl();
  }, [onCloseImpl, stopSlideshow]);

  // Key down listener.
  const onKeyDown = useCallback(
    (e: KeyboardEvent) => {
      switch (e.key) {
        case "Escape":
          if (slideshow.show) {
            // Stop slideshow and back to normal lightbox.
            stopSlideshow();
          } else if (mainPhotoZoomState.zoom > 1) {
            // Zoom out all the way.
            setMainPhotoZoomState(DEFAULT_ZOOMABLE_PHOTO_STATE);
          } else {
            // Exit lightbox.
            onCloseImpl();
          }
          break;

        case "ArrowLeft":
          onPrev();
          break;

        case "ArrowRight":
          onNext();
          break;

        case "d":
          setShowSidebar(!showSidebar);
          break;

        case "p":
          onPlay();
          break;
      }
    },
    [
      mainPhotoZoomState.zoom,
      onCloseImpl,
      onNext,
      onPlay,
      onPrev,
      setShowSidebar,
      showSidebar,
      slideshow.show,
      stopSlideshow,
    ]
  );

  useEventListener(window, "keydown", onKeyDown);

  // Event track on show.
  const trackedLightboxShow = useRef(false);
  if (!trackedLightboxShow.current) {
    addUXEvent(AppEvent.LIGHTBOX_SHOW);
    trackedLightboxShow.current = true;
  }

  // Photo view and detail events.
  useEffect(() => {
    if (!photo) return;
    eventTracker.addEvent({
      event: AppEvent.PHOTO_VIEW_LIGHTBOX,
      object: makeEventPhotoID(photo),
      context: eventContext,
    });

    if (showSidebar) {
      eventTracker.addEvent({
        event: AppEvent.PHOTO_VIEW_DETAIL,
        object: makeEventPhotoID(photo),
        context: eventContext,
      });
    }
  }, [eventContext, eventTracker, photo, showSidebar]);

  // Render elements
  return (
    <Modal backgroundColor="var(--bg-color)">
      {photo !== null ? (
        <>
          {modifyTitle && photo && photo.title && (
            <Helmet>
              <title>{makeTitleString(photo.title)}</title>
              {photo.description && (
                <meta name="description" content={photo.description} />
              )}
            </Helmet>
          )}
          <MainView
            photo={photo}
            prevPhoto={prevPhoto}
            nextPhoto={nextPhoto}
            showSlideshow={slideshow.show}
            showSidebar={showSidebar}
            transitionFromPrev={!!slideshow.transition}
            closeUrl={closeUrl}
            makePhotoLink={makePhotoLink}
            onPlay={onPlay}
            onSidebar={() => setShowSidebar(!showSidebar)}
            onClose={onClose}
            onPrev={onPrev}
            onNext={onNext}
            mainPhotoZoomState={mainPhotoZoomState}
            setMainPhotoZoomState={setMainPhotoZoomState}
          />
        </>
      ) : null}
    </Modal>
  );
}
