// SPDX-License-Identifier: MIT
import React, { useLayoutEffect, useRef } from "react";
import ReactDOM from "react-dom";
import styled, { createGlobalStyle } from "styled-components";
import { useWindowSize } from "../hooks/useWindowSize";

interface Props {
  backgroundColor?: string;
  level?: number;
  children?: React.ReactNode;
}

const HideScrollbar = createGlobalStyle<{ scrollBarWidth: number }>`
  html.hide-sb {
    /* for Firefox */
    scrollbar-width: none;
  }
  body.hide-sb {
    position: fixed;
    /* left/right values force body to stretch, otherwise weird things happen?! */
    left: 0;
    right: 0;

    scrollbar-width: none;
    -ms-overflow-style: none;
  }
  body.hide-sb::-webkit-scrollbar {
    background-color: transparent;
    width: 0px;
  }
  body.hide-sb #root > div:first-child {
    padding-right: ${(p) => p.scrollBarWidth}px;
  }
`;

const ModalDiv = styled.div<{ $backgroundColor: string; $level: number }>`
  position: fixed;
  z-index: ${(p) => 100 + p.$level};
  left: 0;
  top: 0;
  width: 100vw;
  height: 100%;
  overflow: hidden;
  background-color: ${(props) => props.$backgroundColor};
`;

const ModalImpl: React.FC<Props> = ({ backgroundColor, level, children }) => {
  // Prevent scrolling of main body while modal is open.
  // Reference: https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
  const lastScrollY = useRef(0);
  const lastBodyMarginTop = useRef("");

  const windowSize = useWindowSize();

  useLayoutEffect(() => {
    // On mount.

    // Save for restore later.
    lastScrollY.current = window.scrollY;
    lastBodyMarginTop.current = document.body.style.marginTop;

    // Disable scrolling.
    document.body.classList.add("hide-sb");
    document.documentElement.classList.add("hide-sb");

    // Shift the body to visually maintain the scroll position.
    document.body.style.marginTop = `${-lastScrollY.current}px`;

    // On unmount.
    return () => {
      document.body.style.marginTop = lastBodyMarginTop.current;
      document.body.classList.remove("hide-sb");
      document.documentElement.classList.remove("hide-sb");
      window.scrollTo(0, lastScrollY.current);
    };
  }, []);

  return (
    <ModalDiv
      $backgroundColor={backgroundColor ?? "transparent"}
      $level={level ?? 0}
    >
      <HideScrollbar scrollBarWidth={windowSize.scrollBarWidth} />
      {children}
    </ModalDiv>
  );
};

export const Modal: React.FC<Props> = ({ backgroundColor, children }) => {
  return ReactDOM.createPortal(
    <ModalImpl backgroundColor={backgroundColor}>{children}</ModalImpl>,
    document.getElementById("modals")!
  );
};
