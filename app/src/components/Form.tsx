// SPDX-License-Identifier: MIT
import styled from "styled-components";

export const FormGrid = styled.div`
  display: grid;
  grid-template-columns: minmax(0, auto) 1fr;
  grid-auto-rows: auto;
  grid-gap: 0.5em;
`;

export const FormButton = styled.button`
  grid-column: 2/2;
`;

export const DividedRows = styled.div`
  & > div {
    border-top: 1px solid #888;
    padding-top: 0.5em;
  }

  & > &, & > div:first-child {
    border-top: 0;
    padding-top 0;
  }
`;

export const Row = styled.div`
  margin: 0.5em 0;
  line-height: 2em;

  input[type="checkbox"] {
    height: 2em;
  }
`;

export const FlexDiv = styled.div`
  display: flex;
`;

export const FlexRow = styled(Row)<{ $autoGap?: boolean }>`
  display: flex;

  & > * + * {
    margin-left: ${(p) => (p.$autoGap !== false ? 0.5 : 0)}em;
  }
`;

export const FlexRowNoMargin = styled(FlexRow)`
  margin: 0;
`;

export const IndentedFlexRow = styled(FlexRow)<{ $level?: number }>`
  margin-left: ${(p) => p.$level ?? 1}em;
`;
