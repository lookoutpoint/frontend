// SPDX-License-Identifier: MIT
import styled from "styled-components";

export const RatioPaddingWrapper = styled.div<{ $ratio: number }>`
  position: relative;
  padding-bottom: ${(props) => (1 / props.$ratio) * 100 + "%"};
  overflow: hidden;
`;
