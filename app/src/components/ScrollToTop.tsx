// SPDX-License-Identifier: MIT
import React, { useCallback, useLayoutEffect, useMemo, useState } from "react";
import { CircleIconButton } from "./CircleIconButton";
import styled from "styled-components";
import { useWindowSize } from "../hooks/useWindowSize";
import { useEventListener } from "../hooks/useEventListener";
import { EventDesc } from "../helpers/EventTracker";
import { AppEvent, EventButton } from "../api/events";
import { IconArrowUp } from "./Icons";

const Button = styled(CircleIconButton)`
  position: fixed;
  bottom: var(--fixed-bottom-button-offset);
  right: var(--fixed-bottom-button-offset);
  border: 2px solid var(--iconbtn-color);
  z-index: 100;
`;

interface Props {
  eventContext?: string;
}

export const ScrollToTop: React.FC<Props> = (props) => {
  const { eventContext } = props;

  const windowSize = useWindowSize();

  const onClick = useCallback(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  const [show, setShow] = useState(false);

  const onScroll = useCallback(() => {
    const scrollTop =
      document.body.scrollTop || document.documentElement.scrollTop;
    setShow(scrollTop > (windowSize.height * 2) / 3);
  }, [windowSize.height]);

  useEventListener(window, "scroll", onScroll);

  useLayoutEffect(() => {
    // On render, check if should be shown.
    onScroll();
  }, [onScroll]);

  const eventDesc = useMemo<EventDesc | undefined>(() => {
    if (eventContext === undefined) return undefined;
    return {
      event: AppEvent.BUTTON_CLICK,
      object: EventButton.SCROLL_TO_TOP,
      context: eventContext,
    };
  }, [eventContext]);

  return show ? (
    <Button
      background="var(--scrolltotop-bg-color)"
      sizeEm={3}
      fontSizeEm={2}
      onClick={onClick}
      eventDesc={eventDesc}
    >
      <IconArrowUp />
    </Button>
  ) : null;
};
