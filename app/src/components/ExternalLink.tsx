// SPDX-License-Identifier: MIT
import React from "react";
import styled from "styled-components";
import { IconExternalLink } from "./Icons";

const StyledLink = styled.a`
  color: var(--richtext-link-color);
  text-decoration: underline;
  text-decoration-color: var(--richtext-link-color);

  svg {
    font-size: 0.8em;
  }
`;

const StyledIcon = styled(IconExternalLink)`
  margin-left: 0.15em;
`;

export const ExternalLink: React.FC<
  React.AnchorHTMLAttributes<HTMLAnchorElement>
> = ({ children, ...props }) => {
  return (
    <StyledLink {...props} target="_blank" rel="noopener noreferrer">
      {children}
      <StyledIcon />
    </StyledLink>
  );
};
