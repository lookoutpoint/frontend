// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { RatioPaddingWrapper } from "./RatioPaddingWrapper";
import { SERVICE_ROOT } from "../util";
import { FullPhoto } from "../api/photos";

interface Props extends React.ImgHTMLAttributes<HTMLImageElement> {
  photo: Pick<FullPhoto, "id" | "storeGen" | "width" | "height" | "title">;
  ratio?: number; // if undefined, uses parent
  autoResize?: boolean;
}

// Needs to be in sync with the backend.
const maxLengths = [300, 600, 1200, 2400, 4800];

type XYCenter = "x" | "y" | null;

function xyCenterPick<X, Y, Neither>(
  xyCenter: XYCenter,
  x: X,
  y: Y,
  neither: Neither
): X | Y | Neither {
  if (xyCenter === "x") {
    return x;
  } else if (xyCenter === "y") {
    return y;
  } else {
    return neither;
  }
}

const BasicWrapper = styled.div`
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100%;
`;

const FittedImg = styled.img<{ $xyCenter: XYCenter }>`
  position: absolute;
  left: ${(p) => xyCenterPick(p.$xyCenter, "50%", 0, 0)};
  top: ${(p) => xyCenterPick(p.$xyCenter, 0, "50%", 0)};
  transform: ${(p) =>
    xyCenterPick(p.$xyCenter, "translateX(-50%)", "translateY(-50%)", "none")};
  width: ${(p) => xyCenterPick(p.$xyCenter, "auto", "100%", "100%")};
  height: ${(p) => xyCenterPick(p.$xyCenter, "100%", "auto", "100%")};
`;

export const CroppedPhoto: React.FC<Props> = React.memo(
  ({ photo, ratio, autoResize = true, ...imgProps }) => {
    const imgRef = useRef<HTMLImageElement>(null);

    const [rzMaxLength, setRzMaxLength] = useState<number | null>(null);
    const [xyCenter, setXYCenter] = useState<XYCenter>(null);

    const resize = useCallback(() => {
      // This can happen in some corner cases...
      if (!imgRef.current) return;

      // Target dimensions and ratio
      let targetElem = imgRef.current!.parentElement!;
      if (ratio !== undefined) {
        targetElem = targetElem.parentElement!;
      }

      const [tWidth, tHeight] = [
        targetElem.clientWidth,
        targetElem.clientHeight,
      ];
      const tRatio = tWidth / tHeight;

      // Master dimensions and ratio
      const [mWidth, mHeight] = [photo.width, photo.height];
      const mRatio = mWidth / mHeight;

      // Compute resized dimensions to fit target
      let rWidth, rHeight;
      if (tRatio >= mRatio) {
        rWidth = tWidth;
        rHeight = tWidth / mRatio;
      } else {
        rHeight = tHeight;
        rWidth = tHeight * mRatio;
      }

      // Take longest length.
      const rMaxLength = Math.max(rWidth, rHeight);

      // Account for device pixel ratio.
      const dpr = window.devicePixelRatio || 1;
      const tMaxLength = rMaxLength * dpr;

      // Find the smallest max length >= rMaxLength.
      let maxLength;
      for (const ml of maxLengths) {
        if (ml >= tMaxLength) {
          maxLength = ml;
          break;
        }
      }

      if (maxLength === undefined) {
        // Use original image.
        maxLength = 0;
      }

      setRzMaxLength(maxLength);
      setXYCenter(tRatio >= mRatio ? "y" : "x");
    }, [photo.height, photo.width, ratio]);

    // Auto-resize
    useEffect(() => {
      if (!autoResize) return;

      let rafId: number | undefined;

      const resizeObsv = new ResizeObserver(() => {
        // Only trigger resizing at most once per frame
        if (rafId === undefined) {
          rafId = window.requestAnimationFrame(() => {
            resize();
            rafId = undefined;
          });
        }
      });

      let targetElem = imgRef.current!.parentElement!;
      if (ratio !== undefined) {
        targetElem = targetElem.parentElement!; // parent of RatioPaddingWrapper
      }
      resizeObsv.observe(targetElem);

      return () => {
        resizeObsv.disconnect();
      };
    }, [autoResize, ratio, resize]);

    // Initial resize. Use useLayoutEffect so to update before paint.
    useLayoutEffect(() => {
      resize();
    }, [resize]);

    const fittedImg = (
      <FittedImg
        {...imgProps}
        $xyCenter={xyCenter}
        ref={imgRef}
        src={
          rzMaxLength !== null
            ? `${SERVICE_ROOT}/photos/photo/rz/${rzMaxLength}/${photo.id}/${photo.storeGen}.jpg`
            : undefined
        }
        alt={photo.title}
      />
    );

    if (ratio === undefined) {
      return <BasicWrapper>{fittedImg}</BasicWrapper>;
    } else {
      return (
        <RatioPaddingWrapper $ratio={ratio}>{fittedImg}</RatioPaddingWrapper>
      );
    }
  }
);
