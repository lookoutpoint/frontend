// SPDX-License-Identifier: MIT
import React from "react";
import styled from "styled-components";
import { LinkWithEvent } from "./LinkWithEvent";
import { AppEvent, EventContext } from "../api/events";

const WrappedFooter = styled.footer`
  border-top: 1px solid var(--control-bar-border-color);
  background-color: var(--control-bar-bg-color);
  text-align: center;
  padding: 1em;
  margin-top: 0.5em;

  > * {
    display: inline-block;
    padding: 0 0.5em;
    border-left: 1px solid var(--nav-sep-color);
  }

  :first-child {
    border-left-color: var(--control-bar-bg-color);
  }
`;

export const Footer: React.FC = () => {
  const curYear = new Date().getFullYear();
  return (
    <WrappedFooter>
      <span>Copyright 2021-{curYear} Jason Wong.</span>
      <LinkWithEvent
        to="/about/"
        event={{
          event: AppEvent.GROUP_CLICK,
          object: "/about/",
          context: EventContext.DEFAULT,
        }}
      >
        About
      </LinkWithEvent>
      <LinkWithEvent
        to="/licensing/"
        event={{
          event: AppEvent.GROUP_CLICK,
          object: "/licensing/",
          context: EventContext.DEFAULT,
        }}
      >
        Licensing
      </LinkWithEvent>
      <a href="mailto:contact@lookoutpoint.ca">contact@lookoutpoint.ca</a>
    </WrappedFooter>
  );
};
