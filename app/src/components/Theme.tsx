// SPDX-License-Identifier: MIT
import React, { useEffect, useLayoutEffect, useMemo, useState } from "react";
import { AppEvent } from "../api/events";
import {
  localStorageGet,
  LocalStorageKey,
  localStorageSet,
} from "../helpers/localStorage";
import { useEventTracker } from "../hooks/useEventTracker";
import { ThemeType } from "../helpers/Contexts";
import { Theme, ThemeContext } from "../helpers/Contexts";

export const ThemeProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const eventTracker = useEventTracker();

  const [type, setType] = useState<ThemeType>(() => {
    // Try to get from local storage.
    const type = localStorageGet(LocalStorageKey.THEME);
    switch (type) {
      case ThemeType.LIGHT:
      case ThemeType.DARK:
        return type;
      default:
        return ThemeType.DARK; // default theme
    }
  });

  // Apply theme to body
  useLayoutEffect(() => {
    const newTheme = `theme-${type}`;

    // Remove any existing theme classes that differ from the new theme
    // (don't want to remove and then add back the same theme, that's a no-op).
    const curThemeClasses = [...document.body.classList.values()].filter((c) =>
      c.startsWith("theme-")
    );

    // Do nothing if the theme class is not changing.
    if (curThemeClasses.length === 1 && curThemeClasses[0] === newTheme) return;

    // If there are existing theme classes, apply a transition.
    if (curThemeClasses.length > 0) {
      // Remove the transition once it is complete.
      const listener = () => {
        document.body.classList.remove("transition-theme");
      };
      document.body.addEventListener("transitionend", listener, {
        once: true,
      });

      document.body.classList.add("transition-theme");
    }

    // Remove old theme classes.
    document.body.classList.remove(...curThemeClasses);

    // Add new theme class.
    document.body.classList.add(`theme-${type}`);
  }, [type]);

  const theme: Theme = useMemo(
    () => ({
      type,
      setType: (type: ThemeType, save?: boolean) => {
        save = save ?? true;

        if (save) {
          // Update local storage
          localStorageSet(LocalStorageKey.THEME, type);
        }

        setType(type);
      },
    }),
    [type]
  );

  // Event tracking.
  useEffect(() => {
    const event =
      type === ThemeType.LIGHT ? AppEvent.THEME_LIGHT : AppEvent.THEME_DARK;
    eventTracker.addEvent({ event });
  }, [eventTracker, type]);

  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
};
