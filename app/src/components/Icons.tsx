// SPDX-License-Identifier: MIT
import React from "react";
import styled from "styled-components";

import MaterialAdd from "@material-design-icons/svg/outlined/add.svg?react";
import MaterialArrowBack from "@material-design-icons/svg/outlined/arrow_back.svg?react";
import MaterialArrowDownward from "@material-design-icons/svg/outlined/arrow_downward.svg?react";
import MaterialArrowForward from "@material-design-icons/svg/outlined/arrow_forward.svg?react";
import MaterialArrowUpward from "@material-design-icons/svg/outlined/arrow_upward.svg?react";
import MaterialCalendarMonth from "@material-design-icons/svg/outlined/calendar_month.svg?react";
import MaterialCategory from "@material-design-icons/svg/outlined/category.svg?react";
import MaterialCheck from "@material-design-icons/svg/outlined/check.svg?react";
import MaterialChevronLeft from "@material-design-icons/svg/outlined/chevron_left.svg?react";
import MaterialChevronRight from "@material-design-icons/svg/outlined/chevron_right.svg?react";
import MaterialClose from "@material-design-icons/svg/outlined/close.svg?react";
import MaterialContentCopy from "@material-design-icons/svg/outlined/content_copy.svg?react";
import MaterialCopyright from "@material-design-icons/svg/outlined/copyright.svg?react";
import MaterialDarkMode from "@material-design-icons/svg/outlined/dark_mode.svg?react";
import MaterialDescription from "@material-design-icons/svg/outlined/description.svg?react";
import MaterialEdit from "@material-design-icons/svg/outlined/edit.svg?react";
import MaterialExpandMore from "@material-design-icons/svg/outlined/expand_more.svg?react";
import MaterialFeed from "@material-design-icons/svg/outlined/feed.svg?react";
import MaterialFileUpload from "@material-design-icons/svg/outlined/file_upload.svg?react";
import MaterialFlight from "@material-design-icons/svg/outlined/flight.svg?react";
import MaterialHandyman from "@material-design-icons/svg/outlined/handyman.svg?react";
import MaterialHike from "@material-design-icons/svg/outlined/hiking.svg?react";
import MaterialHome from "@material-design-icons/svg/filled/home.svg?react";
import MaterialLabel from "@material-design-icons/svg/outlined/label.svg?react";
import MaterialLightMode from "@material-design-icons/svg/outlined/light_mode.svg?react";
import MaterialLocalFlorist from "@material-design-icons/svg/filled/local_florist.svg?react";
import MaterialLock from "@material-design-icons/svg/outlined/lock.svg?react";
import MaterialMap from "@material-design-icons/svg/outlined/map.svg?react";
import MaterialMenu from "@material-design-icons/svg/outlined/menu.svg?react";
import MaterialMoreHoriz from "@material-design-icons/svg/outlined/more_horiz.svg?react";
import MaterialMoreVert from "@material-design-icons/svg/outlined/more_vert.svg?react";
import MaterialNotes from "@material-design-icons/svg/outlined/notes.svg?react";
import MaterialOpenInNew from "@material-design-icons/svg/outlined/open_in_new.svg?react";
import MaterialPanorama from "@material-design-icons/svg/outlined/panorama.svg?react";
import MaterialPark from "@material-design-icons/svg/outlined/park.svg?react";
import MaterialPause from "@material-design-icons/svg/outlined/pause.svg?react";
import MaterialPermMedia from "@material-design-icons/svg/outlined/perm_media.svg?react";
import MaterialPerson from "@material-design-icons/svg/outlined/person.svg?react";
import MaterialPets from "@material-design-icons/svg/filled/pets.svg?react";
import MaterialPhotoCamera from "@material-design-icons/svg/outlined/photo_camera.svg?react";
import MaterialPlace from "@material-design-icons/svg/filled/place.svg?react";
import MaterialPlayArrow from "@material-design-icons/svg/filled/play_arrow.svg?react";
import MaterialPublic from "@material-design-icons/svg/outlined/public.svg?react";
import MaterialRemove from "@material-design-icons/svg/outlined/remove.svg?react";
import MaterialRestartAlt from "@material-design-icons/svg/outlined/restart_alt.svg?react";
import MaterialSchedule from "@material-design-icons/svg/outlined/schedule.svg?react";
import MaterialShowChart from "@material-design-icons/svg/outlined/show_chart.svg?react";
import MaterialStarOutline from "@material-design-icons/svg/outlined/star_outline.svg?react";
import MaterialUnfoldLess from "@material-design-icons/svg/outlined/unfold_less.svg?react";
import MaterialUnfoldMore from "@material-design-icons/svg/outlined/unfold_more.svg?react";
import MaterialZoomIn from "@material-design-icons/svg/outlined/zoom_in.svg?react";
import MaterialZoomOut from "@material-design-icons/svg/outlined/zoom_out.svg?react";

const CustomCaption: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19 5H5V9H19V5ZM5 19V10H19V19H5ZM3 3V21H21V3H3ZM7 8H17V6H7V8Z"
      />
    </svg>
  );
};

const CustomTimeline: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.75 7C5.16421 7 5.5 6.66421 5.5 6.25C5.5 5.83579 5.16421 5.5 4.75 5.5C4.33579 5.5 4 5.83579 4 6.25C4 6.66421 4.33579 7 4.75 7ZM7 6.25C7 7.0301 6.60299 7.71748 6 8.12111V10.75H12V8.12111C11.397 7.71748 11 7.0301 11 6.25C11 5.00736 12.0074 4 13.25 4C14.4926 4 15.5 5.00736 15.5 6.25C15.5 7.0301 15.103 7.71748 14.5 8.12111V10.75H19.2L17.7808 8.97609C17.3496 8.43701 17.437 7.65039 17.9761 7.21913C18.5151 6.78787 19.3018 6.87527 19.733 7.41435L22.1713 10.4622C22.2788 10.5966 22.3541 10.7463 22.3981 10.9021C22.7865 11.1142 23.05 11.5263 23.05 12C23.05 12.4737 22.7865 12.8858 22.3981 13.0979C22.3541 13.2537 22.2788 13.4034 22.1713 13.5378L19.733 16.5857C19.3018 17.1247 18.5151 17.2121 17.9761 16.7809C17.437 16.3496 17.3496 15.563 17.7808 15.0239L19.2 13.25H10.5V16.8789C11.103 17.2825 11.5 17.9699 11.5 18.75C11.5 19.9926 10.4926 21 9.25 21C8.00736 21 7 19.9926 7 18.75C7 17.9699 7.39701 17.2825 8 16.8789V13.25H1.75V10.75H3.5V8.12111C2.89701 7.71748 2.5 7.0301 2.5 6.25C2.5 5.00736 3.50736 4 4.75 4C5.99264 4 7 5.00736 7 6.25ZM13.25 7C13.6642 7 14 6.66421 14 6.25C14 5.83579 13.6642 5.5 13.25 5.5C12.8358 5.5 12.5 5.83579 12.5 6.25C12.5 6.66421 12.8358 7 13.25 7ZM10 18.75C10 18.3358 9.66421 18 9.25 18C8.83579 18 8.5 18.3358 8.5 18.75C8.5 19.1642 8.83579 19.5 9.25 19.5C9.66421 19.5 10 19.1642 10 18.75Z"
      />
    </svg>
  );
};

const CustomTimeOrderAsc: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.9688 3.28405C19.7148 3.27961 19.4648 3.40431 19.3199 3.63509L17.399 6.6931C17.1786 7.04386 17.2844 7.50681 17.6351 7.72714C17.9859 7.94747 18.4488 7.84174 18.6692 7.49099L19.2188 6.61596V18.25C19.2188 18.6642 19.5546 19 19.9688 19C20.383 19 20.7188 18.6642 20.7188 18.25V6.61596L21.2685 7.49099C21.4888 7.84174 21.9518 7.94747 22.3025 7.72714C22.6533 7.50681 22.759 7.04386 22.5387 6.6931L20.6177 3.63509C20.4728 3.40431 20.2228 3.27961 19.9688 3.28405ZM15 11.5C15 14.5376 12.5376 17 9.5 17C6.46243 17 4 14.5376 4 11.5C4 8.46243 6.46243 6 9.5 6C12.5376 6 15 8.46243 15 11.5ZM17 11.5C17 15.6421 13.6421 19 9.5 19C5.35786 19 2 15.6421 2 11.5C2 7.35786 5.35786 4 9.5 4C13.6421 4 17 7.35786 17 11.5ZM9 8V12L8.99999 12L9 12L12 14L12.5547 13.1679L10 11.4648V8H9Z"
      />
    </svg>
  );
};

const CustomTimeOrderDesc: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.2188 3.75C19.2188 3.33579 19.5546 3 19.9688 3C20.383 3 20.7188 3.33579 20.7188 3.75V15.384L21.2685 14.509C21.4888 14.1583 21.9518 14.0525 22.3025 14.2729C22.6533 14.4932 22.759 14.9561 22.5387 15.3069L20.6177 18.3649C20.4728 18.5957 20.2228 18.7204 19.9688 18.7159C19.7148 18.7204 19.4648 18.5957 19.3199 18.3649L17.399 15.3069C17.1786 14.9561 17.2844 14.4932 17.6351 14.2729C17.9859 14.0525 18.4488 14.1583 18.6692 14.509L19.2188 15.384V3.75ZM15 11.5C15 14.5376 12.5376 17 9.5 17C6.46243 17 4 14.5376 4 11.5C4 8.46243 6.46243 6 9.5 6C12.5376 6 15 8.46243 15 11.5ZM17 11.5C17 15.6421 13.6421 19 9.5 19C5.35786 19 2 15.6421 2 11.5C2 7.35786 5.35786 4 9.5 4C13.6421 4 17 7.35786 17 11.5ZM9 8V12L8.99999 12L9 12L12 14L12.5547 13.1679L10 11.4648V8H9Z"
      />
    </svg>
  );
};

const CustomSidebarLeft: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19 5H11.5V19H19V5ZM5 5H10V7.75H5V5ZM5 12.75V14.75H10V12.75H5ZM5 11.25H10V9.25H5V11.25ZM5 19V16.25H10V19H5ZM3 3V21H21V3H3Z"
      />
    </svg>
  );
};

const CustomSidebarBottom: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5 5L5 12.5L19 12.5V5L5 5ZM5 19V14L7.75 14V19H5ZM12.75 19H14.75V14H12.75V19ZM11.25 19V14H9.25V19H11.25ZM19 19L16.25 19V14H19V19ZM3 21H21V3H3V21Z"
      />
    </svg>
  );
};

const CustomLocal: React.FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z"
      />
    </svg>
  );
};

const MaterialLicense: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      height="24px"
      viewBox="0 -960 960 960"
      width="24px"
      fill="none"
    >
      <path d="M480-440q-50 0-85-35t-35-85q0-50 35-85t85-35q50 0 85 35t35 85q0 50-35 85t-85 35ZM240-40v-309q-38-42-59-96t-21-115q0-134 93-227t227-93q134 0 227 93t93 227q0 61-21 115t-59 96v309l-240-80-240 80Zm240-280q100 0 170-70t70-170q0-100-70-170t-170-70q-100 0-170 70t-70 170q0 100 70 170t170 70ZM320-159l160-41 160 41v-124q-35 20-75.5 31.5T480-240q-44 0-84.5-11.5T320-283v124Zm160-62Z" />
    </svg>
  );
};

const StyledSvgIcon = styled.span`
  svg {
    display: inline-block;
    height: 1.2em;
    width: auto;
    fill: currentColor;
    vertical-align: text-bottom;
  }
`;

function makeSvgIcon(
  icon: React.ReactNode
): React.FC<React.HTMLAttributes<HTMLSpanElement>> {
  return (props) => <StyledSvgIcon {...props}>{icon}</StyledSvgIcon>;
}

export const IconAdd = makeSvgIcon(<MaterialAdd />);
export const IconAdmin = makeSvgIcon(<MaterialHandyman />);
export const IconAngleDown = makeSvgIcon(<MaterialExpandMore />);
export const IconAngleLeft = makeSvgIcon(<MaterialChevronLeft />);
export const IconAngleRight = makeSvgIcon(<MaterialChevronRight />);
export const IconAnimal = makeSvgIcon(<MaterialPets />);
export const IconArrowDown = makeSvgIcon(<MaterialArrowDownward />);
export const IconArrowLeft = makeSvgIcon(<MaterialArrowBack />);
export const IconArrowRight = makeSvgIcon(<MaterialArrowForward />);
export const IconArrowUp = makeSvgIcon(<MaterialArrowUpward />);
export const IconBlog = makeSvgIcon(<MaterialFeed />);
export const IconCamera = makeSvgIcon(<MaterialPhotoCamera />);
export const IconCaption = makeSvgIcon(<CustomCaption />);
export const IconCategories = makeSvgIcon(<MaterialCategory />);
export const IconCheck = makeSvgIcon(<MaterialCheck />);
export const IconClock = makeSvgIcon(<MaterialSchedule />);
export const IconClose = makeSvgIcon(<MaterialClose />);
export const IconCollapse = makeSvgIcon(<MaterialUnfoldLess />);
export const IconCollection = makeSvgIcon(<MaterialPermMedia />);
export const IconCollections = makeSvgIcon(<MaterialPermMedia />);
export const IconCopy = makeSvgIcon(<MaterialContentCopy />);
export const IconCopyright = makeSvgIcon(<MaterialCopyright />);
export const IconDarkMode = makeSvgIcon(<MaterialDarkMode />);
export const IconDate = makeSvgIcon(<MaterialCalendarMonth />);
export const IconDates = makeSvgIcon(<MaterialCalendarMonth />);
export const IconDescription = makeSvgIcon(<MaterialNotes />);
export const IconEdit = makeSvgIcon(<MaterialEdit />);
export const IconEllipsisH = makeSvgIcon(<MaterialMoreHoriz />);
export const IconEllipsisV = makeSvgIcon(<MaterialMoreVert />);
export const IconExpand = makeSvgIcon(<MaterialUnfoldMore />);
export const IconExternalLink = makeSvgIcon(<MaterialOpenInNew />);
export const IconFlower = makeSvgIcon(<MaterialLocalFlorist />);
export const IconHighlight = makeSvgIcon(<MaterialStarOutline />);
export const IconHike = makeSvgIcon(<MaterialHike />);
export const IconHome = makeSvgIcon(<MaterialHome />);
export const IconInfo = makeSvgIcon(<MaterialDescription />);
export const IconKeywords = makeSvgIcon(<MaterialLabel />);
export const IconLicense = makeSvgIcon(<MaterialLicense />);
export const IconLightMode = makeSvgIcon(<MaterialLightMode />);
export const IconLineChart = makeSvgIcon(<MaterialShowChart />);
export const IconLocal = makeSvgIcon(<CustomLocal />);
export const IconLocation = makeSvgIcon(<MaterialPlace />);
export const IconLocations = makeSvgIcon(<MaterialPublic />);
export const IconLock = makeSvgIcon(<MaterialLock />);
export const IconMap = makeSvgIcon(<MaterialMap />);
export const IconMenu = makeSvgIcon(<MaterialMenu />);
export const IconNotPublic = makeSvgIcon(<MaterialLock />);
export const IconPark = makeSvgIcon(<MaterialPark />);
export const IconPause = makeSvgIcon(<MaterialPause />);
export const IconPerson = makeSvgIcon(<MaterialPerson />);
export const IconPhoto = makeSvgIcon(<MaterialPanorama />);
export const IconPlay = makeSvgIcon(<MaterialPlayArrow />);
export const IconRecent = makeSvgIcon(<MaterialSchedule />);
export const IconRemove = makeSvgIcon(<MaterialRemove />);
export const IconSidebarBottom = makeSvgIcon(<CustomSidebarBottom />);
export const IconSidebarLeft = makeSvgIcon(<CustomSidebarLeft />);
export const IconTimeline = makeSvgIcon(<CustomTimeline />);
export const IconTimeOrderAsc = makeSvgIcon(<CustomTimeOrderAsc />);
export const IconTimeOrderDesc = makeSvgIcon(<CustomTimeOrderDesc />);
export const IconTravel = makeSvgIcon(<MaterialFlight />);
export const IconUpload = makeSvgIcon(<MaterialFileUpload />);
export const IconZoomIn = makeSvgIcon(<MaterialZoomIn />);
export const IconZoomOut = makeSvgIcon(<MaterialZoomOut />);
export const IconRestart = makeSvgIcon(<MaterialRestartAlt />);

export const IconCategoryAnimal = IconAnimal;
export const IconCategoryFlower = IconFlower;
export const IconCategoryHike = IconHike;
export const IconCategoryPark = IconPark;
