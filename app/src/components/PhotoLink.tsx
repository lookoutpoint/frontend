// SPDX-License-Identifier: MIT
import React from "react";
import { LinkProps } from "react-router-dom";
import styled from "styled-components";
import { EventDesc } from "../helpers/EventTracker";
import { LinkWithEvent } from "./LinkWithEvent";

// Should be used inside a flex container. Should wrap an image.
const StyledLink = styled(LinkWithEvent)<{ $borderRadius?: number }>`
  color: inherit;
  text-decoration: none;
  position: relative;
  cursor: pointer;
  border-radius: ${(p) => p.$borderRadius ?? 0}em;
  overflow: hidden;

  &:hover,
  &:visited {
    color: inherit;
    text-decoration: none;
  }
`;

const StyledDiv = styled.div<{ $borderRadius?: number }>`
  color: inherit;
  text-decoration: none;
  position: relative;
  cursor: pointer;
  border-radius: ${(p) => p.$borderRadius ?? 0}em;
  overflow: hidden;
`;

// Draws a border on top of the link. Insert as a child within PhotoLink.
const HoverBorder = styled.div<{
  $hoverBorderWidth: string;
  $borderRadius?: number;
}>`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  /* Important so that it doesn't get clipped or obscure things underneath (from an events perspective). */
  pointer-events: none;
  z-index: 2;

  ${StyledLink} > &, ${StyledDiv} > & {
    border: 1px solid var(--photolink-border-color);
    border-radius: ${(p) => p.$borderRadius ?? 0}em;
  }

  /* Only want this effect when hover is truly supported, otherwise annoying on primarily touch devices */
  @media (hover: hover) {
    /* Using && due to https://github.com/styled-components/styled-components/issues/3376 */
    ${StyledLink}:hover &&, ${StyledDiv}:hover && {
      border: ${(p) => p.$hoverBorderWidth} solid
        var(--photolink-hover-border-color);
      border-radius: ${(p) => p.$borderRadius ?? 0}em;
    }
  }
`;

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  to?: LinkProps["to"];
  hoverBorderWidth?: string; // defaults to OVERLAY_PADDING em
  borderRadius?: number;
  refCallback?: (elem: HTMLAnchorElement | null) => void;
  event?: EventDesc;
}

export const PhotoLink: React.FC<Props> = (props) => {
  const {
    hoverBorderWidth = "3px",
    borderRadius = 0.5,
    children,
    to,
    ...aProps
  } = props;

  if (to) {
    return (
      <StyledLink to={to} $borderRadius={borderRadius} {...aProps}>
        <HoverBorder
          $hoverBorderWidth={hoverBorderWidth!}
          $borderRadius={borderRadius}
        />
        {children}
      </StyledLink>
    );
  } else {
    return null;
  }
};

interface DivProps extends React.AnchorHTMLAttributes<HTMLDivElement> {
  hoverBorderWidth?: string; // defaults to OVERLAY_PADDING em
  borderRadius?: number;
  refCallback?: (elem: HTMLDivElement | null) => void;
}

export const PhotoLinkDiv: React.FC<DivProps> = (props) => {
  const {
    hoverBorderWidth = "3px",
    borderRadius = 0.5,
    children,
    refCallback,
    ...divProps
  } = props;
  return (
    <StyledDiv $borderRadius={borderRadius} {...divProps} ref={refCallback}>
      <HoverBorder
        $hoverBorderWidth={hoverBorderWidth!}
        $borderRadius={borderRadius}
      />
      {children}
    </StyledDiv>
  );
};
