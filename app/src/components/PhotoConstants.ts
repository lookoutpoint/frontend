// SPDX-License-Identifier: MIT
export const PHOTO_PADDING = 4; // px
export const OVERLAY_ROW_HEIGHT = 1.2; // em
export const OVERLAY_PADDING = 0.35; // em
