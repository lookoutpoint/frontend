// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { isBot } from "../api/bot";
import { axios } from "../util";
import { useQueryClient } from "@tanstack/react-query";
import { UserSession } from "../helpers/Contexts";
import { UserSessionContext } from "../helpers/Contexts";

// Minimum time to wait before refreshing session
const SESSION_REFRESH_MIN_WAIT_TIME = 60 * 60 * 1000; // 1h in ms

export const UserSessionProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const [isLoggedIn, rawSetIsLoggedIn] = useState(false);
  const queryClient = useQueryClient();

  const lastRefresh = useRef(-1);

  // Set if user is logged in.
  const setIsLoggedIn = useCallback((isLoggedIn: boolean) => {
    rawSetIsLoggedIn(isLoggedIn);
    if (isLoggedIn) {
      // Assuming session just refreshed or created.
      lastRefresh.current = Date.now();
    }
  }, []);

  // Refresh session. Returns true if session successfully refreshed.
  const refresh = useCallback(async () => {
    try {
      const { status } = await axios.post("/users/refresh");
      if (status === 200) {
        lastRefresh.current = Date.now();
        return true;
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (e) {
      /* empty */
    }
    return false;
  }, []);

  // Initial check on mount to see if logged in or not.
  useEffect(() => {
    // Don't check if user agent is a bot.
    if (isBot) return;

    (async () => {
      // Check by attempting to refresh the session.
      setIsLoggedIn(await refresh());
    })();
  }, [refresh, setIsLoggedIn]);

  // If logged in, disable useQuery caching.
  useEffect(() => {
    let staleTime: number;
    if (isLoggedIn) {
      staleTime = 0;
      queryClient.invalidateQueries({ refetchType: "none" });
    } else {
      staleTime = Infinity;
    }

    const defOpts = queryClient.getDefaultOptions();
    defOpts.queries = defOpts.queries || {};
    defOpts.queries.staleTime = staleTime;
    queryClient.setDefaultOptions(defOpts);
  }, [isLoggedIn, queryClient]);

  const markActivity = useCallback(() => {
    if (!isLoggedIn) return;

    const now = Date.now();
    if (now - lastRefresh.current >= SESSION_REFRESH_MIN_WAIT_TIME) {
      // Refresh
      refresh();
    }
  }, [isLoggedIn, refresh]);

  const session: UserSession = useMemo(
    () => ({
      isLoggedIn,
      isAdmin: isLoggedIn, // currently only admins can log in
      addUser(name, password): Promise<void> {
        return new Promise((resolve, reject) => {
          axios
            .post("/users/add", {
              name,
              password,
            })
            .then(
              ({ status }) => {
                if (status === 200) {
                  // Success
                  setIsLoggedIn(true);
                  resolve();
                } else {
                  // Error
                  setIsLoggedIn(false);
                  reject();
                }
              },
              () => {
                setIsLoggedIn(false);
                reject();
              }
            );
        });
      },

      login(name, password): Promise<void> {
        return new Promise((resolve, reject) => {
          axios
            .post("/users/login", {
              name,
              password,
            })
            .then(
              ({ status }) => {
                if (status === 200) {
                  // Success
                  setIsLoggedIn(true);
                  resolve();
                } else {
                  // Error
                  setIsLoggedIn(false);
                  reject();
                }
              },
              () => {
                setIsLoggedIn(false);
                reject();
              }
            );
        });
      },

      logout(): Promise<void> {
        return new Promise((resolve, reject) => {
          axios.post("/users/logout").then(
            ({ status }) => {
              setIsLoggedIn(false);
              if (status === 200) {
                // Success
                resolve();
              } else {
                // Error
                reject();
              }
            },
            () => {
              setIsLoggedIn(false);
              reject();
            }
          );
        });
      },

      markActivity(): void {
        markActivity();
      },
    }),
    [isLoggedIn, markActivity, setIsLoggedIn]
  );

  return (
    <UserSessionContext.Provider value={session}>
      {children}
    </UserSessionContext.Provider>
  );
};
