// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { CircleIconButton } from "../components/CircleIconButton";
import { Modal } from "../components/Modal";
import { useEventListener } from "../hooks/useEventListener";
import { Row } from "./Form";
import { IconClose } from "./Icons";
import { Spinner } from "./Spinner";

export interface RenderProps {
  readonly processing: boolean;
  setProcessing(processing: boolean): void;
  setError(error: string | null): void;
}

interface Props {
  modalLevel?: number;
  title?: JSX.Element;
  width?: string | number; // defaults to 800px with max-width capped at 80%
  onClose?: () => void;
  render: (props: RenderProps) => JSX.Element;
}

const DialogWrapper = styled.div<{
  $isModal: boolean;
  $width: string | number;
}>`
  border: 1px solid var(--dialog-border-color);
  margin: ${(p) => (p.$isModal ? 75 : 15)}px auto;
  box-shadow: ${(p) => (p.$isModal ? "0 0 10px #888" : "0")};

  overflow: ${(p) => (p.$isModal ? "auto" : "hidden")};
  max-height: ${(p) => (p.$isModal ? "calc(100% - 150px)" : "auto")};

  position: relative;

  background-color: var(--bg-color);

  width: ${(p) => (typeof p.$width === "number" ? `${p.$width}px` : p.$width)};
  max-width: 80%;
`;

const DialogPadding = styled.div`
  padding: 1em;
`;

const Title = styled.div`
  font-size: 1.25em;
  margin-bottom: 1em;
`;

const CloseBtn = styled(CircleIconButton)`
  position: absolute;
  /* Based on lining up middle of button (which is 2em in height) with middle of title, accounting for padding */
  top: 0.625em;
  right: 0.625em;
`;

const ProcessingIndicator = styled(Spinner)<{ $hasCloseBtn: boolean }>`
  position: absolute;
  top: 0.625em;
  right: ${(p) => (p.$hasCloseBtn ? 3.125 : 0.625)}em;
`;

const Error = styled(Row)`
  border: 1px solid var(--error-border-color);
  background-color: var(--error-bg-color);
  color: var(--error-text-color);
  padding: 0.5em;
`;

export const Dialog: React.FC<Props> = ({
  modalLevel,
  title,
  onClose,
  render,
  width,
}) => {
  // Key down listener.
  const onKeyDown = useCallback(
    (e: KeyboardEvent) => {
      switch (e.key) {
        case "Escape":
          if (onClose) {
            onClose();
          }
          break;
      }
    },
    [onClose]
  );

  useEventListener(window, "keydown", onKeyDown);

  // Form state
  const [error, setError] = useState<string | null>(null);
  const [processing, setProcessing] = useState(false);

  const isModal = modalLevel !== undefined;

  const dialog = (
    <DialogWrapper $isModal={isModal} $width={width ?? 800}>
      <DialogPadding>
        {title && <Title>{title}</Title>}
        {onClose && (
          <CloseBtn
            title="Close"
            fontSizeEm={1.15}
            disabled={processing}
            onClick={() => onClose()}
          >
            <IconClose />
          </CloseBtn>
        )}
        {processing && (
          <ProcessingIndicator $hasCloseBtn={onClose !== undefined} />
        )}
        {error && <Error>{error}</Error>}

        {render({ processing, setProcessing, setError })}
      </DialogPadding>
    </DialogWrapper>
  );

  if (isModal) {
    return <Modal backgroundColor="var(--modal-bg-color)">{dialog}</Modal>;
  } else {
    return dialog;
  }
};
