import styled from "styled-components";

export const Spinner = styled.div`
  width: 2em;
  height: 2em;
  border: 4px solid var(--spinner-bg-color);
  border-top: 4px solid var(--spinner-color);
  border-radius: 50%;
  box-sizing: border-box;
  animation: spinner 1.5s linear infinite;

  @keyframes spinner {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
