import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import {
  NavigationType,
  useLocation,
  useNavigationType,
} from "react-router-dom";
import styled from "styled-components";
import { useEventListener } from "../hooks/useEventListener";
import { useProcessingIndicator } from "../hooks/useProcessingIndicator";

const ForceScroll = styled.div`
  position: absolute;
  left: 0px;
  height: 100vh;
  width: 1px;
`;

function getLocationPrefix(path: string): string {
  const idx = path.indexOf("@");
  return idx >= 0 ? path.substring(0, idx) : path;
}

export const ScrollMemory: React.FC = () => {
  const location = useLocation();
  const navigateType = useNavigationType();
  const processingIndicator = useProcessingIndicator();

  // Track scroll location by url.
  const scrollCache = useRef(new Map<string, number>());

  const curLocation = useRef(location.pathname);
  const onScroll = useCallback(() => {
    const scrollPos = window.scrollY;
    if (scrollPos) {
      scrollCache.current.set(curLocation.current, scrollPos);
    } else {
      // Don't need to store zero.
      scrollCache.current.delete(curLocation.current);
    }
  }, []);

  useEventListener(window, "scroll", onScroll);

  const onResize = useCallback(() => {
    // Clear cache as other scroll positions may no longer be valid.
    scrollCache.current.clear();

    // Update scroll position.
    onScroll();
  }, [onScroll]);

  useEventListener(window, "resize", onResize);

  // Forced scroll position state.
  const [forceScrollPos, setForceScrollPos] = useState<number | undefined>(
    undefined
  );

  useEffect(() => {
    // If not processing, set a timer in the future to get rid of the forced scroll pos.
    let timer: number | undefined;
    if (forceScrollPos !== undefined && !processingIndicator) {
      timer = window.setTimeout(() => setForceScrollPos(undefined), 500);
    }

    return () => clearTimeout(timer);
  }, [forceScrollPos, processingIndicator]);

  useEffect(() => {
    // This component handles scroll restoration, so tell browser to leave it alone.
    window.history.scrollRestoration = "manual";
  }, []);

  // Current url prefix, which is everything before the first "@" in the url.
  const curLocationPrefix = useRef("");

  useLayoutEffect(() => {
    // Only do something if the URL has changed.
    if (curLocation.current === location.pathname) return;

    curLocation.current = location.pathname;

    if (navigateType === NavigationType.Replace) {
      // Do nothing more than update prefix.
      curLocationPrefix.current = getLocationPrefix(location.pathname);
      return;
    } else if (navigateType === NavigationType.Push) {
      const newLocationPrefix = getLocationPrefix(location.pathname);

      // Always scroll to top when changing prefixes.
      if (curLocationPrefix.current !== newLocationPrefix) {
        curLocationPrefix.current = newLocationPrefix;
        window.scrollTo(0, 0);
        scrollCache.current.delete(location.pathname);
        return;
      }
    }

    // Pop or navigating within the same url prefix. Restore previous scroll location, if any.
    // This may need to be done over multiple frames as it may require 1) layout to convert and
    // 2) for data to be loaded.
    //
    // We can workaround #2 by adding a "fake" div that forces the scroll position to be valid.
    const scrollPos = scrollCache.current.get(location.pathname);
    setForceScrollPos(scrollPos ?? 0);

    // Listen for potential user interaction events that may trigger scrolling. In such cases,
    // stop trying to automatically scroll.
    let hadUserInteraction = false;

    const listener = () => {
      hadUserInteraction = true;
    };
    window.addEventListener("mousedown", listener);
    window.addEventListener("wheel", listener);
    window.addEventListener("keydown", listener);
    window.addEventListener("touchstart", listener);

    const cleanupListeners = () => {
      window.removeEventListener("mousedown", listener);
      window.removeEventListener("wheel", listener);
      window.removeEventListener("keydown", listener);
      window.removeEventListener("touchstart", listener);
    };

    // Anecdotal: use timers and multiple checks to verify that the scroll position has been successfully set.
    let pending: number | undefined;
    let attempts = 0;
    let successfulChecks = 0;
    const scrollUntilSet = () => {
      // If user has done something, don't do anything more.
      if (hadUserInteraction) {
        cleanupListeners();
        return;
      }

      window.scrollTo(0, scrollPos ?? 0);
      if (scrollPos) {
        pending = window.setTimeout(() => {
          pending = undefined;
          attempts++;

          // Attempt to set scroll up to 30 times (equal to 60 frames, so roughly 1 second).
          // After that, just leave it wherever it is.
          if (
            (window.scrollY !== scrollPos || ++successfulChecks < 3) &&
            attempts < 30
          ) {
            scrollUntilSet();
          }
        }, 100);
      } else {
        cleanupListeners();
      }
    };

    scrollUntilSet();

    return () => {
      cleanupListeners();
      clearTimeout(pending);
    };
  }, [location.pathname, navigateType]);

  // Try to scroll immedately after the ForceScroll element is updated.
  useLayoutEffect(() => {
    if (forceScrollPos !== undefined) {
      window.scrollTo(0, forceScrollPos);
    }
  }, [forceScrollPos]);

  return forceScrollPos !== undefined ? (
    <ForceScroll style={{ top: forceScrollPos }} />
  ) : null;
};
