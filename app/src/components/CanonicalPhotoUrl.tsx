import React from "react";
import { makeCanonicalPhotoUrl } from "../helpers/photos";
import { Helmet } from "react-helmet-async";
import { getPhotoMetadataQueryOptions, PhotoIdGen } from "../api/photos";
import { useQuery } from "@tanstack/react-query";

interface Props {
  photoId?: PhotoIdGen;
}

export const CanonicalPhotoUrl: React.FC<Props> = ({ photoId }) => {
  const { data: photo } = useQuery({
    ...getPhotoMetadataQueryOptions(photoId ?? { id: "" }),
    enabled: photoId !== undefined,
  });

  const url = photo ? makeCanonicalPhotoUrl(photo) : undefined;

  return url ? (
    <Helmet>
      <link href={url} rel="canonical" />
    </Helmet>
  ) : null;
};
