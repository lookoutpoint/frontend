// SPDX-License-Identifier: MIT
import React, { useCallback, useMemo } from "react";
import styled from "styled-components";
import { GroupListQuery } from "../helpers/GroupListQuery";
import { Group, GroupType } from "../api/photoGroups";
import { CroppedPhoto } from "./CroppedPhoto";
import { RatioPaddingWrapper } from "./RatioPaddingWrapper";
import { titleCase } from "../helpers/titleCase";
import {
  PhotoOverlayTitle,
  PhotoInfiniteScrollContainer,
  PhotoInfiniteScrollContainerEvents,
} from "./PhotoInfiniteScrollContainer";
import { PHOTO_PADDING } from "./PhotoConstants";
import { AppEvent, makeEventGroupID } from "../api/events";
import { IconCollection } from "./Icons";

export interface GroupInfiniteScrollEvents {
  context: string;
  groupType: GroupType;
}

interface Props<G extends Group> {
  // Query object to get photo objects.
  groupListQuery: GroupListQuery<G>;

  // Get text group name.
  getGroupName: (group: G) => string;

  // Optional get "fancy" group name that will be rendered as provided.
  getFancyGroupName?: (group: G) => JSX.Element;

  getRowGroup?: (group: G) => string | undefined;

  // Converts a group to a url that will be used in a <a> element.
  // If not provided, no link will be provided.
  makeGroupUrl?: (group: G) => string;

  // Handles clicking on a group. Return true to enable default click event handling,
  // which will typically invoke the <a> link.
  onGroupClick?: (group: G) => boolean;

  renderStartOfRowGroup?: (rowGroup: string) => JSX.Element | undefined;

  // Desired photo width. This width will be used to determine the number of groups per row,
  // but the actual photo width may be a bit bigger in order to fully fill the row.
  desiredPhotoWidth?: number;

  // Photo ratio.
  photoRatio?: number;

  // Show group info in overlay.
  showOverlay?: boolean;

  rowLimit?: number;

  // When hidden, the infinite scroll will not attempt to fetch from the list.
  // Should set to true if the infinite scroll is not visible.
  hidden?: boolean;

  // Query forward flag. Defaults to true.
  queryForward?: boolean;

  // Events.
  events?: GroupInfiniteScrollEvents;
}

const StyledIconCollection = styled(IconCollection)`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  text-align: center;
  width: 100%;
  font-size: 500%;
`;

type RowInfo<G extends Group> = {
  lastIdx: number;
  items: G[];
  widths: number[];
  height: number;
  full: boolean;
  startOfRowGroup?: string;
};

function computeGroupsPerRow(maxWidth: number, containerWidth: number): number {
  // Figure out the number of groups per row. Must have at least one.
  let width = maxWidth + PHOTO_PADDING * 2;
  const numGroupsPerRow = Math.ceil(containerWidth / width);

  width = containerWidth / numGroupsPerRow - PHOTO_PADDING * 2;

  if (numGroupsPerRow === 1 || width / maxWidth >= 0.9) {
    return numGroupsPerRow;
  } else {
    return numGroupsPerRow - 1;
  }
}

function photoWidth(groupsPerRow: number, containerWidth: number): number {
  return Math.floor(containerWidth / groupsPerRow - PHOTO_PADDING * 2);
}

function photoHeight(
  groupsPerRow: number,
  containerWidth: number,
  photoRatio: number
): number {
  return photoWidth(groupsPerRow, containerWidth) / photoRatio;
}

function layoutRows<G extends Group>(
  groupIdxStart: number,
  groups: G[],
  groupsPerRow: number,
  containerWidth: number,
  photoRatio: number,
  getRowGroup?: (group: G) => string | undefined
): RowInfo<G>[] {
  const rows: RowInfo<G>[] = [];
  let curRow: RowInfo<G> | undefined;

  let lastRowGroup: string | undefined =
    groupIdxStart > 0 && getRowGroup
      ? getRowGroup(groups[groupIdxStart - 1])
      : undefined;

  // Use floor so that we don't overflow the row.
  const rowHeight = Math.floor(
    photoHeight(groupsPerRow, containerWidth, photoRatio)
  );
  const itemWidth = Math.floor(rowHeight * photoRatio);

  for (let groupIdx = groupIdxStart; groupIdx < groups.length; groupIdx++) {
    const group = groups[groupIdx];
    const rowGroup = getRowGroup ? getRowGroup(group) : undefined;

    // Start a new row if there is no current row or if the row group is different compared to the last item.
    if (curRow === undefined || rowGroup !== lastRowGroup) {
      // Start a new row.
      curRow = {
        lastIdx: -1,
        items: [],
        widths: [],
        height: rowHeight,
        full: false,

        // Annotate row group start if this group has a different group
        startOfRowGroup: rowGroup !== lastRowGroup ? rowGroup : undefined,
      };
      rows.push(curRow);
    }

    // Add group to this row.
    curRow.lastIdx = groupIdx;
    curRow.items.push(group);
    curRow.widths.push(itemWidth);

    if (curRow.items.length === groupsPerRow) {
      curRow.full = true;

      // Adjust widths for row to fill entire container width.
      // Use floor of containerWidth because it may not be an integer and we cannot
      // add a partial pixel.
      let accumWidth = curRow.widths.length * (itemWidth + PHOTO_PADDING * 2);
      for (
        let i = 0;
        i < curRow.widths.length && accumWidth < Math.floor(containerWidth);
        ++i
      ) {
        curRow.widths[i]++;
        accumWidth++;
      }

      curRow = undefined; // reset for next group
    }

    lastRowGroup = rowGroup;
  }

  return rows;
}

// Default number of groups to fetch per request.
export const GROUP_FETCH_COUNT = 15;

export function GroupInfiniteScroll<G extends Group>(
  props: React.PropsWithChildren<Props<G>>
): JSX.Element {
  const {
    groupListQuery,
    getGroupName,
    getFancyGroupName,
    getRowGroup,
    makeGroupUrl,
    onGroupClick,
    renderStartOfRowGroup,
    desiredPhotoWidth: maxWidth = 500,
    photoRatio = 3 / 2,
    showOverlay = true,
    rowLimit,
    hidden,
    queryForward = true,
    events: inEvents,
  } = props;

  const events = useMemo(() => {
    if (inEvents === undefined) return undefined;
    const events: PhotoInfiniteScrollContainerEvents<G> = {
      context: inEvents.context,
      photoView: AppEvent.PHOTO_VIEW_COVER,
      itemClick: {
        event: AppEvent.GROUP_CLICK,
        getEventObject: (item: G): string => {
          return makeEventGroupID(inEvents.groupType, item);
        },
      },
    };

    return events;
  }, [inEvents]);

  return (
    <PhotoInfiniteScrollContainer
      hidden={!!hidden}
      listQuery={groupListQuery}
      queryForward={!!queryForward}
      showOverlay={!!showOverlay}
      rowLimit={rowLimit}
      layoutRows={useCallback(
        (idxStart, photos: G[], containerWidth) =>
          layoutRows(
            idxStart,
            photos,
            computeGroupsPerRow(maxWidth!, containerWidth),
            containerWidth,
            photoRatio!,
            getRowGroup
          ),
        [getRowGroup, maxWidth, photoRatio]
      )}
      getOverlayRowCount={useCallback(() => 1, [])}
      getRowHeight={useCallback((row: RowInfo<G>) => {
        return row.height;
      }, [])}
      getPhotoWidth={useCallback((row: RowInfo<G>, idx) => {
        return row.widths[idx];
      }, [])}
      buildOverlayContent={useCallback(
        (group: G) => {
          return (
            <PhotoOverlayTitle>
              {getFancyGroupName
                ? getFancyGroupName(group)
                : titleCase(getGroupName(group))}
            </PhotoOverlayTitle>
          );
        },
        [getFancyGroupName, getGroupName]
      )}
      renderPhoto={useCallback(
        (row: RowInfo<G>, idx) => {
          const group = row.items[idx];
          return group.coverPhoto
            ? [
                <CroppedPhoto
                  photo={group.coverPhoto}
                  ratio={photoRatio!}
                  loading="lazy"
                />,
                group.coverPhoto.id,
              ]
            : [
                <RatioPaddingWrapper $ratio={photoRatio!}>
                  <StyledIconCollection />
                </RatioPaddingWrapper>,
                undefined,
              ];
        },
        [photoRatio]
      )}
      renderStartOfRowGroup={useCallback(
        (row: RowInfo<G>) => {
          return renderStartOfRowGroup && row.startOfRowGroup
            ? renderStartOfRowGroup(row.startOfRowGroup)
            : undefined;
        },
        [renderStartOfRowGroup]
      )}
      makeUrl={makeGroupUrl}
      onItemClick={onGroupClick}
      events={events}
    />
  );
}
