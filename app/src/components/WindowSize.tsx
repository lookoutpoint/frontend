// SPDX-License-Identifier: MIT
import React, { useCallback, useLayoutEffect, useState } from "react";
import { createGlobalStyle } from "styled-components";
import { useEventListener } from "../hooks/useEventListener";
import { WindowSize, WindowSizeContext } from "../helpers/Contexts";

const ScrollBarMeasureStyle = createGlobalStyle`
  .scrollbar-measure {
    width: 100px;
    height: 100px;
    overflow: scroll;
    position: absolute;
    top: -9999px;
  }
`;

export const WindowSizeProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const [state, setState] = useState<WindowSize>({
    width: window.innerWidth,
    height: window.innerHeight,
    scrollBarWidth: 10, // guess at a default
  });

  // Measure scroll bar width. Only need to do this once.
  useLayoutEffect(() => {
    // Adapted from https://davidwalsh.name/detect-scrollbar-width
    // Create the measurement node
    const scrollDiv = document.createElement("div");
    scrollDiv.className = "scrollbar-measure";
    document.body.appendChild(scrollDiv);

    // Get the scrollbar width
    const width = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    setState((s) => ({ ...s, scrollBarWidth: width }));

    // Delete the DIV
    document.body.removeChild(scrollDiv);
  }, []);

  // Track window size.
  const resizeListener = useCallback(() => {
    const width = window.innerWidth;
    const height = window.innerHeight;

    setState((s) =>
      width === s.width && height === s.height ? s : { ...s, width, height }
    );
  }, []);

  useEventListener(window, "resize", resizeListener);
  useEventListener(window, "orientationChange", resizeListener);

  return (
    <WindowSizeContext.Provider value={state}>
      <ScrollBarMeasureStyle />
      {children}
    </WindowSizeContext.Provider>
  );
};
