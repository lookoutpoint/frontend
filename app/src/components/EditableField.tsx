// SPDX-License-Identifier: MIT
import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { CircleIconButton } from "./CircleIconButton";
import { IconEdit } from "./Icons";

interface Props {
  curValue: string;
  onChange: (newValue: string) => void;
  id?: string;
  placeholder?: string;
  disabled?: boolean;
  readonly?: boolean;
  multiLine?: boolean;
}

const Flex = styled.div`
  display: flex;
  & > * + * {
    margin-left: 0.5em;
  }
`;

const EditButton = styled(CircleIconButton)`
  flex-shrink: 0;
`;

// This InputGrid overlays the input and span inside the same grid area. In doing so,
// the input will stretch to fill the width of the span (which contains the current value).
const InputGrid = styled.div`
  display: grid;
  grid-template: "area" auto / auto;

  & > * {
    grid-area: area;
  }
  & > span {
    padding: 0 0.5em; /* give some buffer for the input element's outline */
    visibility: hidden;
  }
`;

const InputElem = styled.input``;

export const EditableField: React.FC<Props> = ({
  curValue,
  onChange,
  id,
  placeholder,
  disabled,
  readonly,
  multiLine,
}) => {
  const [editing, setEditing] = useState(false);
  const [editValue, setEditValue] = useState(curValue);

  // Update edit value if curValue changes in non-edit mode (so that when switching
  // into edit mode the value is up-to-date).
  if (!editing && curValue !== editValue) {
    setEditValue(curValue);
  }

  const onRef = useCallback((ref: HTMLElement | null) => {
    if (!ref) return;

    // Focus on mount.
    ref.focus();
  }, []);

  const onBlur = useCallback(() => {
    // Save value.
    onChange(editValue);
    setEditing(false);
  }, [editValue, onChange]);

  const onKeyDown = useCallback(
    (e: React.KeyboardEvent) => {
      switch (e.key) {
        case "Escape":
          // Cancel editing, don't save changes.
          setEditing(false);
          setEditValue(curValue);
          e.stopPropagation();
          break;

        case "Enter":
          // Save.
          onBlur();
          e.stopPropagation();
          break;
      }
    },
    [curValue, onBlur]
  );

  if (editing) {
    return (
      <InputGrid>
        <InputElem
          as={multiLine ? "textarea" : "input"}
          ref={onRef}
          id={id}
          placeholder={placeholder}
          disabled={disabled}
          value={editValue}
          onChange={(
            e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) => setEditValue(e.target.value)}
          onKeyDown={onKeyDown}
          onBlur={onBlur}
        />
        <span>{curValue}</span>
      </InputGrid>
    );
  } else {
    return (
      <Flex>
        {curValue.length > 0 && (
          <span
            onDoubleClick={
              readonly || disabled ? undefined : () => setEditing(true)
            }
          >
            {curValue}
          </span>
        )}
        {!readonly && (
          <EditButton
            title="Edit"
            disabled={disabled}
            onClick={() => setEditing(true)}
          >
            <IconEdit />
          </EditButton>
        )}
      </Flex>
    );
  }
};
