// SPDX-License-Identifier: MIT
import React, {
  useCallback,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { LinkWithEvent } from "./LinkWithEvent";
import { EventDesc } from "../helpers/EventTracker";
import { AppEvent, EventContext } from "../api/events";
import { IconHome } from "./Icons";
import { useEventListener } from "../hooks/useEventListener";

export type NavInfo = {
  link: string;
  label: string;
  // View is appended to link as `@{view}/`, if defined
  view?: string;
  event?: EventDesc;
};

type Props = {
  nav: NavInfo[];
  activeLabel: string;
};

const NavList = styled.div`
  margin: 0 0 0.5em 0;
  padding: 0 0.5em;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;

  & > * {
    padding: 0.25em 0.5em;
    border-right: 1px solid var(--nav-sep-color);
    text-align: center;
  }
  & > *:last-child {
    border-right: 0;
  }
  & a {
    color: var(--lesser-text-color);
    text-decoration: none;
  }
  & a:hover {
    color: var(--text-color);
    text-decoration: underline;
  }
`;

// Breadcrumb navigation list. Tries its best to keep the nav list to one row.
export const BreadcrumbNav: React.FC<Props> = ({ nav, activeLabel }) => {
  // The nav list consists of: nav, activeLabel
  // The most we allow to collapse is everything except the last nav entry.
  //
  // In the max collapsed state, we only keep the nav entry.
  // In all other collapsed states, we keep the first nav entry and trailing nav entries.
  const maxNumCollapsed = Math.max(0, nav.length - 1);

  // Track number of collapsed nav entries.
  const [rawNumCollapsed, setNumCollapsed] = useState(0);
  const numCollapsed = Math.min(rawNumCollapsed, maxNumCollapsed);

  // When nav changes, reset the number of collapsed entries. Need to remeasure to see what should be collapsed.
  useLayoutEffect(() => setNumCollapsed(0), [nav]);

  // List of refs of the elements that make up the nav list. Used to check if there is overflow.
  // There should be nav.length + 1 entries (one for each nav link plus active label). In the collapsed state,
  // some of these refs will be null.
  const navListRefs = useRef<Array<HTMLElement | null>>([]);

  const renderNavInfo = (info: NavInfo, refIndex: number): JSX.Element => {
    const { link, label, view, event } = info;
    let fullLink = link;
    if (view !== undefined) {
      fullLink += `@${view}/`;
    }
    return (
      <LinkWithEvent
        key={fullLink}
        to={fullLink}
        event={event}
        refCallback={(el) => (navListRefs.current[refIndex] = el)}
      >
        {label}
      </LinkWithEvent>
    );
  };

  // Build nav parts.
  const navParts: JSX.Element[] = [];
  if (numCollapsed > 0) {
    if (numCollapsed < maxNumCollapsed) {
      // Not max collapsed state, so show first entry.
      navParts.push(renderNavInfo(nav[0], 0));
    }
    navParts.push(<span key="ellipsis">. . .</span>);

    // Rest of the trailing entries.
    const idx = numCollapsed + (numCollapsed < maxNumCollapsed ? 1 : 0);
    navParts.push(
      ...nav.slice(idx).map((info, i) => renderNavInfo(info, idx + i))
    );
  } else {
    navParts.push(...nav.map((info, i) => renderNavInfo(info, i)));
  }

  // Checks location of nav list elements to see if anything has overflowed.
  const checkForOverflow = useCallback(() => {
    if (maxNumCollapsed === 0 || numCollapsed === maxNumCollapsed) return;

    // Slice to expected size to get rid of potentially dangling references (to an old, longer, nav list)
    navListRefs.current = navListRefs.current.slice(0, nav.length + 1);

    // Check if the child elements of the nav list have overflowed onto a different row.
    // Do this by checking that consecutive child elements have increasing x-coordinate.
    // Note: a more exhaustive check is possible that the x-coordinate is greater than all previous elements,
    // but that's probably not necessary.
    let prevX: number | undefined = undefined;
    for (let i = 0; i < navListRefs.current.length; ++i) {
      const elem = navListRefs.current[i];
      if (!elem) continue;
      const x = elem.getBoundingClientRect().x;
      if (prevX !== undefined) {
        if (x <= prevX) {
          // Overflow. Increase number of collapsed entries and retry.
          setNumCollapsed(numCollapsed + 1);
          break;
        }
      }
      prevX = x;
    }
  }, [maxNumCollapsed, nav.length, numCollapsed]);

  // Use layout effect to measure for overflow before rendering.
  // This effectively iterates until there is no overflow or we hit the max number of collapsed entries.
  useLayoutEffect(checkForOverflow, [checkForOverflow]);

  // Also check for overflow on window resize.
  const lastWindowWidth = useRef<number | undefined>(undefined);
  const onWindowResize = useCallback(() => {
    const lastWidth = lastWindowWidth.current;
    const width = window.innerWidth;
    lastWindowWidth.current = width;

    if (lastWidth === undefined) {
      // No comparison, check for overflow.
      return checkForOverflow();
    }

    if (width < lastWidth) {
      // Width decreased, check for overflow.
      return checkForOverflow();
    } else if (width > lastWidth) {
      // Width increased, may be able to uncollapse, so reset.
      setNumCollapsed(0);
    }
  }, [checkForOverflow]);
  useEventListener(window, "resize", onWindowResize);

  return (
    <NavList>
      <LinkWithEvent
        to="/"
        event={useMemo(
          () => ({
            event: AppEvent.LINK_CLICK,
            object: "/",
            context: EventContext.DEFAULT,
          }),
          []
        )}
      >
        <IconHome />
      </LinkWithEvent>
      {navParts}
      <span
        className="active"
        ref={(el) => (navListRefs.current[nav.length] = el)}
      >
        {activeLabel}
      </span>
    </NavList>
  );
};
