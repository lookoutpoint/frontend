// SPDX-License-Identifier: MIT
import { useCallback, useMemo } from "react";
import styled from "styled-components";
import { PhotoListPhoto, PHOTO_GROUP_DISPLAY } from "../api/photos";
import { ConstrainedPhoto } from "./ConstrainedPhoto";
import { PhotoListQuery } from "../helpers/PhotoListQuery";
import {
  PhotoAndOverlayWrapper,
  PhotoInfiniteScrollContainer,
  PhotoInfiniteScrollContainerEvents,
  PhotoOverlayRow,
  PhotoOverlayTitle,
} from "./PhotoInfiniteScrollContainer";
import { PHOTO_PADDING } from "./PhotoConstants";
import { AppEvent, EventContext } from "../api/events";
import { IconLocation } from "./Icons";
import { titleCase } from "../helpers/titleCase";
import { stripLocationPrefix } from "../helpers/locations";

interface Props<K extends keyof PhotoListPhoto> {
  // Query object to get photo objects.
  photoListQuery: PhotoListQuery<K>;
  photoListQueryForward: boolean;

  // Converts a photo to a url that will be used in a <a> element.
  // If not provided, no link will be provided.
  makePhotoUrl?: (photo: PhotoListPhoto) => string;

  // Handles clicking on a photo. Return true to enable default click event handling,
  // which will typically invoke the <a> link.
  onPhotoClick?: (photo: PhotoListPhoto) => boolean;

  // Standard row height.
  stdHeight?: number;

  // Show photo info in overlay.
  showOverlay?: boolean;

  // When hidden, the infinite scroll will not attempt to fetch photos.
  // Should set to true if the infinite scroll is not visible.
  hidden?: boolean;

  // Row limit. Undefined for unlimited.
  rowLimit?: number;

  // Event context. Defaults to default context.
  eventContext?: string;
}

// Styles for location overlay row.
const PhotoLocation = styled(PhotoOverlayRow)`
  color: var(--lesser-text-color);
  font-size: 0.8em;
  line-height: 1.6;

  ${PhotoAndOverlayWrapper}:hover & {
    color: var(--caption-hover-color);
  }
`;

function computeConstrainedWidth(
  photo: PhotoListPhoto,
  height: number
): number {
  return (height * photo.width) / photo.height;
}

function computeHeightToFill(
  photos: PhotoListPhoto[],
  targetWidth: number
): number {
  let ratioSum = 0;
  for (const photo of photos) {
    ratioSum += photo.width / photo.height;
  }

  return (targetWidth - 2 * PHOTO_PADDING * photos.length) / ratioSum;
}

type RowInfo = {
  lastIdx: number;
  items: PhotoListPhoto[];
  widths: number[];
  height: number;
  full: boolean;
};

function layoutRows(
  photoIdxStart: number,
  photos: PhotoListPhoto[],
  containerWidth: number,
  stdHeight: number
): RowInfo[] {
  const rows: RowInfo[] = [];
  let curRow: RowInfo | undefined;
  let curAccumWidth = 0;
  for (let photoIdx = photoIdxStart; photoIdx < photos.length; ) {
    const photo = photos[photoIdx];

    if (curRow === undefined) {
      // Start a new row.
      curRow = {
        lastIdx: -1,
        items: [],
        widths: [],
        height: stdHeight,
        full: false,
      };
      rows.push(curRow);
      curAccumWidth = 0;
    }

    // Try to add photo to this row.
    curAccumWidth +=
      Math.floor(computeConstrainedWidth(photo, stdHeight)) + PHOTO_PADDING * 2;

    if (curAccumWidth > containerWidth) {
      // Exceed container width.
      // Two options: remove this photo and increase height to fill or decrease height to fit all images
      const hWithout =
        curRow.items.length > 0
          ? computeHeightToFill(
              curRow.items.slice(0, curRow.items.length),
              containerWidth
            )
          : 0;
      const hWith = computeHeightToFill(
        [...curRow.items, photo],
        containerWidth
      );

      // Pick the option that deviates least from the standard height.
      const withoutDelta = Math.abs(hWithout - stdHeight) / stdHeight;
      const withDelta = Math.abs(hWith - stdHeight) / stdHeight;

      // Bias towards showing more photos in a row. We can only drop the photo from this row
      // if this wouldn't be the only photo in the row.
      if (curRow.items.length > 0 && withoutDelta <= withDelta - 0.05) {
        // Choose to not include the photo in the current row. Adjust height.
        curRow.height = hWithout;
        curRow.full = true;
        finalizeRowSizing(curRow, containerWidth);

        // Put current photo in a new row, so don't advance.
        curRow = undefined;
      } else {
        // Include photo in the row.
        curRow.items.push(photo);
        curRow.lastIdx = photoIdx;
        curRow.height = hWith;
        curRow.full = true;
        finalizeRowSizing(curRow, containerWidth);

        // Advance to next photo, which must be in new row.
        photoIdx++;
        curRow = undefined;
      }
    } else {
      // Add photo to the row.
      curRow.items.push(photo);
      curRow.lastIdx = photoIdx;

      // Advance to next photo.
      photoIdx++;
    }
  }

  // See if we want to fill out the last row, if it's not full.
  if (curRow !== undefined && !curRow.full) {
    const fillHeight = computeHeightToFill(curRow.items, containerWidth);
    if (
      curAccumWidth / containerWidth >= 0.6 ||
      fillHeight / stdHeight < 1.333
    ) {
      curRow.height = fillHeight;
    }

    finalizeRowSizing(curRow, containerWidth);
  }

  return rows;
}

function finalizeRowSizing(row: RowInfo, containerWidth: number) {
  // Floor the height. Rounding up may lead to an accumulated row width that exceeds
  // what can be accommodated.
  row.height = Math.floor(row.height);
  row.widths = [];

  // Compute initial widths for all photos by rounding widths.
  let accumWidth = 0;
  const deltas: [number, number][] = [];
  for (let i = 0; i < row.items.length; i++) {
    const photo = row.items[i];
    const desiredWidth = computeConstrainedWidth(photo, row.height);
    const actualWidth = Math.round(desiredWidth);

    accumWidth += actualWidth + PHOTO_PADDING * 2;
    row.widths.push(actualWidth);
    deltas.push([i, desiredWidth - actualWidth]);
  }

  // We want the accumulated width to be exactly containerWidth, unless the row is not full.
  // If any adjustments are made, at most one pixel is added/subtracted from each photo.

  // If the row is full and the width isn't exactly containerWidth, add one pixel
  // to each photo until the containerWidth is reached.
  if (row.full && accumWidth < containerWidth) {
    // Add pixels until we get to containerWidth. Sort deltas in decreasing value (positive deltas
    // indicate photos that rounded down, so add pixels to those first).
    deltas.sort((a, b) => b[1] - a[1]);
    for (let i = 0; i < deltas.length && accumWidth + 1 < containerWidth; i++) {
      const [idx] = deltas[i];
      row.widths[idx]++;
      accumWidth++;
    }
  } else if (accumWidth > containerWidth) {
    // Need to subtract pixels until we get to containerWidth. Sort deltas in increasing value
    // (negative deltas indicate photos that rounded up, so take away pixels from those first).
    deltas.sort((a, b) => a[1] - b[1]);
    for (let i = 0; i < deltas.length && accumWidth > containerWidth; i++) {
      const [idx] = deltas[i];
      row.widths[idx]--;
      accumWidth--;
    }
  }
}

function getShortLocation(location: string): string {
  const locParts = location.split(",");

  return locParts
    .slice(Math.max(0, locParts.length - 2))
    .reverse()
    .map((loc) => titleCase(stripLocationPrefix(loc)))
    .join(", ");
}

export function PhotoInfiniteScroll<K extends keyof PhotoListPhoto>(
  props: Props<K>
) {
  const {
    photoListQuery,
    photoListQueryForward,
    makePhotoUrl,
    onPhotoClick,
    stdHeight = 300,
    showOverlay = true,
    hidden,
    rowLimit,
    eventContext = EventContext.DEFAULT,
  } = props;

  const events = useMemo(() => {
    const events: PhotoInfiniteScrollContainerEvents<PhotoListPhoto> = {
      context: eventContext!,
      photoView: AppEvent.PHOTO_VIEW_THUMB,
      itemClick: {
        event: AppEvent.PHOTO_CLICK,
        getEventObject: (item: PhotoListPhoto): string => {
          return `/photo/${item.id}`;
        },
      },
    };

    return events;
  }, [eventContext]);

  return (
    <PhotoInfiniteScrollContainer
      hidden={!!hidden}
      listQuery={photoListQuery}
      queryForward={photoListQueryForward}
      showOverlay={!!showOverlay}
      rowLimit={rowLimit}
      layoutRows={useCallback(
        (idxStart, photos: PhotoListPhoto[], containerWidth) =>
          layoutRows(idxStart, photos, containerWidth, stdHeight!),
        [stdHeight]
      )}
      getOverlayRowCount={useCallback((photo: PhotoListPhoto) => {
        return (
          (photo.title ? 1 : 0) +
          (photo.locations && photo.locations.length > 0 ? 1 : 0)
        );
      }, [])}
      getRowHeight={useCallback((row: RowInfo) => row.height, [])}
      getPhotoWidth={useCallback((row: RowInfo, idx) => row.widths[idx], [])}
      buildOverlayContent={useCallback((photo: PhotoListPhoto) => {
        return (
          <>
            {photo.title && (
              <PhotoOverlayTitle>{photo.title}</PhotoOverlayTitle>
            )}
            {photo.locations && photo.locations.length > 0 && (
              <PhotoLocation>
                <IconLocation />
                &nbsp;
                {
                  // Just use first (primary) location
                  getShortLocation(photo.locations[0][PHOTO_GROUP_DISPLAY])
                }
              </PhotoLocation>
            )}
          </>
        );
      }, [])}
      renderPhoto={useCallback((row: RowInfo, idx: number) => {
        const photo = row.items[idx];
        return [
          <ConstrainedPhoto
            photo={photo}
            constrainWidth
            constrainHeight
            fixWidth={row.widths[idx]}
            fixHeight={row.height}
            // Browser hint to lazy load images as needed
            loading="lazy"
          />,
          photo.id,
        ];
      }, [])}
      makeUrl={makePhotoUrl}
      onItemClick={onPhotoClick}
      events={events}
    />
  );
}
