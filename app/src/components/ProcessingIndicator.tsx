import React, { useEffect, useMemo, useRef, useState } from "react";
import styled from "styled-components";
import { Spinner } from "./Spinner";
import { useIsFetching } from "@tanstack/react-query";
import { useProcessingIndicator } from "../hooks/useProcessingIndicator";
import {
  ProcessingIndicatorActionsContext,
  ProcessingIndicatorContext,
} from "../helpers/Contexts";

export interface ProcessingIndicatorActions {
  start: () => void;
  stop: () => void;
}

export const ProcessingIndicatorProvider: React.FC<{
  children?: React.ReactNode;
}> = ({ children }) => {
  const [processingCount, setProcessingCount] = useState(0);
  const isProcessing = processingCount !== 0;

  const actions = useMemo<ProcessingIndicatorActions>(
    () => ({
      start: () => setProcessingCount((s) => s + 1),
      stop: () => setProcessingCount((s) => s - 1),
    }),
    []
  );

  return (
    <ProcessingIndicatorActionsContext.Provider value={actions}>
      <ProcessingIndicatorContext.Provider value={isProcessing}>
        {children}
      </ProcessingIndicatorContext.Provider>
    </ProcessingIndicatorActionsContext.Provider>
  );
};

// Use visibility to control so that the animation is smooth (doesn't reset each time it is shown).
const SpinnerContainer = styled.div<{ $show: boolean }>`
  position: fixed;
  left: var(--fixed-bottom-button-offset);
  bottom: var(--fixed-bottom-button-offset);
  width: 3em;
  height: 3em;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 100;
  visibility: ${(p) => (p.$show ? "visible" : "hidden")};

  > div {
    box-shadow: 0px 0px 10px var(--text-color);
    background-color: var(--processingindicator-bg-color);
  }
`;

export const ProcessingIndicatorSpinner: React.FC = () => {
  const processingIndicator = useProcessingIndicator();
  const isFetchingQuery = useIsFetching();
  const [show, setShow] = useState(false);

  // Only show processing indicator after 0.5 seconds of waiting.
  const timerRef = useRef<number | undefined>(undefined);
  useEffect(() => {
    if (processingIndicator || isFetchingQuery) {
      if (timerRef.current !== undefined) return;

      // Set timer.
      timerRef.current = window.setTimeout(() => {
        setShow(true);
      }, 500);
    } else {
      clearTimeout(timerRef.current);
      timerRef.current = undefined;
      setShow(false);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [isFetchingQuery, processingIndicator]);

  return (
    <SpinnerContainer $show={show}>
      <Spinner />
    </SpinnerContainer>
  );
};
