// SPDX-License-Identifier: MIT
import React, { useCallback } from "react";
import { Link, LinkProps } from "react-router-dom";
import { EventDesc } from "../helpers/EventTracker";
import { useEventTracker } from "../hooks/useEventTracker";

interface Props extends LinkProps {
  refCallback?: (elem: HTMLAnchorElement | null) => void;
  // Event description. By default added as a UX event.
  event?: EventDesc;

  // Filter out non-link attributes that may be missed.
  borderRadius?: number;
}

export const LinkWithEvent: React.FC<Props> = (props) => {
  const {
    refCallback,
    event,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    borderRadius,
    ...linkProps
  } = props;
  const { onClick: linkOnClick } = linkProps;

  const eventTracker = useEventTracker();
  const onClick = useCallback(
    (e: React.MouseEvent<HTMLAnchorElement>) => {
      if (event) {
        eventTracker.addUXEvent(event);
      }
      if (linkOnClick) {
        linkOnClick(e);
      }
    },
    [event, eventTracker, linkOnClick]
  );

  return <Link {...linkProps} onClick={onClick} ref={refCallback} />;
};
