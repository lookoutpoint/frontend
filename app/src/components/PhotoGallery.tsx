// SPDX-License-Identifier: MIT
import { PhotoInfiniteScroll } from "./PhotoInfiniteScroll";
import { FullPhoto, PhotoIdGen, PhotoListPhoto } from "../api/photos";
import { PhotoListQuery } from "../helpers/PhotoListQuery";
import { PhotoLightbox } from "./PhotoLightbox";

interface Props<K extends keyof PhotoListPhoto> {
  photoListQuery: PhotoListQuery<K>;
  photoListQueryForward: boolean;
  showGalleryOverlay?: boolean;
  hidden?: boolean;
  stdHeight?: number;
  rowLimit?: number;
  makePhotoUrl: (photo: Pick<FullPhoto, "id" | "title">) => string;
  onGalleryClick: (photo: PhotoListPhoto) => boolean;
  lightbox: {
    photoId?: PhotoIdGen;
    closeUrl: string;
    onNav: (photo: PhotoListPhoto) => void;
    onClose: () => void;
    modifyTitle?: boolean;
  };
  eventContext?: string;
}

export function PhotoGallery<K extends keyof PhotoListPhoto>(props: Props<K>) {
  const {
    photoListQuery,
    photoListQueryForward,
    showGalleryOverlay,
    hidden,
    stdHeight,
    rowLimit,
    makePhotoUrl,
    onGalleryClick,
    lightbox,
    eventContext,
  } = props;

  // Element to show photo gallery. May be hidden if the parent wants to hide the gallery or if the
  // lightbox is active (which appears on top).
  return (
    <>
      <PhotoInfiniteScroll
        photoListQuery={photoListQuery}
        photoListQueryForward={photoListQueryForward}
        showOverlay={showGalleryOverlay}
        hidden={hidden || lightbox.photoId !== undefined}
        stdHeight={stdHeight}
        rowLimit={rowLimit}
        makePhotoUrl={makePhotoUrl}
        onPhotoClick={onGalleryClick}
        eventContext={eventContext}
      />
      {lightbox.photoId && (
        <PhotoLightbox
          photoId={lightbox.photoId}
          photoListQuery={photoListQuery}
          queryForward={photoListQueryForward}
          closeUrl={lightbox.closeUrl}
          modifyTitle={lightbox.modifyTitle}
          onClose={lightbox.onClose}
          onPhotoNav={lightbox.onNav}
          makePhotoLink={makePhotoUrl}
          eventContext={eventContext}
        />
      )}
    </>
  );
}
