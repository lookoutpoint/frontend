// SPDX-License-Identifier: MIT
import React, { useState } from "react";
import styled from "styled-components";

const Div = styled.div<{ $show: boolean }>`
  display: ${(p) => (p.$show ? "block" : "none")};
`;

interface Props {
  show: boolean;
  children?: React.ReactNode;
}

// This is a toggle div that keeps children rendered (mounted) even in the hidden state
// after they are first shown. This is intended to help in cases where the initial rendering of
// children is expensive, so once it is done once, keep it.
export const ToggleDiv: React.FC<Props> = ({ show, children }) => {
  const [renderChildren, setRenderChildren] = useState(show);

  // Once the children are showed, always render them even if hidden afterwards.
  if (show && !renderChildren) {
    setRenderChildren(true);
  }

  return <Div $show={show}>{renderChildren && children}</Div>;
};
