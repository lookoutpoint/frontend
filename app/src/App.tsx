// SPDX-License-Identifier: MIT
import React, { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import { AppRoutes } from "./Routes";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";

import { UserSessionProvider } from "./components/UserSession";
import { WindowSizeProvider } from "./components/WindowSize";
import { ThemeProvider } from "./components/Theme";
import { ScrollMemory } from "./components/ScrollMemory";
import { VisibilityTokensProvider } from "./components/VisibilityTokens";
import {
  ProcessingIndicatorProvider,
  ProcessingIndicatorSpinner,
} from "./components/ProcessingIndicator";
import { Helmet } from "react-helmet-async";
import { EventTrackerProvider } from "./components/EventTrackerProvider";
import { useEventTracker } from "./hooks/useEventTracker";
import { AppEvent } from "./api/events";
import { HomeRandomHighlightPhotoSessionStateProvider } from "./views/home/HomeRandomHighlightPhoto";
import {
  localStorageGet,
  LocalStorageKey,
  localStorageSet,
} from "./helpers/localStorage";
import { addHours, compareAsc } from "date-fns";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

// CSS
import "normalize.css";
import "./App.css";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // By default queries never become stale
      staleTime: Infinity,
      // By default do not retry
      retry: false,
    },
  },
});

export const App: React.FC = () => {
  return (
    <>
      <Helmet>
        {/* Duplicate from index.html so that the default shows up unless it is overriden. */}
        <meta
          name="description"
          content="A personal collection of photos revolving around all things outdoors: hikes, camping, animals and general travel."
        />
      </Helmet>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <UserSessionProvider>
            <EventTrackerProvider>
              <VisibilityTokensProvider>
                <WindowSizeProvider>
                  <ThemeProvider>
                    <ProcessingIndicatorProvider>
                      <HomeRandomHighlightPhotoSessionStateProvider>
                        {/* Core content */}
                        <SiteVisitEvent />
                        <div>
                          <Header />
                          <AppRoutes />
                          <Footer />
                        </div>
                        <div id="modals"></div>
                        <ScrollMemory />
                        <ProcessingIndicatorSpinner />
                      </HomeRandomHighlightPhotoSessionStateProvider>
                    </ProcessingIndicatorProvider>
                  </ThemeProvider>
                </WindowSizeProvider>
              </VisibilityTokensProvider>
            </EventTrackerProvider>
          </UserSessionProvider>
        </BrowserRouter>
      </QueryClientProvider>
    </>
  );
};

// Determine if a site visit event should be recorded.
function shouldRecordSiteVisitEvent(): boolean {
  // Only record the site visit if it is a "navigate" type event (not reload or back-forward).
  if (performance) {
    const entries = performance.getEntriesByType("navigation");
    for (const entry of entries) {
      const navEntry = entry as PerformanceNavigationTiming;
      if (navEntry.type !== "navigate") {
        return false;
      }
    }
  }

  const context = window.location.pathname;
  // Don't track admin-related paths.
  if (context.startsWith("/user") || context.startsWith("/admin")) return false;

  return true;
}

// Record a site visit on render, once per app load.
let siteVisitEventAdded = false;

// Capture value of url and referrer immediately. May be changed by the app later.
const siteVisitUrl = window.location.pathname;
const siteVisitReferrer = document.referrer;

const SiteVisitEvent: React.FC = () => {
  const eventTracker = useEventTracker();

  useEffect(() => {
    if (!siteVisitEventAdded && shouldRecordSiteVisitEvent()) {
      let referrer = document.referrer;

      const sessionStart = localStorageGet(LocalStorageKey.SESSION_START);
      if (sessionStart) {
        const sessionStartDate = new Date(sessionStart);
        const now = new Date();
        if (compareAsc(now, addHours(sessionStartDate, 1)) <= 0) {
          // Existing session started within the hour. If the referrer is not set,
          // this is probably from a new tab or something like that. So set the referrer to a special value
          // so that analytics can differentiate.
          if (!referrer) {
            referrer = "existing-session";
          }
        }
      }

      eventTracker.addEvent({
        event: AppEvent.SITE_VISIT,
        context: siteVisitUrl,
        // Track referrer via extra
        extra: siteVisitReferrer,
      });

      siteVisitEventAdded = true;
      localStorageSet(LocalStorageKey.SESSION_START, new Date().toISOString());
    }
  }, [eventTracker]);

  return <></>;
};
