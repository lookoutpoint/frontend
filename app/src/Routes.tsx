// SPDX-License-Identifier: MIT
import React, { Suspense, useEffect } from "react";
import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import { useUserSession } from "./hooks/useUserSession";
import { Spinner } from "./components/Spinner";
import styled from "styled-components";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorView, NotFoundError } from "./views/ErrorView";
import { lazily } from "react-lazily";

const { Home } = lazily(() => import("./views/home/Home"));
const { Recent } = lazily(() => import("./views/Recent"));
const { Blogs } = lazily(() => import("./views/Blogs"));

const { PhotoCollection } = lazily(() => import("./views/PhotoCollection"));
const { PhotoKeyword } = lazily(() => import("./views/PhotoKeyword"));
const { PhotoCategory } = lazily(() => import("./views/PhotoCategory"));
const { PhotoLocation } = lazily(() => import("./views/PhotoLocation"));
const { PhotoDate } = lazily(() => import("./views/PhotoDate"));
const { PageView } = lazily(() => import("./views/Page"));
const { VisTokenActivate } = lazily(() => import("./views/VisTokenActivate"));

const {
  Admin,
  AdminDatabase,
  AdminGroups,
  AdminTokens,
  AdminEvents,
  AdminPages,
  UserAdd,
  UserLogin,
  UserLogout,
} = lazily(() => import("./views/AdminViews"));

const AdminWrapper: React.FC<{ element: React.ReactNode }> = ({ element }) => {
  const session = useUserSession();

  // Don't use NotFound directly as that throws an exception.
  // There's a delay to when an admin session is validated, so we don't want to trigger the error boundary.
  if (!session.isAdmin) {
    return <ErrorView error={new NotFoundError("")} />;
  }
  return <>{element}</>;
};

const CenteredSpinner = styled(Spinner)`
  margin: 0 auto;
`;

export const AppRoutes: React.FC = () => {
  return (
    <Suspense fallback={<CenteredSpinner />}>
      <ErrorBoundary
        fallbackRender={({ error }) => <ErrorView error={error} />}
      >
        <Routes>
          {/* Home */}
          <Route path="/" element={<Home />} />
          <Route path="/@/:photoTitleSlug/:photoId" element={<Home />} />
          <Route path="/@/:photoId" element={<Home />} />
          <Route
            path="/@recent/:photoTitleSlug/:recentPhotoId"
            element={<Home />}
          />
          <Route path="/@recent/:recentPhotoId" element={<Home />} />

          {/* Photo groups */}

          <Route path="/collections/*" element={<PhotoCollection root />} />
          <Route path="/collection/*" element={<PhotoCollection />} />

          {/* Keep folder routes for backwards compatibility (e.g. with search engine indices) */}
          <Route
            path="/folders/*"
            element={
              <RedirectByPrefix prefix="/folders/" replace="/collections/" />
            }
          />
          <Route
            path="/folder/*"
            element={
              <RedirectByPrefix prefix="/folder/" replace="/collection/" />
            }
          />

          <Route path="/keywords/*" element={<PhotoKeyword root />} />
          <Route path="/keyword/*" element={<PhotoKeyword />} />

          <Route path="/categories/*" element={<PhotoCategory root />} />
          <Route path="/category/*" element={<PhotoCategory />} />

          <Route path="/locations/*" element={<PhotoLocation root />} />
          <Route path="/location/*" element={<PhotoLocation />} />

          <Route path="/dates/*" element={<PhotoDate root />} />
          <Route path="/date/*" element={<PhotoDate />} />

          <Route path="/blogs/" element={<Blogs />} />

          {/* Recent */}
          <Route path="/recent/" element={<Recent />} />
          <Route
            path="/recent/@/:photoTitleSlug/:photoId"
            element={<Recent />}
          />

          {/* Users */}
          <Route path="/user/add" element={<UserAdd />} />
          <Route path="/user/login" element={<UserLogin />} />
          <Route path="/user/logout" element={<UserLogout />} />

          {/* Admin */}
          <Route path="/admin" element={<AdminWrapper element={<Admin />} />} />
          <Route
            path="/admin/database"
            element={<AdminWrapper element={<AdminDatabase />} />}
          />
          <Route
            path="/admin/groups"
            element={<AdminWrapper element={<AdminGroups />} />}
          />
          <Route
            path="/admin/tokens"
            element={<AdminWrapper element={<AdminTokens />} />}
          />
          <Route
            path="/admin/events"
            element={<AdminWrapper element={<AdminEvents />} />}
          />
          <Route
            path="/admin/pages/*"
            element={<AdminWrapper element={<AdminPages />} />}
          />

          {/* Visibility token activation */}
          <Route path="/vtactivate/:actValue" element={<VisTokenActivate />} />

          {/* Default: treat as a page */}
          <Route path="*" element={<PageView />} />
        </Routes>
      </ErrorBoundary>
      <ActivityTracker />
    </Suspense>
  );
};

const ActivityTracker: React.FC = () => {
  // Listen for location changes and use that as an indicator of activity.
  const location = useLocation();
  const session = useUserSession();

  useEffect(() => {
    session.markActivity();
  }, [session, location.pathname]);

  return <></>;
};

const RedirectByPrefix: React.FC<{ prefix: string; replace: string }> = ({
  prefix,
  replace,
}) => {
  const location = useLocation();

  const newUrl = replace + location.pathname.slice(prefix.length);
  return <Navigate to={newUrl} replace />;
};
