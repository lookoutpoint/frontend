// SPDX-License-Identifier: MIT
import { Mutex } from "async-mutex";
import { ListOperator } from "../api/list";

// Function that fetches more objects with reference to an optional id and an operator
export type FetchFunc<T, Key extends keyof T> = (
  count: number,
  refId: T[Key] | undefined,
  refOp: ListOperator
) => Promise<T[] | null>;

// Segment of objects is simply an array.
type Segment<T> = T[];

// List objects must have a string id.
export interface ListObject {
  id: string;
}

// Represents the result of list query. Allows for incremental requests to complete the query.
export class ListQuery<T extends ListObject, Key extends keyof T> {
  private _sortKey: Key;
  private _minFetchCount: number;
  private fetch: FetchFunc<T, Key>;

  // Use a mutex to limit batch operations to one at a time.
  // This is necessary as the fetch logic is coded to assume that the segments do not change after a batch fetch is started.
  private batchMutex = new Mutex();

  // key -> { segment, index }
  private keyMap = new Map<T[Key], { segment: Segment<T>; index: number }>();
  private headSegment: Segment<T> | undefined;
  private tailSegment: Segment<T> | undefined;

  private idMap = new Map<string, T>();

  constructor(
    sortKey: Key,
    fetch: FetchFunc<T, typeof sortKey>,
    minFetchCount: number
  ) {
    this._sortKey = sortKey;
    this.fetch = fetch;
    this._minFetchCount = minFetchCount;
  }

  get sortKey(): Key {
    return this._sortKey;
  }

  get minFetchCount(): number {
    return this._minFetchCount;
  }

  // For testing purposes.
  get head(): T | undefined {
    return this.headSegment === undefined ? undefined : this.headSegment[0];
  }

  // For testing purposes.
  get tail(): T | undefined {
    return this.tailSegment === undefined
      ? undefined
      : this.tailSegment[this.tailSegment.length - 1];
  }

  get complete(): boolean {
    return (
      this.headSegment !== undefined && this.headSegment === this.tailSegment
    );
  }

  get count(): number {
    return this.keyMap.size;
  }

  /**
   * Look up object by id. Does not do a fetch if object does not exist.
   * @param id
   */
  getByIdSync(id: string): T | undefined {
    return this.idMap.get(id);
  }

  /**
   * Returns batch of objects beyond given refKey. May have to fetch
   * objects defined by refKey itself, but will try to fetch <count> objects beyond it.
   * @param refSortKey
   * @param count
   * @param forward
   */
  async batchBeyond(
    refSortKey: T[Key],
    count: number,
    forward: boolean
  ): Promise<T[]> {
    return await this.batchMutex.runExclusive(async () => {
      return this.batchBeyondImpl(refSortKey, count, forward);
    });
  }

  /**
   * Returns object batch after given ref key. Ref key may be undefined, in which case fetch from beginning of list.
   * @param refSortKey
   * @param count
   */
  async batchAfter(
    refSortKey: T[Key] | undefined,
    count: number
  ): Promise<T[]> {
    return await this.batchMutex.runExclusive(async () => {
      if (refSortKey === undefined) {
        // Start at beginning of list. So look up first segment.
        if (this.headSegment === undefined) {
          // Create new head segment.
          this.headSegment = [];
        }

        if (
          this.headSegment.length < count &&
          this.tailSegment !== this.headSegment
        ) {
          // Need to fetch additional objects.
          const numToFetch = Math.max(
            this.minFetchCount,
            count - this.headSegment.length
          );

          const curHeadTail =
            this.headSegment.length > 0
              ? this.headSegment[this.headSegment.length - 1]
              : undefined;

          await this.fetchAndAddToSegment(
            numToFetch,
            curHeadTail === undefined ? undefined : curHeadTail[this.sortKey],
            ListOperator.GT,
            this.headSegment
          );
        }

        return this.headSegment.slice(0, count);
      } else {
        return await this.batchBeyondImpl(refSortKey, count, true);
      }
    });
  }

  /**
   * Returns object batch before given ref key. Ref key may be undefined, in which case fetch from end of list.
   * @param refSortKey
   * @param count
   */
  async batchBefore(
    refSortKey: T[Key] | undefined,
    count: number
  ): Promise<T[]> {
    return await this.batchMutex.runExclusive(async () => {
      if (refSortKey === undefined) {
        // Start at end of list. So look up last segment.
        if (this.tailSegment === undefined) {
          // Create new tail segment.
          this.tailSegment = [];
        }

        if (
          this.tailSegment.length < count &&
          this.tailSegment !== this.headSegment
        ) {
          // Need to fetch additional objects.
          const numToFetch = Math.max(
            this.minFetchCount,
            count - this.tailSegment.length
          );

          const curTailHead =
            this.tailSegment.length > 0 ? this.tailSegment[0] : undefined;

          await this.fetchAndAddToSegment(
            numToFetch,
            curTailHead === undefined ? undefined : curTailHead[this.sortKey],
            ListOperator.LT,
            this.tailSegment
          );
        }

        return this.tailSegment
          .slice(this.tailSegment.length - count)
          .reverse();
      } else {
        return await this.batchBeyondImpl(refSortKey, count, false);
      }
    });
  }

  /**
   * Returns object associated with sort key and its adjacent neighbours
   * @param sortKey Object with sort key must exist
   */
  async adjacent(
    sortKey: T[Key],
    forward: boolean
  ): Promise<{ obj: T; prev: T | null; next: T | null }> {
    // Fetch object associated with sortKey and one after (next)
    await this.batchAfter(sortKey, 1);

    // Fetch object before
    await this.batchBeyond(sortKey, 1, false);

    return this.adjacentSync(sortKey, forward)!;
  }

  adjacentSync(
    sortKey: T[Key],
    forward: boolean
  ): { obj: T; prev: T | null; next: T | null } | undefined {
    const existing = this.keyMap.get(sortKey);
    if (existing === undefined) {
      return undefined;
    }

    const { segment, index } = existing;

    const m1 = index > 0 ? segment[index - 1] : null;
    const p1 = index < segment.length - 1 ? segment[index + 1] : null;

    return {
      obj: segment[index],
      prev: forward ? m1 : p1,
      next: forward ? p1 : m1,
    };
  }

  /**
   * Returns batch of objects beyond the sortKey. May have to fetch
   * object defined by sortKey itself, but will try to fetch <count> objects beyond it.
   * @param sortKey
   * @param count
   * @param forward
   */
  private async batchBeyondImpl(
    sortKey: T[Key],
    count: number,
    forward: boolean
  ): Promise<T[]> {
    const existing = this.keyMap.get(sortKey);
    let segment = existing?.segment;
    if (existing === undefined) {
      segment = [];

      const numToFetch = Math.max(this.minFetchCount, count) + 1;

      await this.fetchAndAddToSegment(
        numToFetch,
        sortKey,
        forward ? ListOperator.GTE : ListOperator.LTE,
        segment
      );

      return forward
        ? segment.slice(1, count + 1)
        : segment
            .slice(Math.max(0, segment.length - 1 - count), segment.length - 1)
            .reverse();
    } else {
      const { segment, index } = existing;

      if (
        forward &&
        index + count >= segment.length &&
        this.tailSegment !== segment
      ) {
        // Need to fetch additional objects.
        const numToFetch = Math.max(
          this.minFetchCount,
          index + count - segment.length + 1
        );

        await this.fetchAndAddToSegment(
          numToFetch,
          segment[segment.length - 1][this.sortKey],
          ListOperator.GT,
          segment
        );
      } else if (!forward && index < count && this.headSegment !== segment) {
        // Need to fetch additional objects.
        const numToFetch = Math.max(this.minFetchCount, count - index);

        await this.fetchAndAddToSegment(
          numToFetch,
          segment[0][this.sortKey],
          ListOperator.LT,
          segment
        );
      }

      if (forward) {
        return segment.slice(index + 1, index + count + 1);
      } else {
        // Need new index for sortKey object.
        const { index: newIndex } = this.keyMap.get(sortKey)!;
        return segment.slice(Math.max(0, newIndex - count), newIndex).reverse();
      }
    }
  }

  private errorBackoff = 0;

  private async fetchAndAddToSegment(
    count: number,
    refSortKey: T[Key] | undefined,
    operand: ListOperator,
    segment: Segment<T>
  ) {
    // If an error occured previously, don't just spam requests. Wait for some amount of time.
    if (this.errorBackoff > 0) {
      await new Promise<void>((resolve) =>
        window.setTimeout(() => resolve(), this.errorBackoff)
      );
    }

    // Fetch objects. Use a retry loop in case of errors.
    for (let i = 0; i < 5; ++i) {
      if (this.errorBackoff > 0) {
        // Wait.
        await new Promise<void>((resolve) =>
          window.setTimeout(() => resolve(), this.errorBackoff)
        );
      }

      const newObjects = await this.fetch(count, refSortKey, operand);

      const forward =
        operand === ListOperator.GT || operand === ListOperator.GTE;

      if (newObjects) {
        // Successful fetch.
        this.addToSegment(segment, newObjects, forward);
        this.errorBackoff = 0;

        // If less objects were returned than requested, interpret this to mean that an endpoint has been found (depending on direction).
        // Also, if an error has occured, treat it as an end point.
        if (newObjects !== null && newObjects.length < count) {
          if (forward) {
            this.tailSegment = segment;
          } else {
            this.headSegment = segment;
          }
        }

        return;
      } else {
        // Error occured. Increase error backoff.
        if (this.errorBackoff === 0) {
          this.errorBackoff = 1000;
        } else {
          this.errorBackoff *= 4;
        }
      }
    }

    throw new Error("fetchAndAddToSegment failed");
  }

  /**
   * Adds an array of segments to the existing segment, respecting the direction.
   * Existing segment has S0, S1, ..., Sn
   * New objects to add has P0, P1, ..., Pm
   * If forward, resulting segment: S0, S1, ..., Sn, P0, P1, ..., Pm
   * If backwards, resulting segment: Pm, ..., P1, P0, S0, S1, ..., Sn
   * @param segment
   * @param objects
   * @param forward
   */
  private addToSegment(segment: Segment<T>, objects: T[], forward: boolean) {
    const firstNewIndex = forward ? segment.length : 0;

    for (let i = 0; i < objects.length; ) {
      const obj = objects[i];
      this.idMap.set(obj.id, obj);

      const existing = this.keyMap.get(obj[this.sortKey]);
      if (existing === undefined) {
        if (forward) {
          segment.push(obj);
        } else {
          segment.splice(0, 0, obj);
        }
        i++;
      } else {
        const { segment: extSegment, index: extIndex } = existing;
        if (forward && extIndex !== 0) {
          throw new Error(
            "Inconsistent segment data: found non-zero index object during forward add to segment"
          );
        } else if (!forward && extIndex !== extSegment.length - 1) {
          throw new Error(
            "Inconsistent segment data: found non-end index object during backward add to segment"
          );
        }

        if (forward) {
          // Append the existing segment to the end of segment.
          segment.push(...extSegment);
        } else {
          // Prepend the existing segment to this segment.
          segment.splice(0, 0, ...extSegment);
        }

        // Skip past objects in the existing segment. Assume that they are the same
        // object that are in objects (if not, object list has changed and we silently ignore such changes).
        i += extSegment.length;
      }
    }

    // Update index map for new objects.
    for (let i = firstNewIndex; i < segment.length; ++i) {
      this.keyMap.set(segment[i][this.sortKey], { segment, index: i });
    }
  }
}
