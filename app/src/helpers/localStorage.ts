// SPDX-License-Identifier: MIT
import { useCallback, useState } from "react";
import { GroupType } from "../api/photoGroups";

const hasLocalStorage =
  window !== undefined && window.localStorage !== undefined;

export enum LocalStorageKey {
  THEME = "theme",

  SESSION_START = "session.start",

  PHOTO_LIGHTBOX_SHOW_SIDEBAR = "photo.lightbox.showSidebar",

  GROUP_SHOW_DESCRIPTION = "group.showDescription",

  TIMELINE_ROOT_COLLAPSED = "timeline.root.collapsed",
  TIMELINE_CHILD_COLLAPSED = "timeline.child.collapsed",

  VIEW_RECENT_GALLERY_SHOW_CAPTION = "view.recent.showCaption",

  VIEW_CATEGORY_SORT_BY_PARK = "view.category.sortByPark",

  VIEW_PHOTO_GALLERY_COMPLEX_CONTROLS = "view.photoGallery.complexControls",
  VIEW_PHOTO_GALLERY_SHOW_CAPTION = "view.photoGallery.showCaption",
  VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX = "view.photoGallery.showHighlights.",
  VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX = "view.photoGallery.queryForward.",
  VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX = "view.photoGallery.orderByCamera.",

  FEATURE_MAP_VIEW = "feature.map.view",
}

const defaultBools = {
  [LocalStorageKey.PHOTO_LIGHTBOX_SHOW_SIDEBAR]: false,

  [LocalStorageKey.GROUP_SHOW_DESCRIPTION]: true,

  [LocalStorageKey.TIMELINE_ROOT_COLLAPSED]: false,
  [LocalStorageKey.TIMELINE_CHILD_COLLAPSED]: true,

  [LocalStorageKey.VIEW_RECENT_GALLERY_SHOW_CAPTION]: false,

  [LocalStorageKey.VIEW_CATEGORY_SORT_BY_PARK]: false,

  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_CAPTION]: false,

  // VIEW_PHOTO_GALLERY_SHOW_HIGLIGHTS*
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
  GroupType.CATEGORY]: true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX + GroupType.DATE]:
    false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
  GroupType.COLLECTION]: false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
  GroupType.KEYWORD]: true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
  GroupType.LOCATION]: true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_SHOW_HIGHLIGHTS_PREFIX +
  GroupType.TIMELINE_GROUP]: true,

  // - For folders: show in camera order by default assuming that a folder
  //   is intended to be view chronologically
  // - For dates: chronologically
  // - For everything else: show latest photos first

  // VIEW_PHOTO_GALLERY_QUERY_FORWARD*
  [LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX +
  GroupType.COLLECTION]: true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX +
  GroupType.CATEGORY]: false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX + GroupType.DATE]:
    true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX + GroupType.KEYWORD]:
    false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_QUERY_FORWARD_PREFIX +
  GroupType.LOCATION]: false,

  // VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA*
  [LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX +
  GroupType.COLLECTION]: true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX +
  GroupType.CATEGORY]: false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX + GroupType.DATE]:
    true,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX +
  GroupType.KEYWORD]: false,
  [LocalStorageKey.VIEW_PHOTO_GALLERY_ORDER_BY_CAMERA_PREFIX +
  GroupType.LOCATION]: false,

  [LocalStorageKey.FEATURE_MAP_VIEW]: false,
};

export function localStorageGet(key: string): string | null {
  return hasLocalStorage ? window.localStorage.getItem(key) : null;
}

export function localStorageSet(key: string, value: string) {
  if (!hasLocalStorage) return;
  window.localStorage.setItem(key, value);
}

export function localStorageGetBool(key: string): boolean {
  const val = localStorageGet(key);
  if (val !== null) {
    return val === "1";
  }

  return key in defaultBools ? defaultBools[key] : false;
}

export function localStorageSetBool(key: string, value: boolean) {
  if (key in defaultBools && value === defaultBools[key]) {
    // Since this matches the default, remove the key.
    window.localStorage.removeItem(key);
  } else {
    localStorageSet(key, value ? "1" : "0");
  }
}

export function useLocalStorageBool(
  key: string
): [boolean, (val: boolean) => void, () => void] {
  const [value, setRawValue] = useState(() => localStorageGetBool(key));
  const setValue = useCallback(
    (value: boolean) => {
      setRawValue(value);
      localStorageSetBool(key, value);
    },
    [key]
  );
  const reset = useCallback(() => {
    setValue(defaultBools[key]);
  }, [key, setValue]);

  return [value, setValue, reset];
}
