// SPDX-License-Identifier: MIT
import { beforeEach, describe, expect, test, vi } from "vitest";

import { ListOperator } from "../../api/list";
import { PhotoListPhoto } from "../../api/photos";
import { PhotoListQuery } from "../PhotoListQuery";

let mockPhotoListLen = 26;

function makeMockFetchPhoto() {
  return vi.fn(
    async (
      count: number,
      refSortKey: string | undefined,
      operator: ListOperator
    ): Promise<PhotoListPhoto[]> => {
      const photos: PhotoListPhoto[] = [];

      const forward =
        operator === ListOperator.GT || operator === ListOperator.GTE;

      const sortKey = refSortKey ?? (forward ? "a" : "z");
      const baseKeyOrd = "a".charCodeAt(0);
      const sortKeyVal = sortKey.charCodeAt(0) - baseKeyOrd;
      if (
        sortKey.length !== 1 ||
        sortKeyVal < 0 ||
        sortKeyVal >= mockPhotoListLen
      ) {
        throw new Error(`Invalid sort key ${sortKey}`);
      }

      if (forward) {
        const start =
          refSortKey && operator === ListOperator.GT
            ? sortKeyVal + 1
            : sortKeyVal;
        for (let i = 0; i < count && start + i < mockPhotoListLen; i++) {
          const photoIdx = start + i;
          const k = String.fromCharCode(baseKeyOrd + photoIdx);
          photos.push({
            id: `id${k}`,
            storeGen: "0",
            width: 100 + photoIdx,
            height: 200 + photoIdx,
            sortKey: k,
            tsSortKey: k,
            public: true,
          });
        }
      } else {
        const start =
          refSortKey && operator === ListOperator.LT
            ? sortKeyVal - 1
            : sortKeyVal;
        for (let i = 0; i < count && start - i >= 0; i++) {
          const photoIdx = start - i;
          const k = String.fromCharCode(baseKeyOrd + photoIdx);
          photos.push({
            id: `id${k}`,
            storeGen: "0",
            width: 100 + photoIdx,
            height: 200 + photoIdx,
            sortKey: k,
            tsSortKey: k,
            public: true,
          });
        }
      }

      return photos;
    }
  );
}

function r(key: string): { id: string } {
  return { id: `id${key}` };
}

function rs(start: string, end: string): { id: string }[] {
  const results: { id: string }[] = [];
  const startVal = start.charCodeAt(0);
  const endVal = end.charCodeAt(0);
  if (endVal >= startVal) {
    for (let val = startVal; val <= endVal; val++) {
      results.push(r(String.fromCharCode(val)));
    }
  } else {
    for (let val = startVal; val >= endVal; val--) {
      results.push(r(String.fromCharCode(val)));
    }
  }

  return results;
}

describe("PhotoListQuery", () => {
  let mockFetchPhoto: ReturnType<typeof makeMockFetchPhoto>;
  let query: PhotoListQuery<"sortKey">;

  beforeEach(() => {
    mockFetchPhoto = makeMockFetchPhoto();
    mockPhotoListLen = 26;
    query = new PhotoListQuery("sortKey", mockFetchPhoto, 5);
  });

  describe("batchAfter", () => {
    test("from beginning", async () => {
      const photos = await query.batchAfter(undefined, 1);
      expect(photos).toMatchObject(rs("a", "a"));
      expect(query.head).not.toBeUndefined();
      expect(query.tail).toBeUndefined();
      expect(mockFetchPhoto.mock.calls.length).toBe(1);
    });

    test("from beginning with length 1 list", async () => {
      mockPhotoListLen = 1;

      const photos = await query.batchAfter(undefined, 1);
      expect(photos).toMatchObject(rs("a", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);
    });

    test("from middle", async () => {
      let photos = await query.batchAfter("f", 3);
      expect(photos).toMatchObject(rs("g", "i"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Query after i, should fetch from previous batch
      photos = await query.batchAfter("i", 2);
      expect(photos).toMatchObject(rs("j", "k"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Query immediately after last batch
      photos = await query.batchAfter("k", 1);
      expect(photos).toMatchObject(rs("l", "l"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      expect(query.head).toBeUndefined();
      expect(query.tail).toBeUndefined();
    });

    test("start from middle of segment", async () => {
      let photos = await query.batchAfter(undefined, 5);
      expect(photos).toMatchObject(rs("a", "e"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      photos = await query.batchAfter("a", 5);
      expect(photos).toMatchObject(rs("b", "f"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });

    test("to end", async () => {
      let photos = await query.batchAfter("x", 5);
      expect(photos).toMatchObject(rs("y", "z"));
      expect(query.tail).toMatchObject({ id: "idz" });
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Additional queries to tail should not cause more queries.
      photos = await query.batchAfter("z", 2);
      expect(photos).toMatchObject([]);
      expect(query.tail).toMatchObject(r("z"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Additional queries before last segment should cause a query
      // but not change the tail.
      photos = await query.batchAfter("w", 10);
      expect(photos).toMatchObject(rs("x", "z"));
      expect(query.tail).toMatchObject(r("z"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });

    test("back-to-back to end", async () => {
      let photos = await query.batchAfter(undefined, 10);
      expect(photos).toMatchObject(rs("a", "j"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toBeUndefined();
      expect(query.complete).toBe(false);
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      photos = await query.batchAfter("j", 10);
      expect(photos).toMatchObject(rs("k", "t"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toBeUndefined();
      expect(query.complete).toBe(false);
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      photos = await query.batchAfter("t", 10);
      expect(photos).toMatchObject(rs("u", "z"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toMatchObject(r("z"));
      expect(query.complete).toBe(true);
      expect(mockFetchPhoto.mock.calls.length).toBe(3);
    });

    test("disjoint segments", async () => {
      // b, c, d, e, f
      let photos = await query.batchAfter("a", 1);
      expect(photos).toMatchObject(rs("b", "b"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // h, i, j, k, l
      photos = await query.batchAfter("g", 1);
      expect(photos).toMatchObject(rs("h", "h"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      // g, h, i, j, k
      photos = await query.batchAfter("f", 5);
      expect(photos).toMatchObject(rs("g", "k"));
      expect(mockFetchPhoto.mock.calls.length).toBe(3);

      // b, c, d, e, f, g, h, i, j, k, l
      photos = await query.batchAfter("a", 11);
      expect(photos).toMatchObject(rs("b", "l"));
      expect(mockFetchPhoto.mock.calls.length).toBe(3);

      // Check from beginning, which will require a fetch
      photos = await query.batchAfter(undefined, 12);
      expect(photos).toMatchObject(rs("a", "l"));
      expect(mockFetchPhoto.mock.calls.length).toBe(4);
    });

    test("disjoint segments and then join past last", async () => {
      // a, b, c, d, e
      let photos = await query.batchAfter(undefined, 1);
      expect(photos).toMatchObject(rs("a", "a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // g, h, i, j, k
      photos = await query.batchAfter("f", 1);
      expect(photos).toMatchObject(rs("g", "g"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      // f, g, h, i, j, k, l, m
      photos = await query.batchAfter("e", 8);
      expect(photos).toMatchObject(rs("f", "m"));
      expect(mockFetchPhoto.mock.calls.length).toBe(3);

      // a - m
      photos = await query.batchAfter(undefined, 13);
      expect(photos).toMatchObject(rs("a", "m"));
      expect(mockFetchPhoto.mock.calls.length).toBe(3);

      // a - n
      photos = await query.batchAfter(undefined, 14);
      expect(photos).toMatchObject(rs("a", "n"));
      expect(mockFetchPhoto.mock.calls.length).toBe(4);
    });
  });

  describe("batchBefore", () => {
    test("from end", async () => {
      const photos = await query.batchBefore(undefined, 1);
      expect(photos).toMatchObject(rs("z", "z"));
      expect(query.tail).not.toBeUndefined();
      expect(query.head).toBeUndefined();
      expect(mockFetchPhoto.mock.calls.length).toBe(1);
    });

    test("from beginning with length 1 list", async () => {
      mockPhotoListLen = 1;

      const photos = await query.batchAfter(undefined, 1);
      expect(photos).toMatchObject(rs("a", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);
    });

    test("from middle", async () => {
      let photos = await query.batchBefore("m", 3);
      expect(photos).toMatchObject(rs("l", "j"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Query before j, should fetch from previous batch
      photos = await query.batchBefore("l", 2);
      expect(photos).toMatchObject(rs("k", "j"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Query immediately after last batch
      photos = await query.batchBefore("h", 1);
      expect(photos).toMatchObject(rs("g", "g"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      expect(query.head).toBeUndefined();
      expect(query.tail).toBeUndefined();
    });

    test("to head", async () => {
      let photos = await query.batchBefore("c", 5);
      expect(photos).toMatchObject(rs("b", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Additional queries to head should not cause more queries.
      photos = await query.batchBefore("a", 2);
      expect(photos).toMatchObject([]);
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Additional queries before head segment should cause a query
      // but not change the tail.
      photos = await query.batchBefore("f", 10);
      expect(photos).toMatchObject(rs("e", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });
  });

  describe("batchBeyond backwards", () => {
    test("from middle to beginning", async () => {
      let photos = await query.batchBeyond("c", 5, false);
      expect(photos).toMatchObject(rs("b", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      photos = await query.batchBeyond("b", 5, false);
      expect(photos).toMatchObject(rs("a", "a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);
    });

    test("disjointed and merge", async () => {
      let photos = await query.batchBeyond("n", 5, false);
      expect(photos).toMatchObject(rs("m", "i"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      photos = await query.batchBeyond("g", 5, false);
      expect(photos).toMatchObject(rs("f", "b"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      photos = await query.batchBeyond("n", 100, false);
      expect(photos).toMatchObject(rs("m", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(3);
    });

    test("big batch", async () => {
      let photos = await query.batchBeyond("z", 30, false);
      expect(photos).toMatchObject(rs("y", "a"));
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Go forward direction, should be able to get full segment without any queries
      photos = await query.batchAfter(undefined, 26);
      expect(photos).toMatchObject(rs("a", "z"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toBeUndefined();
      expect(mockFetchPhoto.mock.calls.length).toBe(1);

      // Go forward direction to get end, need query to confirm end.
      photos = await query.batchAfter(undefined, 100);
      expect(photos).toMatchObject(rs("a", "z"));
      expect(query.head).toMatchObject(r("a"));
      expect(query.tail).toMatchObject(r("z"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      // Go forward again to get end, should not have new query.
      photos = await query.batchAfter(undefined, 100);
      expect(photos).toMatchObject(rs("a", "z"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });
  });

  describe("adjacent", () => {
    test("middle", async () => {
      let { obj, next, prev } = await query.adjacent("k", true);
      expect(obj).toMatchObject(r("k"));
      expect(next).toMatchObject(r("l"));
      expect(prev).toMatchObject(r("j"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);

      // A subsequent call should reuse batch fetched from before.
      ({ obj, next, prev } = await query.adjacent("j", true));
      expect(obj).toMatchObject(r("j"));
      expect(next).toMatchObject(r("k"));
      expect(prev).toMatchObject(r("i"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });

    test("first", async () => {
      const { obj, next, prev } = await query.adjacent("a", true);
      expect(obj).toMatchObject(r("a"));
      expect(next).toMatchObject(r("b"));
      expect(prev).toBeNull();
      expect(query.head).toMatchObject(r("a"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });

    test("last", async () => {
      const { obj, next, prev } = await query.adjacent("z", true);
      expect(obj).toMatchObject(r("z"));
      expect(next).toBeNull();
      expect(prev).toMatchObject(r("y"));
      expect(query.tail).toMatchObject(r("z"));
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });

    test("length 1 list", async () => {
      mockPhotoListLen = 1;
      const { obj, next, prev } = await query.adjacent("a", true);
      expect(obj).toMatchObject(r("a"));
      expect(next).toBeNull();
      expect(prev).toBeNull();
      expect(mockFetchPhoto.mock.calls.length).toBe(2);
    });
  });
});
