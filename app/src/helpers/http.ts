// SPDX-License-Identifier: MIT
import LRUCache from "lru-cache";
import { UserSession } from "./Contexts";
import { axios } from "../util";

export function success(statusCode: number): boolean {
  return statusCode >= 200 && statusCode < 300;
}

// Get url. Returns a promise that never throws/rejects.
// On success, returns success = true and the data.
// On failure, returns success = false and null data.
export async function axiosGet<T>(
  url: string
): Promise<{ data: T | null; success: boolean }> {
  try {
    const { data, status } = await axios.get<T>(url);
    return { success: success(status), data };
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
  } catch (e) {
    return { success: false, data: null };
  }
}

// Get url. Returns a promise for the data of type T or throws an error on failure.
export async function axiosPureGet<T>(url: string): Promise<T> {
  const { data, status } = await axios.get<T>(url);
  if (success(status)) {
    return data;
  }
  throw new Error(`get failed to ${url}, status ${status}`);
}

// Post to url with given data. Returns a promise that never throws/rejects.
// On success, returns success = true and the data.
// On failure, returns success = false and null data.
export async function axiosPost<T>(
  url: string,
  postData: unknown
): Promise<{ data: T | null; success: boolean }> {
  try {
    const { data, status } = await axios.post<T>(url, postData);
    return { success: success(status), data };
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
  } catch (e) {
    return { success: false, data: null };
  }
}

// Post to url with given data. Returns a promise for the data of type T or throws an error on failure.
export async function axiosPurePost<T>(
  url: string,
  postData: unknown
): Promise<T> {
  const { data, status } = await axios.post<T>(url, postData);
  if (success(status)) {
    return data;
  }
  throw new Error(`post failed to ${url}, status ${status}`);
}

export async function axiosCachedPost<T>(
  session: UserSession | undefined,
  url: string,
  postData: unknown,
  key: string,
  cache: LRUCache<string, T>
): Promise<{ data: T | null; success: boolean }> {
  // When admin, never cache data as changes may be happening.
  if (!session || !session.isAdmin) {
    const cachedData = cache.get(key);
    if (cachedData !== undefined) {
      // Cache hit!
      return new Promise((resolve) => {
        resolve({ data: cachedData, success: true });
      });
    }
  }

  // Make request.
  const result = await axiosPost<T>(url, postData);
  if (result.success && result.data) {
    // Update cache.
    cache.set(key, result.data);
  }

  return result;
}
