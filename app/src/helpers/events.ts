// SPDX-License-Identifier: MIT
import {
  eachDayOfInterval,
  eachMonthOfInterval,
  eachYearOfInterval,
} from "date-fns";
import { useMemo } from "react";
import { UTCDate } from "../api/date";
import { EventGranularity } from "../api/events";
import { DateSelection } from "../views/AdminEvents";

const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

function getIntervalFn(
  granularity: EventGranularity
): typeof eachDayOfInterval | undefined {
  if (granularity === EventGranularity.DAY) {
    return eachDayOfInterval;
  } else if (granularity === EventGranularity.MONTH) {
    return eachMonthOfInterval;
  } else if (granularity === EventGranularity.YEAR) {
    return eachYearOfInterval;
  } else {
    // No interval for ALL_TIME.
    return undefined;
  }
}

export function useChartDateSelectionLabels(dateSel: DateSelection): string[] {
  return useMemo(() => {
    const intervalFn = getIntervalFn(dateSel.granularity);
    if (!intervalFn) return [];

    const labels: string[] = [];

    const showYears =
      dateSel.granularity === EventGranularity.YEAR ||
      dateSel.startDate.date.getFullYear() !==
        dateSel.endDate.date.getFullYear();
    const showMonths =
      dateSel.granularity !== EventGranularity.YEAR &&
      (showYears ||
        dateSel.startDate.date.getMonth() !== dateSel.endDate.date.getMonth());
    const showDay = dateSel.granularity === EventGranularity.DAY;

    for (const date of intervalFn({
      start: dateSel.startDate.date,
      end: dateSel.endDate.date,
    })) {
      const year = date.getFullYear();
      const month = date.getMonth();
      const day = date.getDate();

      let dateStr = "";
      if (showMonths) {
        dateStr += `${MONTHS[month]} `;
      }
      if (showDay) {
        dateStr += `${day}`;
      }
      if (showYears) {
        dateStr += ` ${year}`;
      }

      labels.push(dateStr);
    }

    return labels;
  }, [dateSel.endDate.date, dateSel.granularity, dateSel.startDate.date]);
}

export function buildChartDateSelectionNumberData(
  dateSel: DateSelection,
  n: number,
  getDate: (i: number) => UTCDate,
  getValue: (i: number) => number,
  defaultValue = 0
): number[] {
  const intervalFn = getIntervalFn(dateSel.granularity);
  if (!intervalFn) return [];

  // Sort data by date.
  const idxs: number[] = [];
  for (let i = 0; i < n; i++) {
    idxs.push(i);
  }

  idxs.sort((a, b) => getDate(a).date.getTime() - getDate(b).date.getTime());

  const values: number[] = [];

  let i = 0;
  for (const date of intervalFn({
    start: dateSel.startDate.date,
    end: dateSel.endDate.date,
  })) {
    if (i < n) {
      const iDate = getDate(idxs[i]);
      if (date.getTime() === iDate.date.getTime()) {
        // Found a date with a value.
        values.push(getValue(idxs[i]));
        i++;
        continue;
      }
    }

    // Use default value.
    values.push(defaultValue);
  }

  return values;
}
