// SPDX-License-Identifier: MIT
import React from "react";
import { EventTracker } from "./EventTracker";
import { ProcessingIndicatorActions } from "../components/ProcessingIndicator";

export const EventTrackerContext = React.createContext<EventTracker | null>(
  null
);
export const ProcessingIndicatorContext = React.createContext<boolean>(false);

export const ProcessingIndicatorActionsContext =
  React.createContext<ProcessingIndicatorActions | null>(null);

export enum ThemeType {
  LIGHT = "light",
  DARK = "dark",
}

export interface Theme {
  readonly type: ThemeType;
  setType(type: ThemeType, save?: boolean): void;
}

export const ThemeContext = React.createContext<Theme>({
  type: ThemeType.LIGHT,
  setType: () => {},
});

export interface UserSession {
  readonly isLoggedIn: boolean;
  readonly isAdmin: boolean;
  addUser(name: string, password: string): Promise<void>;
  login(name: string, password: string): Promise<void>;
  logout(): Promise<void>;
  markActivity(): void;
}

export const UserSessionContext = React.createContext<UserSession | null>(null);

export interface WindowSize {
  width: number;
  height: number;
  scrollBarWidth: number;
}

export const WindowSizeContext = React.createContext<WindowSize>({
  width: 0,
  height: 0,
  scrollBarWidth: 0,
});

export interface VisibilityTokens {
  readonly hasTokens: boolean;
  clearTokens(): void;
  onTokenActivated(): void;
}

export const VisibilityTokensContext =
  React.createContext<VisibilityTokens | null>(null);
