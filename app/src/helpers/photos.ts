// SPDX-License-Identifier: MIT
import { FullPhoto, PHOTO_GROUP_ID } from "../api/photos";
import { slugifyToken } from "./slugify";
import { VIEW_PHOTOS } from "./views";

export function makeLightboxPhotoUrl(
  rootUrl: string,
  photo: Pick<FullPhoto, "id" | "title">
): string {
  const { id, title } = photo;

  // Convert the title into a slug. Preserve case (the title is just for SEO).
  const titleSlug = slugifyToken(title ?? "");

  // Build URL, which is of the following format:
  // <rootUrl>[/][<title>/]<id>
  let url = rootUrl;
  if (url.length === 0 || url[url.length - 1] !== "/") {
    url += "/";
  }
  url += titleSlug;
  if (titleSlug.length > 0) {
    url += "/";
  }
  url += id;

  return url;
}

// The canonical photo url is the one in its owning folder.
export function makeCanonicalPhotoUrl(
  photo: Pick<FullPhoto, "id" | "title" | "folder">
): string {
  return makeLightboxPhotoUrl(
    `/collection${photo.folder[PHOTO_GROUP_ID]}@${VIEW_PHOTOS}/`,
    photo
  );
}

export function swipeAnimationDuration(
  delta: number,
  windowWidth: number
): number {
  const p = 1 - Math.abs(delta) / windowWidth;
  return Math.min(1.2, Math.max(0.3, p * 0.75));
}
