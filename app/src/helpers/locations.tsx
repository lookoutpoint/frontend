// SPDX-License-Identifier: MIT

import { IconHike, IconPark } from "../components/Icons";
import { titleCase } from "./titleCase";

// Location-specific APIs

const hikePrefix = "!hike:";
const parkPrefix = "!park:";

// Special location type
export enum LocationType {
  OTHER,
  PARK,
  HIKE,
}

// Special location types are denoted by prefixes in the display string.
// This function strips any prefixes and returns the corresponding LocationType, if any.
export function stripAndGetLocationType(display: string): {
  display: string;
  type: LocationType;
} {
  if (display.startsWith(hikePrefix)) {
    return {
      display: display.substring(hikePrefix.length),
      type: LocationType.HIKE,
    };
  }
  if (display.startsWith(parkPrefix)) {
    return {
      display: display.substring(parkPrefix.length),
      type: LocationType.PARK,
    };
  }
  return { display, type: LocationType.OTHER };
}

// Strips location prefixes from display string.
export function stripLocationPrefix(display: string) {
  return stripAndGetLocationType(display).display;
}

export function getLocFancyGroupName(str: string): JSX.Element {
  const { display, type } = stripAndGetLocationType(str);
  const title = titleCase(display);
  if (type === LocationType.OTHER) {
    return <>{title}</>;
  } else if (type === LocationType.PARK) {
    return (
      <>
        <IconPark /> {title}
      </>
    );
  } else if (type === LocationType.HIKE) {
    return (
      <>
        <IconHike /> {title}
      </>
    );
  } else {
    return <></>;
  }
}
