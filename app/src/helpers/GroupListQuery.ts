// SPDX-License-Identifier: MIT
import { ListOperator } from "../api/list";
import { Group } from "../api/photoGroups";
import { FetchFunc, ListQuery } from "./ListQuery";

export class GroupListQuery<G extends Group = Group> extends ListQuery<
  G,
  "id"
> {
  constructor(fetchGroups: FetchFunc<G, "id">, minFetchCount = 15) {
    super("id", fetchGroups, minFetchCount);
  }
}

export function makeFetchGroupsFromList<
  K extends keyof G,
  G extends Group = Group
>(key: K, groups: G[]): FetchFunc<G, K> {
  return async (count, refKeyValue, refOp) => {
    let refIdx: number | undefined;
    if (refKeyValue) {
      refIdx = groups.findIndex((v) => v[key] === refKeyValue);
      if (refIdx < 0) {
        return [];
      }
    }

    const forward = refOp === ListOperator.GT || refOp === ListOperator.GTE;
    if (forward) {
      let start = refIdx ?? 0;
      if (refKeyValue && refOp === ListOperator.GT) {
        start++;
      }

      return groups.slice(start, Math.min(groups.length, start + count));
    } else {
      let end = (refIdx ?? groups.length - 1) + 1;
      if (refKeyValue && refOp === ListOperator.LT) {
        end--;
      }

      return groups.slice(Math.max(0, end - count), end).reverse();
    }
  };
}
