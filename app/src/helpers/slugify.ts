// SPDX-License-Identifier: MIT
// These should match up with the backend regex.
const tokenRegex = new RegExp(
  "[\\s~`!@#$%^&*()_+=[\\]{}\\\\|:;\"'<>,.?/-]+",
  "g"
);
const pathRegex = new RegExp(
  "[\\s~`!@#$%^&*()_+=[\\]{}\\\\|:;\"'<>,.?-]+",
  "g"
);

function trimToken(token: string): string {
  // Trim any beginning or ending "-"
  let i = 0;
  while (i < token.length && token[i] === "-") {
    i++;
  }
  token = token.substring(i);

  i = token.length;
  while (i > 0 && token[i - 1] === "-") {
    i--;
  }
  token = token.substring(0, i);

  return token;
}

export function slugifyToken(token: string): string {
  return trimToken(token.replaceAll(tokenRegex, "-"));
}

export function slugifyTokenLower(token: string): string {
  return slugifyToken(token).toLowerCase();
}

export function slugifyPath(path: string): string {
  return trimToken(path.replaceAll(pathRegex, "-"));
}

export function slugifyPathLower(path: string): string {
  return slugifyPath(path).toLowerCase();
}

export function approxDeslugify(s: string): string {
  return s.replaceAll("-", " ");
}

export function trimSlash(s: string): string {
  if (s.length > 0 && s[s.length - 1] === "/") {
    return s.substring(0, s.length - 1);
  }
  return s;
}

export function slugPathDepth(path: string): number {
  let depth = 0;
  for (let i = 0; i < path.length; ++i) {
    if (path[i] === "/") {
      depth++;
    }
  }
  return depth - 1;
}
