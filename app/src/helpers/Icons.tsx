// SPDX-License-Identifier: MIT
import React from "react";
import {
  IconCategoryAnimal,
  IconCategoryHike,
  IconCategoryPark,
  IconCategoryFlower,
  IconCategories,
} from "../components/Icons";

const CATEGORY_ICON: { [cat: string]: React.ReactNode } = {
  animal: <IconCategoryAnimal />,
  hike: <IconCategoryHike />,
  park: <IconCategoryPark />,
  flower: <IconCategoryFlower />,
};

export function getCategoryIcon(cat: string): React.ReactNode {
  return CATEGORY_ICON[cat] ?? <IconCategories />;
}
