// SPDX-License-Identifier: MIT

// ============================================================================
// Rich text parsing
// ============================================================================
interface Doc {
  sections: Section[];
}
export interface Section {
  title?: string;
  blocks: Block[];
}
export enum BlockType {
  PARAGRAPH,
  PHOTOS,
  LIST,
  TABLE,
}
type ParagraphLink = [string, string]; // [url, text]

export interface ParagraphBlock {
  type: BlockType.PARAGRAPH;
  parts: (string | ParagraphLink)[];
}
interface ListItem {
  contents: ParagraphBlock;
  subList?: ListBlock;
}
export interface ListBlock {
  type: BlockType.LIST;
  items: ListItem[];
}
export interface TableBlock {
  type: BlockType.TABLE;
  caption?: string;
  highlight?: boolean;
  contents: ParagraphBlock[][]; // [row][col]
  numCols: number;
}
export interface PhotosBlock {
  type: BlockType.PHOTOS;
  grid?: {
    rows: number;
    cols: number;
    ratio?: number;
  };
  photoIds: string[];
  caption: boolean;
  maxWidth?: number;
  maxHeight?: number;
}
type Block = ParagraphBlock | ListBlock | TableBlock | PhotosBlock;

export function parseRichText(text: string): Doc | string {
  const textParts = text.split(/\n\n+/);

  const sections: Section[] = [];
  for (let part of textParts) {
    part = part.trim();
    if (part.length === 0) continue;

    if (part.startsWith("=====")) {
      // Start of new section.
      const title = part.substring(5).trim();
      if (title.length === 0) {
        return "Section title is empty";
      }

      sections.push({ title, blocks: [] });
      continue;
    }

    // A block.
    if (sections.length === 0) {
      // Create first section (with no title).
      sections.push({ blocks: [] });
    }
    const section = sections[sections.length - 1];

    if (part.startsWith("[[") && part.endsWith("]]")) {
      const photos = parsePhotoSection(part);
      if (typeof photos === "string") {
        // error
        return photos;
      }
      section.blocks.push(photos);
    } else if (part.startsWith("* ")) {
      const list = parseListSection(part);
      if (typeof list === "string") {
        // error
        return list;
      }
      section.blocks.push(list);
    } else if (part.startsWith("|")) {
      const table = parseTableSection(part);
      if (typeof table === "string") {
        // error
        return table;
      }
      section.blocks.push(table);
    } else {
      const para = parseParagraphSection(part);
      if (typeof para === "string") {
        // error
        return para;
      }
      section.blocks.push(para);
    }
  }

  return { sections };
}
export function isParagraphLink(
  value: string | ParagraphLink
): value is ParagraphLink {
  return typeof value === "object";
}
function parseParagraphSection(text: string): ParagraphBlock | string {
  const para: ParagraphBlock = { type: BlockType.PARAGRAPH, parts: [] };

  // Look for links, which are of the form {{<url>:<text>}}.
  let i = 0;
  while (i < text.length) {
    const linkStart = text.indexOf("{{", i);
    if (linkStart === -1) {
      // No more links, just add rest of the text as a text.
      para.parts.push(text.substring(i));
      i = text.length;
    } else {
      // Add text before link start.
      para.parts.push(text.substring(i, linkStart));

      const linkEnd = text.indexOf("}}", linkStart);
      const linkSep = text.lastIndexOf(":", linkEnd);
      if (linkSep === -1 || linkEnd === -1 || linkSep > linkEnd) {
        // This is just text.
        para.parts.push("{{");
        i += 2;
        continue;
      }

      // This is a link.
      const url = text.substring(linkStart + 2, linkSep);
      const desc = text.substring(linkSep + 1, linkEnd);
      para.parts.push([url, desc]);

      i = linkEnd + 2;
    }
  }

  return para;
}
function parseListSection(text: string): ListBlock | string {
  // List items are split by new lines.
  const lines = text.split("\n");

  const listStack: ListBlock[] = [{ type: BlockType.LIST, items: [] }];
  for (const line of lines) {
    const bulletIdx = line.indexOf("* ");
    if (bulletIdx < 0) {
      return "List item must start with '* '.";
    } else if (bulletIdx % 3 !== 0) {
      return "List indent character width must be a multiple of three.";
    }

    const indentLevel = bulletIdx / 3 + 1; // one-based

    const contents = parseParagraphSection(line.substring(bulletIdx + 2));
    if (typeof contents === "string") {
      return `List paragraph error: ${contents}`;
    }

    while (indentLevel < listStack.length) {
      // Pop back up.
      listStack.pop();
    }

    if (indentLevel === listStack.length) {
      // Add item to current list block.
      listStack[listStack.length - 1].items.push({ contents });
    } else if (indentLevel === listStack.length + 1) {
      // Next indent level, so start a new sub-list on the last item.
      const subList: ListBlock = {
        type: BlockType.LIST,
        items: [{ contents }],
      };

      const items = listStack[listStack.length - 1].items;
      items[items.length - 1].subList = subList;
      listStack.push(subList);
    } else {
      return "List indent level too large.";
    }
  }

  return listStack[0];
}
function parseTableSection(text: string): TableBlock | string {
  // Each row is its own line
  const rows = text.split("\n");

  const table: TableBlock = { type: BlockType.TABLE, contents: [], numCols: 0 };

  for (const row of rows) {
    if (!row.startsWith("|")) {
      return "Table row must start with '|'.";
    }

    if (row.startsWith("|!")) {
      // Table parameters
      const param = row.slice(2);
      const mCaption = param.match(/caption\s*=\s*(.*)$/);
      if (mCaption) {
        table.caption = mCaption[1];
        continue;
      }

      const mHighlight = param.match(/highlight(?:\s*=\s*(true|false))?$/);
      if (mHighlight) {
        if (mHighlight[1] === undefined || mHighlight[1] === "true") {
          table.highlight = true;
        } else if (mHighlight[1] === "false") {
          table.highlight = false;
        } else {
          return "Unrecognized value for highlight parameter.";
        }
        continue;
      }

      return "Unrecognized table parameter.";
    } else {
      // Normal table row.
      row.trimEnd();
      const cols = row.slice(1).split(/\|/);
      if (cols[cols.length - 1].length === 0) {
        cols.pop();
      }

      const tableRow: ParagraphBlock[] = [];
      for (const col of cols) {
        const para = parseParagraphSection(col);
        if (typeof para === "string") {
          return para;
        }
        tableRow.push(para);
      }

      table.contents.push(tableRow);
      if (tableRow.length > table.numCols) {
        table.numCols = tableRow.length;
      }
    }
  }

  return table;
}
function parsePhotoSection(text: string): PhotosBlock | string {
  // Strip off delimiters.
  text = text.substring(2, text.length - 2);

  const section: PhotosBlock = {
    type: BlockType.PHOTOS,
    photoIds: [],
    caption: false,
  };

  // Format: [<param>,<param>,...:]<photoId>,<photoId>,...
  const paramSep = text.indexOf(":");
  if (paramSep !== -1) {
    // Parse params
    const params = text.substring(0, paramSep).split(",");
    for (let param of params) {
      param = param.trim();

      // Grid
      const mGrid = param.match(/^grid\s*=\s*(\d+)x(\d+)$/);
      if (mGrid) {
        section.grid = { rows: +mGrid[1], cols: +mGrid[2] };
        continue;
      }

      // Grid ratio
      const mGridRatio = param.match(/^ratio\s*=\s*(\d+)\/(\d+)$/);
      if (mGridRatio) {
        if (!section.grid) {
          return `Grid ratio ${param} without grid`;
        }
        section.grid.ratio = +mGridRatio[1] / +mGridRatio[2];
        continue;
      }

      // Max width/height
      const mMaxWidth = param.match(/^maxw=(\d+)$/);
      if (mMaxWidth) {
        section.maxWidth = +mMaxWidth[1];
        continue;
      }

      const mMaxHeight = param.match(/^maxh=(\d+)$/);
      if (mMaxHeight) {
        section.maxHeight = +mMaxHeight[1];
        continue;
      }

      return `Unrecogonized param ${param}`;
    }
  }

  const ids = text.substring(paramSep + 1).split(",");
  for (let i = 0; i < ids.length; ++i) {
    // Take last part of id path to be the actual id (may have text before for easier reading).
    let id = ids[i];
    const sep = id.lastIndexOf("/");
    if (sep !== -1) {
      id = id.substring(sep + 1);
    }

    id = id.trim();
    if (id.length === 0) {
      // Error
      return `Zero-length photo id in ${text}`;
    }

    ids[i] = id;
  }

  section.photoIds = ids;

  return section;
}
