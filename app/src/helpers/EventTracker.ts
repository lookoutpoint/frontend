// SPDX-License-Identifier: MIT
import { isBot } from "../api/bot";
import { EventContext, EventInfo } from "../api/events";

export const EVENT_ADD_EVENTS = "addEvents";

export interface EventDesc extends EventInfo {
  unique?: boolean; // defaults to true
}

// Event tracker.
// By default events are uniquefied, that is each event is tracked at once per session.
// UX events are not uniquefied.
export class EventTracker extends EventTarget {
  // String format: <event>;<object>;<context>
  private uniqueEvents = new Set<string>();
  private pendingEvents: {
    [event: string]: {
      [object: string]: {
        [context: string]: {
          [extra: string]: number;
        };
      };
    };
  } = {};
  private defaultContext?: string;
  private enabled = true;

  // Add generic event with defaults. No-op if event is omitted.
  public addEvent(eventDesc: Partial<EventDesc>) {
    // Bot check here because EventTracker may still be enabled before the bot check is applied on EventTrackerProvider.
    if (!this.enabled || isBot || eventDesc.event === undefined) return;

    // Default options.
    // Unique defaults to true so that we by default don't duplicate events.
    eventDesc.object = eventDesc.object ?? "";
    eventDesc.context = eventDesc.context ?? "";
    eventDesc.extra = eventDesc.extra ?? "";
    eventDesc.count = eventDesc.count ?? 1;
    eventDesc.unique = eventDesc.unique ?? true;

    if (eventDesc.context === EventContext.DEFAULT) {
      eventDesc.context = this.defaultContext ?? "";
    }

    this.fixupEventDesc(eventDesc);

    if (eventDesc.unique) {
      const str = `${eventDesc.event};${eventDesc.object};${eventDesc.context};${eventDesc.extra}`;
      if (this.uniqueEvents.has(str)) {
        return;
      }
      this.uniqueEvents.add(str);
    }

    const obj1 = (this.pendingEvents[eventDesc.event] =
      this.pendingEvents[eventDesc.event] || {});
    const obj2 = (obj1[eventDesc.object] = obj1[eventDesc.object] || {});
    const obj3 = (obj2[eventDesc.context] = obj2[eventDesc.context] || {});
    obj3[eventDesc.extra] = (obj3[eventDesc.extra] ?? 0) + eventDesc.count;

    this.dispatchEvent(new CustomEvent(EVENT_ADD_EVENTS));
  }

  // Fixups.
  private fixupEventDesc(eventDesc: Partial<EventDesc>) {
    // Handle frontend -> backend mapping of collection -> folder.
    if (eventDesc.object && eventDesc.object.startsWith("/collection/")) {
      eventDesc.object =
        "/folder/" + eventDesc.object.slice("/collection/".length);
    }
    if (eventDesc.context && eventDesc.context.startsWith("/collection/")) {
      eventDesc.context =
        "/folder/" + eventDesc.context.slice("/collection/".length);
    }
  }

  // Add UX event, which is a generic event with default unique=false. No-op if objType or event is omitted.
  public addUXEvent(eventDesc: Partial<EventDesc>) {
    this.addEvent({ unique: false, ...eventDesc });
  }

  public setDefaultContext(context?: string) {
    // Default context should only be set when there is one set.
    if (context !== undefined && this.defaultContext !== undefined) {
      throw new Error("Default context already set");
    }
    this.defaultContext = context;
  }

  // Returns all pending events. The tracker will no longer track the returned events.
  public acquirePendingEvents(): EventInfo[] {
    const events: EventInfo[] = [];

    for (const event in this.pendingEvents) {
      for (const object in this.pendingEvents[event]) {
        for (const context in this.pendingEvents[event][object]) {
          for (const extra in this.pendingEvents[event][object][context]) {
            const info: EventInfo = { event };
            if (object.length > 0) {
              info.object = object;
            }
            if (context.length > 0) {
              info.context = context;
            }
            if (extra.length > 0) {
              info.extra = extra;
            }
            if (this.pendingEvents[event][object][context][extra] !== 1) {
              info.count = this.pendingEvents[event][object][context][extra];
            }
            events.push(info);
          }
        }
      }
    }

    this.pendingEvents = {};
    return events;
  }

  public setEnabled(enabled: boolean) {
    this.enabled = enabled;
  }
}
