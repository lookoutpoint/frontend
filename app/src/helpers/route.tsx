import React from "react";
import { NotFound } from "../views/NotFound";

export interface RegexRouteEntry {
  regex: RegExp;
  render: (m: RegExpMatchArray) => React.ReactElement;
}

export function regexRoute(
  url: string,
  routes: RegexRouteEntry[]
): React.ReactElement {
  for (const route of routes) {
    const m = url.match(route.regex);
    if (m) {
      return route.render(m);
    }
  }

  return <NotFound what="" />;
}
