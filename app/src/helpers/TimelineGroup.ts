import {
  compareAsc,
  differenceInCalendarDays,
  isAfter,
  isBefore,
  parseISO,
  startOfDay,
} from "date-fns";
import { TimelineGroup } from "../api/photoGroups";
import { slugPathDepth } from "./slugify";

export interface TimelineGroupOrderInfo {
  group: TimelineGroup;
  startDate: Date;
  numDays: number;
  depth: number;
}

export function orderTimelineGroups(
  rootGroup: TimelineGroup,
  groups: TimelineGroup[],
  showHidden: boolean
): TimelineGroupOrderInfo[] {
  // Determine date range.
  if (!rootGroup.startDate || !rootGroup.endDate) return [];

  const startDate = startOfDay(new Date(rootGroup.startDate));
  const endDate = startOfDay(new Date(rootGroup.endDate));
  const numDays = differenceInCalendarDays(endDate, startDate) + 1;
  if (numDays < 1) return []; // something is wrong, bail

  // Build result array. Start off converting.
  const orderedTimelineGroups: TimelineGroupOrderInfo[] = [];
  for (const group of groups) {
    let gStartDate: Date;
    let gEndDate: Date;

    if (!group.startDate || !group.endDate) {
      // Hidden group.
      if (!showHidden) continue; // skip

      // Show as first day.
      gStartDate = gEndDate = startDate;
    } else {
      // Strip the trailing "Z" from the dates so that the dates and times are interpreted as local time zone.
      // We really just want all the date components to be as stated in the date string.
      gStartDate = startOfDay(parseISO(group.startDate.slice(0, -1)));
      if (isBefore(gStartDate, startDate)) {
        gStartDate = startDate;
      }
      gEndDate = startOfDay(parseISO(group.endDate.slice(0, -1)));
      if (isAfter(gEndDate, endDate)) {
        gEndDate = endDate;
      }
    }

    const gNumDays = differenceInCalendarDays(gEndDate, gStartDate) + 1;
    if (gNumDays < 1) continue; // skip

    const depth = slugPathDepth(group.id);
    orderedTimelineGroups.push({
      group,
      startDate: gStartDate,
      numDays: gNumDays,
      depth,
    });
  }

  // Sort by start date, then depth.
  orderedTimelineGroups.sort((a, b) => {
    const dateCmp = compareAsc(a.startDate, b.startDate);
    if (dateCmp !== 0) {
      return dateCmp;
    }
    return a.depth - b.depth;
  });

  return orderedTimelineGroups;
}
