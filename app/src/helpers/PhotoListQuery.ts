// SPDX-License-Identifier: MIT
import { ListOperator } from "../api/list";
import { PhotoListPhoto, PhotoMapListPhoto } from "../api/photos";
import { FetchFunc, ListQuery } from "./ListQuery";

export class PhotoListQuery<K extends keyof PhotoListPhoto> extends ListQuery<
  PhotoListPhoto,
  K
> {
  constructor(
    key: K,
    fetchPhotos: FetchFunc<PhotoListPhoto, K>,
    minFetchCount = 25
  ) {
    super(key, fetchPhotos, minFetchCount);
  }
}

export function makeFetchPhotosFromList<K extends keyof PhotoListPhoto>(
  key: K,
  photos: PhotoListPhoto[]
): FetchFunc<PhotoListPhoto, K> {
  return async (count, refKeyValue, refOp) => {
    let refIdx: number | undefined;
    if (refKeyValue) {
      refIdx = photos.findIndex((v) => v[key] === refKeyValue);
      if (refIdx < 0) {
        return [];
      }
    }

    const forward = refOp === ListOperator.GT || refOp === ListOperator.GTE;
    if (forward) {
      let start = refIdx ?? 0;
      if (refKeyValue && refOp === ListOperator.GT) {
        start++;
      }

      return photos.slice(start, Math.min(photos.length, start + count));
    } else {
      let end = (refIdx ?? photos.length - 1) + 1;
      if (refKeyValue && refOp === ListOperator.LT) {
        end--;
      }

      return photos.slice(Math.max(0, end - count), end).reverse();
    }
  };
}

export class MappedPhotoListQuery<
  K extends keyof PhotoMapListPhoto
> extends ListQuery<PhotoMapListPhoto, K> {
  constructor(
    key: K,
    fetchPhotos: FetchFunc<PhotoMapListPhoto, K>,
    minFetchCount = 25
  ) {
    super(key, fetchPhotos, minFetchCount);
  }
}
