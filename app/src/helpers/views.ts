// SPDX-License-Identifier: MIT
export const VIEW_CHILDREN = "children";
export const VIEW_PHOTOS = "photos";
export const VIEW_TIMELINE = "timeline";
export const VIEW_INFO = "info";
export const VIEW_MAP = "map";
export const VIEW_NOT_FOUND = "notfound";
export const VIEW_EMPTY = "empty";
export type ViewSelection = string;

export function parseViewFromUrl(url: string): ViewSelection | undefined {
  const m = url.match(/@([a-z]+)\//i);
  if (m) {
    return m[1];
  }
  return undefined;
}

export function stripViewFromUrl(url: string): string {
  const atIdx = url.indexOf("@");
  return atIdx >= 0 ? url.substring(0, atIdx) : url;
}
