// SPDX-License-Identifier: MIT
import { PHOTO_PADDING } from "../components/PhotoConstants";
import { useWindowSize } from "./useWindowSize";
import { WindowSize } from "../helpers/Contexts";

interface PhotoListSizes {
  // For GroupInfiniteScroll
  groupPhotoRatio: number;
  groupDesiredPhotoWidth: number;

  // For PhotoInfiniteScroll
  photoStdHeight: number;
}

// Some height for stuff at the top of the page (e.g. control bar)
const HEIGHT_ADJUSTMENT = 50;

function computeDesiredMaxWidth(
  windowSize: WindowSize,
  ratio: number,
  nx: number,
  ny: number
): number {
  // Goal is to try to fit a nx x ny grid.
  return Math.min(
    windowSize.width / nx - PHOTO_PADDING * 2,
    ((windowSize.height - HEIGHT_ADJUSTMENT) / ny - PHOTO_PADDING * 2) * ratio
  );
}

export function usePhotoListSizes(): PhotoListSizes {
  const windowSize = useWindowSize();

  // 1:1 ratio for group list
  const maxWidth_1_1 = Math.floor(
    Math.max(100, Math.min(175, computeDesiredMaxWidth(windowSize, 1, 2, 2)))
  );

  // 3:2 ratio for group list
  const maxWidth_3_2 = Math.floor(
    Math.max(
      250,
      Math.min(350, computeDesiredMaxWidth(windowSize, 3 / 2, 2, 2))
    )
  );

  const groupUse3_2 = maxWidth_3_2 >= 300;

  // Std height for photo gallery
  const stdHeight = Math.floor(
    Math.max(
      150,
      Math.min(
        300,
        (windowSize.height - HEIGHT_ADJUSTMENT) / 2 - PHOTO_PADDING * 2
      )
    )
  );

  return {
    groupPhotoRatio: groupUse3_2 ? 3 / 2 : 1,
    groupDesiredPhotoWidth: groupUse3_2 ? maxWidth_3_2 : maxWidth_1_1,
    photoStdHeight: stdHeight,
  };
}
