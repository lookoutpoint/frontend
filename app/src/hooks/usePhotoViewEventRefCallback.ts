// SPDX-License-Identifier: MIT
import { useCallback, useRef, useEffect } from "react";
import { AppEvent, makeEventPhotoID } from "../api/events";
import { useEventTracker } from "./useEventTracker";

type RefCallback = (elem: HTMLElement | null) => void;

export function usePhotoViewEventRefCallback(
  event?: AppEvent,
  eventContext?: string,
  visThreshold = 0.75
): RefCallback | undefined {
  // Photo view event. We use an IntersectionObserver to determine when an element is visible and then
  // track the corresponding photo visibility event.
  const eventTracker = useEventTracker();

  // The IntersectionObserver is only created once and is stored as a ref, so it's callback is fixed.
  // To handle the (unexpected) scenario that the intersection observer callback needs to change, we
  // store the callback in a ref that is updated if any of its dependencies change.
  const makeIntersectObsvCb = useCallback((): IntersectionObserverCallback => {
    return (entries, obsv) => {
      for (const entry of entries) {
        if (!entry.isIntersecting) continue;

        // Use target to get the <a>, which has data-id annotated on it.
        // Try photo-id first, then id.
        const { target } = entry;
        const elem = target as HTMLElement;
        const id = elem.dataset.photoId ?? elem.dataset.id;
        if (!id) continue;

        // Use addEvent; photo views are uniqified.
        eventTracker.addEvent({
          event,
          object: makeEventPhotoID(id),
          context: eventContext,
        });

        // Photo view is at most once per session, so no need to track anymore.
        obsv.unobserve(target);
      }
    };
  }, [event, eventContext, eventTracker]);

  const onIntersectionCb = useRef(makeIntersectObsvCb());
  useEffect(() => {
    // Update callback on some change (callback will change if any dependencies change).
    onIntersectionCb.current = makeIntersectObsvCb();
  }, [makeIntersectObsvCb]);

  // The intersection observer.
  const intersectObsv = useRef<IntersectionObserver>(
    new IntersectionObserver(
      (entries, obsv) => onIntersectionCb.current(entries, obsv),
      { threshold: visThreshold }
    )
  );

  useEffect(() => {
    // Clean up on unmount.
    const o = intersectObsv.current;
    return () => o.disconnect();
  }, []);

  const refCallback = useCallback((ref: HTMLElement | null) => {
    if (ref) {
      intersectObsv.current.observe(ref);
    }
  }, []);

  return refCallback;
}
