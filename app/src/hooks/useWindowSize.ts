// SPDX-License-Identifier: MIT
import { useContext } from "react";
import { WindowSize, WindowSizeContext } from "../helpers/Contexts";

export function useWindowSize(): WindowSize {
  return useContext(WindowSizeContext);
}
