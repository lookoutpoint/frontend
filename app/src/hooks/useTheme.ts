// SPDX-License-Identifier: MIT
import { useContext } from "react";
import { Theme, ThemeContext } from "../helpers/Contexts";

export function useTheme(): Theme {
  return useContext(ThemeContext);
}
