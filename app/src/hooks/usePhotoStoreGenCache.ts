// SPDX-License-Identifier: MIT

import { useCallback, useState } from "react";
import { PhotoIdGen, PhotoListPhoto } from "../api/photos";

export function usePhotoStoreGenCache() {
  const [cachedPhoto, setCachedPhoto] = useState<Pick<
    PhotoListPhoto,
    "id" | "storeGen"
  > | null>(null);

  const makePhotoIdGen = useCallback(
    (id: string | undefined | null): PhotoIdGen | undefined => {
      if (!id) return undefined;
      if (!cachedPhoto || cachedPhoto.id !== id) return { id };
      return { id, storeGen: cachedPhoto.storeGen };
    },
    [cachedPhoto]
  );

  return {
    setCachedPhoto,
    makePhotoIdGen,
  };
}
