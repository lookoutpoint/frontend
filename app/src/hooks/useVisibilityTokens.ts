// SPDX-License-Identifier: MIT
import { useContext } from "react";
import { VisibilityTokens, VisibilityTokensContext } from "../helpers/Contexts";

export function useVisibilityTokens(): VisibilityTokens {
  const vtokens = useContext(VisibilityTokensContext);
  if (vtokens === null) {
    throw new Error("VisibilityTokens not found; not inside a Provider?");
  }
  return vtokens;
}
