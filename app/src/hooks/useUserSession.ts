import { useContext } from "react";
import { UserSessionContext } from "../helpers/Contexts";
import { UserSession } from "../helpers/Contexts";

export function useUserSession(): UserSession {
  const session = useContext(UserSessionContext);
  if (session === null) {
    throw new Error("Need a UserSessionProvider");
  }
  return session;
}
