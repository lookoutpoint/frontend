// SPDX-License-Identifier: MIT
import { useCallback, useState } from "react";
import { AppEvent, EventContext } from "../api/events";
import { NavInfo } from "../components/BreadcrumbNav";
import { approxDeslugify } from "../helpers/slugify";
import { titleCase } from "../helpers/titleCase";

export interface PhotoGroupNavState {
  navInfo: NavInfo[];
  activeLabel: string;
  activeView?: string;
}

export interface Options {
  groupUrl?: string;
  groupLabel?: string;
  navUrlPrefix: string;
  displaySep?: string;
  adjustView?: (link: string, view: string | undefined) => string | undefined;
}

export interface Addition {
  urlPrefix: string;
  slug: string;
  display?: string;
  displaySep?: string;
  getEventObject?: (path: string) => string;
}

export function usePhotoGroupNavState(
  options: Options,
  initialSlug: string,
  initialDisplay?: string,
  initialView?: string,
  ...initialAdditions: Addition[]
): [
  // buildNavState
  (
    slug: string,
    display?: string,
    view?: string,
    ...additions: Addition[]
  ) => PhotoGroupNavState,
  // current state
  PhotoGroupNavState,
  // setNavState
  (slug: string, display?: string, ...additions: Addition[]) => void,
  // setNavView
  (view?: string) => void
] {
  const { adjustView } = options;

  const buildNavState = useCallback(
    (
      slug: string,
      display?: string,
      view?: string,
      ...additions: Addition[]
    ): PhotoGroupNavState => {
      if (slug.length > 1 || additions.length > 0) {
        const navInfo: NavInfo[] = [];

        let accumPath = "";

        if (options.groupUrl && options.groupLabel) {
          navInfo.push({
            link: options.groupUrl,
            label: options.groupLabel,
            view,
            event: {
              event: AppEvent.GROUP_CLICK,
              object: options.navUrlPrefix,
              context: EventContext.DEFAULT,
            },
          });
        }

        const process = (
          slug: string,
          display?: string,
          displaySep?: string,
          getEventObject?: (path: string) => string
        ) => {
          if (slug.length === 1) return;

          // Ignore the leading and trailing slash on the slug path
          const pathParts = slug.substring(1, slug.length - 1).split("/");
          const displayParts = display
            ? displaySep
              ? display.split(displaySep)
              : [display]
            : undefined;

          for (let i = 0; i < pathParts.length; i++) {
            const pathPart = pathParts[i];
            const displayPart = displayParts ? displayParts[i] : undefined;

            accumPath += `${pathPart}/`;

            navInfo.push({
              link: accumPath,
              label: titleCase(displayPart ?? approxDeslugify(pathPart)),
              view,
              event: {
                event: AppEvent.GROUP_CLICK,
                object: getEventObject ? getEventObject(accumPath) : accumPath,
                context: EventContext.DEFAULT,
              },
            });
          }
        };

        // Process main slug.
        accumPath += options.navUrlPrefix;
        process(slug, display, options.displaySep);

        // Process additions.
        for (const addition of additions) {
          accumPath += addition.urlPrefix;
          process(
            addition.slug,
            addition.display,
            addition.displaySep,
            addition.getEventObject
          );
        }

        // Pop the last nav info and use that for the active label.
        const lastEntry = navInfo.pop();

        // Adjust views if there is a callback.
        if (adjustView) {
          for (let i = 0; i < navInfo.length; ++i) {
            navInfo[i].view = adjustView(navInfo[i].link, navInfo[i].view);
          }
        }

        return {
          navInfo,
          activeLabel: lastEntry?.label ?? "",
          activeView: view,
        };
      } else {
        // Slug points to root of the group.
        return {
          navInfo: [],
          activeLabel: options.groupLabel ?? "",
          activeView: view,
        };
      }
    },
    [
      options.groupUrl,
      options.groupLabel,
      options.navUrlPrefix,
      options.displaySep,
      adjustView,
    ]
  );

  const [navState, setNavStateRaw] = useState(() =>
    buildNavState(initialSlug, initialDisplay, initialView, ...initialAdditions)
  );

  const setNavState = useCallback(
    (slug: string, display?: string, ...additions: Addition[]) => {
      setNavStateRaw((prevState) =>
        buildNavState(slug, display, prevState.activeView, ...additions)
      );
    },
    [buildNavState]
  );

  const setView = useCallback(
    (view?: string) => {
      setNavStateRaw((prevState) => {
        const state = structuredClone(prevState);
        state.activeView = view;
        for (let i = 0; i < state.navInfo.length; ++i) {
          state.navInfo[i].view = adjustView
            ? adjustView(state.navInfo[i].link, view)
            : view;
        }
        return state;
      });
    },
    [adjustView]
  );

  return [buildNavState, navState, setNavState, setView];
}
