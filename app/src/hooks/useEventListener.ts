// SPDX-License-Identifier: MIT
import { useEffect, useRef } from "react";

interface SpecificEventTarget<ET extends string, E extends Event> {
  addEventListener(type: ET, listener: (e: E) => void): void;
  removeEventListener(type: ET, listener: (e: E) => void): void;
}

export function useEventListener<
  ET extends string,
  E extends Event,
  T extends SpecificEventTarget<ET, E>
>(target: T, type: ET, listener: (e: E) => void) {
  const listenerRef = useRef(listener);
  listenerRef.current = listener;

  useEffect(() => {
    const wrappedListener = (e: E) => {
      listenerRef.current(e);
    };

    target.addEventListener(type, wrappedListener);

    return () => target.removeEventListener(type, wrappedListener);
  }, [target, type]);
}
