// SPDX-License-Identifier: MIT
import { useCallback, useContext, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { AppEvent } from "../api/events";
import { EventDesc, EventTracker } from "../helpers/EventTracker";
import { parseViewFromUrl, VIEW_NOT_FOUND } from "../helpers/views";
import { EventTrackerContext } from "../helpers/Contexts";

// If a context is provided, it becomes the default context (note: there must not already be a default context set).
// The default context is reset on unmount.
//
// Will also add a page view event on mount.

export function useEventTracker(context?: string): EventTracker {
  const eventTracker = useContext(EventTrackerContext);
  const location = useLocation();
  if (eventTracker === null) {
    throw new Error("No EventTracker");
  }

  // PAGE_VIEW event and default context
  useEffect(() => {
    if (context === undefined) return;

    // Record page view.
    eventTracker.addEvent({ event: AppEvent.PAGE_VIEW, context });

    // Set default context.
    eventTracker.setDefaultContext(context);
    return () => eventTracker.setDefaultContext(undefined);
  }, [context, eventTracker]);

  // PAGE_VIEW_TYPE if there is a view
  useEffect(() => {
    if (context === undefined) return;

    const view = parseViewFromUrl(location.pathname);
    if (view === undefined || view === VIEW_NOT_FOUND) return;

    eventTracker.addEvent({
      event: AppEvent.PAGE_VIEW_TYPE,
      object: view,
      context,
    });
  }, [context, eventTracker, location.pathname]);

  return eventTracker;
}
export function useEventTrackerCallback(eventDesc?: EventDesc): () => void {
  const eventTracker = useEventTracker();

  return useCallback(() => {
    if (!eventDesc) return;
    eventTracker.addEvent(eventDesc);
  }, [eventDesc, eventTracker]);
}
export function useUXEventTrackerCallback(eventDesc?: EventDesc): () => void {
  const eventTracker = useEventTracker();

  return useCallback(() => {
    if (!eventDesc) return;
    eventTracker.addUXEvent(eventDesc);
  }, [eventDesc, eventTracker]);
}
