// SPDX-License-Identifier: MIT
import { useContext } from "react";
import { ProcessingIndicatorActions } from "../components/ProcessingIndicator";
import {
  ProcessingIndicatorActionsContext,
  ProcessingIndicatorContext,
} from "../helpers/Contexts";

export function useProcessingIndicator(): boolean {
  return useContext(ProcessingIndicatorContext);
}
export function useProcessingIndicatorActions(): ProcessingIndicatorActions {
  const actions = useContext(ProcessingIndicatorActionsContext);
  if (actions === null) {
    throw new Error("Missing ProcessingIndicatorProvider");
  }
  return actions;
}
