// SPDX-License-Identifier: MIT

import { useQueryClient } from "@tanstack/react-query";
import {
  listMappedPhotos,
  listRandomMappedPhotos,
  PhotoListFilterExpr,
  PhotoMapListPhoto,
} from "../api/photos";
import { MappedPhotoListQuery } from "../helpers/PhotoListQuery";
import { useEffect, useRef, useState } from "react";
import { ListComparisonOp } from "../api/list";

// Max limit on server side.
const BATCH_SIZE = 1000;

interface Options {
  onNewPhotos?: (photos: PhotoMapListPhoto[]) => void;
  maxIfNonHighlight?: number;
  enabled?: boolean;
}

// Lists all mapped photos with rating >= 4 and with provided filter.
// If the number of photos is less than maxIfNonHighlight, then further
// random mapped photos are queried (with rating <= 3) such that the total
// number of photos does not exceed maxIfNonHighlight.
export function useMappedPhotos(
  filter: PhotoListFilterExpr,
  options?: Options
): PhotoMapListPhoto[] {
  options = options ?? {};
  const { onNewPhotos } = options;
  const maxIfNonHighlight = options.maxIfNonHighlight ?? 100;
  const enabled = options.enabled ?? true;

  const [photos, setPhotos] = useState<PhotoMapListPhoto[]>([]);

  // Reset photo list upon filter change.
  const lastFilter = useRef<PhotoListFilterExpr>({});

  const queryClient = useQueryClient();

  // Fetch photos.
  useEffect(() => {
    // Only fetch when the photo count is zero and enabled.
    if (!enabled || filter === lastFilter.current) return;

    lastFilter.current = filter;
    setPhotos([]);

    const highlightList = new MappedPhotoListQuery<"sortKey">(
      "sortKey",
      (count, refKey, refOp) => {
        return listMappedPhotos(queryClient, {
          limit: count,
          refKeyValue: refKey,
          refOp,
          filter: {
            exprs: [filter],
            ratings: [
              {
                value: 4,
                cmpOp: ListComparisonOp.GTE,
              },
            ],
          },
          mapping: true,
        });
      },
      // fetch count
      BATCH_SIZE
    );

    const addPhotos = (photos: PhotoMapListPhoto[]) => {
      if (photos.length === 0) return;

      if (onNewPhotos) {
        onNewPhotos(photos);
      }
      setPhotos((prevPhotos) => [...prevPhotos, ...photos]);
    };

    let lastPhotoKey: string | undefined;
    const fetch = async () => {
      // Fetch all highlight photos.
      while (!highlightList.complete) {
        const batch = await highlightList.batchAfter(lastPhotoKey, BATCH_SIZE);
        addPhotos(batch);
        if (batch.length > 0) {
          lastPhotoKey = batch[batch.length - 1].sortKey;
        }
      }

      if (highlightList.count < maxIfNonHighlight) {
        // Query non-highlight mappable photos up to a limit
        // such that the _total_ number of photos does not exceed the max.
        const randomCount = maxIfNonHighlight - highlightList.count;
        const batch = await listRandomMappedPhotos(
          {
            exprs: [filter],
            ratings: [{ value: 4, cmpOp: ListComparisonOp.LT }],
          },
          randomCount
        );
        addPhotos(batch);
      }
    };

    fetch();
  }, [
    enabled,
    filter,
    maxIfNonHighlight,
    onNewPhotos,
    photos.length,
    queryClient,
  ]);

  return photos;
}
