// SPDX-License-Identifier: MIT
import { useEffect, useMemo, useState } from "react";

// Hook that returns if a media query is matched.
export function useMediaQuery(query: string): boolean {
  const mm = useMemo(() => window.matchMedia(query), [query]);

  const [match, setMatch] = useState(mm.matches);
  useEffect(() => {
    const listener = () => setMatch(mm.matches);
    mm.addEventListener("change", listener);

    return () => mm.removeEventListener("change", listener);
  }, [mm]);

  return match;
}
