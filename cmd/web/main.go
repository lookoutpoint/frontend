// SPDX-License-Identifier: MIT

package main

import (
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	// Start server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Printf("Listening on port %s", port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}
